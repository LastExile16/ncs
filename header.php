<!-- A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php date_default_timezone_set("Asia/Baghdad"); ?>
<!DOCTYPE HTML>
<html>
<head>

<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
<link rel="icon" type="image/png" href="favicon-32x32.png?v=1" sizes="32x32">
<link rel="icon" type="image/png" href="favicon-16x16.png?v=1" sizes="16x16">
<link rel="manifest" href="manifest.json">
<link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">
<meta name="apple-mobile-web-app-title" content="NCS">
<meta name="application-name" content="NCS">
<meta name="theme-color" content="#ffffff">
<title>Nawras Clinical System</title>
<meta charset="UTF-8">
<!-- bootstrap already included by datatables -->
<!-- <link rel="stylesheet" href="./css/bootstrap-3.3.7.min.css"> -->

<link href="./css/font-awesome/css/font-awesome.min.css" rel='stylesheet' type='text/css' />
<!-- <link rel="stylesheet" type='text/css' href="./datatables/buttons/buttons.dataTables.min.css" > -->
<!-- <link rel="stylesheet" type='text/css' href="./datatables/select/select.dataTables.min.css" > -->
<link rel="stylesheet" href="./css/lightbox.css">
<!-- Custom Theme files -->
<link href="./css/custom.css" rel='stylesheet' type='text/css' />
<link href="./css/responsive-table.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="./datatables/datatables.min.css"/>
<link href="./css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="./css/style4.css" />
<link rel="stylesheet" type="text/css" href="./css/common.css" />
<link rel="stylesheet" type="text/css" href="./css/animations.css" />
<link rel="stylesheet" type="text/css" href="./bootstrap-toggle/css/bootstrap-toggle.min.css" />
<link rel="stylesheet" type="text/css" href="./css/inc-dec-number/inc-dec-num.css" />

	<link rel="stylesheet" type="text/css" href="./tokenselect/dist/css/tokenfield-typeahead.min.css">
	<link rel="stylesheet" type="text/css" href="./tokenselect/dist/css/bootstrap-tokenfield.css">

<link rel="stylesheet" type="text/css" href="./sweetalert/sweetalert.css">
<!-- <link rel="stylesheet" type="text/css" href="./sweetalert/themes/facebook/facebook.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="./sweetalert/themes/google/google.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="./sweetalert/themes/twitter/twitter.css"> -->
<!--  below css turns off the round radius -->

<link rel="stylesheet" href="./css/datepicker/css/bootstrap-datepicker3.standalone.css">
 <?php 
	$page="";
	if (isset($_GET['page']) && !empty($_GET['page'])) {
		switch ($_GET['page']) {
			case "med_rep":
				echo '<link href="./css/med_report.css" rel="stylesheet" type="text/css" />';
				break;
		}
	}
?>

<!-- jQuery (necessary JavaScript plugins) -->
<!-- <script src="./js/lightbox-plus-jquery.min.js"></script> -->
<script src="./js/jquery.min.js"></script>
<script src="./js/lightbox.js"></script>
<script src="./css/datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="./datatables/datatables.min.js"></script>
<script type="text/javascript" src="./bootstrap-toggle/js/bootstrap-toggle.min.js"></script>

	<script src="./tokenselect/dist/bootstrap-tokenfield.min.js"  type="text/javascript"></script>
	<script src="./tokenselect/dist/typeahead.bundle.min.js"  type="text/javascript"></script>

<script src="./sweetalert/sweetalert.min.js"></script>
<!--//theme style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script type="text/javascript">
function empty(data)
{
	if(typeof(data) == 'number' || typeof(data) == 'boolean')
	{ 
		return false; 
	}
	if(typeof(data) == 'undefined' || data === null)
	{
		return true; 
	}
	if(typeof(data.length) != 'undefined')
	{
		return data.length == 0;
	}
	
	var count = 0;
	for(var i in data)
	{
		if(data.hasOwnProperty(i))
		{
		  count ++;
		}
	}
	return count == 0;
}
</script>

<style type="text/css">
	/** {
		direction: rtl !important;
	}
	.navbar-nav > li{
		float: right;
	}

	.navbar-nav{
		float: right;
	}

	.navbar-toggle {
    	float: left;
	}
	.copywrite, .copywrite .container, .copywrite .container p{
		direction: ltr !important;
	}*/
	/* Portlet */
	/*.portlet, .search-panel, .student-table, .add-panel{
		float: right;
	}*/
	/*.table {
		width: "100%" !important;
	}*/
	/*table > thead > th{
		width: auto !important;
	}*/
	.payments {
	    border-width: 1px;
	    border-color: rgba(233, 30, 99, 0.38);
	    border-style: dotted;
	    padding: 5px;
	    margin: 15px;
	    background-color: rgba(250, 235, 215, 0.58);
	    /* box-shadow: 0px 0px 10px 2px rgba(253, 100, 100, 0.18); */
	}
	.enabled_dates {
		/*color: #fff !important;
	    background-color: #5cb85c !important;
	    border-color: #4cae4c !important;*/
	    color: red !important;
	    font-weight: bold;
	}

	.enabled_dates:hover {
	    color: #fff !important;
	    background-color: #449d44 !important;
	    border-color: #398439 !important;
	}
	/*below code disables background scroll when modal is opened*/
	body.modal-open {
	    overflow: hidden !important;
	}
	.table-condensed > thead > tr > th,
	.table-condensed > tbody > tr > th,
	.table-condensed > tfoot > tr > th,
	.table-condensed > thead > tr > td,
	.table-condensed > tbody > tr > td,
	.table-condensed > tfoot > tr > td {
		padding: 5px !important;
		font-size: 115%;
	}
	.table-bordered {
	  border: 1px solid #ddd;
	}
	.table-bordered > thead > tr > th,
	.table-bordered > tbody > tr > th,
	.table-bordered > tfoot > tr > th,
	.table-bordered > thead > tr > td,
	.table-bordered > tbody > tr > td,
	.table-bordered > tfoot > tr > td {
	  border: 1px solid #ddd !important;
	}
	.table-bordered > thead > tr > th,
	.table-bordered > thead > tr > td {
	  border-bottom-width: 2px !important;
	}
	.even{
		background-color: rgb(248, 248, 248) !important;
	}
	.odd{
		background-color: rgb(255, 255, 255) !important;
	}
	.dt-buttons a{
		padding: 5px 10px 3px 10px !important;
	}
	.fa-1-5x{
		font-size: 1.6em !important;
	}

	table.dataTable tbody>tr.selected,
	table.dataTable tbody>tr.selected>.danger,
    table.dataTable tbody>tr>.selected {
      background-color: #A2D3F6 !important;
    }
    table.dataTable tbody>tr{
    	cursor: pointer;
    }

    .datepick {
    	margin-bottom: 0px;
    }

    .toggledboxdiv {
    	padding-top: 0px !important;
    }
	.contact{
		padding: 2em 0 2em;
	}
	a, a:hover, a:active, a:focus {
	   outline: 0px;
	}
	#logo_a{
	    background: #fd6464;
		padding: 0;
		padding-top: 3px;
    	/*border: 1px solid #E7E7E7;*/
	    border-radius: 50%;
	    text-align: center;
		background-position: -1px -4px;
		background-size: 100px 100px;
		box-shadow: 0px 0px 8px 2px #867575;
	    height: 110px;
	    width: 110px;

	}
	object {
		height: 100px;
		width: 100px;
	}
	#logo_a > span {
	    font-size: 2.5em;
	    position: absolute;
	    top: 40px;
	    left: 15px;
	}
	#logo_img{
		height: 100px;
	    width: 100px;
	    border-radius: 25%;
	    box-shadow: 0px 0px 8px 2px #867575;
	}
	.logo{
		top: 4px;
	}
/* use this to make logo flat on black panel #logo_a {
    background: #fd6464;
    padding: 0;
    border: 1px solid #E7E7E7;
    border-radius: 50%;
    text-align: center;
    border-width: 2px;
    background-image: url("./images/logo.jpg");
    background-position: -1px -4px;
    background-size: 100px 100px;
    box-shadow: 0px 0px 8px 2px #867575;
    height: 60px;
    width: 107px;
    line-height: 56px;
    font-size: 1.6em;
    position: relative;
    top: 11px;
    vertical-align: middle;
    display: inline-block;
}
*/
@media (max-width: 991px){
	.content.white {
	    margin-left: 5.8em;
	}
}
	.content.white{
		width: 90%;
		/*background-color: #357;*/
		background-color: #565656;
		color: white;
	}
	.top-menu{
		/*background-color: #357;*/
		background-color: #565656;
		color: white;
	}
	.navbar-default{
		border: 0px;
	}
	ul.nav.navbar-nav li a,
	.navbar-default .navbar-nav .open .dropdown-menu > li > a {
    	 padding: 14px 40px;
    	 color: white;
	}
	.top-header{
		padding: 10px 0px;
		background-color: white;
	}
	.login-info{
	    float: right;
	    color: white;
	    font-size: small;
	    margin-right: 10px;
	}
	ul.nav.navbar-nav li {
	    border-right: 1px solid #333;
	}


	/*top button background colors*/
	.navbar-default .navbar-nav > .active > a, 
	.navbar-default .navbar-nav > .active > a:hover, 
	.navbar-default .navbar-nav > .active > a:focus {
	    color: #fd7364;
	    background-color: black;
	}
	.navbar-default .navbar-nav > .open > a, 
	.navbar-default .navbar-nav > .open > a:focus, 
	.navbar-default .navbar-nav > .open > a:hover li.open,
	.navbar-default .navbar-nav .open .dropdown-menu > li > a:hover

	{
		background-color: black !important;
		color:#fd7364 !important;
	}
	.mynavbar {
		background-color: #333;
	}

	.mynavbar li a:hover {
		background-color: rgba(0,0,0,0);
	}
	/*end top buttons*/

	.portlet > .portlet-body.green,
	.portlet.green {
	  /*background-color: #35aa47;*/
	  background-color: #73EAC4;
	  /*margin-right: 15px;*/
	  /*padding-right: 15px;*/
	  /*padding-left: 15px;*/
	}
	.portlet.box.green > .portlet-title {
	  /*background-color: #35aa47;*/
	  background-color: #73EAC4;
	}
	.portlet.box.green {
	    border: 1px solid #73EAC4;
	}
	table th, table td {
	    /*text-align: right;*/
	    vertical-align: middle;
	}


	.actionbutton {
		float: right;
		margin-right: -10px;
		margin-left: 30px;
	}
	
	.panel-heading {
		padding-top: 5px;
		padding-bottom: 5px;
	}
	.panel-title {
		font-size: 110%;
	}
	.panel {
		box-shadow: 0px 2px 10px 4px rgba(0,0,0,.07);
	}
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
		color: #000;
	}

	.modal-body-scroll{
		overflow-y: scroll !important;
		max-height: 30em;
	}
	.modal_data{
		border: 1px dashed grey;
		padding: 1% 0% 1% 1%;
	}

	.active-row{
		background-color: #E7E7E7 !important; 
	}

	.spin-backw{
		webkit-animation: fa-spin 6s infinite linear;
		animation: fa-spin 1.5s infinite cubic-bezier(.68,-0.55,.27,1.55);
	}

	/*turn off the html5 spinner for number input*/
	input[type="number"]::-webkit-outer-spin-button,
	input[type="number"]::-webkit-inner-spin-button {
	    /* display: none; <- Crashes Chrome on hover */
	    -webkit-appearance: none;
	    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
	}
	input[type="number"] {
	    -moz-appearance: textfield;
	}

	/*style the patient whose already in queue*/
	.inqueue {
	    color: blue;
	    font-weight: bold;
	}

	tfoot input {
        width: 100% !important;
        padding: 3px;
        box-sizing: border-box;
    }
    tfoot {
	    /*//display: table-header-group;*/
	}
	.top_label_modal {
		background-color: aliceblue;
	    padding: 10px;
	    /*height: 3em;*/
	    border-radius: 5px;
	    margin-bottom: 10px;
	}
	.hxform_en {
	    box-shadow: 0px 0px 11px 1px red;
	}

	.danger_{
	    background-color: #c9302c !important;
	    color: white;
	}

	#cancel_patient {
		margin-bottom: 10px;
	}

	.copywrite p a {
	    color: #AD1212;
	}

	@media screen and (max-width: 1024px) {
	    tfoot {
	        display: none;
	    }
	    .tfoot_button {
	    	display: block;
	    }

	    .rtable--flip thead th {
		    width: auto !important;
		}
	    
	}

	@page { margin: 0; }
</style>

</head>
<body>
<!-- header -->
<div class="top-header">
		 <!-- <div class="login-info"><span>Logged In as <?php echo $_SESSION['fullname']; ?></span> | <span><a href="logout.php">Logout</a></span></div> -->
	 <div class="container">
		 <div class="logo">
			<h1>
				<a id="logo_a" href="index.php">
				 	<!-- <img id="logo_img" src=".././images/logo.jpg"> -->
				 	<!-- <span class="fa fa-user-md"></span> -->
				 	<object data="./images/nawrascs_logo_2_white.svg" type="image/svg+xml">
						<img src="./images/nawrascs_logo_2.png" />
					</object>
				</a>
			</h1>
		 </div>	
	 </div>
</div>
<!---->
<div class="top-menu">
	 <div class="container">
	  <div class="content white">
		 <nav class="navbar navbar-default">
			 <div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>				
			 </div>
			 <!--/.navbar-header-->		
			 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				 <ul class="nav navbar-nav">
				 	<?php 
				 		$page="";
				 		if (isset($_GET['page']) && !empty($_GET['page'])) {
				 			// $page = $_GET['page'];
				 			switch ($_GET['page']) {
				 				case "dashboard":
				 					$page = "dashboard";
				 					break;
				 				case "current_p":
				 					$page = "current_p";
				 					break;
				 				case "all_p":
				 					$page = "all_p";
				 					break;
				 				case "drcalender":
				 					$page = "drcalender";
				 					break;
				 				case "my_profile":
				 					$page = "my_profile";
				 					break;
				 				case "med_rep":
				 					$page = "med_rep";
				 					break;
				 				case "expenses":
				 					$page = "expenses";
				 					break;
				 				case "users":
				 					$page = "users";
				 					break;
				 				
				 				default:
				 					$page = "dashboard";
				 					break;
				 			}
				 		}
				 		else{
							$page = "dashboard";
				 		}

				 	 		
				 	 ?>
					 <li <?php echo $page == "dashboard" ?'class="active"':""; ?> ><a href="index.php?page=dashboard">Dashboard</a></li>

					 <li <?php echo $page == "current_p"?'class="active"':""; ?> ><a href="index.php?page=current_p">Current Patient</a></li>
					  <!-- setupakan leran wata daxl krdni hospital w disease w ... -->
					 
					 
					 <li <?php echo $page == "all_p"?'class="active"':""; ?> ><a href="index.php?page=all_p">All Patients</a></li>
					 <li <?php echo $page == "drcalender"?'class="active"':""; ?> ><a href="index.php?page=drcalender">Calender</a></li>

					 <li <?php echo ($page == "expenses" || $page=="med_rep")?'class="active dropdown"':'class="dropdown"'; ?> >
					 	<a href="index.php?page=tools" class="dropdown-toggle" data-toggle="dropdown">Tools <b class="caret"></b></a>
						 	<ul class="dropdown-menu mynavbar">
						 		<li><a href="index.php?page=med_rep">Medical Report</a></li>
								<li><a href="index.php?page=expenses">Expenses</a></li>
							</ul>
					 </li>
					<li <?php echo ($page == "my_profile" || $page == "users")?'class="active dropdown"':'class="dropdown"'; ?> >
					 	<a href="index.php?page=my_profile" class="dropdown-toggle" data-toggle="dropdown">My Profile <b class="caret"></b></a>
						 	<ul class="dropdown-menu mynavbar">
								<li><a href="index.php?page=my_profile"><?php echo $_SESSION['fullname']; ?></a></li>
								<li><a href="index.php?page=users">Secretary</a></li>
								<li><a href="./logout.php">Logout</a></li>
							</ul>
					 </li>
											
				 </ul>
				</div>
			  <!--/.navbar collapse-->
		 </nav>
		  <!--/.navbar-->		 
	  </div>
	 <div class="clearfix"></div>
		<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
				<!-- <script type="text/javascript" src="../js/bootstrap-3.1.1.min.js"></script> -->

		</div>
</div>