<!DOCTYPE html>
<?php
    // require_once 'header.php';
/*add password recovery to email address*/
?>
<html>
<head>
    <title>racheta</title>
    <meta charset="UTF-8">
<!-- bootstrap already included by datatables -->
<!-- <link rel="stylesheet" href="./css/bootstrap-3.3.7.min.css"> -->

<link href="./css/font-awesome/css/font-awesome.min.css" rel='stylesheet' type='text/css' />
<!-- <link rel="stylesheet" type='text/css' href="./datatables/buttons/buttons.dataTables.min.css" > -->
<!-- <link rel="stylesheet" type='text/css' href="./datatables/select/select.dataTables.min.css" > -->
<link rel="stylesheet" href="./css/lightbox.css">
<!-- Custom Theme files -->
<link href="./css/custom.css" rel='stylesheet' type='text/css' />
<link href="./css/responsive-table.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="./datatables/datatables.min.css"/>
<link href="./css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="./css/style4.css" />
<link rel="stylesheet" type="text/css" href="./css/common.css" />
<link rel="stylesheet" type="text/css" href="./css/animations.css" />
<link rel="stylesheet" type="text/css" href="./bootstrap-toggle/css/bootstrap-toggle.min.css" />
<link rel="stylesheet" type="text/css" href="./css/inc-dec-number/inc-dec-num.css" />

    <link rel="stylesheet" type="text/css" href="./tokenselect/dist/css/tokenfield-typeahead.min.css">
    <link rel="stylesheet" type="text/css" href="./tokenselect/dist/css/bootstrap-tokenfield.css">

<link rel="stylesheet" type="text/css" href="./sweetalert/sweetalert.css">
<!-- <link rel="stylesheet" type="text/css" href="./sweetalert/themes/facebook/facebook.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="./sweetalert/themes/google/google.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="./sweetalert/themes/twitter/twitter.css"> -->
<!--  below css turns off the round radius -->

<link rel="stylesheet" href="./css/datepicker/css/bootstrap-datepicker3.standalone.css">
 

<!-- jQuery (necessary JavaScript plugins) -->
<!-- <script src="./js/lightbox-plus-jquery.min.js"></script> -->
<script src="./js/lightbox-plus-jquery.min.js"></script>
<script src="./js/jquery.min.js"></script>
<script src="./css/datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="./datatables/datatables.min.js"></script>
<script type="text/javascript" src="./bootstrap-toggle/js/bootstrap-toggle.min.js"></script>

    <script src="./tokenselect/dist/bootstrap-tokenfield.min.js"  type="text/javascript"></script>
    <script src="./tokenselect/dist/typeahead.bundle.min.js"  type="text/javascript"></script>

<script src="./sweetalert/sweetalert.min.js"></script>
<!--//theme style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html" />

    <style type="text/css">
    .rachetacontent{
  height:400px;
  /*background-color:green;*/
  /*border: solid yellow 2px;*/
  
  /*FLEX BOX */
  display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
    -webkit-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-justify-content: flex-start;
    -ms-flex-pack: start;
    justify-content: flex-start;
    -webkit-align-content: stretch;
    -ms-flex-line-pack: stretch;
    align-content: stretch;
    -webkit-align-items: flex-start;
    -ms-flex-align: start;
    align-items: flex-start;
}

.rachetafooter{
  /*background-color:red;*/
/*border: solid black 3px;*/
  
   /*FLEX BOX CHILD */
 -webkit-order: 0;
    -ms-flex-order: 0;
    order: 0;
    -webkit-flex: 0 1 auto;
    -ms-flex: 0 1 auto;
    flex: 0 1 auto;
    -webkit-align-self: flex-end;
    -ms-flex-item-align: end;
    align-self: flex-end;
 
}
    .racheta {
        width:135mm;
        /*border: 1px solid black;*/
    }

    table {
        border: 1px solid black;
    }

    .showbord{
        /*border: 1px solid black;*/
        /*min-width: 210mm !important;*/

    }

    .setfont {
        font-family: Arial;
    }

    #certificate {
        background-color: blue !important;
        color: white !important;
        font-weight: bold;
        padding: 0px 4px;
        /*font-family: serif;*/
    }

    .inmid{
        text-align: center;

    }

    #arabic {
        margin-top: 1em; 
    }
    #address {
        margin-top: 2em; 
    }

    #pname {
        text-align: left;
    }
    #pcod {
        text-align: center;
    }
    #pdate {
        text-align: right;
    }

    #pname, #pdate{
        font-size: 15px;
    }
    #patient_info{
        border:1px solid black;
        border-radius: 20px;
    }

    #patient_n_treats {
        /*border: blue 3px solid;*/
        /*border-left: 1px solid black;*/
    }
    
    #docinfo {
        border-right: 2px solid black;
        margin-left: 0px !important;
        padding-left: 0px !important;
    }

    #docname {
        font-weight: bolder;
        font-size: 16pt;
    }
    #treatments {
        /*background-image: url("images/nawrascs_logo_tans_wide_red.png");*/
        position: relative;
        text-align: left;
        padding-top: 10px;
    }

    #bgimage {
        height: 20em;
        opacity: 0.2;
    }
    #back {
        position: absolute;
        text-align: center;
    }

    #kurdish {
        margin-top: 2em;
    }

    #footer {
        /*margin-top: 10px;*/
    }
    #phone {
        /*text-align: left;*/
    }
    #email, #email span {
        color: blue !important;
        /*font-family: monospace;*/

    }
    #logoncs {

        /*background-image: url("images/nawrascs_logo_2_red.png");
        height: 42px;
        width: 41px;
        background-size: cover;*/
        padding-right: 0px;
        padding-left: 0px;
    }
    #footer {
        padding-right: 0px;
    }
    .footer-text{
        font-size: small !important;
    }

</style>
</head>
<body>

<div class="row">
    <div class="col-md-3 col-sm-3 col-xs-3" id="docinfo">
    <div class="inmid">
        
        <h3 class="setfont">الــدکتــــور</h3>
        <p id="docname">هێمن عبدالرحمن عبداللە</p>
        <p><span id="certificate">M.B.CH.B - F.I.B.M.S</span></p>
        <div id="arabic">
            <p><b>جراح اختصاصي</b></p>
            <p>بورد<b>(دکتورا)</b> في جراحة القلب و الصدر و الاوعیة الدمویة</p>
        </div>
        <div id="kurdish">
            <p>پزیشکی پسپۆر لە نەشتەرگەری سینگ و دڵ و بۆڕیەکانی خوێن </p>
        </div>
        
        <img id="sideimage" src="images/heart_1.png">
    </div>
        

    </div>
    <div class="col-md-9 col-sm-9 col-xs-9" id="patient_n_treats">
        <div id="patient_info" class="row">
            <div class="col-md-6 col-sm-6 col-xs-6" id="pname">
                <span>Patient Name: </span>
                <span><?php echo isset($_GET['code_name'])?$_GET['code_name']:""; ?></span>
            </div>
            
            <div class="col-md-3 col-sm-3 col-xs-3" id="page">Age:
                <span><?php echo isset($_GET['age'])?$_GET['age']:""; ?></span>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3" id="pdate">
                <span><?php echo isset($_GET['today'])?$_GET['today']:date('Y-m-d'); ?></span>
            </div>
        </div>
        <div class="row rachetacontent">
            <div class="col-md-12 col-sm-12 col-xs-12" id="back">
                <img id="bgimage" src="images/lungs.jpg">
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" id="treatments">
                <?php echo isset($_GET['treats'])?$_GET['treats']:""; ?>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 rachetafooter">
                <div class="row">
                    <div class="col-md-10 col-sm-10 col-xs-10" id="address">
                        <span>هەولێر/ شەقامی پزیشکان - کۆمەلگای هیوا</span>
                        <p style="float: right;"><span>اربیل/ شارع اطباء - مجمع هیوا</span></p>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2" id="empty"></div>
                    <div class="col-md-10 col-sm-10 col-xs-10 footer-text" >
                        <span id="email"><span>E-mail: </span><span>hemnabdulrahman@yahoo.com</span></span>
                        <p style="float: right;"><span>Mob: </span><span>0750 473 3931</span></p>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2" id="logoncs">
                        <img src="images/nawrascs_logo_2_red.png" height="42" width="41">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>

