//---------Function to Display modal editButton---------

  function editClick(pointer, oTT, button, conf, columnDefs) {
    var adata = oTT.rows({
      selected: true
    });

    // console.log('adata', adata)
    // console.log(adata.data()[0]);
    // console.log(adata.data()[0][0]);
    console.log(adata.ids()[0]);
    var id = adata.data()[0][0];

    var data = "";
    var myTable = pointer;
    data += "<form name='editForm' role='form'>";
    data += "<div class='form-group' style='display:none'><label for='" + columnDefs[0].title + "'>" + columnDefs[0].title + ":</label><input type='hidden'  id='" + columnDefs[0].title + "' name='" + columnDefs[0].title + "' placeholder='" + columnDefs[0].title + "' style='overflow:hidden'  class='form-control' value='" + adata.data()[0][0] + "'></div>";
    // for (i in columnDefs) {
    for (i=1; i<columnDefs.length; i++) {
      data += "<div class='form-group'><label for='" + columnDefs[i].title + "'>" + columnDefs[i].title + ":</label><input type='text'  id='" + columnDefs[i].title + "' name='" + columnDefs[i].title + "' placeholder='" + columnDefs[i].title + "' style='overflow:hidden'  class='form-control' value='" + adata.data()[0][i] + "'></div>";

    }
    data += "</form>";


    $('#myModal').on('show.bs.modal', function() {
      $('#myModal').find('.modal-title').html('Edit Record');
      $('#myModal').find('.modal-body').html('<pre>' + data + '</pre>');
      $('#myModal').find('.modal-footer').html("<button type='button' data-content='remove' class='btn btn-primary' id='editRowBtn'>Update</button>");
    });

    $('#myModal').modal('show');
    $('#myModal input[1]').focus();

  };
$('#example tbody').on( 'click', 'tr', function () {
  /*rows returns an array of that row, but rows() returns objects that consist of 
  many data including the array of row*/
    console.log( myTable.row( this ).data() );
    var d = myTable.row( this ).data();
     d[1]="Yusuf";
    myTable
        .row( this )
        .data( d )
        .draw();
} );
  // edit row functionality
  $(document).on('click', '#editRowBtn', function() {

    // edit row function needs to go here, here use your ajax then hide the modal
    console.log('Update Row');
    $('#myModal').modal('hide');

    /*I use row() not rows() because I want to edit only one row and only one row is selectable
    otherwise I should use rows and loop through each selected row*/
    /*var adata = myTable.rows({
      selected: true
    }).data()[0];*/

    // below copies the row data into adata variable, so after updating it you should give the data back to
    // myTable object to update its' cache
    var adata = myTable.row({
      selected: true
    }).data();
    // console.log(editForm[1].value);
    // console.log(adata);
    for (i in adata) {
        adata[i] = editForm[i].value;
    }
    console.table(adata);
   myTable.row( {selected: true} ).data( adata );
       myTable.draw();

       /*
        when using ajax it's better to invalidate data to force datatable do a re-read from the sourse
        then draw() the table again.
        that way you don't need to loop through new data and update the variable <i.e. adata> then give it back to datatable.
        above function is useful only when you don't want to make the change immediately in the database
        instead you update the shown table then create a button like "save" when it's clicked then you make changes permenantly.
        same applies to add and delete
       */
  });

  //---------Function to Display modal deleteButton---------

  function deleteClick(pointer, oTT, button, conf) {

    var adata = oTT.rows({
      selected: true
    });

    // console.log('adata', adata)
    // console.log(adata.data()[0]);
    // console.log(adata.data()[0][0]);
    console.log(adata.ids()[0]);
    var id = adata.data()[0][0];
    var data = "";
    var myTable = pointer;
    data += "<form name='editForm' role='form'>";
    for (i in columnDefs) {

      data += "<div class='form-group'><label for='" + columnDefs[i].title + "'>" + columnDefs[i].title + " : </label><input  type='hidden'  id='" + columnDefs[i].title + "' name='" + columnDefs[i].title + "' placeholder='" + columnDefs[i].title + "' style='overflow:hidden'  class='form-control' value='" + adata.data()[0][i] + "' >" + adata.data()[0][i] + "</input></div>";

    }
    data += "</form>";

    $('#myModal').on('show.bs.modal', function() {
      $('#myModal').find('.modal-title').html('Delete Record');
      $('#myModal').find('.modal-body').html('<pre>' + data + '</pre>');
      $('#myModal').find('.modal-footer').html("<button type='button' data-content='remove' class='btn btn-primary' id='deleteRowBtn'>Delete</button>");
    });

    $('#myModal').modal('show');
    $('#myModal input[0]').focus();
  };

  // delete row functionality
  $(document).on('click', '#deleteRowBtn', function() {
    // delete row function needs to go here
    console.log('Delete Row');
    $('#myModal').modal('hide');

    var adata = myTable.rows({
      selected: true
    }).data()[0];
    console.log(myTable.rows({selected: true}).data());
    adata.delete();
    // myTable.row( {selected: true} ).data( adata );
    // myTable.draw();
    console.log('data to delete : ', adata[0])
  });