<div class="copywrite">
	 <div class="container">
			 <p> © 2016 Nawras Clinical System. All rights reserved | Developed by <a href="mailto:nawras.nzar@gmail.com">Eng. Nawras Nazar - <span id="viber">Viber:</span> +81 808 4183696</a></p>
			 <a target="_blank" href="../tutorials.php">Click here to open NCS Tutorials - <span dir="rtl">کلیک لێرە بکە بۆ کردنەوەی فێرکاریەکانی سیستەمی NCS</span></a>
	 </div>
</div>

<!---->

<!---->
<!--/animatedcss-->
<script type="text/javascript" src="../js/move-top.js"></script>
<script type="text/javascript" src="../js/easing.js"></script>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>
<!--script-->
<script type="text/javascript">
var delay = (function(){
		var timer = 0;
		return function(callback, ms){
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
		};
		})();

		
		$(document).ready(function() {
				/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
				*/
		$().UItoTop({ easingType: 'easeOutQuart' });

		// disable mousewheel on a input number field when in focus
		// (to prevent Cromium browsers change the value when scrolling)
		$('form').on('focus', 'input[type=number]', function (e) {
		  $(this).on('mousewheel.disableScroll', function (e) {
		    e.preventDefault()
		  })
		})
		$('form').on('blur', 'input[type=number]', function (e) {
		  $(this).off('mousewheel.disableScroll')
		})

		$('.datepick').datepicker({
            format: "yyyy-mm-dd",
		    todayBtn: "linked",
		    autoclose: true,
		    todayHighlight: true,
		    daysOfWeekHighlighted:'5',
		    weekStart: 5,
            startDate: "+0d"

        });

        $('.rangepick').datepicker({
            format: "yyyy-mm-dd",
		    todayBtn: "linked",
		    autoclose: true,
		    todayHighlight: true,
		    daysOfWeekHighlighted:'5',
		    weekStart: 5

        });

        

		// $("tr:even").css("background-color", "#f8f8f8");


//number increment and decrement


  window.inputNumber = function(el) {

    var min = el.attr('min') || false;
    var max = el.attr('max') || false;

    var els = {};

    els.dec = el.prev();
    els.inc = el.next();

    el.each(function() {
      init($(this));
    });

    function init(el) {

      els.dec.on('click', decrement);
      els.inc.on('click', increment);

      function decrement() {
        var value = el[0].value;
        value--;
        if (!min || value >= min) {
          el[0].value = value;
        }
      }

      function increment() {
        var value = el[0].value;
        value++;
        if (!max || value <= max) {
          el[0].value = value++;
        }
      }
    }
  }


});
</script>
<a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!---->
<!-- <script src="js/responsiveslides.min.js"></script>  -->
</body>
</html>
