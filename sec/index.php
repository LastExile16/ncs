<?php
require_once '../includes/connection.php';
// require_once "../includes/functions.php";
if(!isset($_SESSION))
{
    session_start();
}
require_once '../includes/functions.php';
if (!logged_in()) 
{
    // if (isset($_SESSION['admin'])) {
        header("Location: ../login.php");
        exit;
    // }
}
?>

<?php
require_once 'header.php';
?>

<?php
if (isset($_GET['page']) AND file_exists("../includes/".$_GET['page'] . ".php")) {
	// include_once "../includes/".$_GET['page'] . ".php";
	switch ($_GET['page']) {
		case "queue":
			include_once "../includes/queue.php";
			break;
		case "patient":
			include_once "../includes/patient.php";
			break;
		case "calender":
			include_once "../includes/calender.php";
			break;
		case "my_profile":
			include_once "../includes/my_profile.php";
			break;
		
		default:
			include_once "../includes/queue.php";
			break;
	}
}
else {
	include_once '../includes/queue.php';
}
?>


<?php
require_once './footer.php';
?>

<?php
mysql_close();
?>