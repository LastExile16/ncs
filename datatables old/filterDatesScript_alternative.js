/*
     FILE ARCHIVED ON 21:18:32 Mar 4, 2015 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 17:07:08 Nov 20, 2016.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
$.fn.dataTableExt.afnFiltering.push(
	function( oSettings, aData, iDataIndex ) {
		if ($('#min').val() == '' && $('#max').val() == '') {
			return true;
		}

		if ($('#min').val() != '' || $('#max').val() != '') {
			var iMin_temp = $('#min').val();
			
			if (iMin_temp == '') {
			  iMin_temp = '01/01/2000';
			}
			
			var iMax_temp = $('#max').val();
			
			if (iMax_temp == '') {
			  iMax_temp = '31/12/2999'
			}
			
			var arr_min = iMin_temp.split("/");
			var arr_max = iMax_temp.split("/");

			// aData[column with dates]
			// 4 is the column where my dates are. Just replace it according to your needs. First column is 0.
			var arr_date = aData[4].split("/"); 
  			var iMin = new Date(arr_min[2], arr_min[0], arr_min[1], 0, 0, 0, 0)
			var iMax = new Date(arr_max[2], arr_max[0], arr_max[1], 0, 0, 0, 0)
			var iDate = new Date(arr_date[2], arr_date[0], arr_date[1], 0, 0, 0, 0)
			
			if ( iMin == "" && iMax == "" )
			{
				return true;
			}
			else if ( iMin == "" && iDate < iMax )
			{
				return true;
			}
			else if ( iMin <= iDate && "" == iMax )
			{
				return true;
			}
			else if ( iMin <= iDate && iDate <= iMax )
			{
				return true;
			}
			return false;
		}
	}
);



// to Filter based on TWO columns
$.fn.dataTableExt.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {
        var iFini = document.getElementById('fini').value;
        var iFfin = document.getElementById('ffin').value;
        var iStartDateCol = 6;
        var iEndDateCol = 7;
 
        iFini=iFini.substring(6,10) + iFini.substring(3,5)+ iFini.substring(0,2);
        iFfin=iFfin.substring(6,10) + iFfin.substring(3,5)+ iFfin.substring(0,2);
 
        var datofini=aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(3,5)+ aData[iStartDateCol].substring(0,2);
        var datoffin=aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(3,5)+ aData[iEndDateCol].substring(0,2);
 
        if ( iFini === "" && iFfin === "" )
        {
            return true;
        }
        else if ( iFini <= datofini && iFfin === "")
        {
            return true;
        }
        else if ( iFfin >= datoffin && iFini === "")
        {
            return true;
        }
        else if (iFini <= datofini && iFfin >= datoffin)
        {
            return true;
        }
        return false;
    }
);