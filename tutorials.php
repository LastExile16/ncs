<?php  
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
require_once './includes/connection.php';
if(!isset($_SESSION))
{
    session_start();
}

require_once './includes/functions.php';

if (logged_in()) 
{
	$page = "
	<!DOCTYPE html>
		<html>
		<head>
			<title>NCS Tutorial</title>
		</head>
		<body>";
    if ($_SESSION['role']==9) {
        $page .= "<ol>
	<li>
		<a href='http://www.nawrascs.com/tutorial/Doctor/1-sec-doc-Login'>Login</a>
	</li>
	<li>
		<a href='http://www.nawrascs.com/tutorial/Doctor/1-doc-interface'>Interface</a>
	</li>
	<li>
		<a href='http://www.nawrascs.com/tutorial/Doctor/2-doc-dashboard'>Dashboard</a>
	</li>
	<li>
		<a href='http://www.nawrascs.com/tutorial/Doctor/3-doc-current-patient'>Current Patient</a>
	</li>
	<li>
		<a href='http://www.nawrascs.com/tutorial/Doctor/4-doc-all-patients'>All Patients</a>
	</li>
	<li>
		<a href='http://www.nawrascs.com/tutorial/Doctor/5-doc-calender'>Calender</a>
	</li>
	<li>
		<a href='http://www.nawrascs.com/tutorial/Doctor/6-doc-my-profile'>Profile</a>
	</li>
</ol>";
    }else{
    	$page .= "<ol>
	<li>
		<a href='http://www.nawrascs.com/tutorial/Secretery/1-sec-doc-Login'>Login</a>
	</li>
	<li>
		<a href='http://www.nawrascs.com/tutorial/Secretery/2-sec-interface'>Interface</a>
	</li>
	<li>
		<a href='http://www.nawrascs.com/tutorial/Secretery/3-sec-clinic-queuecproj'>Clinic Queue</a>
	</li>
	<li>
		<a href='http://www.nawrascs.com/tutorial/Secretery/4-sec-patient'>Patient</a>
	</li>
	<li>
		<a href='http://www.nawrascs.com/tutorial/Secretery/5-sec-calender'>Calender</a>
	</li>
	<li>
		<a href='http://www.nawrascs.com/tutorial/Secretery/6-sec-my-profile'>Profile</a>
	</li>
</ol>";
    }
    $page .= "</body>
		</html>";

	echo $page;
}else{
	var_dump($_SESSION['user_id']);
	print_r($_SESSION);
	echo "<h1>You Don't Have Permission to View This Folder</h1>";
    exit;
}
?>
