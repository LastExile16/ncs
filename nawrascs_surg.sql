-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 27, 2021 at 09:52 PM
-- Server version: 5.6.41-84.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nawrascs_hemnv2`
--

-- --------------------------------------------------------

--
-- Table structure for table `attachment`
--

CREATE TABLE `attachment` (
  `a_id` int(11) NOT NULL,
  `filename` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'name of the file',
  `file_desc` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'patient attachment' COMMENT 'patient attachment',
  `uploaded_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date of atachment upload',
  `v_id_f` int(11) UNSIGNED NOT NULL COMMENT 'visit foreign key',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attachment`
--

INSERT INTO `attachment` (`a_id`, `filename`, `file_desc`, `uploaded_date`, `v_id_f`, `view`) VALUES
(1, '20/44/2017030523103401.JPG', '20 clinics attachment', '2017-03-05 20:10:35', 44, 1),
(2, '20/44/2017030523103511.JPG', '20 clinics attachment', '2017-03-05 20:10:35', 44, 1),
(3, '20/44/2017030523103521.jpg', '20 clinics attachment', '2017-03-05 20:10:35', 44, 1),
(4, '19/45/2017030523125201.jpg', '19 clinics attachment', '2017-03-05 20:12:52', 45, 1),
(5, '19/45/2017030523125311.JPG', '19 clinics attachment', '2017-03-05 20:12:53', 45, 1),
(6, '19/45/2017030523125421.jpg', '19 clinics attachment', '2017-03-05 20:12:54', 45, 1),
(7, '18/46/2017030523183601.jpg', '18 clinics attachment', '2017-03-05 20:18:36', 46, 1),
(9, '21/48/2017030813462901.JPG', '21 clinics attachment', '2017-03-08 10:46:29', 48, 1),
(10, '21/48/2017030813462911.jpg', '21 clinics attachment', '2017-03-08 10:46:29', 48, 1),
(11, '21/54/2017031114125701.png', '21 clinics attachment', '2017-03-11 11:12:57', 54, 1),
(12, '23/52/2017031311413601.png', '23 clinics attachment', '2017-03-13 08:41:36', 52, 1),
(13, '23/58/2017031318344401.jpg', '23 clinics attachment', '2017-03-13 15:34:44', 58, 1),
(14, '33/64/201703140474701.png', '33 clinics attachment', '2017-03-13 21:47:47', 64, 1),
(15, '36/63/201703140490401.png', '36 clinics attachment', '2017-03-13 21:49:04', 63, 1),
(16, '19/71/201703140525001.png', '19 clinics attachment', '2017-03-13 21:52:50', 71, 1),
(17, '33/73/201703151073601.jpg', '33 clinics attachment', '2017-03-14 22:07:36', 73, 1),
(18, '46/77/2017031511362101.png', '46 clinics attachment', '2017-03-15 08:36:21', 77, 1),
(19, '46/81/201703189270101.jpg', '46 clinics attachment', '2017-03-18 06:27:01', 81, 1),
(20, '46/90/2017040818345401.PNG', '46 clinics attachment', '2017-04-08 15:34:54', 90, 1),
(21, '36/99/2017041220505901.jpg', '36 clinics attachment', '2017-04-12 17:50:59', 99, 1),
(22, '36/101/2017041319524801.png', '36 clinics attachment', '2017-04-13 16:52:48', 101, 1),
(23, '36/101/2017041319541501.png', '36 clinics attachment', '2017-04-13 16:54:15', 101, 1),
(24, '33/102/2017041319591401.png', '33 clinics attachment', '2017-04-13 16:59:14', 102, 1),
(25, '64/111/2017041718005601.png', '64 clinics attachment', '2017-04-17 15:00:56', 111, 1),
(26, '64/114/2017050117180601.png', '64 clinics attachment', '2017-05-01 14:18:06', 114, 1),
(27, '19/120/2017050719503801.jpg', '19 clinics attachment', '2017-05-07 16:50:38', 120, 1),
(28, '23/121/2017050719520201.jpg', '23 clinics attachment', '2017-05-07 16:52:02', 121, 1),
(29, '21/123/2017051020073601.png', '21 clinics attachment', '2017-05-10 17:07:36', 123, 1),
(30, '18/124/2017051317135501.png', '18 clinics attachment', '2017-05-13 14:13:55', 124, 1),
(31, '66/125/2017051317145901.png', '66 clinics attachment', '2017-05-13 14:14:59', 125, 1),
(32, '64/126/2017051317155801.png', '64 clinics attachment', '2017-05-13 14:15:58', 126, 1),
(33, '20/136/2017052717473201.JPG', '20 clinics attachment', '2017-05-27 14:47:32', 136, 1),
(34, '33/137/2017052717483601.JPG', '33 clinics attachment', '2017-05-27 14:48:36', 137, 1),
(35, '64/138/2017052717512301.JPG', '64 clinics attachment', '2017-05-27 14:51:23', 138, 1),
(36, '59/108/2017061417240301.JPG', '59 clinics attachment', '2017-06-14 14:24:03', 108, 1),
(37, '18/140/2017061918314201.JPG', '18 clinics attachment', '2017-06-19 15:31:42', 140, 1),
(38, '64/149/2017062118521001.JPG', '64 clinics attachment', '2017-06-21 15:52:10', 149, 1),
(39, '59/154/2017070418390801.JPG', '59 clinics attachment', '2017-07-04 15:39:08', 154, 1),
(40, '19/155/2017070419075601.JPG', '19 clinics attachment', '2017-07-04 16:07:56', 155, 1),
(41, '71/157/2017070913342601.JPG', '71 clinics attachment', '2017-07-09 10:34:26', 157, 1),
(42, '20/158/2017070918473001.JPG', '20 clinics attachment', '2017-07-09 15:47:30', 158, 1),
(44, '82/180/2017071618041101.JPG', '82 clinics attachment', '2017-07-16 15:04:11', 180, 1),
(45, '19/186/2017071619300501.JPG', '19 clinics attachment', '2017-07-16 16:30:05', 186, 1),
(46, '59/189/2017071622221501.JPG', '59 clinics attachment', '2017-07-16 19:22:15', 189, 1),
(47, '36/188/2017071622241101.JPG', '36 clinics attachment', '2017-07-16 19:24:11', 188, 1),
(48, '18/156/2017071718330301.JPG', '18 clinics attachment', '2017-07-17 15:33:03', 156, 1),
(49, '78/176/2017071719544101.JPG', '78 clinics attachment', '2017-07-17 16:54:41', 176, 1),
(50, '82/191/2017071722384901.JPG', '82 clinics attachment', '2017-07-17 19:38:49', 191, 1),
(51, '72/192/2017071722425701.jpg', '72 clinics attachment', '2017-07-17 19:42:57', 192, 1),
(52, '64/194/2017071722450901.JPG', '64 clinics attachment', '2017-07-17 19:45:09', 194, 1),
(53, '72/199/2017071823193501.JPG', '72 clinics attachment', '2017-07-18 20:19:36', 199, 1),
(54, '83/200/2017071823201601.JPG', '83 clinics attachment', '2017-07-18 20:20:16', 200, 1),
(55, '23/203/2017071823241601.JPG', '23 clinics attachment', '2017-07-18 20:24:16', 203, 1),
(56, '70/204/2017071823245301.JPG', '70 clinics attachment', '2017-07-18 20:24:53', 204, 1),
(57, '87/210/2017072223152801.JPG', '87 clinics attachment', '2017-07-22 20:15:28', 210, 1),
(58, '89/211/2017072223163801.JPG', '89 clinics attachment', '2017-07-22 20:16:38', 211, 1),
(59, '90/212/2017072223180401.JPG', '90 clinics attachment', '2017-07-22 20:18:04', 212, 1),
(60, '91/213/2017072223213701.JPG', '91 clinics attachment', '2017-07-22 20:21:37', 213, 1),
(61, '92/214/2017072223235601.JPG', '92 clinics attachment', '2017-07-22 20:23:56', 214, 1),
(62, '93/215/2017072223250901.JPG', '93 clinics attachment', '2017-07-22 20:25:09', 215, 1),
(63, '94/216/2017072223262101.JPG', '94 clinics attachment', '2017-07-22 20:26:22', 216, 1),
(64, '83/218/2017072317084101.JPG', '83 clinics attachment', '2017-07-23 14:08:41', 218, 1),
(65, '92/221/2017072320271201.JPG', '92 clinics attachment', '2017-07-23 17:27:12', 221, 1),
(66, '76/166/201707240061801.JPG', '76 clinics attachment', '2017-07-23 21:06:18', 166, 1),
(67, '85/183/2017072521183201.JPG', '85 clinics attachment', '2017-07-25 18:18:32', 183, 1),
(68, '18/225/2017072621124001.JPG', '18 clinics attachment', '2017-07-26 18:12:40', 225, 1),
(69, '46/226/2017072621134201.JPG', '46 clinics attachment', '2017-07-26 18:13:42', 226, 1),
(70, '21/227/2017072621141401.JPG', '21 clinics attachment', '2017-07-26 18:14:14', 227, 1),
(71, '19/228/2017072621145601.JPG', '19 clinics attachment', '2017-07-26 18:14:56', 228, 1),
(72, '95/229/2017072621152401.JPG', '95 clinics attachment', '2017-07-26 18:15:24', 229, 1),
(73, '96/230/2017072912144501.JPG', '96 clinics attachment', '2017-07-29 09:14:45', 230, 1),
(74, '96/240/2017080118160701.JPG', '96 clinics attachment', '2017-08-01 15:16:07', 240, 1),
(75, '72/239/2017080122061001.JPG', '72 clinics attachment', '2017-08-01 19:06:10', 239, 1),
(76, '83/246/2017080211464501.JPG', '83 clinics attachment', '2017-08-02 08:46:45', 246, 1),
(77, '91/247/2017081223485301.JPG', '91 clinics attachment', '2017-08-12 20:48:53', 247, 1),
(78, '95/248/2017081223494001.JPG', '95 clinics attachment', '2017-08-12 20:49:40', 248, 1),
(79, '94/251/2017091316060001.JPG', '94 clinics attachment', '2017-09-13 13:06:00', 251, 1),
(80, '90/212/201803067354801.jpeg', '90 clinics attachment', '2018-03-06 04:35:48', 212, 1),
(81, '93/283/201803067370801.jpeg', '93 clinics attachment', '2018-03-06 04:37:08', 283, 1),
(82, '59/289/201804056012301.png', '59 clinics attachment', '2018-04-05 03:01:23', 289, 1),
(83, '59/289/201804056012311.gif', '59 clinics attachment', '2018-04-05 03:01:23', 289, 1),
(84, '95/306/2018050519391301.jpg', '95 clinics attachment', '2018-05-05 16:39:13', 306, 1),
(85, '94/307/2018050519410901.jpg', '94 clinics attachment', '2018-05-05 16:41:09', 307, 1),
(86, '97/329/2018052713302401.gif', '97 clinics attachment', '2018-05-27 10:30:24', 329, 1),
(87, '103/334/2018052717414201.png', '103 clinics attachment', '2018-05-27 14:41:42', 334, 1),
(89, '60/161/201806078052801.png', '60 clinics attachment', '2018-06-07 05:05:28', 161, 1),
(90, '103/334/2018061710113301.png', '103 clinics attachment', '2018-06-17 07:11:33', 334, 1),
(91, '104/352/2018061817351301.png', '104 clinics attachment', '2018-06-18 14:35:13', 352, 1),
(92, '101/359/2018062017550701.jpg', '101 clinics attachment', '2018-06-20 14:55:07', 359, 1),
(93, '97/360/2018062018235201.gif', '97 clinics attachment', '2018-06-20 15:23:52', 360, 1),
(94, '104/356/2018062018322301.png', '104 clinics attachment', '2018-06-20 15:32:23', 356, 1),
(95, '104/364/2018062018363801.jpg', '104 clinics attachment', '2018-06-20 15:36:38', 364, 1),
(96, '104/364/2018062018502101.png', '104 clinics attachment', '2018-06-20 15:50:21', 364, 1),
(97, '98/369/2018062116244301.gif', '98 clinics attachment', '2018-06-21 13:24:43', 369, 1),
(98, '87/372/2018062118004801.png', '87 clinics attachment', '2018-06-21 15:00:48', 372, 1),
(99, '87/372/2018062118004811.png', '87 clinics attachment', '2018-06-21 15:00:48', 372, 1),
(100, '104/375/2018062118084501.png', '104 clinics attachment', '2018-06-21 15:08:45', 375, 1),
(101, '104/375/2018062118084511.gif', '104 clinics attachment', '2018-06-21 15:08:45', 375, 1),
(102, '104/375/2018062118160201.png', '104 clinics attachment', '2018-06-21 15:16:02', 375, 1),
(103, '104/375/2018062118160211.png', '104 clinics attachment', '2018-06-21 15:16:02', 375, 1),
(104, '87/376/2018062214294801.png', '87 clinics attachment', '2018-06-22 11:29:48', 376, 1),
(105, '87/376/2018062214294811.png', '87 clinics attachment', '2018-06-22 11:29:48', 376, 1),
(106, '87/376/2018062214294821.png', '87 clinics attachment', '2018-06-22 11:29:48', 376, 1),
(107, '104/378/2018062214483701.png', '104 clinics attachment', '2018-06-22 11:48:37', 378, 1),
(108, '104/378/2018062214483711.png', '104 clinics attachment', '2018-06-22 11:48:37', 378, 1),
(109, '104/378/2018062214483721.png', '104 clinics attachment', '2018-06-22 11:48:37', 378, 1),
(110, '23/381/2018062215060401.png', '23 clinics attachment', '2018-06-22 12:06:04', 381, 1),
(111, '23/381/201806234185201.png', '23 clinics attachment', '2018-06-23 01:18:52', 381, 1),
(112, '23/381/201806234185311.png', '23 clinics attachment', '2018-06-23 01:18:53', 381, 1),
(113, '105/382/201806236285701.gif', '105 clinics attachment', '2018-06-23 03:28:57', 382, 1),
(114, '59/387/2018062313244001.jpg', '59 clinics attachment', '2018-06-23 10:24:40', 387, 1),
(115, '59/387/2018062313244011.jpg', '59 clinics attachment', '2018-06-23 10:24:40', 387, 1),
(116, '107/390/2018062416291301.png', '107 clinics attachment', '2018-06-24 13:29:13', 390, 1),
(118, '107/390/2018062416320701.gif', '107 clinics attachment', '2018-06-24 13:32:07', 390, 1),
(119, '107/390/2018062416325201.jpg', '107 clinics attachment', '2018-06-24 13:32:52', 390, 1),
(120, '104/380/2018062511412101.jpg', '104 clinics attachment', '2018-06-25 08:41:21', 380, 1),
(121, '94/384/2018062511430501.jpg', '94 clinics attachment', '2018-06-25 08:43:05', 384, 1),
(122, '110/394/2018062914494001.JPG', '110 clinics attachment', '2018-06-29 11:49:40', 394, 1),
(123, '105/401/201807055483001.jpg', '105 clinics attachment', '2018-07-05 02:48:30', 401, 1),
(124, '98/393/201807056250501.png', '98 clinics attachment', '2018-07-05 03:25:05', 393, 1),
(125, '101/359/201807056394301.jpeg', '101 clinics attachment', '2018-07-05 03:39:43', 359, 1),
(126, '33/287/2018101413522101.jpg', '33 clinics attachment', '2018-10-14 10:52:21', 287, 1),
(127, '33/287/2018101416321401.jpg', '33 clinics attachment', '2018-10-14 13:32:14', 287, 1),
(128, '33/202/2018101416351701.png', '33 clinics attachment', '2018-10-14 13:35:17', 202, 1),
(129, '98/393/2018102916541801.jpg', '98 clinics attachment', '2018-10-29 13:54:18', 393, 1),
(130, '113/420/201811226123201.png', '113 clinics attachment', '2018-11-22 03:12:32', 420, 1),
(131, '87/422/201811226472601.png', '87 clinics attachment', '2018-11-22 03:47:26', 422, 1),
(132, '71/424/201811226524801.png', '71 clinics attachment', '2018-11-22 03:52:48', 424, 1),
(133, '89/431/2018112210275101.png', '89 clinics attachment', '2018-11-22 07:27:51', 431, 1),
(134, '89/431/2018112210284301.png', '89 clinics attachment', '2018-11-22 07:28:43', 431, 1),
(135, '89/431/2018112210314101.png', '89 clinics attachment', '2018-11-22 07:31:41', 431, 1),
(136, '89/431/2018112210335201.png', '89 clinics attachment', '2018-11-22 07:33:52', 431, 1),
(137, '89/431/2018112210364401.png', '89 clinics attachment', '2018-11-22 07:36:44', 431, 1),
(138, '89/431/2018112210444501.png', '89 clinics attachment', '2018-11-22 07:44:45', 431, 1),
(139, '89/431/2018112211041101.png', '89 clinics attachment', '2018-11-22 08:04:11', 431, 1),
(140, '89/431/2018112211141501.png', '89 clinics attachment', '2018-11-22 08:14:15', 431, 1),
(141, '104/433/2018112211260801.jpg', '104 clinics attachment', '2018-11-22 08:26:08', 433, 1),
(142, '104/433/2018112211260811.jpg', '104 clinics attachment', '2018-11-22 08:26:08', 433, 1),
(143, '104/433/2018112211260821.jpg', '104 clinics attachment', '2018-11-22 08:26:08', 433, -1),
(144, '104/433/2018112211260831.jpg', '104 clinics attachment', '2018-11-22 08:26:08', 433, 1),
(145, '59/434/2018112211283601.jpg', '59 clinics attachment', '2018-11-22 08:28:36', 434, 1),
(146, '59/434/2018112211283611.jpg', '59 clinics attachment', '2018-11-22 08:28:36', 434, 1),
(147, '59/434/2018112211283621.jpg', '59 clinics attachment', '2018-11-22 08:28:36', 434, 1),
(148, '59/434/2018112211283631.jpg', '59 clinics attachment', '2018-11-22 08:28:36', 434, 1),
(149, '21/436/2018112211310201.jpg', '21 clinics attachment', '2018-11-22 08:31:02', 436, 1),
(150, '21/436/2018112211310211.jpg', '21 clinics attachment', '2018-11-22 08:31:02', 436, 1),
(151, '21/436/2018112211310221.jpg', '21 clinics attachment', '2018-11-22 08:31:02', 436, 1),
(152, '21/436/2018112211310231.jpg', '21 clinics attachment', '2018-11-22 08:31:02', 436, 1),
(153, '19/437/2018112211382601.jpg', '19 clinics attachment', '2018-11-22 08:38:26', 437, 1),
(154, '19/437/2018112211382611.jpg', '19 clinics attachment', '2018-11-22 08:38:26', 437, 1),
(155, '19/437/2018112211382621.jpg', '19 clinics attachment', '2018-11-22 08:38:26', 437, 1),
(156, '19/437/2018112211382631.jpg', '19 clinics attachment', '2018-11-22 08:38:26', 437, 1),
(157, '23/435/2018112211411301.jpg', '23 clinics attachment', '2018-11-22 08:41:13', 435, 1),
(158, '23/435/2018112211411311.jpg', '23 clinics attachment', '2018-11-22 08:41:13', 435, 1),
(159, '23/435/2018112211411321.jpg', '23 clinics attachment', '2018-11-22 08:41:13', 435, 1),
(160, '23/435/2018112211411331.jpg', '23 clinics attachment', '2018-11-22 08:41:13', 435, 1),
(161, '19/437/2018112211451601.jpg', '19 clinics attachment', '2018-11-22 08:45:16', 437, 1),
(162, '19/437/2018112211451611.jpg', '19 clinics attachment', '2018-11-22 08:45:16', 437, 1),
(163, '19/437/2018112211451621.jpg', '19 clinics attachment', '2018-11-22 08:45:16', 437, 1),
(164, '19/437/2018112211451631.jpg', '19 clinics attachment', '2018-11-22 08:45:16', 437, 1),
(165, '19/437/2018112211461601.jpg', '19 clinics attachment', '2018-11-22 08:46:16', 437, 1),
(166, '19/437/2018112211461611.jpg', '19 clinics attachment', '2018-11-22 08:46:16', 437, 1),
(167, '19/437/2018112211461621.jpg', '19 clinics attachment', '2018-11-22 08:46:16', 437, 1),
(168, '19/437/2018112211461631.jpg', '19 clinics attachment', '2018-11-22 08:46:16', 437, 1),
(169, '19/437/2018112212155501.jpg', '19 clinics attachment', '2018-11-22 09:15:55', 437, 1),
(170, '19/437/2018112212155511.jpg', '19 clinics attachment', '2018-11-22 09:15:55', 437, 1),
(171, '19/437/2018112212155521.jpg', '19 clinics attachment', '2018-11-22 09:15:55', 437, 1),
(172, '19/437/2018112212155531.jpg', '19 clinics attachment', '2018-11-22 09:15:55', 437, 1),
(173, '19/437/2018112212172801.jpg', '19 clinics attachment', '2018-11-22 09:17:28', 437, 1),
(174, '19/437/2018112212172811.jpg', '19 clinics attachment', '2018-11-22 09:17:28', 437, 1),
(175, '19/437/2018112212172821.jpg', '19 clinics attachment', '2018-11-22 09:17:28', 437, 1),
(176, '19/437/2018112212172831.jpg', '19 clinics attachment', '2018-11-22 09:17:28', 437, 1),
(177, '112/439/2019012712243801.jpg', '112 clinics attachment', '2019-01-27 09:24:38', 439, 1),
(178, '46/441/2019022418375201.jpg', '46 clinics attachment', '2019-02-24 15:37:52', 441, 1),
(179, '115/447/2019072815354001.jpg', '115 clinics attachment', '2019-07-28 12:35:40', 447, 1),
(180, '115/485/2021030113154601.MOV', '115 clinics attachment', '2021-03-01 10:15:46', 485, -1),
(181, '115/485/202103021202060.mp4', '115 clinics attachment', '2021-03-02 09:02:06', 485, -1),
(182, '115/485/202103021207080.mp4', '115 clinics attachment', '2021-03-02 09:07:08', 485, -1),
(183, '115/485/202103021214220.MOV', '115 clinics attachment', '2021-03-02 09:14:22', 485, -1),
(184, '114/414/2021030312231801.mp4', '114 clinics attachment', '2021-03-03 09:23:18', 414, -1),
(185, '115/485/2021030312420401.mp4', '115 clinics attachment', '2021-03-03 09:42:04', 485, -1),
(186, '114/414/202103047203001.mp4', '114 clinics attachment', '2021-03-04 04:20:30', 414, -1),
(187, '114/414/202103047212901.MOV', '114 clinics attachment', '2021-03-04 04:21:29', 414, -1),
(188, '114/414/202103047232301.MOV', '114 clinics attachment', '2021-03-04 04:23:23', 414, -1),
(189, '114/414/202103047345601.mp4', '114 clinics attachment', '2021-03-04 04:34:56', 414, 1),
(190, '114/414/202103048513001.mp4', '114 clinics attachment', '2021-03-04 05:51:30', 414, 1),
(191, '114/414/202103049074901.png', '114 clinics attachment', '2021-03-04 06:07:49', 414, 1),
(192, '105/487/2021030410525101.mp4', '105 clinics attachment', '2021-03-04 07:52:51', 487, 1),
(193, '105/487/2021030410525111.png', '105 clinics attachment', '2021-03-04 07:52:51', 487, 1),
(194, '64/493/202103096182901.mp4', '64 clinics attachment', '2021-03-09 03:18:29', 493, -1),
(195, '64/493/202103096182911.png', '64 clinics attachment', '2021-03-09 03:18:29', 493, 1),
(196, '64/493/202103096182921.png', '64 clinics attachment', '2021-03-09 03:18:29', 493, 1),
(197, '64/493/202103096182931.png', '64 clinics attachment', '2021-03-09 03:18:29', 493, 1),
(198, '110/494/2021030917033501.png', '110 clinics attachment', '2021-03-09 14:03:35', 494, 1),
(199, '110/494/2021030917104101.jpg', '110 clinics attachment', '2021-03-09 14:10:41', 494, 1);

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `exp_id` int(11) NOT NULL,
  `expense_name` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'name of the expense',
  `amount` int(10) UNSIGNED NOT NULL COMMENT 'amount of expense',
  `exp_date` date NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`exp_id`, `expense_name`, `amount`, `exp_date`, `view`) VALUES
(1, 'molida', 100000, '2018-04-10', 1),
(2, 'secretary', 250000, '2018-04-15', 1),
(3, 'test', 50000, '2018-02-07', 1),
(4, 'Cake', 5, '2018-04-15', 1),
(5, '30000', 5, '2018-04-15', 1),
(6, 'molida', 220000, '2018-04-16', 1),
(7, 'cake', 25000, '2018-04-16', 1),
(8, '25000', 3, '2018-05-05', 1),
(9, '15000', 2, '2018-05-05', 1),
(10, '5000', 1, '2018-05-05', 1),
(11, '155555', 1, '2018-05-05', 1),
(12, '25000', 2, '2018-05-05', 1),
(13, '25000', 5, '2018-05-05', 1),
(14, '2', 20000, '2018-05-05', 1),
(15, '6', 20000, '2018-06-23', 1),
(16, '15000', 6, '2018-06-23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `p_id` int(11) UNSIGNED NOT NULL COMMENT 'key: yymmdd[session(autonumber)], autonum: count today visits then start increment, plan failed',
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'patient name',
  `sex` tinyint(1) NOT NULL COMMENT '1:male, 2:female, 3:transgender',
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'patient address',
  `dob` int(4) DEFAULT NULL COMMENT 'year of birth',
  `phone` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'phone number, max is 2 numbers',
  `occupation` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'patient occupation',
  `past_hx` text COLLATE utf8_unicode_ci COMMENT 'past history (should be merged in each visit not replaced and use nl2b function)',
  `family_hx` text COLLATE utf8_unicode_ci COMMENT 'family history (should be merged in each visit)',
  `drug_hx` text COLLATE utf8_unicode_ci COMMENT 'drug history (should be merged ...)',
  `other_hx` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other history to mention (should be merged ...)',
  `latest_visit` date DEFAULT NULL COMMENT 'store latest visit if available (i added this to increase query performance for "patients" page) )',
  `latest_followup_visit` date DEFAULT NULL COMMENT 'latest_followup_visit to show expected visit in current_p',
  `surgeries` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'here we combine all surgeries from visit_surgery table here to show it easily in all_p',
  `procedures_list` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'list of procedures so that I don''t need to query from visits to show in patient table',
  `queue_status` tinyint(1) NOT NULL DEFAULT '-1' COMMENT '1:in queue, -1:not in queue; current queue status',
  `u_id_f` int(11) NOT NULL COMMENT 'user foreign key',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted',
  `update_record` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='later make view index';

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`p_id`, `fullname`, `sex`, `address`, `dob`, `phone`, `occupation`, `past_hx`, `family_hx`, `drug_hx`, `other_hx`, `latest_visit`, `latest_followup_visit`, `surgeries`, `procedures_list`, `queue_status`, `u_id_f`, `view`, `update_record`) VALUES
(18, 'shukria anwar', 2, 'hawar', 1993, '45678', 'student', 'some hx', 'family was ...', 'a lot of drugs', 'and something else and more', '2018-11-22', '2018-11-29', 'surg1, surg2', 'special procedure 1', 1, 2, 1, '0000-00-00 00:00:00'),
(19, 'bazhdar haji', 1, 'Tokyo', 1980, '123', 'any', 'past data is important', 'NEW', '', '', '2018-11-22', '2017-08-05', 'surg2, surg4', NULL, -1, 1, 1, '0000-00-00 00:00:00'),
(20, 'sami xayat', 1, 'erbil', 1985, '07701234567', 'worker', NULL, NULL, NULL, '', '2018-04-15', '0000-00-00', 'surg2, surg4, surg1', NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(21, 'kamaran aziz', 1, 'waziran', 1978, '07731234567', 'worker', 'test past history', 'test family history', '', 'other test', '2018-11-22', '2017-08-05', 'surg2, surg1, surg3', NULL, -1, 1, 1, '0000-00-00 00:00:00'),
(23, 'Lana Jamil Jamal', 2, 'Duhok', 1992, '07501234576', 'qutabi', NULL, NULL, NULL, '', '2018-11-22', '2017-08-05', 'surg2, surg3, new surgery,', NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(33, 'Barin Kawa', 2, 'Hawler-badawa', 2003, '0750 381 1213', 'Student', 'the past history', 'family history', 'Drug History', 'Other History', '2017-07-18', '0000-00-00', 'surg3, surg2, surg3', 'procedure test', -1, 1, 1, '0000-00-00 00:00:00'),
(36, 'Payam Qadr', 2, 'Hawler', 1996, '0750 340 5609', 'Student', NULL, NULL, NULL, '', '2020-08-31', '2017-07-28', NULL, NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(46, 'Sara Ahmed', 2, 'Hawler', 1994, '0750 51543455', 'any', '', '', '', '', '2019-02-24', '2017-08-06', 'surg2', NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(59, 'Lorin Toma', 1, 'ankawa', 1984, '07505789853', 'st', 'some hx', 'family hx', 'no drugs', 'nothing', '2019-02-23', '2018-07-05', 'surg2, surg1, new surgery', 'special procedure 1, other', -1, 1, 1, '0000-00-00 00:00:00'),
(60, 'Sara Hawkar', 2, 'Choman', 1986, '07515558899', 'doctor', NULL, NULL, NULL, '', '2018-07-04', '2018-05-27', 'surg1, surg2', NULL, -1, 1, 1, '0000-00-00 00:00:00'),
(64, 'Kamaran Ali Hashim', 1, 'Karkuk', 1964, '07705444845', 'any', NULL, NULL, NULL, '', '2021-03-09', '2017-08-06', NULL, NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(66, 'hawkar', 1, 'hawler', 1994, '07503163655', 'student', NULL, NULL, NULL, '', '2017-07-17', '2017-07-31', NULL, NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(70, 'Shawnm Ali', 2, 'Hawler-waziran', 1979, '07703163686', 'any', NULL, NULL, NULL, '', '2018-04-08', '0000-00-00', 'surg2', 'other', -1, 2, 1, '0000-00-00 00:00:00'),
(71, 'Hawkar', 1, 'Roonaky', 1995, '07504220854', 'not available', NULL, NULL, NULL, '', '2018-11-22', '2018-11-29', 'surg3', NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(72, 'Kawsar wali', 1, 'Soran', 1990, '07835432112', 'not available', NULL, NULL, NULL, '', '2021-03-09', '2017-08-08', 'surg2', NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(76, 'Shagwl koye', 2, 'koya', 2000, '0771 544 6655', 'not available', NULL, NULL, NULL, '', '2021-03-09', '0000-00-00', NULL, NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(78, 'Aliza Toma', 2, 'Ankawa', 1996, '0771 654 8878', 'not available', NULL, NULL, NULL, '', '2020-12-12', '0000-00-00', NULL, NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(79, 'Sakar Jabbar', 2, 'Zhyan', 1984, '0750 406 6461', 'not available', NULL, NULL, NULL, '', '2017-07-19', '0000-00-00', NULL, NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(82, 'Aram Jihad', 1, 'Mahabad', 1998, '0750 358 7878', 'not available', NULL, NULL, NULL, '', '2017-07-17', '2017-08-06', NULL, NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(83, 'Dana Jihad', 1, 'Mahabad', 1994, '0750 438 3737', 'not available', NULL, NULL, NULL, '', '2017-08-02', '2017-08-05', NULL, NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(85, 'Ari Jihad', 1, 'Mahabad', 2001, '0770 644 5445', 'not available', NULL, NULL, NULL, '', '2018-04-15', '0000-00-00', NULL, NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(87, 'Abdulla Najmadin', 1, 'Chamchmal', 2005, '0770 644 5544', 'Student', NULL, NULL, NULL, '', '2018-11-22', '2018-11-29', 'surg1, surg4, surg2, surg3', 'new procedure, procedure test, special procedure 1', -1, 1, 1, '0000-00-00 00:00:00'),
(89, 'Shilan Kawa', 2, 'Chamchmal', 1984, '0771 554 8877', 'any', NULL, NULL, NULL, '', '2018-11-22', '0000-00-00', 'surg2, surg3', NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(90, 'Taha Najmadin', 1, 'Chamchmal', 1991, '0770198 7212', 'Worker', NULL, NULL, NULL, '', '2018-05-06', '0000-00-00', NULL, NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(91, 'Shirin Taha', 2, 'Chamchmal', 1995, '0750 316 3544', 'any', NULL, NULL, NULL, '', '2018-06-21', '2018-06-23', 'surg3, surg1', 'procedure test', -1, 1, 1, '0000-00-00 00:00:00'),
(92, 'Laila Toma', 2, 'Soran', 1989, '0771 998 7854', 'not available', NULL, NULL, NULL, '', '2017-10-27', '0000-00-00', NULL, NULL, -1, 2, 1, '0000-00-00 00:00:00'),
(93, 'Rozhan Raswl', 2, 'Hawler-waziran', 1998, '0750 778 9878', 'Student', NULL, NULL, NULL, '', '2018-03-06', '0000-00-00', NULL, NULL, -1, 1, 1, '0000-00-00 00:00:00'),
(94, 'Bnar Raf3at', 2, 'Ankawa', 1995, '0750 325 4477', 'Student', NULL, NULL, NULL, '', '2018-06-25', '2018-05-28', 'new surgery,, new surgery', 'procedure onr na na twi,one', -1, 1, 1, '0000-00-00 00:00:00'),
(95, 'Hewr Ahmad ALI', 1, 'Hawler', 1990, '0750 457 8877', 'any', NULL, NULL, NULL, '', '2018-05-05', '2018-05-30', 'surg1, surg4', NULL, 1, 2, 1, '0000-00-00 00:00:00'),
(96, 'Essa ismail', 1, 'Badawa', 1997, '0750 433 5554', 'not known', NULL, NULL, NULL, '', '2017-12-18', '0000-00-00', NULL, NULL, -1, 1, 1, '0000-00-00 00:00:00'),
(97, 'Ahmed Ali', 1, 'Rasti', 1996, '0750 125 545445', 'Student', NULL, NULL, NULL, '', '2018-06-20', '2018-06-22', 'surg3, surg3', 'new procedure', -1, 2, 1, '2018-04-15 15:35:59'),
(98, 'Hawre Jawad', 1, 'Choman', 1995, '0770 455 4545', 'Student', 'past hx', 'family hx', 'drug hx', 'other hx', '2018-06-21', '2018-06-28', 'surg1, surg4, surg2, surg3', 'proce one, proce two, new procedure, procedure test, special procedure 1', -1, 1, 1, '2018-05-04 21:47:35'),
(101, 'testingTime', 2, 'someaddr', 1990, '123', 'not available', NULL, NULL, NULL, '', '2018-07-10', '2018-06-21', 'surg1, surg3', 'procedure test', -1, 2, 1, '2018-05-23 08:54:01'),
(102, 'testingTime2', 1, 'adr', 2006, '123', 'not available', NULL, NULL, NULL, '', '2018-07-01', '2018-06-27', NULL, 'special procedure 1', -1, 2, 1, '2018-05-23 14:32:28'),
(103, 'Riko kuramoto', 2, 'Japan', 1998, '123', 'not available', NULL, NULL, NULL, '', '2018-06-18', '0000-00-00', 'surg1, surg4, surg1, surg2', 'procedures, new procedure', -1, 1, 1, '2018-05-27 14:33:48'),
(104, 'Cathrin bahram', 2, 'koya', 1979, '123', 'korektel', NULL, NULL, NULL, '', '2018-11-22', '2018-06-25', 'surg2, surg3, surg4, surg1, surg1, su new', 'procedure test, new procedure, SCLEROTHERAPY, new pro', -1, 1, 1, '2018-06-18 13:15:32'),
(105, 'Aram Abdulla', 1, 'hawler', 1990, '12345', 'student', NULL, NULL, NULL, '', '2021-03-04', '2018-06-30', 'surg3', 'procedure test', -1, 1, 1, '2018-06-23 02:44:15'),
(107, 'Tayar Tofiq', 1, 'badawa', 1980, '0750 444 4444', 'Ancient singer', NULL, NULL, NULL, '', '2018-06-23', NULL, 'surg1', 'procedure onr na na twi,one, hi pro', -1, 1, 1, '2018-06-23 15:54:34'),
(110, 'Aryan2 Wasman', 2, 'daratw', 1995, '0770 316 54548', 'Student', NULL, NULL, NULL, '', '2021-03-09', NULL, 'surg2', 'special procedure 1', -1, 2, 1, '2018-06-29 11:46:53'),
(112, 'xarman ali', 2, 'hawler', 1990, '123', 'student', NULL, NULL, NULL, '', '2019-01-27', NULL, NULL, NULL, -1, 2, 1, '2018-07-08 13:22:47'),
(113, '3wsman', 1, 'Hawler-kasnazan', 1989, '0750 3153585', 'any', NULL, NULL, NULL, '', '2018-11-22', '2018-11-29', 'surg3', NULL, -1, 2, 1, '2018-07-09 13:39:43'),
(114, 'saya kawa', 2, 'hawler', 1986, '123', 'student', NULL, NULL, NULL, '', '2018-11-15', NULL, NULL, NULL, -1, 2, 1, '2018-11-15 09:19:06'),
(115, 'ahmad ali', 1, 'hawler', 1988, '123', 'driver', NULL, NULL, NULL, '', '2021-03-01', NULL, 'surg2', 'special procedure 1', -1, 2, 1, '2018-11-15 09:19:37'),
(116, 'Hardi', 1, 'hawler', 2000, '123456', 'student', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, -1, 2, 1, '2020-12-10 06:15:02'),
(117, 'Anas Anwar', 1, 'Baghdad', 1989, '01234567', 'worker', NULL, NULL, NULL, '', '2021-03-09', NULL, NULL, NULL, -1, 2, 1, '2021-03-09 13:56:54');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `u_id` int(11) NOT NULL,
  `fullname` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:secretary, 9:admin',
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email address',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `fullname`, `username`, `password`, `role`, `email`, `view`) VALUES
(1, 'NCS Representative', 'test', '5ae207fdc1969c31a8120707cde39c9f57474353', 9, 'hawrejawad97@gmail.com', 1),
(2, 'secretary2', 'sec', '5ae207fdc1969c31a8120707cde39c9f57474353', 1, 'nawras.nzar@gmail.com', 1),
(4, 'Secretary 1', 'Sec1', 'b2736859f31fab32243470793ebd46a1610a3c09', 1, 'not available', -1),
(6, 'test1 test2', 'SOMEBODY', '5ae207fdc1969c31a8120707cde39c9f57474353', 1, 'not available', -1);

-- --------------------------------------------------------

--
-- Table structure for table `visit`
--

CREATE TABLE `visit` (
  `v_id` int(11) UNSIGNED NOT NULL COMMENT 'visits id of patient',
  `p_id_f` int(11) UNSIGNED NOT NULL COMMENT 'patient foreign key',
  `visit_date` date NOT NULL COMMENT 'patient date of the visit',
  `visit_time` time DEFAULT NULL COMMENT 'time for the visit]',
  `followup_date` date DEFAULT NULL COMMENT 'followup date: next visit',
  `marital_status` tinyint(1) UNSIGNED NOT NULL COMMENT '1:single, 2:married, 3:divorced, 4:widowed.',
  `pregnant` tinyint(1) UNSIGNED NOT NULL COMMENT '1:pregnant, 2:not pregnant',
  `breast_feed` tinyint(1) UNSIGNED NOT NULL COMMENT '1:breasfeeding, 2:not breast feeding',
  `no_of_child` int(2) UNSIGNED NOT NULL COMMENT 'number of children',
  `other_info` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other patient-specific info ',
  `chief_comp_n_dur` text COLLATE utf8_unicode_ci COMMENT 'chief_complaint and duration',
  `examination` text COLLATE utf8_unicode_ci,
  `investigation` text COLLATE utf8_unicode_ci,
  `investigation_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'investigation notes',
  `note` text COLLATE utf8_unicode_ci COMMENT 'notes',
  `treatment_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'a note to describe the treatments given, this note will be printed along with the treatment_description data',
  `fee` decimal(13,3) NOT NULL COMMENT 'per visit fee',
  `procedure` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'procedures on the patient',
  `procedure_date` date NOT NULL COMMENT 'date  of procedure',
  `procedure_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'procedure notes',
  `in_queue` tinyint(1) NOT NULL DEFAULT '1' COMMENT '-1: visit completed so not in queue, 1:in queue, 2:in doctors room',
  `u_id_f` int(11) NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active:1, delete:-1',
  `update_record` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'to check date of record insert'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `visit`
--

INSERT INTO `visit` (`v_id`, `p_id_f`, `visit_date`, `visit_time`, `followup_date`, `marital_status`, `pregnant`, `breast_feed`, `no_of_child`, `other_info`, `chief_comp_n_dur`, `examination`, `investigation`, `investigation_note`, `note`, `treatment_note`, `fee`, `procedure`, `procedure_date`, `procedure_note`, `in_queue`, `u_id_f`, `view`, `update_record`) VALUES
(44, 20, '2017-03-05', '18:21:00', '2017-04-26', 2, 0, 0, 3, '', 'some test data', 'some test data examination', 'some test data investigation', '', 'more notes on patient', 'some notes for treatments \nmore notes\nand notes', '25000.000', '', '0000-00-00', '', -1, 2, -1, '0000-00-00 00:00:00'),
(45, 19, '2017-03-05', NULL, '2017-04-17', 1, 0, 0, 0, '', 'some test data complain', 'some test data examination 2', 'some test data investigation 2', '', 'here is some test notes', 'writing notes is important', '25000.000', '', '0000-00-00', '', -1, 2, -1, '0000-00-00 00:00:00'),
(46, 18, '2017-03-05', NULL, '2017-05-15', 1, 2, 2, 0, '', 'some test data complain 3', 'some test data examination 3', 'some test data investigation 3', '', '', 'notes to be written', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(47, 21, '2017-03-07', NULL, '2017-04-11', 1, 0, 0, 0, '', 'a complain', 'test Examination', 'test investigation', '', '', 'some notes', '20000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(48, 21, '2017-03-08', NULL, '2017-04-17', 1, 0, 0, 0, '', 'online test complain', 'on test examination', 'on test investigation', '', '', 'some notes', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(52, 23, '2017-03-13', NULL, '2017-03-20', 2, 2, 2, 3, '', 'data test1', 'data test 2', 'data test 3', 'data test 4 notes', '', 'some notes', '2500.000', 'procedure test', '2018-04-29', '', -1, 1, 1, '0000-00-00 00:00:00'),
(54, 21, '2017-03-11', NULL, '2017-04-11', 1, 0, 0, 0, '', '', '', '', '', '', '5x2 for 1 week\r\n2x1 for 2 weeks', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(58, 23, '2017-03-13', NULL, '0000-00-00', 2, 2, 2, 2, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(63, 36, '2017-03-14', NULL, '2017-04-05', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(64, 33, '2017-03-14', NULL, '2017-03-28', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(69, 36, '2017-03-14', NULL, '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', '', '0.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(70, 18, '2017-03-14', NULL, '2017-03-16', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(71, 19, '2017-03-14', NULL, '2017-03-16', 1, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(73, 33, '2017-03-15', NULL, '2017-03-26', 2, 1, 2, 1, '', '', '', '', '', '', 'some notes', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(74, 46, '2017-03-15', NULL, '0000-00-00', 1, 2, 2, 0, '', 'some info', 'dsd', 'asd', '', '', 'aDdfgv', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(75, 33, '2017-03-15', NULL, '0000-00-00', 2, 1, 2, 3, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(76, 18, '2017-03-15', NULL, '0000-00-00', 2, 2, 1, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(77, 46, '2017-03-15', NULL, '2017-03-29', 1, 2, 2, 0, '', 'some info,', 'dg', 'hcfxh', '', '', 'dxghfgh', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(78, 46, '2017-03-15', NULL, '2017-04-01', 2, 1, 2, 2, '', 'new', 'an examination', 'new investigation', '', '', 'good treatment', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(81, 46, '2017-03-18', NULL, '2017-03-20', 2, 2, 2, 0, '', 'some chief complain', 'new examination', '', '', '', 'good treatment', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(82, 23, '2017-03-18', NULL, '2017-03-22', 1, 2, 2, 0, '', '', 'new examination', '', '', '', '', '25.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(84, 23, '2017-03-22', NULL, '0000-00-00', 1, 2, 2, 0, '', 'some data', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(88, 36, '2017-04-08', NULL, '0000-00-00', 2, 2, 1, 3, '', 'some data', 'non', '', '', '', '1x2 use it\r\nand more use is here', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(89, 19, '2017-04-08', NULL, '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '2500.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(90, 46, '2017-04-08', NULL, '2017-04-17', 1, 2, 2, 0, '', 'Chief complain', 'Exam', 'Gation', '', '', 'Good treatment,\r\nBest new treatment', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(99, 36, '2017-04-12', NULL, '2017-05-11', 2, 2, 2, 1, '', 'Present history', 'New examination', 'Good inv', '', '', 'It is use three time in the day,\r\nOne time', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(100, 46, '2017-04-13', NULL, '0000-00-00', 2, 1, 2, 0, '', 'any chief complain and present histroy', 'more examination', 'inv', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(101, 36, '2017-04-13', NULL, '2017-05-02', 1, 2, 2, 0, '', 'chief complain', 'exa', 'invistigation', '', '', 'good treatment,daily treatment,one time in a day,three time,free use', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(102, 33, '2017-04-13', NULL, '2017-04-30', 1, 2, 2, 0, '', 'chief complain', 'exa', 'invistigation', '', '', 'good treatment\r\ndaily treatment\r\none time in a day\r\nthree time\r\nfree use', '35000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(103, 18, '2017-04-14', NULL, '2017-04-23', 2, 1, 2, 0, '', 'history', 'ex', 'inves', '', '', 'one time\r\ntwo time\r\nfree time\r\nevery day', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(108, 59, '2017-06-14', NULL, '0000-00-00', 1, 2, 2, 0, '', 'chife', 'e', 'ines', '', '', 'daily treatment\r\none time\r\nthree time', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(109, 60, '2017-06-14', NULL, '0000-00-00', 1, 2, 2, 0, '', 'chife', 'new examination', 'ines', '', '', 'ONE TIME\r\nTWO TIME', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(111, 64, '2017-04-17', NULL, '2017-04-24', 2, 0, 0, 2, '', 'chief complain', 'exam', 'invistigation', '', '', 'One day\r\nMore than one time', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(114, 64, '2017-05-01', NULL, '2017-05-21', 2, 0, 0, 0, '', 'chief', 'exa', 'invistigation', '', '', 'good treatment\r\nhigh treatment\r\nbest treatment', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(115, 64, '2017-05-01', NULL, '2017-05-01', 2, 0, 0, 3, '', 'chief complain', 'ex', 'hcfxh', '', '', 'good treatment', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(116, 46, '2017-05-01', NULL, '2017-05-01', 1, 2, 2, 0, '', 'chief complain', '', 'invistigation', '', '', 'note', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(118, 66, '2017-05-01', NULL, '0000-00-00', 2, 0, 0, 1, '', 'chief', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(120, 19, '2017-05-07', NULL, '2017-05-30', 1, 0, 0, 0, '', 'chief complain', 'new examination', 'new inves', '', '', 'hi treatment\r\ndaily treatment', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(121, 23, '2017-05-07', NULL, '2017-05-25', 2, 2, 2, 0, '', 'complain', 'ex', 'i', '', '', 'one time', '0.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(122, 18, '2017-05-09', NULL, '0000-00-00', 2, 2, 2, 0, '', 'chief complain', 'ex', 'new inves', '', '', '', '0.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(123, 21, '2017-05-10', NULL, '2017-05-24', 2, 0, 0, 0, '', 'new chief complain', 'examination', 'new inves', '', '', 'daily treatment', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(124, 18, '2017-05-13', NULL, '2017-05-31', 1, 2, 2, 0, '', 'some chief complain', 'new examination', 'ines', '', '', 'daily', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(125, 66, '2017-05-13', NULL, '2017-05-30', 2, 0, 0, 1, '', 'new', 'new examination', 'new', '', '', 'good', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(126, 64, '2017-05-13', NULL, '2017-06-08', 2, 0, 0, 2, '', 'new', 'new examination', 'new examinationin', '', '', 'daily', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(127, 20, '2017-05-13', NULL, '2017-06-06', 1, 0, 0, 0, '', 'c', 'e', 'i', '', '', 'best', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(128, 33, '2017-05-13', NULL, '2017-08-30', 1, 2, 2, 0, '', 'new', 'new examination', 'ines', '', '', 'one time\r\ntwo time', '0.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(129, 70, '2017-05-13', NULL, '0000-00-00', 2, 2, 2, 1, '', '', '', '', '', '', '', '0.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(131, 19, '2017-05-17', NULL, '2017-05-31', 1, 0, 0, 0, '', 'chif', 'e', '', '', '', 'good', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(135, 36, '2017-05-27', NULL, '0000-00-00', 1, 2, 2, 0, 'some data', 'new chief', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(136, 20, '2017-05-27', NULL, '2017-06-05', 1, 0, 0, 0, '', 'new chief', 'new chief', 'i', '', '', 'first treatment\r\nsecond treatment', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(137, 33, '2017-05-27', NULL, '2017-06-05', 1, 2, 2, 0, '', 'chif', 'e', 'ines', '', '', 'daily', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(138, 64, '2017-05-27', NULL, '0000-00-00', 2, 0, 0, 2, '', 'some chief complain', 'new examination', 'new examinationin', '', '', 'daily', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(140, 18, '2017-06-19', NULL, '2017-06-28', 1, 2, 2, 0, '', 'chife', 'new examination', 'ines', '', '', 'one time', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(146, 64, '2017-06-21', NULL, '0000-00-00', 2, 0, 0, 2, '', 'new', 'new chief', 'new', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(149, 64, '2017-06-21', NULL, '2017-06-27', 2, 0, 0, 2, '', '', '', '', '', '', 'hfjdfh\r\nkfgjdk', '20000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(150, 70, '2017-06-21', NULL, '0000-00-00', 1, 2, 2, 0, '', 'ch', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(151, 70, '2017-06-21', NULL, '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(152, 19, '2017-06-21', NULL, '0000-00-00', 1, 0, 0, 0, '', 'chife', 'new examination', 'in', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(153, 60, '2017-07-04', NULL, '0000-00-00', 1, 2, 2, 0, '', 'chif', 'e', 'new examinationin', '', '', 'new\r\nneww', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(154, 59, '2017-07-04', NULL, '2017-07-24', 2, 2, 2, 0, '', 'new chief', 'new chief', 'new examinationin', '', '', 'one\r\ntwo\r\nthree', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(155, 19, '2017-07-04', NULL, '2017-07-30', 2, 0, 0, 0, '', 'chif', 'e', 'ines', '', '', 'hhjgh\r\ngfbnh', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(156, 18, '2017-07-17', NULL, '2017-09-12', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(157, 71, '2017-07-09', NULL, '2017-08-23', 1, 0, 0, 0, '', 'chief complain', 'examination', 'investigation', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(158, 20, '2017-07-09', NULL, '2017-07-19', 2, 0, 0, 0, '', '', '', '', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(159, 72, '2017-07-09', NULL, '2017-07-25', 1, 0, 0, 0, '', 'ch', '', '', '', '', 'one', '25.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(160, 72, '2017-07-09', NULL, '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(161, 60, '2017-07-09', NULL, '2017-07-23', 2, 2, 2, 0, '', 'complain', 'ex1', 'some inv', 'notes', '', 'one\ntwo', '25000.000', 'special procedure 1', '2018-05-26', '', -1, 1, 1, '0000-00-00 00:00:00'),
(162, 59, '2017-07-09', NULL, '2017-07-24', 2, 2, 2, 0, '', 'chief complain', 'examination', 'investigation', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(166, 76, '2017-07-24', NULL, '0000-00-00', 1, 2, 2, 0, '', 'Chief', 'Exam', 'Inves', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(167, 60, '2017-07-10', NULL, '0000-00-00', 2, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(171, 71, '2017-07-11', NULL, '0000-00-00', 2, 0, 0, 0, '', 'chief complain', 'examination', 'in', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(173, 33, '2017-07-11', NULL, '0000-00-00', 1, 2, 2, 0, '', 'c', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(176, 78, '2017-07-17', NULL, '2017-08-06', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(180, 82, '2017-07-16', NULL, '0000-00-00', 2, 0, 0, 1, '', 'some chief complain', 'new examination', 'new', '', '', 'one\r\ntwo\r\nthree', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(181, 83, '2017-07-17', NULL, '2017-07-24', 1, 0, 0, 0, '', 'new', 'new examination', 'new', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(183, 85, '2017-07-25', NULL, '2017-08-04', 1, 0, 0, 0, '', 'chief complain', 'best examination', 'best chief complain', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(185, 20, '2017-07-16', NULL, '0000-00-00', 2, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(186, 19, '2017-07-16', NULL, '2017-07-31', 2, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(187, 21, '2017-07-16', NULL, '2017-07-30', 2, 0, 0, 0, '', 'chief complain', '', '', '', '', '', '20000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(188, 36, '2017-07-16', NULL, '2017-07-28', 1, 2, 2, 0, '', 'chief complain', 'new examination', 'i', '', '', 'one', '20000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(189, 59, '2017-07-16', NULL, '2017-08-08', 2, 2, 2, 0, '', '', '', '', '', '', 'tfyt gvg\r\ntwo\r\nthree', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(191, 82, '2017-07-17', NULL, '2017-08-06', 2, 0, 0, 1, '', 'new', 'e', 'in', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(192, 72, '2017-07-17', NULL, '0000-00-00', 1, 0, 0, 0, '', 'Chief', 'Examination', 'Invi', '', '', 'One\r\nTwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(193, 21, '2017-07-17', NULL, '2017-07-19', 2, 0, 0, 0, '', 'New', 'Examination', 'New', '', '', 'One\r\nTwo\r\nThree', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(194, 64, '2017-07-17', NULL, '2017-08-06', 2, 0, 0, 2, '', 'new chief', 'new', 'new examinationin', '', '', 'one\r\ntwo\r\nthree\r\nfour', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(197, 66, '2017-07-17', NULL, '2017-07-31', 2, 0, 0, 1, '', 'Chief', '', '', '', '', 'Tr', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(198, 78, '2017-07-18', NULL, '2017-07-22', 1, 2, 2, 0, '', 'chief complain', 'e', 'investigation', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(199, 72, '2017-07-18', NULL, '0000-00-00', 1, 0, 0, 0, '', 'chief complain', 'new chief', 'new examinationin', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(200, 83, '2017-07-18', NULL, '0000-00-00', 1, 0, 0, 0, '', 'ch', 'new chief', 'new examinationin', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(201, 46, '2017-07-18', NULL, '2017-07-29', 1, 2, 2, 0, '', 'ch', 'some Examination', 'some invistigation', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(202, 33, '2017-07-18', NULL, '0000-00-00', 1, 2, 2, 0, '', 'chief complain', 'examination', 'in', '', '', '', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(203, 23, '2017-07-18', NULL, '2017-08-05', 2, 2, 2, 0, '', 'chief complain', 'some Examination', 'some invistigation', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(204, 70, '2017-07-18', NULL, '2017-07-29', 1, 2, 2, 0, '', 'ch', 'some Examination', 'ines', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(205, 18, '2017-07-18', NULL, '2017-08-09', 1, 2, 2, 0, '', 'chief complain', 'e', 'i', '', '', '', '2500.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(206, 78, '2017-07-19', NULL, '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', '', '0.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(210, 87, '2017-07-22', NULL, '2017-07-31', 1, 0, 0, 0, '', 'chief complain', 'best examination', 'best chief complain', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(211, 89, '2017-07-22', NULL, '0000-00-00', 2, 2, 2, 3, '', 'new chief complain', 'new chief', 'best chief complain', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(212, 90, '2017-07-22', NULL, '2017-08-05', 1, 0, 0, 0, '', 'chief complain', 'best examination', 'best investigation2', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(213, 91, '2017-07-22', NULL, '2017-07-30', 2, 2, 2, 3, '', 'chief complain', 'best examination', 'some invistigation', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(214, 92, '2017-07-22', NULL, '2017-08-05', 2, 2, 2, 4, '', 'new', 'e', 'best chief complain', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(215, 93, '2017-07-22', NULL, '2017-08-07', 1, 2, 2, 0, '', 'ch', 'examination', 'ines', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(216, 94, '2017-07-22', NULL, '2017-08-08', 1, 2, 2, 0, '', 'new chief', 'new chief', 'new', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(217, 91, '2017-07-23', NULL, '0000-00-00', 2, 2, 2, 3, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(218, 83, '2017-07-23', NULL, '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(220, 20, '2017-07-23', NULL, '0000-00-00', 2, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(221, 92, '2017-07-23', NULL, '2017-07-25', 2, 2, 2, 4, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(223, 90, '2017-07-24', NULL, '2017-07-31', 1, 0, 0, 0, '', '', '', '', '', '', '500mg  for 10 days', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(225, 18, '2017-07-26', NULL, '0000-00-00', 1, 2, 2, 0, '', 'c', 'best examination', 'best investigation', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(226, 46, '2017-07-26', NULL, '2017-08-06', 1, 2, 2, 0, '', 'some chief complain', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(227, 21, '2017-07-26', NULL, '2017-08-05', 2, 0, 0, 0, '', 'c', 'best examination', 'best chief complain', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(228, 19, '2017-07-26', NULL, '2017-08-05', 2, 0, 0, 0, '', 'chief complain', 'best examination', 'investigation', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(229, 95, '2017-07-26', NULL, '2017-08-05', 2, 0, 0, 1, '', 'ch', 'best examination', 'best investigation', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(230, 96, '2017-07-29', NULL, '2017-08-31', 2, 0, 0, 0, '', 'chief complain', 'best examination', 'best investigation', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(238, 93, '2017-08-01', NULL, '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(239, 72, '2017-08-01', NULL, '2017-08-08', 1, 0, 0, 0, '', 'chief complain', 'best examination', 'best chief complain', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(240, 96, '2017-08-01', NULL, '2017-09-07', 2, 0, 0, 0, '', 'Chief complain', 'Examination', 'Investigation', '', '', 'One\r\nTwo\r\nThree\r\nFour', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(246, 83, '2017-08-02', NULL, '2017-08-05', 1, 0, 0, 0, '', 'ch', 'best examination', 'best chief complain', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(247, 91, '2017-08-12', NULL, '2017-09-04', 2, 2, 2, 3, '', 'chief complain', 'best examination', 'best chief complain', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(248, 95, '2017-08-12', NULL, '0000-00-00', 2, 0, 0, 1, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(249, 94, '2017-08-14', NULL, '0000-00-00', 1, 2, 2, 0, '', 'chief complain', 'exa', 'invistigation', '', '', '', '250000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(250, 96, '2017-09-13', NULL, '0000-00-00', 2, 0, 0, 0, '', '', 'best examination', 'best investigation', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(251, 94, '2017-09-13', NULL, '0000-00-00', 1, 2, 2, 0, '', 'chief complain', '', '', '', '', '', '2500.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(252, 93, '2017-10-26', NULL, '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(253, 93, '2017-10-27', NULL, '0000-00-00', 2, 1, 2, 2, '', '', '', 'dag', '', '', 'note', '2000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(254, 92, '2017-10-27', NULL, '0000-00-00', 2, 2, 2, 4, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(255, 96, '2017-11-02', NULL, '0000-00-00', 2, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(257, 18, '2017-11-25', NULL, '0000-00-00', 1, 2, 2, 0, '', 'chief complain', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(258, 87, '2017-12-16', NULL, '0000-00-00', 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(259, 78, '2017-12-16', NULL, '0000-00-00', 1, 2, 2, 0, '', NULL, NULL, 'inv1, inv2, inv3', 'note1\nnote2\nnote3', NULL, 'note t1\nnote t2\nnote t3', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(262, 96, '2017-12-18', NULL, '0000-00-00', 2, 0, 0, 0, '', '', '', '', '', '', '', '250000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(269, 91, '2018-02-02', NULL, '0000-00-00', 2, 2, 2, 3, '', NULL, NULL, NULL, '', NULL, '', '20000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(270, 92, '2018-02-02', NULL, '0000-00-00', 2, 2, 2, 4, '', NULL, NULL, NULL, '', NULL, '', '20000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(271, 96, '2018-02-02', NULL, '0000-00-00', 2, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(272, 89, '2018-02-02', NULL, '0000-00-00', 2, 2, 2, 3, '', NULL, NULL, 'new inv', '', NULL, '', '20000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(273, 87, '2018-02-05', NULL, '0000-00-00', 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '250000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(276, 91, '2018-02-06', NULL, '0000-00-00', 2, 2, 2, 3, '', '', '', '', '', '', '', '20000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(278, 91, '2018-02-07', NULL, '0000-00-00', 2, 2, 2, 3, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(280, 87, '2018-02-07', NULL, '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '20000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(281, 94, '2018-02-14', NULL, '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', '500mg 1x4\r\n200mg 2x5\r\nonly at night, one half at a time.', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(282, 96, '2018-02-14', NULL, '0000-00-00', 2, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(283, 93, '2018-03-06', NULL, '0000-00-00', 2, 1, 2, 2, '', '', '', '', '', '', '', '20000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(286, 23, '2018-03-28', NULL, '0000-00-00', 2, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(287, 33, '2018-03-31', NULL, '0000-00-00', 1, 2, 2, 0, '', 'complain', 'ex', 'inv', 'inv notes', 'some notes', 'some notes treats', '0.000', 'procedure test', '2018-10-14', 'procedure notes', -1, 2, 1, '0000-00-00 00:00:00'),
(288, 19, '2018-04-06', NULL, '0000-00-00', 2, 0, 0, 0, '', '', '120/8 mmhg,70 b/min,ch test,h test,abd test,others', '', '', '', '', '22000.000', '', '2018-04-06', '', -1, 2, 1, '0000-00-00 00:00:00'),
(289, 59, '2018-04-05', NULL, '2018-04-11', 2, 2, 2, 0, '', 'not important', 'ex1', 'someinvs, another inv, and more', 'we have notes for investigation 2', 'general notes', 'notes1\r\nnotes 2', '20000.000', 'special procedure 1', '2018-04-05', 'notes for procedures ...', -1, 2, 1, '0000-00-00 00:00:00'),
(290, 60, '2018-04-06', NULL, '0000-00-00', 2, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '2018-04-06', '', -1, 2, 1, '0000-00-00 00:00:00'),
(291, 70, '2018-04-08', NULL, '0000-00-00', 1, 2, 2, 0, '', '', 'PR: 70 b/min,Chest: chtest,Heart: hrtest,Other: othtest', '', '', '', '', '25000.000', 'other', '2018-04-01', '', -1, 2, 1, '0000-00-00 00:00:00'),
(292, 76, '2018-04-08', NULL, '0000-00-00', 1, 2, 2, 0, '', '', 'BP: 120/8 mmhg, PR: 70 b/min, Chest: chtest, Heart: Htest, Abdomin: abdtest, Other: others', '', '', '', '', '25000.000', '', '2018-04-08', '', -1, 2, 1, '0000-00-00 00:00:00'),
(293, 85, '2018-04-15', NULL, '0000-00-00', 1, 0, 0, 0, '', '', 'BP: 120/8 mmhg, PR: 70 b/min, Heart: H12, Other: other', '', '', '', '', '25000.000', '', '2018-04-15', '', -1, 2, 1, '0000-00-00 00:00:00'),
(294, 97, '2018-04-15', NULL, '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '2018-04-15', '', -1, 1, 1, '0000-00-00 00:00:00'),
(295, 18, '2018-04-15', NULL, '0000-00-00', 2, 2, 2, 3, '', '', '', 'new Investigation', '', '', '', '0.000', '', '2018-04-15', '', -1, 2, 1, '0000-00-00 00:00:00'),
(296, 20, '2018-04-15', NULL, '0000-00-00', 2, 0, 0, 0, '', '', '', 'best chief complain      new', '', '', '', '20000.000', '', '2018-04-15', '', -1, 2, 1, '0000-00-00 00:00:00'),
(297, 87, '2018-04-16', NULL, '0000-00-00', 1, 0, 0, 0, '', '', 'BP: 25/45 mmhg, PR: 22/4 b/min', '', '', '', '', '25000.000', '', '2018-04-16', '', -1, 2, 1, '0000-00-00 00:00:00'),
(298, 71, '2018-04-16', NULL, '0000-00-00', 2, 0, 0, 0, '', '', 'Chest: helo helo helo helo helo helo helo helo helo helo, Heart: helo helo helo helo helo helo helo helo helo helo, Abdomin: helo helo helo helo helo helo helo helo helo helo', '', '', '', '', '25000.000', '', '2018-04-16', '', -1, 2, 1, '0000-00-00 00:00:00'),
(299, 21, '2018-04-16', NULL, '0000-00-00', 2, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(301, 94, '2018-05-03', NULL, '0000-00-00', 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '20000.000', '', '0000-00-00', '', -1, 2, 1, '2018-05-03 03:16:21'),
(302, 98, '2018-05-05', NULL, '0000-00-00', 1, 0, 0, 0, '', 'chief complain one', 'BP: 21/5 mmhg, PR: 22/5 b/min, Chest: mjhg, Heart: nhg, Abdomin: jh', 'best chief complain,twodndaskjf', 'one jswnef jjjjjjj\r\ntwoa  jndfnsf jsf', '', 'pme\r\ntwo', '25000.000', 'proce one, proce two', '2018-05-05', 'one\r\ntwo', -1, 2, 1, '2018-05-04 21:47:35'),
(303, 19, '2018-05-05', NULL, '2018-05-27', 2, 0, 0, 0, '', 'c', 'BP: 28/4 mmhg, PR: 72/2 b/min, Chest: mmm, Heart: heat, Abdomin: adb', 'best chief complain,twodndaskjf', 'one\r\ntwo', '', 'oneeeeeeeeeee\r\ntwoooooooooo', '25000.000', 'procedure onr na na twi,one', '2018-05-05', 'hello', -1, 2, 1, '2018-05-04 21:58:43'),
(304, 18, '2018-05-05', NULL, '0000-00-00', 2, 2, 2, 3, '', 'test', '', '', '', '', '', '0.000', '', '2018-05-05', '', -1, 2, 1, '2018-05-05 06:46:45'),
(306, 95, '2018-05-05', NULL, '2018-05-30', 2, 0, 0, 1, '', '', 'BP: 25/5 mmhg, PR: 10/4 b/min, Chest: hello, Heart: heart, Abdomin: new abdo', 'best chief complain,twodndaskjf,new', 'good one', '', 'one\r\ntwo\r\nthree', '0.000', '', '2018-05-05', '', -1, 2, 1, '2018-05-05 16:36:17'),
(307, 94, '2018-05-05', NULL, '2018-05-28', 1, 2, 2, 0, '', 'first time this person in here', 'BP: 1/1 mmhg, PR: 4/5 b/min, Chest: new chest, Heart: new heart, Abdomin: new abdo', 'best chief complain,new,one', 'hello world', '', 'one\r\ntwo', '25000.000', '', '2018-05-05', '', -1, 2, 1, '2018-05-05 16:39:37'),
(308, 36, '2018-05-06', NULL, '0000-00-00', 1, 2, 2, 0, '', 's', '', '', '', '', '', '25000.000', 'sclerotherapy', '2018-05-06', '', -1, 2, 1, '2018-05-06 05:21:38'),
(309, 90, '2018-05-06', NULL, '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '20000.000', 'someproc', '2018-05-06', '', -1, 1, 1, '2018-05-06 05:32:46'),
(317, 101, '2018-05-23', '14:20:00', '2018-06-18', 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', 'procedure test', '2018-06-19', '', -1, 2, 1, '2018-05-23 08:54:01'),
(318, 102, '2018-05-23', '14:50:00', '0000-00-00', 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '20000.000', '', '0000-00-00', '', -1, 2, 1, '2018-05-23 14:32:28'),
(319, 98, '2018-05-23', '14:20:00', '0000-00-00', 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '30000.000', '', '0000-00-00', '', -1, 2, 1, '2018-05-23 15:02:54'),
(320, 96, '2018-05-23', '19:10:00', '0000-00-00', 2, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-05-23 15:05:58'),
(321, 94, '2018-05-23', '19:10:00', '0000-00-00', 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-05-23 15:07:02'),
(322, 82, '2018-05-27', '15:25:00', '2018-05-27', 2, 0, 0, 1, '', '', '', '', '', '', '', '25000.000', '', '2018-05-27', '', -1, 2, 1, '2018-05-27 08:04:38'),
(323, 60, '2018-05-27', '07:00:00', '2018-05-27', 2, 2, 2, 0, '', '', '', '', '', '', '', '0.000', '', '2018-05-27', '', -1, 2, 1, '2018-05-27 09:28:57'),
(328, 60, '2018-05-27', '15:35:00', '2018-05-29', 2, 2, 2, 0, 'Her health is going to be good', 'some info', 'BP: 05/02 mmhg, PR: 05/02 b/min, Chest: 05/02', 'invistigation,kaedls', 'note new', '', 'note new treatment', '20000.000', 'new procedure', '2018-05-27', 'note pro', -1, 2, 1, '2018-05-27 09:50:15'),
(329, 97, '2018-05-27', '00:00:00', '2018-05-29', 1, 0, 0, 0, '', 'some info', 'BP: 05/02 mmhg, PR: 45/8 b/min', 'invistigation,cjccj', 'note', '', 'note', '2000.000', 'procedure test', '2018-05-27', 'note', -1, 2, 1, '2018-05-27 10:29:52'),
(330, 95, '2018-05-27', '00:00:00', '2018-05-30', 2, 0, 0, 1, '', '', '', '', '', '', '', '25000.000', '', '2018-05-27', '', -1, 2, 1, '2018-05-27 12:13:02'),
(334, 103, '2018-05-27', '09:25:00', '2018-05-28', 1, 2, 2, 0, '', '', 'BP: 120/8 mmhg, PR: 70 b/min', 'inv1', 'notes', 'other notes', 'some notes', '2500.000', 'procedures', '2018-05-27', 'some notes about procedure', -1, 2, 1, '2018-05-27 14:33:48'),
(335, 103, '2018-05-28', '15:15:00', '0000-00-00', 2, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '20000.000', 'new procedure', '2018-06-16', 'notes pro', -1, 1, 1, '2018-05-27 14:36:26'),
(340, 92, '2018-06-06', '07:20:00', '0000-00-00', 2, 2, 2, 4, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-06-06 08:43:45'),
(342, 60, '2018-06-07', '07:15:00', '0000-00-00', 2, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-06-07 03:23:53'),
(345, 23, '2018-06-10', '07:20:00', '0000-00-00', 2, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', 'proc test2, special procedure 1', '2018-06-10', 'notes for proc', -1, 2, 1, '2018-06-10 08:57:43'),
(347, 23, '2018-06-16', '07:10:00', '0000-00-00', 2, 1, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-06-16 08:55:30'),
(350, 103, '2018-06-18', '00:00:00', '0000-00-00', 2, 2, 2, 0, '', '', '', 'inv1', '', '', 'notes for treatment', '25000.000', 'new procedure', '2018-06-18', '', -1, 2, 1, '2018-06-18 11:51:19'),
(351, 98, '2018-06-18', '00:00:00', '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', 'new procedure', '2018-06-18', 'with notes', -1, 2, 1, '2018-06-18 12:23:27'),
(352, 104, '2018-06-18', '07:15:00', '2018-06-20', 1, 0, 0, 0, '', '', '', '', '', '', '', '0.000', 'procedure test', '2018-06-18', 'a note here', -1, 1, 1, '2018-06-18 13:15:32'),
(356, 104, '2018-06-20', '07:15:00', '1900-12-27', 1, 0, 0, 0, '', '', '', '', '', '', '', '0.000', 'new procedure', '2018-06-20', '', -1, 1, 1, '2018-06-18 14:35:13'),
(357, 102, '2018-06-18', '00:00:00', '2018-06-27', 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-06-18 14:38:02'),
(358, 101, '2018-06-19', '07:15:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-06-19 12:33:01'),
(359, 101, '2018-06-20', '00:00:00', '2018-06-21', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', 'procedure test', '2018-06-20', '', -1, 2, 1, '2018-06-20 14:54:03'),
(360, 97, '2018-06-20', '00:00:00', '2018-06-22', 1, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', 'new procedure', '2018-06-20', '', -1, 2, 1, '2018-06-20 14:54:12'),
(364, 104, '2018-06-20', '07:15:00', '2018-06-28', 1, 0, 0, 0, '', '', '', '', '', '', '', '0.000', 'SCLEROTHERAPY', '2018-06-20', '', -1, 1, 1, '2018-06-20 15:32:23'),
(369, 98, '2018-06-21', '06:14:31', '2018-06-28', 1, 0, 0, 0, '', '', '', 'inv of 21', 'inv note for 21', '', '', '25000.000', 'new procedure', '2018-06-21', '', -1, 2, 1, '2018-06-21 12:35:29'),
(371, 91, '2018-06-21', '07:15:00', '0000-00-00', 2, 2, 1, 2, '', '', '', '', '', '', '', '25000.000', 'SCLEROTHERAPY', '2018-06-21', '', -1, 2, 1, '2018-06-21 13:32:09'),
(372, 87, '2018-06-21', '07:15:00', '2018-06-22', 1, 0, 0, 0, '', '', '', '', '', '', '', '0.000', 'SCLEROTHERAPY', '2018-06-21', '', -1, 2, 1, '2018-06-21 13:34:47'),
(374, 91, '2018-06-23', '18:30:00', '0000-00-00', 2, 2, 1, 2, '', '', '', '', '', '', '', '25000.000', 'SCLEROTHERAPY', '2018-06-23', '', -1, 2, 1, '2018-06-21 14:48:28'),
(375, 104, '2018-06-21', '07:20:00', '2018-06-23', 2, 0, 0, 1, '', '', '', '', '', '', '', '25000.000', 'procedure test', '2018-06-21', '', -1, 2, 1, '2018-06-21 14:58:54'),
(376, 87, '2018-06-22', '07:15:00', '2018-06-24', 1, 0, 0, 0, '', '', '', '', '', '', 'some notes', '25000.000', 'special procedure 1', '2018-06-22', '', -1, 2, 1, '2018-06-21 15:00:48'),
(378, 104, '2018-06-22', '13:15:00', '2018-06-25', 2, 0, 0, 2, '', '', '', '', '', '', 'some notes', '0.000', 'SCLEROTHERAPY', '2018-06-22', '', -1, 2, 1, '2018-06-22 10:52:00'),
(380, 104, '2018-06-25', '07:10:00', '0000-00-00', 2, 0, 0, 2, '', 'some chief complain', 'BP: 20/5 mmhg, PR: 35/54 b/min, Chest: 20/5, Heart: new abdo, Abdomin: new abdo', 'best chief complain, another', 'one\r\ntwo', '', 'one \r\ntwo', '25000.000', 'new pro', '2018-06-30', 'not pro', -1, 2, 1, '2018-06-22 11:48:37'),
(381, 23, '2018-06-23', '07:15:00', NULL, 2, 1, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-06-22 12:03:46'),
(382, 105, '2018-06-23', '08:05:00', '2018-06-30', 1, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', 'procedure test', '2018-07-01', '', -1, 2, 1, '2018-06-23 02:44:15'),
(383, 105, '2018-06-30', '07:15:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-06-23 03:28:57'),
(384, 94, '2018-06-25', '07:30:00', '0000-00-00', 1, 2, 2, 0, '', 'new one', 'BP: 20/5 mmhg, PR: 75/200 b/min, Chest: 20/5', 'best chief complain', '', '', 'one \r\ntwo', '0.000', 'procedure onr na na twi,one', '2018-06-25', 'pro note', -1, 2, 1, '2018-06-23 07:43:29'),
(385, 102, '2018-07-01', '08:10:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', 'special procedure 1', '2018-07-04', '', -1, 2, 1, '2018-06-23 07:47:40'),
(386, 60, '2018-07-04', '00:00:00', NULL, 0, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 1, -1, '2018-06-23 07:48:21'),
(387, 59, '2018-06-23', '21:35:00', '2018-07-05', 2, 2, 2, 0, '', 'chief complain one', 'BP: 20/5 mmhg, PR: 37/70 b/min, Chest: 20/5, Abdomin: new abdo', 'best chief complain,new,one', 'invest\r\ntwo \r\nthree', '', 'note one \r\nnote two', '25000.000', 'SCLEROTHERAPY', '2018-06-01', '', -1, 2, 1, '2018-06-23 10:22:10'),
(388, 59, '2018-07-05', '07:05:00', NULL, 2, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 1, -1, '2018-06-23 10:24:40'),
(390, 107, '2018-06-23', '07:05:00', '0000-00-00', 2, 0, 0, 5, 'Hello world i am a singer', 'chief complain', 'BP: 20/5 mmhg, PR: 20/5 b/min, Chest: 20/5', 'best chief complain, two', '', '', '', '0.000', 'procedure onr na na twi,one, hi pro', '2018-07-04', 'awa wamlekrd view bbta -1 lakati cancel krdnawa', -1, 2, -1, '2018-06-23 15:54:34'),
(391, 104, '2018-06-28', '07:15:00', NULL, 2, 0, 0, 2, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, -1, '2018-06-28 02:11:38'),
(392, 98, '2018-06-28', '07:15:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, -1, '2018-06-28 02:30:39'),
(393, 98, '2018-06-28', '00:00:00', NULL, 1, 0, 0, 0, '', 'chief complain', 'ex1', 'inv', 'some notes inv', NULL, 'new treats', '25.000', 'procedure test, special procedure 1', '2018-10-15', 'some proc notes', -1, 2, 1, '2018-06-28 02:33:07'),
(394, 110, '2018-06-29', '15:10:00', '0000-00-00', 1, 2, 2, 0, '', 'new chief complain', 'BP: 05/02 mmhg, PR: 05/02 b/min, Chest: 15/5', 'i, new onee', 'note inve', '', 'one\r\ntwo', '25000.000', 'special procedure 1', '2018-06-29', 'note', -1, 2, 1, '2018-06-29 11:46:53'),
(395, 102, '2018-07-01', '07:15:00', NULL, 0, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, -1, '2018-07-01 14:28:19'),
(396, 102, '2018-07-01', '00:00:00', NULL, 0, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, -1, '2018-07-01 14:29:22'),
(397, 102, '2018-07-01', '00:00:00', NULL, 0, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, -1, '2018-07-01 14:37:55'),
(398, 101, '2018-07-01', '00:00:00', NULL, 0, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, -1, '2018-07-01 14:39:06'),
(399, 101, '2018-07-01', '00:00:00', NULL, 0, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, -1, '2018-07-01 14:42:19'),
(400, 105, '2018-07-01', '00:00:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, -1, '2018-07-01 14:45:25'),
(401, 105, '2018-07-01', '00:00:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '2.000', '', '0000-00-00', '', -1, 2, 1, '2018-07-01 14:45:46'),
(402, 107, '2018-07-01', '07:15:00', NULL, 2, 0, 0, 5, '', NULL, NULL, NULL, '', NULL, '', '25.000', '', '0000-00-00', '', -1, 2, 1, '2018-07-01 14:57:39'),
(403, 104, '2018-07-11', '07:05:00', NULL, 0, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 1, -1, '2018-07-05 03:22:39'),
(404, 112, '2018-07-08', '08:05:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, -1, '2018-07-08 13:22:47'),
(405, 113, '2018-07-09', '07:10:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-07-09 13:39:43'),
(406, 101, '2018-07-10', '07:15:00', '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', '', '20000.000', '', '2018-07-10', '', -1, 2, 1, '2018-07-10 06:00:40'),
(407, 107, '2018-07-22', '08:35:00', NULL, 2, 0, 0, 5, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, -1, '2018-07-22 13:42:44'),
(408, 112, '2018-07-22', '07:05:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, -1, '2018-07-22 13:42:56'),
(409, 110, '2018-09-11', '07:10:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-09-11 09:13:02'),
(410, 110, '2018-09-12', '07:10:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '22222.000', '', '0000-00-00', '', -1, 2, 1, '2018-09-12 02:34:22'),
(411, 18, '2018-09-20', '07:15:00', NULL, 2, 2, 2, 3, '', NULL, NULL, NULL, '', NULL, '', '222222.000', '', '0000-00-00', '', -1, 2, 1, '2018-09-20 09:05:21'),
(412, 60, '2018-09-30', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-09-30 04:54:53'),
(413, 110, '2018-10-14', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-10-14 10:47:23'),
(414, 114, '2018-11-15', '13:45:00', '0000-00-00', 2, 2, 1, 4, '', '', '', '', '', '', '', '25000.000', '', '2018-11-15', '', -1, 2, 1, '2018-11-15 09:19:06'),
(415, 115, '2018-11-15', '21:40:00', '0000-00-00', 2, 0, 0, 3, '', '', '', '', '', '', 'use them properly', '25000.000', '', '2018-11-15', '', -1, 2, 1, '2018-11-15 09:19:55'),
(416, 114, '2018-11-15', '13:15:00', NULL, 2, 2, 1, 4, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, -1, '2018-11-15 09:22:13'),
(417, 115, '2018-11-15', '00:00:00', NULL, 2, 0, 0, 3, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, -1, '2018-11-15 09:24:45'),
(418, 112, '2018-11-20', '07:15:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, -1, '2018-11-20 09:29:38'),
(419, 110, '2018-11-20', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '22222.000', '', '0000-00-00', '', -1, 2, 1, '2018-11-20 10:07:23'),
(420, 113, '2018-11-22', '00:00:00', '2018-11-29', 1, 0, 0, 0, '', 'complain', 'BP: 25 mmhg, PR: 52/56 b/min, Chest: clear, Heart: 70, Abdomin: clear', 'inv1, inv2', 'some notes for both', '', '', '25000.000', '', '2018-11-22', '', -1, 2, 1, '2018-11-22 03:10:31'),
(421, 113, '2018-11-29', '07:15:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 1, 1, '2018-11-22 03:12:31'),
(422, 87, '2018-11-22', '00:00:00', '2018-11-29', 1, 0, 0, 0, '', 'complain', 'BP: 25 mmhg, PR: 25 b/min, Heart: 70', '', '', '', 'treats usage\r\ntreats usage2', '111111.000', '', '2018-11-22', '', -1, 2, 1, '2018-11-22 03:44:51'),
(423, 76, '2018-11-22', '00:00:00', '0000-00-00', 2, 1, 2, 1, '', '', '', '', '', '', 'notes', '22222.000', '', '2018-11-22', '', -1, 2, 1, '2018-11-22 03:44:57'),
(424, 71, '2018-11-22', '00:00:00', '2018-11-29', 2, 0, 0, 0, '', 'complain', 'BP: 25 mmhg, PR: 25 b/min, Heart: 70', '', '', '', 'some notes1\r\nsome notes2', '222222.000', '', '2018-11-22', '', -1, 2, 1, '2018-11-22 03:45:05'),
(425, 18, '2018-11-22', '00:00:00', '2018-11-29', 2, 2, 2, 0, '', 'complain', 'BP: 25 mmhg, Chest: 25', '', '', '', 'some treats used more\r\nanother treatment', '22222.000', 'special procedure 1', '2018-11-22', '', -1, 2, 1, '2018-11-22 03:45:15'),
(426, 87, '2018-11-29', '00:00:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 1, 1, '2018-11-22 03:47:26'),
(427, 71, '2018-11-29', '00:00:00', NULL, 2, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 1, 1, '2018-11-22 03:52:48'),
(428, 18, '2018-11-29', '00:00:00', NULL, 2, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 1, 1, '2018-11-22 04:06:28'),
(429, 115, '2018-11-22', '00:00:00', NULL, 2, 0, 0, 3, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-11-22 04:29:54'),
(430, 101, '2018-11-22', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2018-11-22 05:25:00'),
(431, 89, '2018-11-22', '00:00:00', '0000-00-00', 2, 2, 2, 3, '', '', '', '', '', '', '', '30.000', '', '2018-11-22', '', -1, 1, 1, '2018-11-22 07:23:49'),
(432, 87, '2018-11-22', '00:00:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2018-11-22 08:05:17'),
(433, 104, '2018-11-22', '00:00:00', '0000-00-00', 1, 2, 2, 0, '', 'the testing time', 'BP: 25 mmhg, PR: 25 b/min, Heart: 70', 'inv1, inv2', '', '', 'note', '25000.000', '', '2018-11-22', '', -1, 2, 1, '2018-11-22 08:16:10'),
(434, 59, '2018-11-22', '00:00:00', '0000-00-00', 2, 2, 2, 0, '', 'complain', 'BP: 25 mmhg', '', '', '', 'note', '25000.000', '', '2018-11-22', '', -1, 2, 1, '2018-11-22 08:27:26'),
(435, 23, '2018-11-22', '00:00:00', '0000-00-00', 2, 1, 2, 0, '', '', '', '', '', '', '', '0.000', '', '2018-11-22', '', -1, 2, 1, '2018-11-22 08:29:28'),
(436, 21, '2018-11-22', '00:00:00', '0000-00-00', 2, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '2018-11-22', '', -1, 2, 1, '2018-11-22 08:29:33'),
(437, 19, '2018-11-22', '00:00:00', '0000-00-00', 2, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '2018-11-22', '', -1, 2, 1, '2018-11-22 08:29:43'),
(438, 112, '2018-12-02', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, 1, '2018-12-02 07:20:21'),
(439, 112, '2019-01-27', '07:10:00', '0000-00-00', 1, 2, 2, 0, '', 'ch', 'BP: 20/5 mmhg, PR: 20/5 b/min, Chest: 35/54, Heart: 35/54, Abdomin: 75/200, Other: 20/5', 'best chief complain', 'ansans a sjnjb', '', '', '25000.000', '', '2019-01-27', '', -1, 2, 1, '2019-01-27 09:23:17'),
(440, 59, '2019-02-23', '07:10:00', NULL, 0, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 1, -1, '2019-02-23 11:46:49'),
(441, 46, '2019-02-24', '07:20:00', '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', 'using', '25000.000', '', '2019-02-24', '', -1, 2, 1, '2019-02-24 14:26:41'),
(442, 72, '2019-02-24', '07:10:00', '0000-00-00', 1, 0, 0, 0, '', '', 'BP: 70 mmhg, PR: 20 b/min', 'inv1, inv2', 'some notes1\r\nsome notes 2', '', '', '25000.000', '', '2019-02-24', '', -1, 2, 1, '2019-02-24 14:26:51'),
(443, 83, '2019-02-24', '10:35:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2019-02-24 14:27:00'),
(444, 92, '2019-02-24', '07:20:00', NULL, 2, 2, 2, 4, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2019-02-24 14:27:09'),
(445, 95, '2019-02-24', '07:15:00', NULL, 2, 0, 0, 1, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', 1, 2, -1, '2019-02-24 14:27:19'),
(446, 112, '2019-07-28', '16:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2019-07-28 12:31:43'),
(447, 115, '2019-07-28', '17:35:00', '0000-00-00', 2, 0, 0, 3, '', 'chief complain', 'BP: 20/5 mmhg, PR: 35/54 b/min, Chest: 20/5, Heart: 20/5, Abdomin: 35/54, Other: 75/200', 'best chief complain      new', 'Test Investigation Note', '', 'Note for first treatment\r\nSecond', '25000.000', 'special procedure 1', '2019-07-08', 'Pro Notes', -1, 2, 1, '2019-07-28 12:32:01'),
(448, 60, '2019-07-28', '17:15:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2019-07-28 12:32:18'),
(449, 36, '2019-07-30', '18:10:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2019-07-28 12:32:34'),
(450, 19, '2019-07-28', '21:15:00', NULL, 2, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2019-07-28 12:32:53'),
(451, 104, '2019-07-29', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2019-07-29 13:52:10'),
(452, 110, '2019-08-20', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2019-08-20 02:04:50'),
(453, 114, '2019-11-05', '00:00:00', NULL, 2, 2, 1, 4, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', 2, 2, 1, '2019-11-05 01:58:43'),
(454, 115, '2020-01-28', '07:15:00', NULL, 2, 0, 0, 3, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2020-01-28 06:47:27'),
(455, 105, '2020-01-28', '07:20:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2020-01-28 07:39:31'),
(456, 104, '2020-01-29', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2020-01-28 07:46:39'),
(457, 103, '2020-02-05', '07:15:00', NULL, 2, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2020-01-28 07:57:03');
INSERT INTO `visit` (`v_id`, `p_id_f`, `visit_date`, `visit_time`, `followup_date`, `marital_status`, `pregnant`, `breast_feed`, `no_of_child`, `other_info`, `chief_comp_n_dur`, `examination`, `investigation`, `investigation_note`, `note`, `treatment_note`, `fee`, `procedure`, `procedure_date`, `procedure_note`, `in_queue`, `u_id_f`, `view`, `update_record`) VALUES
(458, 110, '2020-02-24', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2020-02-17 08:24:45'),
(459, 113, '2020-03-08', '00:00:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2020-02-17 08:25:06'),
(460, 21, '2020-08-31', '00:00:00', NULL, 2, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '20000.000', '', '0000-00-00', '', 1, 2, -1, '2020-08-31 06:11:37'),
(461, 36, '2020-08-31', '00:00:00', '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', 'notes2\r\nnotes3', '20000.000', '', '2020-08-31', '', -1, 2, 1, '2020-08-31 06:11:44'),
(462, 59, '2020-08-31', '00:00:00', NULL, 0, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '20000.000', '', '0000-00-00', '', 1, 2, -1, '2020-08-31 06:11:52'),
(463, 60, '2020-08-31', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '20000.000', '', '0000-00-00', '', 1, 2, -1, '2020-08-31 06:12:01'),
(464, 19, '2020-08-31', '00:00:00', NULL, 2, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '20000.000', '', '0000-00-00', '', 1, 2, -1, '2020-08-31 06:12:08'),
(465, 76, '2020-08-31', '00:00:00', NULL, 2, 1, 2, 1, '', NULL, NULL, NULL, '', NULL, '', '20000.000', '', '0000-00-00', '', 1, 2, -1, '2020-08-31 06:12:15'),
(466, 116, '2020-12-24', '13:30:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2020-12-10 06:15:02'),
(467, 115, '2020-12-10', '00:00:00', NULL, 2, 0, 0, 3, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2020-12-10 06:21:15'),
(468, 79, '2020-12-11', '00:00:00', NULL, 1, 2, 2, 0, 'I WROTE NOTE IN SEC', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, -1, '2020-12-11 05:48:27'),
(469, 113, '2020-12-11', '00:00:00', NULL, 1, 0, 0, 0, 'some information\r\npurpose of visit: is to check himself\r\noth', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, -1, '2020-12-11 06:26:58'),
(470, 112, '2020-12-11', '00:00:00', NULL, 1, 2, 2, 0, 'info1: this is infor1\r\npurpose of the visit: I can do whatever I like\r\nthrid info: this is  the third information', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, -1, '2020-12-11 06:38:09'),
(471, 105, '2020-12-11', '00:00:00', '0000-00-00', 1, 0, 0, 0, 'information1: this is the first info\r\ninformation2: this is the second note\r\npurpose of visit: this is the purpose of the visit today', 'asx', 'BP: 123 mmhg', '', '', 'testing data', '', '25000.000', '', '2020-12-11', '', -1, 2, 1, '2020-12-11 06:53:41'),
(472, 103, '2020-12-11', '00:00:00', NULL, 2, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, -1, '2020-12-11 10:01:31'),
(473, 78, '2020-12-12', '00:00:00', '0000-00-00', 1, 2, 2, 0, 'info1: here is info one\r\ninfo2: here is the info two\r\ninfo3: here is the info three', '', '', '', '', '', 'nj\r\nik', '25000.000', '', '2020-12-12', '', -1, 1, 1, '2020-12-12 04:51:45'),
(474, 110, '2020-12-13', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2020-12-13 04:28:22'),
(475, 18, '2021-03-16', '00:00:00', NULL, 2, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, 1, '2020-12-13 04:28:58'),
(476, 105, '2020-12-12', '20:55:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2020-12-14 02:24:00'),
(477, 105, '2020-12-20', '00:00:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2020-12-14 02:29:33'),
(478, 112, '2020-12-14', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, -1, '2020-12-14 02:31:03'),
(479, 112, '2020-12-14', '00:00:00', NULL, 1, 2, 2, 0, '123\r\n1234\r\n34534', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2020-12-14 02:34:09'),
(480, 103, '2020-12-14', '00:00:00', NULL, 2, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '18000.000', '', '0000-00-00', '', 1, 2, -1, '2020-12-14 02:39:14'),
(481, 104, '2020-12-12', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2020-12-14 03:13:31'),
(482, 115, '2020-12-14', '00:00:00', NULL, 2, 0, 0, 3, 'pov:\r\n1. reason\r\n2. test2\r\n3. test3\r\n\r\nmore info:\r\nabcd', NULL, NULL, NULL, '', NULL, '', '20000.000', '', '0000-00-00', '', -1, 2, -1, '2020-12-14 03:19:38'),
(483, 89, '2020-12-14', '18:15:00', NULL, 2, 2, 2, 3, 'P.O.V:\r\n1- purpose 1\r\n2- purpose 2\r\nextra notes:\r\nthis patient has extra notes', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2020-12-14 03:43:09'),
(484, 115, '2021-01-10', '00:00:00', NULL, 2, 0, 0, 3, 'Jshd', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2021-01-09 09:09:34'),
(485, 115, '2021-03-01', '00:00:00', '0000-00-00', 2, 0, 0, 3, '', '', '', '', '', '', '', '25000.000', '', '2021-03-01', '', -1, 2, 1, '2021-03-01 08:13:51'),
(486, 112, '2021-03-01', '00:00:00', NULL, 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2021-03-01 08:13:57'),
(487, 105, '2021-03-04', '00:00:00', '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '2021-03-04', '', -1, 2, 1, '2021-03-04 07:51:17'),
(488, 105, '2021-03-04', '00:00:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '2021-03-04 07:56:25'),
(489, 113, '2021-03-04', '00:00:00', NULL, 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, -1, '2021-03-04 09:14:24'),
(490, 95, '2021-03-10', '00:00:00', NULL, 2, 0, 0, 1, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', 1, 2, 1, '2021-03-04 09:14:32'),
(491, 76, '2021-03-09', '00:00:00', '0000-00-00', 2, 1, 2, 1, '', '', '', '', '', '', '', '25000.000', '', '2021-03-09', '', -1, 2, 1, '2021-03-09 02:21:16'),
(492, 72, '2021-03-09', '00:00:00', '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '2021-03-09', '', -1, 2, 1, '2021-03-09 02:21:23'),
(493, 64, '2021-03-09', '00:00:00', '0000-00-00', 2, 0, 0, 2, '', '', '', '', '', '', '', '25000.000', '', '2021-03-09', '', -1, 2, 1, '2021-03-09 02:21:30'),
(494, 110, '2021-03-09', '00:00:00', '0000-00-00', 1, 2, 2, 0, '', '', '', 'inv', 'inv no', '', 'sak', '2000.000', '', '2021-03-09', '', -1, 2, 1, '2021-03-09 13:55:58'),
(495, 117, '2021-03-09', '18:10:00', '0000-00-00', 1, 0, 0, 0, '', '', 'BP: 123 mmhg, PR: 1 b/min, Chest: 3, Heart: 9', 'inv1, inv2', 'inv1 note\r\ninv2 note', '', 'note treat 1\r\nnote treat 2', '20000.000', '', '2021-03-09', '', -1, 2, 1, '2021-03-09 13:56:54');

-- --------------------------------------------------------

--
-- Table structure for table `visit_attr`
--

CREATE TABLE `visit_attr` (
  `attr_id` int(11) UNSIGNED NOT NULL,
  `v_id_f` int(11) UNSIGNED NOT NULL,
  `attr` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `attr_order` tinyint(1) NOT NULL COMMENT 'to know which one is attr1 and which one is attr2 ...',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '-1:delete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `visit_attr`
--

INSERT INTO `visit_attr` (`attr_id`, `v_id_f`, `attr`, `attr_order`, `view`) VALUES
(1, 280, 'Cardiac', 1, 1),
(2, 280, 'Adult', 2, 1),
(3, 280, 'CABG', 3, 1),
(16, 289, 'Thoracic', 1, 1),
(17, 289, 'Decortication', 2, 1),
(20, 294, 'Thoracic', 1, 1),
(21, 294, 'Hydatid. Lung', 2, 1),
(30, 297, 'Vascular', 1, 1),
(31, 297, 'Peripheral Vascular', 2, 1),
(34, 298, 'Vascular', 1, 1),
(35, 298, 'Arterial Diseases', 2, 1),
(36, 299, 'Vascular', 1, 1),
(37, 299, 'AV-Fistula Creation', 2, 1),
(38, 282, 'Cardiac', 1, 1),
(39, 282, 'Adult', 2, 1),
(40, 282, 'CABG', 3, 1),
(50, 303, 'Cardiac', 1, 1),
(51, 303, 'Pediatric', 2, 1),
(52, 303, 'ASD', 3, 1),
(59, 301, 'Vascular', 1, 1),
(60, 301, 'Aneurysm', 2, 1),
(61, 249, 'Thoracic', 1, 1),
(62, 249, 'Lung Rejection Surgery', 2, 1),
(63, 226, 'Vascular', 1, 1),
(64, 226, 'Peripheral Vascular', 2, 1),
(65, 78, 'Vascular', 1, 1),
(66, 78, 'Arterial Diseases', 2, 1),
(67, 296, 'Thoracic', 1, 1),
(68, 296, 'Lung Rejection Surgery', 2, 1),
(69, 223, 'Cardiac', 1, 1),
(70, 223, 'Pediatric', 2, 1),
(71, 223, 'VSD', 3, 1),
(72, 302, 'Thoracic', 1, 1),
(73, 302, 'Lung Rejection Surgery', 2, 1),
(78, 307, 'Vascular', 1, 1),
(79, 307, 'Arterial Diseases', 2, 1),
(80, 306, 'Cardiac', 1, 1),
(81, 306, 'Adult', 2, 1),
(82, 306, 'CABG', 3, 1),
(83, 317, 'Cardiac', 1, 1),
(84, 317, 'Pediatric', 2, 1),
(85, 317, 'ASD', 3, 1),
(98, 322, 'Vascular', 1, 1),
(99, 322, 'Peripheral Vascular', 2, 1),
(100, 329, 'Thoracic', 1, 1),
(101, 329, 'Lung Rejection Surgery', 2, 1),
(106, 328, 'Thoracic', 1, 1),
(107, 328, 'Lung Rejection Surgery', 2, 1),
(120, 334, 'Vascular', 1, 1),
(121, 334, 'Venous Diseases', 2, 1),
(122, 334, 'DVT', 3, 1),
(123, 161, 'Cardiac', 1, 1),
(124, 161, 'Pediatric', 2, 1),
(125, 161, 'TOF', 3, 1),
(126, 345, 'Vascular', 1, 1),
(127, 345, 'Arterial Diseases', 2, 1),
(128, 52, 'Cardiac', 1, 1),
(129, 52, 'Adult', 2, 1),
(130, 52, 'Valve', 3, 1),
(131, 350, 'Cardiac', 1, 1),
(132, 350, 'Adult', 2, 1),
(133, 350, 'CABG', 3, 1),
(134, 358, 'Vascular', 1, 1),
(135, 358, 'Arterial Diseases', 2, 1),
(136, 359, 'Thoracic', 1, 1),
(137, 359, 'Lung Rejection Surgery', 2, 1),
(138, 378, 'Thoracic', 1, 1),
(139, 378, 'Lung Rejection Surgery', 2, 1),
(164, 374, 'Cardiac', 1, 1),
(165, 374, 'Pediatric', 2, 1),
(166, 374, 'ASD', 3, 1),
(167, 387, 'Vascular', 1, 1),
(168, 387, 'Venous Diseases', 2, 1),
(169, 387, 'DVT', 3, 1),
(172, 390, 'Vascular', 1, 1),
(173, 390, 'Aneurysm', 2, 1),
(174, 380, 'Cardiac', 1, 1),
(175, 380, 'Pediatric', 2, 1),
(176, 380, 'TOF', 3, 1),
(177, 384, 'Vascular', 1, 1),
(178, 384, 'Venous Diseases', 2, 1),
(179, 384, 'DVT', 3, 1),
(180, 394, 'Cardiac', 1, 1),
(181, 394, 'Adult', 2, 1),
(182, 394, 'CABG', 3, 1),
(186, 405, 'Vascular', 1, 1),
(187, 405, 'Venous Diseases', 2, 1),
(188, 405, 'Varicose Veins', 3, 1),
(189, 287, 'Cardiac', 1, 1),
(190, 287, 'Adult', 2, 1),
(191, 287, 'CABG', 3, 1),
(194, 414, 'Vascular', 1, 1),
(195, 414, 'Peripheral Vascular', 2, 1),
(198, 420, 'Vascular', 1, 1),
(199, 420, 'Arterial Diseases', 2, 1),
(200, 422, 'Thoracic', 1, 1),
(201, 422, 'Hydatid. Lung', 2, 1),
(202, 424, 'Thoracic', 1, 1),
(203, 424, 'Hydatid. Lung', 2, 1),
(208, 431, 'Vascular', 1, 1),
(209, 431, 'Arterial Diseases', 2, 1),
(210, 433, 'Thoracic', 1, 1),
(211, 433, 'Lung Rejection Surgery', 2, 1),
(212, 434, 'Vascular', 1, 1),
(213, 434, 'Arterial Diseases', 2, 1),
(214, 442, 'Vascular', 1, 1),
(215, 442, 'Arterial Diseases', 2, 1),
(219, 441, 'Cardiac', 1, 1),
(220, 441, 'Adult', 2, 1),
(221, 441, 'CABG', 3, 1),
(222, 471, 'Vascular', 1, 1),
(223, 471, 'Aneurysm', 2, 1),
(224, 495, 'Vascular', 1, 1),
(225, 495, 'Aneurysm', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `visit_bcp`
--

CREATE TABLE `visit_bcp` (
  `v_id` int(11) UNSIGNED NOT NULL COMMENT 'visits id of patient',
  `p_id_f` int(11) UNSIGNED NOT NULL COMMENT 'patient foreign key',
  `visit_date` date NOT NULL COMMENT 'patient date of the visit',
  `followup_date` date NOT NULL COMMENT 'followup date: next visit',
  `marital_status` tinyint(1) UNSIGNED NOT NULL COMMENT '1:single, 2:married, 3:divorced, 4:widowed.',
  `pregnant` tinyint(1) UNSIGNED NOT NULL COMMENT '1:pregnant, 2:not pregnant',
  `breast_feed` tinyint(1) UNSIGNED NOT NULL COMMENT '1:breasfeeding, 2:not breast feeding',
  `no_of_child` int(2) UNSIGNED NOT NULL COMMENT 'number of children',
  `other_info` varchar(60) COLLATE utf8_unicode_ci NOT NULL COMMENT 'other patient-specific info ',
  `chief_comp_n_dur` text COLLATE utf8_unicode_ci COMMENT 'chief_complaint and duration',
  `examination` text COLLATE utf8_unicode_ci,
  `investigation` text COLLATE utf8_unicode_ci,
  `investigation_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'investigation notes',
  `note` text COLLATE utf8_unicode_ci COMMENT 'notes',
  `treatment_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'a note to describe the treatments given, this note will be printed along with the treatment_description data',
  `fee` decimal(13,3) NOT NULL COMMENT 'per visit fee',
  `procedure` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'procedures on the patient',
  `procedure_date` date NOT NULL COMMENT 'date  of procedure',
  `procedure_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'procedure notes',
  `in_queue` tinyint(1) NOT NULL DEFAULT '1' COMMENT '-1: visit completed so not in queue, 1:in queue, 2:in doctors room',
  `u_id_f` int(11) NOT NULL,
  `view` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'active:1, delete:-1',
  `update_record` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'to check date of record insert'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `visit_bcp`
--

INSERT INTO `visit_bcp` (`v_id`, `p_id_f`, `visit_date`, `followup_date`, `marital_status`, `pregnant`, `breast_feed`, `no_of_child`, `other_info`, `chief_comp_n_dur`, `examination`, `investigation`, `investigation_note`, `note`, `treatment_note`, `fee`, `procedure`, `procedure_date`, `procedure_note`, `in_queue`, `u_id_f`, `view`, `update_record`) VALUES
(44, 20, '2017-03-05', '2017-04-26', 2, 0, 0, 3, '', 'some test data', 'some test data examination', 'some test data investigation', '', 'more notes on patient', 'some notes for treatments \nmore notes\nand notes', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(45, 19, '2017-03-05', '2017-04-17', 1, 0, 0, 0, '', 'some test data complain', 'some test data examination 2', 'some test data investigation 2', '', 'here is some test notes', 'writing notes is important', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(46, 18, '2017-03-05', '2017-05-15', 1, 2, 2, 0, '', 'some test data complain 3', 'some test data examination 3', 'some test data investigation 3', '', '', 'notes to be written', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(47, 21, '2017-03-07', '2017-04-11', 1, 0, 0, 0, '', 'a complain', 'test Examination', 'test investigation', '', '', 'some notes', '20000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(48, 21, '2017-03-08', '2017-04-17', 1, 0, 0, 0, '', 'online test complain', 'on test examination', 'on test investigation', '', '', 'some notes', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(52, 23, '2017-03-13', '2017-03-20', 2, 2, 2, 3, '', '', '', '', '', '', '', '2500.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(54, 21, '2017-03-11', '2017-04-11', 1, 0, 0, 0, '', '', '', '', '', '', '5x2 for 1 week\r\n2x1 for 2 weeks', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(58, 23, '2017-03-13', '0000-00-00', 2, 2, 2, 2, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(63, 36, '2017-03-14', '2017-04-05', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(64, 33, '2017-03-14', '2017-03-28', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(69, 36, '2017-03-14', '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', '', '0.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(70, 18, '2017-03-14', '2017-03-16', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(71, 19, '2017-03-14', '2017-03-16', 1, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(73, 33, '2017-03-15', '2017-03-26', 2, 1, 2, 1, '', '', '', '', '', '', 'some notes', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(74, 46, '2017-03-15', '0000-00-00', 1, 2, 2, 0, '', 'some info', 'dsd', 'asd', '', '', 'aDdfgv', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(75, 33, '2017-03-15', '0000-00-00', 2, 1, 2, 3, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(76, 18, '2017-03-15', '0000-00-00', 2, 2, 1, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(77, 46, '2017-03-15', '2017-03-29', 1, 2, 2, 0, '', 'some info,', 'dg', 'hcfxh', '', '', 'dxghfgh', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(78, 46, '2017-03-15', '2017-04-01', 2, 1, 2, 2, '', 'new', 'an examination', 'new investigation', '', '', 'good treatment', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(81, 46, '2017-03-18', '2017-03-20', 2, 2, 2, 0, '', 'some chief complain', 'new examination', '', '', '', 'good treatment', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(82, 23, '2017-03-18', '2017-03-22', 1, 2, 2, 0, '', '', 'new examination', '', '', '', '', '25.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(84, 23, '2017-03-22', '0000-00-00', 1, 2, 2, 0, '', 'some data', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(88, 36, '2017-04-08', '0000-00-00', 2, 2, 1, 3, '', 'some data', 'non', '', '', '', '1x2 use it\r\nand more use is here', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(89, 19, '2017-04-08', '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '2500.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(90, 46, '2017-04-08', '2017-04-17', 1, 2, 2, 0, '', 'Chief complain', 'Exam', 'Gation', '', '', 'Good treatment,\r\nBest new treatment', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(99, 36, '2017-04-12', '2017-05-11', 2, 2, 2, 1, '', 'Present history', 'New examination', 'Good inv', '', '', 'It is use three time in the day,\r\nOne time', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(100, 46, '2017-04-13', '0000-00-00', 2, 1, 2, 0, '', 'any chief complain and present histroy', 'more examination', 'inv', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(101, 36, '2017-04-13', '2017-05-02', 1, 2, 2, 0, '', 'chief complain', 'exa', 'invistigation', '', '', 'good treatment,daily treatment,one time in a day,three time,free use', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(102, 33, '2017-04-13', '2017-04-30', 1, 2, 2, 0, '', 'chief complain', 'exa', 'invistigation', '', '', 'good treatment\r\ndaily treatment\r\none time in a day\r\nthree time\r\nfree use', '35000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(103, 18, '2017-04-14', '2017-04-23', 2, 1, 2, 0, '', 'history', 'ex', 'inves', '', '', 'one time\r\ntwo time\r\nfree time\r\nevery day', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(108, 59, '2017-06-14', '0000-00-00', 1, 2, 2, 0, '', 'chife', 'e', 'ines', '', '', 'daily treatment\r\none time\r\nthree time', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(109, 60, '2017-06-14', '0000-00-00', 1, 2, 2, 0, '', 'chife', 'new examination', 'ines', '', '', 'ONE TIME\r\nTWO TIME', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(111, 64, '2017-04-17', '2017-04-24', 2, 0, 0, 2, '', 'chief complain', 'exam', 'invistigation', '', '', 'One day\r\nMore than one time', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(114, 64, '2017-05-01', '2017-05-21', 2, 0, 0, 0, '', 'chief', 'exa', 'invistigation', '', '', 'good treatment\r\nhigh treatment\r\nbest treatment', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(115, 64, '2017-05-01', '2017-05-01', 2, 0, 0, 3, '', 'chief complain', 'ex', 'hcfxh', '', '', 'good treatment', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(116, 46, '2017-05-01', '2017-05-01', 1, 2, 2, 0, '', 'chief complain', '', 'invistigation', '', '', 'note', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(118, 66, '2017-05-01', '0000-00-00', 2, 0, 0, 1, '', 'chief', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(120, 19, '2017-05-07', '2017-05-30', 1, 0, 0, 0, '', 'chief complain', 'new examination', 'new inves', '', '', 'hi treatment\r\ndaily treatment', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(121, 23, '2017-05-07', '2017-05-25', 2, 2, 2, 0, '', 'complain', 'ex', 'i', '', '', 'one time', '0.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(122, 18, '2017-05-09', '0000-00-00', 2, 2, 2, 0, '', 'chief complain', 'ex', 'new inves', '', '', '', '0.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(123, 21, '2017-05-10', '2017-05-24', 2, 0, 0, 0, '', 'new chief complain', 'examination', 'new inves', '', '', 'daily treatment', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(124, 18, '2017-05-13', '2017-05-31', 1, 2, 2, 0, '', 'some chief complain', 'new examination', 'ines', '', '', 'daily', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(125, 66, '2017-05-13', '2017-05-30', 2, 0, 0, 1, '', 'new', 'new examination', 'new', '', '', 'good', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(126, 64, '2017-05-13', '2017-06-08', 2, 0, 0, 2, '', 'new', 'new examination', 'new examinationin', '', '', 'daily', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(127, 20, '2017-05-13', '2017-06-06', 1, 0, 0, 0, '', 'c', 'e', 'i', '', '', 'best', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(128, 33, '2017-05-13', '2017-08-30', 1, 2, 2, 0, '', 'new', 'new examination', 'ines', '', '', 'one time\r\ntwo time', '0.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(129, 70, '2017-05-13', '0000-00-00', 2, 2, 2, 1, '', '', '', '', '', '', '', '0.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(131, 19, '2017-05-17', '2017-05-31', 1, 0, 0, 0, '', 'chif', 'e', '', '', '', 'good', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(135, 36, '2017-05-27', '0000-00-00', 1, 2, 2, 0, 'some data', 'new chief', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(136, 20, '2017-05-27', '2017-06-05', 1, 0, 0, 0, '', 'new chief', 'new chief', 'i', '', '', 'first treatment\r\nsecond treatment', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(137, 33, '2017-05-27', '2017-06-05', 1, 2, 2, 0, '', 'chif', 'e', 'ines', '', '', 'daily', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(138, 64, '2017-05-27', '0000-00-00', 2, 0, 0, 2, '', 'some chief complain', 'new examination', 'new examinationin', '', '', 'daily', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(140, 18, '2017-06-19', '2017-06-28', 1, 2, 2, 0, '', 'chife', 'new examination', 'ines', '', '', 'one time', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(146, 64, '2017-06-21', '0000-00-00', 2, 0, 0, 2, '', 'new', 'new chief', 'new', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(149, 64, '2017-06-21', '2017-06-27', 2, 0, 0, 2, '', '', '', '', '', '', 'hfjdfh\r\nkfgjdk', '20000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(150, 70, '2017-06-21', '0000-00-00', 1, 2, 2, 0, '', 'ch', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(151, 70, '2017-06-21', '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(152, 19, '2017-06-21', '0000-00-00', 1, 0, 0, 0, '', 'chife', 'new examination', 'in', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(153, 60, '2017-07-04', '0000-00-00', 1, 2, 2, 0, '', 'chif', 'e', 'new examinationin', '', '', 'new\r\nneww', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(154, 59, '2017-07-04', '2017-07-24', 2, 2, 2, 0, '', 'new chief', 'new chief', 'new examinationin', '', '', 'one\r\ntwo\r\nthree', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(155, 19, '2017-07-04', '2017-07-30', 2, 0, 0, 0, '', 'chif', 'e', 'ines', '', '', 'hhjgh\r\ngfbnh', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(156, 18, '2017-07-17', '2017-09-12', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(157, 71, '2017-07-09', '2017-08-23', 1, 0, 0, 0, '', 'chief complain', 'examination', 'investigation', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(158, 20, '2017-07-09', '2017-07-19', 2, 0, 0, 0, '', '', '', '', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(159, 72, '2017-07-09', '2017-07-25', 1, 0, 0, 0, '', 'ch', '', '', '', '', 'one', '25.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(160, 72, '2017-07-09', '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(161, 60, '2017-07-09', '2017-07-23', 2, 2, 2, 0, '', '', '', '', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(162, 59, '2017-07-09', '2017-07-24', 2, 2, 2, 0, '', 'chief complain', 'examination', 'investigation', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(166, 76, '2017-07-24', '0000-00-00', 1, 2, 2, 0, '', 'Chief', 'Exam', 'Inves', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(167, 60, '2017-07-10', '0000-00-00', 2, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(171, 71, '2017-07-11', '0000-00-00', 2, 0, 0, 0, '', 'chief complain', 'examination', 'in', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(173, 33, '2017-07-11', '0000-00-00', 1, 2, 2, 0, '', 'c', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(176, 78, '2017-07-17', '2017-08-06', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(177, 79, '2017-07-19', '0000-00-00', 2, 2, 2, 0, '', '', '', '', '', '', '', '0.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(180, 82, '2017-07-16', '0000-00-00', 2, 0, 0, 1, '', 'some chief complain', 'new examination', 'new', '', '', 'one\r\ntwo\r\nthree', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(181, 83, '2017-07-17', '2017-07-24', 1, 0, 0, 0, '', 'new', 'new examination', 'new', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(183, 85, '2017-07-25', '2017-08-04', 1, 0, 0, 0, '', 'chief complain', 'best examination', 'best chief complain', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(185, 20, '2017-07-16', '0000-00-00', 2, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(186, 19, '2017-07-16', '2017-07-31', 2, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(187, 21, '2017-07-16', '2017-07-30', 2, 0, 0, 0, '', 'chief complain', '', '', '', '', '', '20000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(188, 36, '2017-07-16', '2017-07-28', 1, 2, 2, 0, '', 'chief complain', 'new examination', 'i', '', '', 'one', '20000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(189, 59, '2017-07-16', '2017-08-08', 2, 2, 2, 0, '', '', '', '', '', '', 'tfyt gvg\r\ntwo\r\nthree', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(191, 82, '2017-07-17', '2017-08-06', 2, 0, 0, 1, '', 'new', 'e', 'in', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(192, 72, '2017-07-17', '0000-00-00', 1, 0, 0, 0, '', 'Chief', 'Examination', 'Invi', '', '', 'One\r\nTwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(193, 21, '2017-07-17', '2017-07-19', 2, 0, 0, 0, '', 'New', 'Examination', 'New', '', '', 'One\r\nTwo\r\nThree', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(194, 64, '2017-07-17', '2017-08-06', 2, 0, 0, 2, '', 'new chief', 'new', 'new examinationin', '', '', 'one\r\ntwo\r\nthree\r\nfour', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(197, 66, '2017-07-17', '2017-07-31', 2, 0, 0, 1, '', 'Chief', '', '', '', '', 'Tr', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(198, 78, '2017-07-18', '2017-07-22', 1, 2, 2, 0, '', 'chief complain', 'e', 'investigation', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(199, 72, '2017-07-18', '0000-00-00', 1, 0, 0, 0, '', 'chief complain', 'new chief', 'new examinationin', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(200, 83, '2017-07-18', '0000-00-00', 1, 0, 0, 0, '', 'ch', 'new chief', 'new examinationin', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(201, 46, '2017-07-18', '2017-07-29', 1, 2, 2, 0, '', 'ch', 'some Examination', 'some invistigation', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(202, 33, '2017-07-18', '0000-00-00', 1, 2, 2, 0, '', 'chief complain', 'examination', 'in', '', '', '', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(203, 23, '2017-07-18', '2017-08-05', 2, 2, 2, 0, '', 'chief complain', 'some Examination', 'some invistigation', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(204, 70, '2017-07-18', '2017-07-29', 1, 2, 2, 0, '', 'ch', 'some Examination', 'ines', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(205, 18, '2017-07-18', '2017-08-09', 1, 2, 2, 0, '', 'chief complain', 'e', 'i', '', '', '', '2500.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(206, 78, '2017-07-19', '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', '', '0.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(210, 87, '2017-07-22', '2017-07-31', 1, 0, 0, 0, '', 'chief complain', 'best examination', 'best chief complain', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(211, 89, '2017-07-22', '0000-00-00', 2, 2, 2, 3, '', 'new chief complain', 'new chief', 'best chief complain', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(212, 90, '2017-07-22', '2017-08-05', 1, 0, 0, 0, '', 'chief complain', 'best examination', 'best investigation2', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(213, 91, '2017-07-22', '2017-07-30', 2, 2, 2, 3, '', 'chief complain', 'best examination', 'some invistigation', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(214, 92, '2017-07-22', '2017-08-05', 2, 2, 2, 4, '', 'new', 'e', 'best chief complain', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(215, 93, '2017-07-22', '2017-08-07', 1, 2, 2, 0, '', 'ch', 'examination', 'ines', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(216, 94, '2017-07-22', '2017-08-08', 1, 2, 2, 0, '', 'new chief', 'new chief', 'new', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(217, 91, '2017-07-23', '0000-00-00', 2, 2, 2, 3, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(218, 83, '2017-07-23', '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(220, 20, '2017-07-23', '0000-00-00', 2, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(221, 92, '2017-07-23', '2017-07-25', 2, 2, 2, 4, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(223, 90, '2017-07-24', '2017-07-31', 1, 0, 0, 0, '', '', '', '', '', '', '500mg  for 10 days', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(225, 18, '2017-07-26', '0000-00-00', 1, 2, 2, 0, '', 'c', 'best examination', 'best investigation', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(226, 46, '2017-07-26', '2017-08-06', 1, 2, 2, 0, '', 'some chief complain', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(227, 21, '2017-07-26', '2017-08-05', 2, 0, 0, 0, '', 'c', 'best examination', 'best chief complain', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(228, 19, '2017-07-26', '2017-08-05', 2, 0, 0, 0, '', 'chief complain', 'best examination', 'investigation', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(229, 95, '2017-07-26', '2017-08-05', 2, 0, 0, 1, '', 'ch', 'best examination', 'best investigation', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(230, 96, '2017-07-29', '2017-08-31', 2, 0, 0, 0, '', 'chief complain', 'best examination', 'best investigation', '', '', 'one\r\ntwo', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(238, 93, '2017-08-01', '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(239, 72, '2017-08-01', '2017-08-08', 1, 0, 0, 0, '', 'chief complain', 'best examination', 'best chief complain', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(240, 96, '2017-08-01', '2017-09-07', 2, 0, 0, 0, '', 'Chief complain', 'Examination', 'Investigation', '', '', 'One\r\nTwo\r\nThree\r\nFour', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(246, 83, '2017-08-02', '2017-08-05', 1, 0, 0, 0, '', 'ch', 'best examination', 'best chief complain', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(247, 91, '2017-08-12', '2017-09-04', 2, 2, 2, 3, '', 'chief complain', 'best examination', 'best chief complain', '', '', 'one', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(248, 95, '2017-08-12', '0000-00-00', 2, 0, 0, 1, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(249, 94, '2017-08-14', '0000-00-00', 1, 2, 2, 0, '', 'chief complain', 'exa', 'invistigation', '', '', '', '250000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(250, 96, '2017-09-13', '0000-00-00', 2, 0, 0, 0, '', '', 'best examination', 'best investigation', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(251, 94, '2017-09-13', '0000-00-00', 1, 2, 2, 0, '', 'chief complain', '', '', '', '', '', '2500.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(252, 93, '2017-10-26', '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(253, 93, '2017-10-27', '0000-00-00', 2, 1, 2, 2, '', '', '', 'dag', '', '', 'note', '2000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(254, 92, '2017-10-27', '0000-00-00', 2, 2, 2, 4, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(255, 96, '2017-11-02', '0000-00-00', 2, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(257, 18, '2017-11-25', '0000-00-00', 1, 2, 2, 0, '', 'chief complain', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(258, 87, '2017-12-16', '0000-00-00', 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(259, 78, '2017-12-16', '0000-00-00', 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(262, 96, '2017-12-18', '0000-00-00', 2, 0, 0, 0, '', '', '', '', '', '', '', '250000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(269, 91, '2018-02-02', '0000-00-00', 2, 2, 2, 3, '', NULL, NULL, NULL, '', NULL, '', '20000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(270, 92, '2018-02-02', '0000-00-00', 2, 2, 2, 4, '', NULL, NULL, NULL, '', NULL, '', '20000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(271, 96, '2018-02-02', '0000-00-00', 2, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(272, 89, '2018-02-02', '0000-00-00', 2, 2, 2, 3, '', NULL, NULL, 'new inv', '', NULL, '', '20000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(273, 87, '2018-02-05', '0000-00-00', 1, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '250000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(276, 91, '2018-02-06', '0000-00-00', 2, 2, 2, 3, '', '', '', '', '', '', '', '20000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(278, 91, '2018-02-07', '0000-00-00', 2, 2, 2, 3, '', '', '', '', '', '', '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(280, 87, '2018-02-07', '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '20000.000', '', '0000-00-00', '', -1, 1, 1, '0000-00-00 00:00:00'),
(281, 94, '2018-02-14', '0000-00-00', 1, 2, 2, 0, '', '', '', '', '', '', '500mg 1x4\r\n200mg 2x5\r\nonly at night, one half at a time.', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(282, 96, '2018-02-14', '0000-00-00', 2, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(283, 93, '2018-03-06', '0000-00-00', 2, 1, 2, 2, '', '', '', '', '', '', '', '20000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(286, 23, '2018-03-28', '0000-00-00', 2, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(287, 33, '2018-03-31', '0000-00-00', 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '0.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(288, 19, '2018-04-06', '0000-00-00', 2, 0, 0, 0, '', '', '120/8 mmhg,70 b/min,ch test,h test,abd test,others', '', '', '', '', '22000.000', '', '2018-04-06', '', -1, 2, 1, '0000-00-00 00:00:00'),
(289, 59, '2018-04-05', '2018-04-11', 2, 2, 2, 0, '', 'not important', 'ex1', 'someinvs, another inv, and more', 'we have notes for investigation 2', 'general notes', 'notes1\r\nnotes 2', '20000.000', 'special procedure 1', '2018-04-05', 'notes for procedures ...', -1, 2, 1, '0000-00-00 00:00:00'),
(290, 60, '2018-04-06', '0000-00-00', 2, 2, 2, 0, '', '', '', '', '', '', '', '25000.000', '', '2018-04-06', '', -1, 2, 1, '0000-00-00 00:00:00'),
(291, 70, '2018-04-08', '0000-00-00', 1, 2, 2, 0, '', '', 'PR: 70 b/min,Chest: chtest,Heart: hrtest,Other: othtest', '', '', '', '', '25000.000', '', '2018-04-08', '', -1, 2, 1, '0000-00-00 00:00:00'),
(292, 76, '2018-04-08', '0000-00-00', 1, 2, 2, 0, '', '', 'BP: 120/8 mmhg, PR: 70 b/min, Chest: chtest, Heart: Htest, Abdomin: abdtest, Other: others', '', '', '', '', '25000.000', '', '2018-04-08', '', -1, 2, 1, '0000-00-00 00:00:00'),
(293, 85, '2018-04-15', '0000-00-00', 1, 0, 0, 0, '', '', 'BP: 120/8 mmhg, PR: 70 b/min, Heart: H12, Other: other', '', '', '', '', '25000.000', '', '2018-04-15', '', -1, 2, 1, '0000-00-00 00:00:00'),
(294, 97, '2018-04-15', '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '25000.000', '', '2018-04-15', '', -1, 1, 1, '0000-00-00 00:00:00'),
(295, 18, '2018-04-15', '0000-00-00', 2, 2, 2, 3, '', '', '', 'new Investigation', '', '', '', '0.000', '', '2018-04-15', '', -1, 2, 1, '0000-00-00 00:00:00'),
(296, 20, '2018-04-15', '0000-00-00', 2, 0, 0, 0, '', '', '', 'best chief complain      new', '', '', '', '20000.000', '', '2018-04-15', '', -1, 2, 1, '0000-00-00 00:00:00'),
(297, 87, '2018-04-16', '0000-00-00', 1, 0, 0, 0, '', '', 'BP: 25/45 mmhg, PR: 22/4 b/min', '', '', '', '', '25000.000', '', '2018-04-16', '', -1, 2, 1, '0000-00-00 00:00:00'),
(298, 71, '2018-04-16', '0000-00-00', 2, 0, 0, 0, '', '', 'Chest: helo helo helo helo helo helo helo helo helo helo, Heart: helo helo helo helo helo helo helo helo helo helo, Abdomin: helo helo helo helo helo helo helo helo helo helo', '', '', '', '', '25000.000', '', '2018-04-16', '', -1, 2, 1, '0000-00-00 00:00:00'),
(299, 21, '2018-04-16', '0000-00-00', 2, 0, 0, 0, '', NULL, NULL, NULL, '', NULL, '', '25000.000', '', '0000-00-00', '', -1, 2, 1, '0000-00-00 00:00:00'),
(301, 94, '2018-05-03', '0000-00-00', 1, 2, 2, 0, '', NULL, NULL, NULL, '', NULL, '', '20000.000', '', '0000-00-00', '', -1, 2, 1, '2018-05-03 03:16:21'),
(302, 98, '2018-05-05', '0000-00-00', 1, 0, 0, 0, '', 'chief complain one', 'BP: 21/5 mmhg, PR: 22/5 b/min, Chest: mjhg, Heart: nhg, Abdomin: jh', 'best chief complain,twodndaskjf', 'one jswnef jjjjjjj\r\ntwoa  jndfnsf jsf', '', 'pme\r\ntwo', '25000.000', 'proce one, proce two', '2018-05-05', 'one\r\ntwo', -1, 2, 1, '2018-05-04 21:47:35'),
(303, 19, '2018-05-05', '2018-05-27', 2, 0, 0, 0, '', 'c', 'BP: 28/4 mmhg, PR: 72/2 b/min, Chest: mmm, Heart: heat, Abdomin: adb', 'best chief complain,twodndaskjf', 'one\r\ntwo', '', 'oneeeeeeeeeee\r\ntwoooooooooo', '25000.000', 'procedure onr na na twi,one', '2018-05-05', 'hello', -1, 2, 1, '2018-05-04 21:58:43'),
(304, 18, '2018-05-05', '0000-00-00', 2, 2, 2, 3, '', 'test', '', '', '', '', '', '0.000', '', '2018-05-05', '', -1, 2, 1, '2018-05-05 06:46:45'),
(306, 95, '2018-05-05', '0000-00-00', 2, 0, 0, 1, '', '', 'BP: 25/5 mmhg, PR: 10/4 b/min, Chest: hello, Heart: heart, Abdomin: new abdo', 'best chief complain,twodndaskjf,new', 'good one', '', 'one\r\ntwo\r\nthree', '0.000', '', '2018-05-05', '', -1, 2, 1, '2018-05-05 16:36:17'),
(307, 94, '2018-05-05', '2018-05-28', 1, 2, 2, 0, '', 'first time this person in here', 'BP: 1/1 mmhg, PR: 4/5 b/min, Chest: new chest, Heart: new heart, Abdomin: new abdo', 'best chief complain,new,one', 'hello world', '', 'one\r\ntwo', '25000.000', '', '2018-05-05', '', -1, 2, 1, '2018-05-05 16:39:37'),
(308, 36, '2018-05-06', '0000-00-00', 1, 2, 2, 0, '', 's', '', '', '', '', '', '25000.000', 'sclerotherapy', '2018-05-06', '', -1, 2, 1, '2018-05-06 05:21:38'),
(309, 90, '2018-05-06', '0000-00-00', 1, 0, 0, 0, '', '', '', '', '', '', '', '20000.000', 'someproc', '2018-05-06', '', -1, 1, 1, '2018-05-06 05:32:46');

-- --------------------------------------------------------

--
-- Table structure for table `visit_diagnose`
--

CREATE TABLE `visit_diagnose` (
  `vd_id` int(11) NOT NULL,
  `v_id_f` int(11) UNSIGNED NOT NULL COMMENT 'visit foreign key',
  `diagnose` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'each visit of patient may have multiple diagnoses, but I preferred to have the description on the same table not different table so that not make queries more complex, and if I want to get list of available diagnoses I should query for distinct values of treatment_desc (for example if I want to use it in autocomplete. )',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='each visit may result in many diagnoses';

--
-- Dumping data for table `visit_diagnose`
--

INSERT INTO `visit_diagnose` (`vd_id`, `v_id_f`, `diagnose`, `view`) VALUES
(1, 44, 'dig1', 1),
(2, 44, 'dig2', 1),
(3, 44, 'dig3', 1),
(4, 45, 'dig2', 1),
(5, 45, 'dig3', 1),
(6, 45, 'new dig', 1),
(7, 46, 'dig1', 1),
(11, 48, 'dig1', 1),
(12, 48, 'dig3', 1),
(13, 47, 'dig3', 1),
(14, 54, 'dig5', 1),
(15, 54, 'new dig', 1),
(17, 64, 'dig2', 1),
(18, 64, 'new diadnos', 1),
(19, 63, 'dig1', 1),
(20, 63, 'new diadnos', 1),
(21, 63, 'new dig', 1),
(22, 70, 'dig1', 1),
(23, 70, 'new dig', 1),
(24, 71, 'dig5', 1),
(25, 71, 'new dig', 1),
(26, 73, 'dig2', 1),
(27, 74, 'new dig', 1),
(28, 75, 'dig2', 1),
(29, 77, 'new', 1),
(30, 78, 'dig1', 1),
(31, 81, 'dig2', 1),
(32, 81, 'dig3', 1),
(33, 90, 'new diadnos', 1),
(34, 90, 'new', 1),
(35, 99, 'dig2', 1),
(36, 99, 'dig1', 1),
(37, 100, 'new dig', 1),
(38, 100, 'other diagnosis', 1),
(39, 100, 'dig1', 1),
(40, 100, 'dig3', 1),
(41, 101, 'dig2', 1),
(42, 101, 'dig2', 1),
(43, 102, 'dig1', 1),
(44, 102, 'dig2', 1),
(45, 103, 'dig1', 1),
(46, 111, 'dig1', 1),
(47, 111, 'new dig', 1),
(48, 111, 'dig2', 1),
(49, 114, 'dig1', 1),
(50, 115, 'dig1', 1),
(51, 115, 'new dig', 1),
(52, 116, 'dig1', 1),
(53, 120, 'dig3', 1),
(54, 120, 'new dig', 1),
(55, 121, 'dig2', 1),
(56, 122, 'dig2', 1),
(57, 122, 'dig1', 1),
(58, 123, 'dig1', 1),
(59, 124, 'dig1', 1),
(60, 125, 'new dig', 1),
(61, 126, 'dig5', 1),
(62, 127, 'dig1', 1),
(63, 128, 'dig3', 1),
(64, 131, 'dig3', 1),
(65, 136, 'dig1', 1),
(66, 137, 'new diadnos', 1),
(67, 138, 'new dig', 1),
(68, 138, 'dig3', 1),
(69, 108, 'dig1', 1),
(70, 109, 'dig2', 1),
(71, 140, 'new dig', 1),
(72, 146, 'dig5', 1),
(73, 152, 'new dig', 1),
(74, 153, 'dig1', 1),
(75, 153, 'new dig', 1),
(76, 153, 'dig2', 1),
(77, 154, 'dig5', 1),
(78, 154, 'new dig', 1),
(79, 154, 'new', 1),
(80, 155, 'dig1', 1),
(81, 155, 'dig3', 1),
(82, 157, 'diagnosis1', 1),
(83, 159, 'dig1', 1),
(84, 162, 'dig3', 1),
(85, 171, 'new dig', 1),
(86, 180, 'dig5', 1),
(87, 188, 'dig1', 1),
(88, 181, 'new dig', 1),
(89, 181, 'dig1', 1),
(90, 191, 'dig1', 1),
(91, 192, 'new dig', 1),
(92, 193, 'dig3', 1),
(93, 193, 'new dig', 1),
(94, 194, 'dig3', 1),
(95, 194, 'dig1', 1),
(96, 197, 'dig3', 1),
(97, 202, 'dig2', 1),
(98, 202, 'dig10', 1),
(99, 198, 'dig3', 1),
(100, 199, 'dig1', 1),
(101, 199, 'dig3', 1),
(102, 200, 'new dig', 1),
(103, 201, 'dig2', 1),
(104, 203, 'dig2', 1),
(105, 204, 'dig1', 1),
(106, 205, 'dig2', 1),
(107, 210, 'new dig', 1),
(108, 211, 'new dig', 1),
(109, 214, 'new dig', 1),
(110, 215, 'new dig', 1),
(111, 216, 'dig2', 1),
(112, 166, 'new dig', 1),
(113, 183, 'dig3', 1),
(114, 225, 'dig1', 1),
(115, 228, 'new dig', 1),
(116, 230, 'dig1', 1),
(117, 230, 'new dig', 1),
(118, 230, 'new diadnos', 1),
(119, 240, 'new dig', 1),
(120, 239, 'dig2', 1),
(121, 239, 'new dig', 1),
(122, 246, 'dig1', 1),
(123, 247, 'dig2', 1),
(124, 249, 'dig1', 1),
(125, 253, 'dig3', 1),
(126, 257, 'new dig', 1),
(127, 289, 'dig1', 1),
(128, 289, 'new dig', 1),
(129, 306, 'dig2', 1),
(130, 307, 'dig2', 1),
(131, 307, 'new diadnos', 1),
(132, 334, 'dig2', 1),
(133, 161, 'dig2', 1),
(134, 161, 'dig3', 1),
(136, 52, 'dig1', 1),
(137, 371, 'dig2', 1),
(138, 371, 'dig2', 1),
(139, 376, 'dig2', 1),
(140, 378, 'dig2', 1),
(141, 374, 'dig1', 1),
(142, 387, 'dig1', 1),
(143, 380, 'dig1', 1),
(144, 380, 'newwww', 1),
(145, 384, 'new dig', 1),
(146, 394, 'new dig', 1),
(150, 287, 'dig1', 1),
(151, 393, 'dig1', 1),
(152, 420, 'dig2', 1),
(153, 420, 'dig3', 1),
(154, 422, 'dig2', 1),
(155, 422, 'new dig', 1),
(156, 424, 'dig2', 1),
(157, 424, 'new dig', 1),
(158, 425, 'dig2', 1),
(159, 425, 'dig3', 1),
(160, 433, 'new dig', 1),
(161, 434, 'dig2', 1),
(162, 439, 'dig1', 1),
(163, 439, 'dig3', 1),
(164, 439, 'new dig', 1),
(165, 447, 'new dig', 1),
(166, 447, 'dig1', 1),
(167, 447, 'new diadnos', 1),
(168, 495, 'dig1', 1),
(169, 495, 'dig2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `visit_surgery`
--

CREATE TABLE `visit_surgery` (
  `vs_id` int(11) UNSIGNED NOT NULL,
  `v_id_f` int(11) UNSIGNED NOT NULL,
  `surgery` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `surgery_date` date NOT NULL COMMENT 'hamu surgery of single visit have the same date, bas har lera duplicate i dakam bashtre nawak la dahatu judai bkamawa',
  `surgery_note` text COLLATE utf8_unicode_ci COMMENT 'note for the surgeries',
  `view` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `visit_surgery`
--

INSERT INTO `visit_surgery` (`vs_id`, `v_id_f`, `surgery`, `surgery_date`, `surgery_note`, `view`) VALUES
(26, 47, 'surg2', '2017-03-14', '0', 1),
(27, 47, 'surg1', '2017-03-14', '0', 1),
(28, 47, 'surg3', '2017-03-14', '0', 1),
(41, 45, 'surg2', '2017-03-07', '0', 1),
(42, 45, 'surg4', '2017-03-07', '0', 1),
(49, 44, 'surg1', '2017-03-09', '0', 1),
(50, 44, 'surg2', '2017-03-09', '0', 1),
(51, 44, 'surg4', '2017-03-09', '0', 1),
(55, 46, 'surg1', '2017-03-18', '0', 1),
(56, 46, 'surg2', '2017-03-18', '0', 1),
(57, 48, 'surg1', '2017-03-08', '0', 1),
(58, 48, 'surg3', '2017-03-08', '0', 1),
(59, 54, 'surg3', '2017-03-14', '0', 1),
(60, 73, 'surg3', '2017-03-16', '0', 1),
(61, 189, 'surg2', '2017-07-25', '0', 1),
(66, 272, 'surg2', '2017-12-11', '0', 1),
(67, 272, 'surg3', '2017-12-11', '0', 1),
(68, 289, 'surg1', '2018-04-05', '0', 1),
(77, 306, 'surg1', '2018-05-05', '0', 1),
(78, 306, 'surg4', '2018-05-05', '0', 1),
(83, 161, 'surg1', '2018-05-26', NULL, 1),
(84, 161, 'surg2', '2018-05-26', NULL, 1),
(86, 52, 'surg2', '2018-06-03', NULL, 1),
(113, 334, 'surg1', '2018-03-11', 'I write some notes to those surgeries', 1),
(114, 334, 'surg4', '2018-03-11', 'I write some notes to those surgeries', 1),
(116, 347, 'surg3', '2018-06-03', 'new notes', 1),
(117, 345, 'new surgery,', '2018-04-16', 'second note', 1),
(119, 335, 'surg1', '2018-06-17', 'some notes here for surg', 1),
(120, 351, 'surg1', '2018-06-11', 'I added notes', 1),
(121, 351, 'surg4', '2018-06-11', 'I added notes', 1),
(122, 352, 'surg2', '2018-06-18', 'some notes', 1),
(123, 352, 'surg3', '2018-06-18', 'some notes', 1),
(124, 359, 'surg3', '2018-06-20', '', 1),
(125, 360, 'surg3', '2018-06-21', '', 1),
(126, 364, 'surg4', '2018-06-21', '', 1),
(132, 371, 'surg3', '2018-06-19', '', 1),
(133, 372, 'surg1', '2018-06-21', '', 1),
(134, 372, 'surg4', '2018-06-21', '', 1),
(135, 375, 'surg1', '2018-06-10', '', 1),
(136, 375, 'surg3', '2018-06-10', '', 1),
(137, 317, 'surg1', '2018-06-02', '', 1),
(138, 329, 'surg3', '2018-06-21', '', 1),
(139, 376, 'surg1', '2018-06-16', 'some notes here', 1),
(140, 376, 'surg2', '2018-06-16', 'some notes here', 1),
(141, 378, 'surg1', '2018-06-22', '', 1),
(143, 374, 'surg1', '2018-06-23', '', 1),
(144, 387, 'new surgery', '2018-06-01', 'note sur', 1),
(145, 382, 'surg3', '2018-01-08', '', 1),
(146, 291, 'surg2', '2018-06-01', '', 1),
(147, 390, 'surg1', '2018-05-29', '', 1),
(148, 380, 'surg1', '2018-06-09', 'note surg', 1),
(149, 380, 'su new', '2018-06-09', 'note surg', 1),
(150, 384, 'new surgery,', '2018-06-19', 'no sur', 1),
(151, 384, 'new surgery', '2018-06-19', 'no sur', 1),
(152, 394, 'surg2', '2018-06-29', 'note sur', 1),
(153, 350, 'surg2', '2018-07-02', '', 1),
(154, 406, 'surg1', '2018-07-01', '', 1),
(156, 287, 'surg2', '2018-10-14', 'some notes', 1),
(157, 287, 'surg3', '2018-10-14', 'some notes', 1),
(160, 393, 'surg2', '2018-11-12', 'surg notes', 1),
(161, 393, 'surg3', '2018-11-12', 'surg notes', 1),
(162, 420, 'surg3', '2018-11-05', '', 1),
(163, 422, 'surg3', '2018-11-06', 'surge notes', 1),
(164, 424, 'surg3', '2018-11-15', 'surgery notes are important', 1),
(165, 425, 'surg1', '2018-11-15', 'surg', 1),
(166, 442, 'surg2', '2019-02-10', 'some notes on surg2', 1),
(167, 441, 'surg2', '2019-02-18', '', 1),
(172, 447, 'surg2', '2019-07-09', 'Surgery new notes', 1);

-- --------------------------------------------------------

--
-- Table structure for table `visit_treatment`
--

CREATE TABLE `visit_treatment` (
  `vt_id` int(11) UNSIGNED NOT NULL,
  `v_id_f` int(11) UNSIGNED NOT NULL COMMENT 'visit foreign key',
  `treatment` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'each visit of patient may need multiple treatments, but I preferred to have the description on the same table not different table so that not make queries more complex, and if I want to get list of available treatments I should query for distinct values of treatment_desc (for example if I want to use it in autocomplete. )',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='to get list of available treatments I should query for distinct values of desc';

--
-- Dumping data for table `visit_treatment`
--

INSERT INTO `visit_treatment` (`vt_id`, `v_id_f`, `treatment`, `view`) VALUES
(1, 44, 'treat1', 1),
(2, 44, 'treat2', 1),
(3, 44, 'treat3', 1),
(4, 45, 'treat2', 1),
(5, 45, 'treat3', 1),
(6, 46, 'treat1', 1),
(7, 47, 'treat2', 1),
(8, 47, 'treat3', 1),
(9, 47, 'new treatment', 1),
(10, 48, 'treat2', 1),
(11, 54, 'new treatment', 1),
(12, 54, 'all new treatment', 1),
(16, 64, 'treat3', 1),
(17, 63, 'all new treatment', 1),
(18, 63, 'treat1', 1),
(19, 69, 'treat1', 1),
(20, 70, 'treat1', 1),
(21, 70, 'all new treatment', 1),
(22, 71, 'treat3', 1),
(23, 74, 'all new treatment', 1),
(24, 77, 'tre', 1),
(25, 78, 'treat2', 1),
(26, 81, 'treat1', 1),
(27, 73, 'treat1', 1),
(28, 88, 'treat2', 1),
(29, 88, 'all new treatment', 1),
(30, 88, 'こんいちわ', 1),
(31, 90, 'treat3', 1),
(32, 90, 'all new treatment', 1),
(33, 99, 'treat3', 1),
(34, 99, 'new treatment', 1),
(35, 100, 'treat2', 1),
(36, 100, 'onther treatment', 1),
(37, 100, 'treat1', 1),
(38, 101, 'treat1', 1),
(39, 101, 'treat3', 1),
(40, 101, 'new treatment', 1),
(41, 101, 'all new treatment', 1),
(42, 101, 'onther treatment', 1),
(43, 101, 'treat1', 1),
(44, 101, 'treat3', 1),
(45, 101, 'new treatment', 1),
(46, 101, 'all new treatment', 1),
(47, 101, 'onther treatment', 1),
(48, 102, 'treat1', 1),
(49, 102, 'new treatment', 1),
(50, 102, 'all new treatment', 1),
(51, 102, 'onther treatment', 1),
(52, 102, 'treat3', 1),
(53, 103, 'treat1', 1),
(54, 103, 'new treatment', 1),
(55, 103, 'all new treatment', 1),
(56, 103, 'onther treatment', 1),
(57, 111, 'treat1', 1),
(58, 111, 'new treatment', 1),
(59, 114, 'treat1', 1),
(60, 114, 'treat2', 1),
(61, 114, 'treat3', 1),
(62, 115, 'new treatment', 1),
(63, 116, 'treat1', 1),
(64, 120, 'new treatment', 1),
(65, 120, 'treat2', 1),
(66, 121, 'treat3', 1),
(67, 123, 'treat1', 1),
(68, 124, 'treat1', 1),
(69, 125, 'treat3', 1),
(70, 126, 'all new treatment', 1),
(71, 127, 'treat1', 1),
(72, 128, 'good treatment', 1),
(73, 128, 'test treatment', 1),
(74, 131, 'treat3', 1),
(75, 136, 'treat1', 1),
(76, 136, 'treat3', 1),
(77, 137, 'all new treatment', 1),
(78, 138, 'new treatment', 1),
(79, 108, 'treat3', 1),
(80, 108, 'new treatment', 1),
(81, 108, 'treat1', 1),
(82, 109, 'all new treatment', 1),
(83, 109, 'new treatment', 1),
(84, 140, 'new treatment', 1),
(85, 146, 'treat3', 1),
(86, 149, 'treat1', 1),
(87, 149, 'new treatment', 1),
(88, 153, 'treat2', 1),
(89, 153, 'new treatment', 1),
(90, 154, 'all new treatment', 1),
(91, 154, 'treat3', 1),
(92, 154, 'new treatment', 1),
(93, 155, 'treat2', 1),
(94, 155, 'new treatment', 1),
(95, 157, 'treatments1', 1),
(96, 157, 'treatments2', 1),
(97, 158, 'treat1', 1),
(98, 158, 'new treatment', 1),
(99, 159, 'treat3', 1),
(100, 159, 'new treatment', 1),
(103, 162, 'new treatment', 1),
(104, 171, 'treat2', 1),
(105, 173, 'treat2', 1),
(106, 180, 'treat2', 1),
(107, 180, 'all new treatment', 1),
(108, 180, 'treat1', 1),
(109, 186, 'treat1', 1),
(110, 186, 'treat2', 1),
(111, 186, 'new treatment', 1),
(112, 187, 'treat2', 1),
(113, 187, 'new treatment', 1),
(114, 187, 'all new treatment', 1),
(115, 189, 'treat1', 1),
(116, 189, 'all new treatment', 1),
(117, 189, 'treat2', 1),
(118, 188, 'new treatment', 1),
(119, 181, 'treat2', 1),
(120, 181, 'all new treatment', 1),
(121, 191, 'new treatment', 1),
(122, 192, 'new treatment', 1),
(123, 192, 'treat3', 1),
(124, 193, 'new treatment', 1),
(125, 193, 'treat1', 1),
(126, 193, 'treat2', 1),
(127, 194, 'treat1', 1),
(128, 194, 'treat2', 1),
(129, 194, 'new treatment', 1),
(130, 194, 'all new treatment', 1),
(131, 197, 'treat3', 1),
(132, 202, 'treat1', 1),
(133, 198, 'treat2', 1),
(134, 199, 'treat2', 1),
(135, 199, 'new treatment', 1),
(136, 200, 'all new treatment', 1),
(137, 201, 'treat2', 1),
(138, 203, 'treat2', 1),
(139, 203, 'all new treatment', 1),
(140, 204, 'new treatment', 1),
(141, 205, 'treat2', 1),
(142, 213, 'all new treatment', 1),
(143, 214, 'treat1', 1),
(144, 214, 'new treatment', 1),
(145, 215, 'treat1', 1),
(146, 215, 'treat3', 1),
(147, 216, 'treat3', 1),
(148, 217, 'best new treatment', 1),
(149, 166, 'new treatment', 1),
(150, 223, 'paracetol', 1),
(151, 183, 'treat3', 1),
(152, 225, 'treat1', 1),
(153, 227, 'all new treatment', 1),
(154, 228, 'new treatment', 1),
(155, 230, 'treat2', 1),
(156, 230, 'high treatment', 1),
(157, 240, 'treat1', 1),
(158, 240, 'new treatment', 1),
(159, 240, 'all new treatment', 1),
(160, 240, 'treat3', 1),
(161, 239, 'tr', 1),
(162, 246, 'treat1', 1),
(163, 247, 'new treatment', 1),
(169, 253, 'treat3', 1),
(170, 257, 'treat1', 1),
(171, 278, 'treat2', 1),
(172, 278, 'treat3', 1),
(173, 281, 'paracetamol', 1),
(174, 281, 'amoxilin', 1),
(175, 281, 'ampisilin', 1),
(179, 250, 'treat2', 1),
(180, 251, 'treat1', 1),
(181, 251, 'all new treatment', 1),
(182, 251, 'paracetamol', 1),
(183, 249, 'treat1', 1),
(184, 249, 'paracetamol', 1),
(185, 282, 'paracetamol', 1),
(186, 289, 'paracetamol', 1),
(187, 289, 'all new treatment', 1),
(188, 296, 'treat new', 1),
(189, 306, 'treat2', 1),
(190, 306, 'new treatment', 1),
(191, 306, 'all new treatment', 1),
(192, 307, 'new treatment', 1),
(193, 307, 'amoxilin', 1),
(194, 334, 'treat2', 1),
(195, 161, 'treat2', 1),
(196, 161, 'treat3', 1),
(198, 52, 'treat3', 1),
(199, 350, 'treat2', 1),
(205, 352, '', 1),
(206, 359, 'treat1', 1),
(207, 360, 'treat3', 1),
(208, 376, 'treat3', 1),
(209, 378, 'treat2', 1),
(210, 387, 'new treatment', 1),
(211, 387, 'all new treatment', 1),
(212, 380, 'treat1', 1),
(213, 380, 'newwww', 1),
(214, 384, 'treat3', 1),
(215, 384, 'treat new', 1),
(216, 394, 'treat1', 1),
(217, 394, 'treat2', 1),
(218, 406, 'treat2', 1),
(220, 287, 'treat2', 1),
(221, 287, 'new treatment', 1),
(222, 393, 'treat2', 1),
(223, 393, 'new treatment', 1),
(224, 415, 'treat1', 1),
(225, 415, 'new treatment', 1),
(226, 422, 'treat3', 1),
(227, 422, 'treat1', 1),
(228, 423, 'treat3', 1),
(229, 424, 'treat3', 1),
(230, 424, 'all new treatment', 1),
(231, 425, 'treat1', 1),
(232, 425, 'treat2', 1),
(233, 433, 'new treatment', 1),
(234, 434, 'new treatment', 1),
(235, 439, 'treat1', 1),
(236, 439, 'new treatment', 1),
(237, 439, 'all new treatment', 1),
(238, 441, 'treat1', 1),
(239, 447, 'treat2', 1),
(240, 447, 'all new treatment', 1),
(241, 461, 'treat2', 1),
(242, 461, 'treat3', 1),
(246, 259, 'treat1', 1),
(247, 259, 'treat2', 1),
(248, 259, 'all new treatment', 1),
(249, 473, 'treat1', 1),
(250, 473, 'new treatment', 1),
(251, 495, 'treat1', 1),
(252, 495, 'treat2', 1),
(253, 494, 'treat2', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attachment`
--
ALTER TABLE `attachment`
  ADD PRIMARY KEY (`a_id`),
  ADD KEY `v_id_f` (`v_id_f`),
  ADD KEY `view` (`view`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`exp_id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`p_id`),
  ADD KEY `name` (`fullname`),
  ADD KEY `u_id_f` (`u_id_f`),
  ADD KEY `view` (`view`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`u_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `visit`
--
ALTER TABLE `visit`
  ADD PRIMARY KEY (`v_id`),
  ADD KEY `p_id_f` (`p_id_f`),
  ADD KEY `u_id_f` (`u_id_f`),
  ADD KEY `in_queue` (`in_queue`),
  ADD KEY `visit_date` (`visit_date`),
  ADD KEY `view` (`view`);

--
-- Indexes for table `visit_attr`
--
ALTER TABLE `visit_attr`
  ADD PRIMARY KEY (`attr_id`),
  ADD KEY `v_id_f` (`v_id_f`),
  ADD KEY `v_id_f_2` (`v_id_f`);

--
-- Indexes for table `visit_bcp`
--
ALTER TABLE `visit_bcp`
  ADD PRIMARY KEY (`v_id`),
  ADD KEY `p_id_f` (`p_id_f`),
  ADD KEY `u_id_f` (`u_id_f`),
  ADD KEY `in_queue` (`in_queue`),
  ADD KEY `visit_date` (`visit_date`),
  ADD KEY `view` (`view`);

--
-- Indexes for table `visit_diagnose`
--
ALTER TABLE `visit_diagnose`
  ADD PRIMARY KEY (`vd_id`),
  ADD KEY `v_id_f` (`v_id_f`),
  ADD KEY `view` (`view`);

--
-- Indexes for table `visit_surgery`
--
ALTER TABLE `visit_surgery`
  ADD PRIMARY KEY (`vs_id`),
  ADD KEY `v_id_f` (`v_id_f`);

--
-- Indexes for table `visit_treatment`
--
ALTER TABLE `visit_treatment`
  ADD PRIMARY KEY (`vt_id`),
  ADD KEY `v_id_f` (`v_id_f`),
  ADD KEY `view` (`view`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attachment`
--
ALTER TABLE `attachment`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;

--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `p_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'key: yymmdd[session(autonumber)], autonum: count today visits then start increment, plan failed', AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `visit`
--
ALTER TABLE `visit`
  MODIFY `v_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'visits id of patient', AUTO_INCREMENT=496;

--
-- AUTO_INCREMENT for table `visit_attr`
--
ALTER TABLE `visit_attr`
  MODIFY `attr_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=226;

--
-- AUTO_INCREMENT for table `visit_bcp`
--
ALTER TABLE `visit_bcp`
  MODIFY `v_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'visits id of patient', AUTO_INCREMENT=310;

--
-- AUTO_INCREMENT for table `visit_diagnose`
--
ALTER TABLE `visit_diagnose`
  MODIFY `vd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `visit_surgery`
--
ALTER TABLE `visit_surgery`
  MODIFY `vs_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;

--
-- AUTO_INCREMENT for table `visit_treatment`
--
ALTER TABLE `visit_treatment`
  MODIFY `vt_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attachment`
--
ALTER TABLE `attachment`
  ADD CONSTRAINT `attachment_ibfk_1` FOREIGN KEY (`v_id_f`) REFERENCES `visit` (`v_id`) ON UPDATE CASCADE;

--
-- Constraints for table `patient`
--
ALTER TABLE `patient`
  ADD CONSTRAINT `patient_ibfk_1` FOREIGN KEY (`u_id_f`) REFERENCES `user` (`u_id`) ON UPDATE CASCADE;

--
-- Constraints for table `visit`
--
ALTER TABLE `visit`
  ADD CONSTRAINT `visit_ibfk_2` FOREIGN KEY (`u_id_f`) REFERENCES `user` (`u_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `visit_ibfk_3` FOREIGN KEY (`p_id_f`) REFERENCES `patient` (`p_id`) ON UPDATE CASCADE;

--
-- Constraints for table `visit_attr`
--
ALTER TABLE `visit_attr`
  ADD CONSTRAINT `visit_attr_ibfk_1` FOREIGN KEY (`v_id_f`) REFERENCES `visit` (`v_id`) ON UPDATE CASCADE;

--
-- Constraints for table `visit_diagnose`
--
ALTER TABLE `visit_diagnose`
  ADD CONSTRAINT `visit_diagnose_ibfk_1` FOREIGN KEY (`v_id_f`) REFERENCES `visit` (`v_id`) ON UPDATE CASCADE;

--
-- Constraints for table `visit_surgery`
--
ALTER TABLE `visit_surgery`
  ADD CONSTRAINT `visit_surgery_ibfk_1` FOREIGN KEY (`v_id_f`) REFERENCES `visit` (`v_id`) ON UPDATE CASCADE;

--
-- Constraints for table `visit_treatment`
--
ALTER TABLE `visit_treatment`
  ADD CONSTRAINT `visit_treatment_ibfk_1` FOREIGN KEY (`v_id_f`) REFERENCES `visit` (`v_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
