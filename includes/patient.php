<link rel="stylesheet" type="text/css" href="../css/jquery.timepicker.min.css">
<script type="text/javascript" src="../js/modernizr.custom.79639.js"></script>
<script type="text/javascript" src="../js/jquery.timepicker.min.js"></script>
<div class="contact row">

  <!-- BEGIN SEARCH TABLE PORTLET-->
  
<!-- END SEARCH TABLE PORTLET-->
  <div class="col-md-2">
  </div>
</div>
  <div class="row" >
    <div class="col-md-12" >
      <a href="#editnewpatient" class="btn btn-primary" data-toggle='modal'><i id="newP_btn_icon" class="fa fa-plus-square"></i> add new patient</a>
      <!-- <a href="javascript:;" class="btn btn-danger" id="cleaner"><i class="fa fa-circle-o-notch"></i> clean queue</a> -->
    </div>
  </div>
<div class="row">
   
   <!-- BEGIN SAMPLE TABLE PORTLET-->
   <div class="col-md-12">
   
  <div class="panel panel-info">
    <div class="panel-heading">
      <div class="caption panel-title">
        <span class='fa fa-address-card-o'></span> Patient
      </div>
    </div>
    <div class="panel-body resposive">
  <table class="rtable rtable--flip table table-bordered table-condensed" id="patient" style="width:100%">
  <thead>
    <th>Code</th>
    <th>Full Name</th>
    <th>Sex</th>
    <th>Address</th>
    <th>DOB</th>
    <th>Phone Number</th>
    <th>Occupation</th>
    <th>Latest Visit</th>
    <!-- <th>Next Visit</th> -->
  </thead>

  <tfoot>
    <th>Code</th>
    <th>Full Name</th>
    <th><select class="form-control input-sm">
            <option value=""></option>
            <option value="1">Male</option>
            <option value="2">Female</option>
            <option value="3">Transgender</option>
        </select></th>
    <th>Address</th>
    <th>DOB</th>
    <th>Phone Number</th>
    <th>Occupation</th>
    <th>Latest Visit</th>
    <!-- <th>Next Visit</th> -->
  </tfoot>
  
  </table>

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <form class="form-horizontal" action="index.php" id="addQu">
        <div class="modal-body">
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div class="modal fade" id="editnewpatient" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Add New Patient</h4>
        </div>
          <form class="form-horizontal" action="" id="editnewpatientform">
        <div class="modal-body">
            <input id="p_id" name="p_id" class="form-control" type="hidden">

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-3 control-label" for="fullname">Full Name</label>  
        <div class="col-md-7">
        <input id="fullname" name="fullname" autofocus="autofocus" placeholder="Full Name" class="form-control" required="required" type="text">
           <input type="hidden" id="queue_status" name="queue_status">
        </div>
      </div>

      <!-- Select Basic -->
      <div class="form-group">
        <label class="col-md-3 control-label" for="sex">Sex</label>
        <div class="col-md-7">
          <select id="sex" name="sex" required="required" class="form-control">
            <option value="1">male</option>
            <option value="2">female</option>
            <option value="3">transgender</option>
          </select>
        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-3 control-label" for="address">Address</label>  
        <div class="col-md-7">
        <input id="address" name="address" placeholder="Address" class="form-control"  type="text">
          
        </div>
      </div>

      <div class="form-group">
         <label for="datetimepicker1" class="col-md-3 control-label">Year of Birth</label>
         <div class="col-md-7">
          <div class='input-group date datepick' id='datetimepicker1'>
          <input type='text' autocomplete="off" id="yearOfB" class="form-control" name="date"  />
          <span class="input-group-addon">
           <span class="glyphicon glyphicon-calendar"></span>
          </span>
           </div>
         </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-3 control-label" for="phone">Phone Number</label>  
        <div class="col-md-7">
        <input id="phone" name="phone" autocomplete="off" placeholder="Phone Number" class="form-control" required="required" type="text">
          
        </div>
      </div>

      <div class="form-group">
        <label class="col-md-3 control-label" for="occupation">Occupation</label>  
        <div class="col-md-7">
        <input id="occupation" name="occupation" placeholder="Occupation" class="form-control"  type="text">
          
        </div>
      </div>
      <div class="form-group tobehidden">
             <label for="datetimepicker2" class="col-md-3 control-label">Visit Date</label>
             <div class="col-md-7">
                <div class='input-group date datepick' id='datetimepicker2'>
                <input type='text' autocomplete="off" class="form-control" name="visit_date" id="visit_date" />
                <span class="input-group-addon">
                   <span class="glyphicon glyphicon-calendar"></span>
                </span>
                   </div>
             </div>
          </div>
    		<div class="form-group tobehidden">
    			<label for="datetimepicker5" class="col-md-3 control-label">Visit Time</label>
    			<div class="col-md-7">
    			    <div class='input-group' id='thetime'>
    			        <input type='text' autocomplete="off" class="form-control" name="visit_time" id="datetimepicker5" />
    			        <span class="input-group-addon">
    			            <span class="glyphicon glyphicon-time"></span>
    			        </span>
    			    </div>
    			</div>
    		</div>
      <?php 
        if ($_SESSION['user_id'] == 1) {
          
          $visit_time_n_date = `<div class="form-group">
             <label for="datetimepicker2" class="col-md-3 control-label">Visit Date</label>
             <div class="col-md-7">
                <div class='input-group date datepick' id='datetimepicker2'>
                <input type='text' autocomplete="off" class="form-control" name="visit_date" id="visit_date" />
                <span class="input-group-addon">
                   <span class="glyphicon glyphicon-calendar"></span>
                </span>
                   </div>
             </div>
          </div>
    		<div class="form-group">
    			<label for="datetimepicker5" class="col-md-3 control-label">Visit Time</label>
    			<div class="col-md-7">
    			    <div class='input-group' id='thetime'>
    			        <input type='text' autocomplete="off" class="form-control" name="visit_time" id="datetimepicker5" />
    			        <span class="input-group-addon">
    			            <span class="glyphicon glyphicon-time"></span>
    			        </span>
    			    </div>
    			</div>
    		</div>`;

        // echo $visit_time_n_date;
        }
      ?>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" id="addnewP" name="addnewP" class="btn btn-primary" >Save changes</button>
        </div>
      </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

</div>

    </div>
  </div>
  
  </div>
  
</div>

<!-- <script src="../js/lightbox.js"></script>
<script>
    lightbox.option({
      'positionFromTop': 30
    })
</script> -->
<script type="text/javascript">

// $.fn.dataTableExt.afnFiltering.push(function( oSettings, aData, iDataIndex ) { ... <== old function
/*$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var iMin = $('#min').val();
        var iMax = $('#max').val();
        d1 = new Date(iMin);
        d2 = new Date(iMax);

        iMin = isNaN(d1.getTime())?"":d1.getTime();
        iMax = isNaN(d2.getTime())?"":d2.getTime();

        target_col_data = data[7];

        iDate = new Date(target_col_data).getTime();

    // console.log("iMin = "+iMin);
   //      console.log("iMax = "+iMax);
   //      console.log("iDate = "+iDate);
    // console.log(target_col_data);
        if ( iMin == "" && iMax == "" )
    {
      return true;
    }
    else if ( iMin == "" && iDate < iMax )
    {
      return true;
    }
    else if ( iMin <= iDate && "" == iMax )
    {
      return true;
    }
    else if ( iMin <= iDate && iDate <= iMax )
    {
      return true;
    }
        return false;

    }
);*/
  $(document).ready(function() {

  var columnDefs = [{
    name: "p_id", title:"Code"
  }, {
    name: "fullname", title:"Full Name"
  }, {
    name: "sex", title:"Sex"
  }, {
    name: "address", title:"Address"
  }, {
    name: "dob", title:"Year of Birth"
  }, {
    name: "phone", title:"Phone Number"
  }, {
    name: "occupation", title:"Occupation"
  }, {
    name: "latest_visit", title:"Latest Visit"
  }/*, {
    name: "next_visit", title:"Next Visit"
  }*/];

  var patient;

  patient = $('#patient').DataTable({
    processing: true,
    "pagingType": "full_numbers",
    // "autoWidth": false,
    columns: columnDefs,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "order": [[ 0, "desc" ]],

    "deferRender": true,
    dom: '<"top row"<"col-md-2"l><"col-md-2"B><"col-md-5 rangefilter"><"col-md-3"f>><"row"<"col-md-12" <"row"r>t>><"bottom row"<"col-md-4"i><"paginationright col-md-8"p>><"clear">',
    select: 'single',
    "serverSide": true,
    "ajax": {
            "url": "../includes/ajax/get_patient.php",
            "data": function(d){
              d.type = "load";
              d.minDate = $('#min').val();
              d.maxDate = $('#max').val();
            },
            "type": "POST"
        },
    /*"drawCallback": function( settings ) {

      var api = this.api();

      // Output the data for the visible rows to the browser's console
      console.log( api.rows( {page:'current'} ).data() );
  },*/
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-BUTTONS
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-BUTTONS
buttons: [/*{
      text: 'pdf',
            extend: 'pdfHtml5',
            download: 'open',
            message: 'pdf created by Nawras Clinical Service'
        },*/{
  text: '<i data-toggle="tooltip" title="add to queue" id="add_to_queue_btn_icon" class="fa fa-arrow-circle-o-up fa-1-5x"></i>',
  extend: 'selected',
  action: function(e, dt, node, config) {
    addToQueuClick(this, dt, node, config)
  }
}, {
  extend: 'selected',
  text: '<i data-toggle="tooltip" title="Edit" id="editP_btn_icon" class="fa fa-pencil-square-o fa-1-5x"></i>',
  action: function(e, dt, node, config) {
    var rows = dt.rows({
      selected: true
    }).count();
    //above code just to indicate how many rows are selected
// console.log(rows);
// console.log(dt.rows({selected: true}));
    editClick(this, dt, node, config)
  }
}, {
  extend: 'print',
  text: '<i data-toggle="tooltip" title="Print" class="fa fa-print fa-1-5x"></i>',
    message: 'This print was produced by Nawras Clinical System',
    exportOptions: {
        columns: ':visible'
    }
}, {
  text: '<i data-toggle="tooltip" title="hide columns" class="fa fa-eye-slash fa-1-5x"></i>',
  extend:'colvis'
}]/*,
"createdRow": function ( row, data, index ) {
  console.table(data);
  // console.log(data);
      if (data[2]=='1') {
         // $('td', row).eq(5).addClass('highlight');
      $('td:eq(2)', row).html("<div style='text-align:center'><i class='fa fa-mars'></i></div>"); 
    }else if (data[2]=='2') {
      $('td:eq(2)', row).html("<div style='text-align:center'><i class='fa fa-venus'></i></div>");  
    }else if (data[2]=='3') {
      $('td:eq(2)', row).html("<div style='text-align:center'><i class='fa fa-venus-mars'></i></div>"); 
    }
  }*/
  });

  /*rows returns an array of that row, but rows() returns objects that consist of 
  many data including the array of row*/
/*$('#patient tbody').on( 'click', 'tr', function () {
    console.log( patient.row( this ).data() );
    var d = patient.row( this ).data();
     d[1]="Yusuf";
    patient
        .row( this )
        .data( d )
        .draw();
} );

//custom search
$('#myInput').on( 'keyup', function () {
    patient.search( this.value ).draw();
} );
*/
  //---------Function to Display modal editButton---------

  function editClick(pointer, oTT, button, conf) {
    var adata = oTT.rows({
      selected: true
    });

    var id = adata.data()[0][0];
    var p_fullname = adata.data()[0][1];
    var p_sex = adata.data()[0][2];
    var p_address = adata.data()[0][3];
    var p_dob = adata.data()[0][4];
    var p_phone = adata.data()[0][5];
    var p_occupation = adata.data()[0][6];

    $("#p_id").val(id);
    if (p_sex != null) {
        if (p_sex.indexOf('mars')>0) {
          $("#sex").val('1');
        }else if (p_sex.indexOf('venus')>0) {
          $("#sex").val('2');
        }else {
          $("#sex").val('3');
        }
    }
    if (p_fullname.indexOf('span')>0) {
        start = p_fullname.indexOf(">");
        end = p_fullname.indexOf("</");
        clean_name = p_fullname.slice(start+1, end);
        $("#fullname").val(clean_name);
        var p_queue_status = 1;
    }else{
        $("#fullname").val(p_fullname);
        var p_queue_status = -1;
    }
    
    $("#address").val(p_address);
    $("#yearOfB").val(p_dob);
    $("#phone").val(p_phone);
    $("#occupation").val(p_occupation);
    $("#queue_status").val(p_queue_status);
    $(".tobehidden").hide();
    $('#editnewpatient').find('.modal-title').html('Edit Patient Information');
    $("#editnewpatient").modal("show");

    // $('form :input:enabled:visible:first').focus();
    

  };

  //---------Function to Display modal add to queue---------

  function addToQueuClick(pointer, oTT, button, conf) {

    var adata = oTT.rows({
      selected: true
    });
    // console.log('adata', adata)
    // console.log(adata.data()[0]);
    // console.log(adata.data()[0][0]);
    // console.log(adata.ids()[0]);
    var id = adata.data()[0][0];
    var data = "";
    var patient = pointer;
    var isfemale = adata.data()[0][2].indexOf("venus")>0; // >0 "true" "female", <0 "false" "male or trans"

    var new_visit_date = '';
    if ((adata.data()[0][1]).includes("data-queue='1'")) {
      console.log(adata.data()[0][1]);
      $.ajax({
        url: "../includes/ajax/patient_operation.php",
        type: 'POST',
        async: false,
        data: "p_id="+adata.data()[0][0]+"&new_visit_date=true",
        success: function(result) {
            if (result != null) {
                new_visit_date = result+"";
                // console.log(new_visit_date);
            }
        }
      });
    }else {
      console.log("what~");
    }

    data += "<div class='row top_label_modal col-md-12'>";
    data += "<div class='col-md-3'>" + columnDefs[0].title + " : <p class='label_data'>"+adata.data()[0][0]+"</p></div>";
    data += "<div class='col-md-5'>" + columnDefs[1].title + " : <p class='label_data'>"+adata.data()[0][1]+"</p></div>";
    data += "<div class='col-md-4'>" + "New Visit Date" + " : <p class='label_data'>"+new_visit_date+"</p></div>";
    data += "</div>";

    data += "<input type='hidden' value='" + adata.data()[0][0] + "' name='p_id' >";
    data += "<input type='hidden' value='" + isfemale + "' name='sex' >";
    data +=`<!-- Text input-->
<div class="form-group">
   <label for="datetimepicker2" class="col-md-4 control-label">Visit Date</label>
   <div class="col-md-7">
      <div class='input-group date datepick' id='datetimepicker2'>
      <input type='text' autocomplete="off" class="form-control" name="visit_date" id="visit_date" />
      <span class="input-group-addon">
         <span class="glyphicon glyphicon-calendar"></span>
      </span>
         </div>
   </div>
</div>


    <div class="form-group">
   <label for="datetimepicker4" class="col-md-4 control-label">Visit Time</label>
   <div class="col-md-7">
        <div class='input-group' id='thetime'>
            <input type='text' autocomplete="off" class="form-control" name="visit_time" id="datetimepicker4" />
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-time"></span>
            </span>
        </div>
   </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="maritalst">Marital Status</label>
  <div class="col-md-7">
    <select id="maritalst" name="maritalst" class="form-control">`;
    /*
      nochange added on special reques
      the secretery wants the same old data to be set as new data for any patient
      unless he intentionaly wants to change something
    */
      if(!empty(adata.data()[0][7])){ data += `<option value="-1">no change</option>`; }
      data += `<option value="1">Single</option>
      <option value="2">Married</option>
      <option value="3">Divorced</option>
      <option value="4">Widowed</option>
    </select>
  </div>
</div>`;
data += "<div id='female_n_childnum_div' style='display:none' >"
if (isfemale) {
data +=`<div class="form-group">
  <label class="col-md-4 control-label" for="preg">Pregnant? </label>
  <div class="col-md-7">
  <div class="checkbox toggledboxdiv">
      <input type="checkbox" class="toggledbox" name="preg" id="preg" value="1">
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="breastfeed">Breast Feeding </label>
  <div class="col-md-7">
  <div class="checkbox toggledboxdiv">
      <input type="checkbox" class="toggledbox" name="breastfeed" id="breastfeed" value="1">
    </div>
  </div>
</div>`
}

data +=`<div class='form-group' id='childnumdiv'>
  <label class='col-md-4 control-label' for='childnum'>Number of Children</label>  
  <div class='col-md-7'>
  <span class="input-number-decrement">–</span><input class="input-number" name='childnum' id='childnum' type="number" min="0" max="20"><span class="input-number-increment">+</span>
  </div>
</div>
</div>
<div class='form-group'>
  <label class='col-md-4 control-label' for='other_inf'>Other Information</label>  
  <div class='col-md-7'>
        <textarea id='other_inf' name='other_inf' placeholder='Other Information' class='form-control'></textarea>
  </div>
</div>

<div class='form-group'>
  <label class='col-md-4 control-label' for='visit_fee'>Visit Fee</label>  
  <div class='col-md-7'>
  <div class='input-group'>
  <input id='visit_fee' name='visit_fee' placeholder='fee' class='form-control' min='0' type='number'>
    <span class="input-group-addon">IQD</span>
  </div>
  </div>
</div>`;
    $('#myModal').on('show.bs.modal', function() {
      $('#myModal').find('.modal-title').html('Add to Queue');
      $('#myModal').find('.modal-body').html( data);
      $('#myModal').find('.modal-footer').html("<button type='submit' class='btn btn-primary' id='addToQueuebtn'>Confirm</button>");
    });

    $('#myModal').modal('show');
        $('#datetimepicker2').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: "linked",
            todayHighlight: true,
            weekStart: 5,
            startDate: "+0d"

        });

        $("#datetimepicker4").timepicker({
        	'minTime' : '7:00am',
		    'maxTime' : '12:00am',
		    'step' : 5,
		    'timeFormat' : 'g:i a'
        });

        $('.toggledbox').bootstrapToggle({
            on: 'Yes',
            off: 'No',
            onstyle: 'success'
        });
        inputNumber($('.input-number'));
  };

  // add to queue row functionality
  // $(document).on('click', '#addToQueuebtn', function(e) {
  $(document).on('submit', '#addQu', function(e) {
    // add to queue row function needs to go here
    /*if ($('#visit_date').val()=="" || $('#visit_date').val()==null) {
        return;
    }*/
    
    $("#add_to_queue_btn_icon").removeClass("fa-arrow-circle-o-up");
    $("#add_to_queue_btn_icon").addClass("fa-cog fa-spin");
    var selectedrow_td = $("#patient tr.selected td");
    e.preventDefault();
    var $form = $(e.target);
    $.ajax({
        // url: $form.attr('action'),
        url: "../includes/ajax/patient_operation.php",
        type: 'POST',
        data: $form.serialize()+"&addPtoQ=true",
        success: function(result) {
            if (result == -1) {
                swal({
                    title: "Operation Failed!",
                    text: "The operation failed, please write information correctly when filling the form.",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
            }else if(result == -2) {
                swal({
                    title: "Operation Failed!",
                    text: "This patient already added to queue.",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                
            }else {

                if ($form.serialize().search('visit_date=&')==-1) {
                  var pname = selectedrow_td.eq(1)[0].innerHTML;
                  var newpname = '<span class="inqueue" data-queue="1">'+pname+'</span>';
                  selectedrow_td.eq(1)[0].innerHTML = newpname;
                }
            }

            $("#add_to_queue_btn_icon").removeClass("fa-cog fa-spin");
            $("#add_to_queue_btn_icon").addClass("fa-arrow-circle-o-up");
        }
    });
    $('#myModal').modal('hide');

    // var adata = patient.rows({
    //   selected: true
    // }).data();
// console.log(patient.rows({selected: true}).data());
    // adata.row().remove();
    // patient.row( {selected: true} ).data( adata );
    // patient.draw();
// patient.row({selected: true}).remove().draw(false);
    // patient.context[0].jqXHR.abort(); abort ajax request
    // console.log('data to add to queue : ', adata[0]);
  });

    //number of all columns
    // var numCols = $('#patient').DataTable().columns().nodes().length;
  //number of visible columns
    // var numCols = $('#patient thead th').length;

    
    $("div.rangefilter").html("<div id='baseDateControl'><div class='dateControlBlock'>Between <input type='text' name='min' id='min' class='date rangepick form-control input-sm' value='' /> and <input type='text' name='max' id='max' class='date rangepick form-control input-sm' value=''/> <a href='javascript:;' class='btn default btn-sm blue' id='clear_search'>clear</a></div></div>");
  
// l - Length changing
// f - Filtering input
// t - The Table!
// i - Information
// p - Pagination
// r - pRocessing

    /*$('#min, #max').keyup( function() {
        clinics.draw();});*/

    $("#min").keyup ( function() { patient.draw(); } );
  $("#min").change( function() { patient.draw(); } );
  $("#max").keyup ( function() { patient.draw(); } );
  $("#max").change( function() { patient.draw(); } );

  $("#clear_search").on("click", function() {
    $('#min').val("");
    $('#max').val("");
    patient.draw();
  });

// Setup - add a text input to each footer cell
    $('#patient tfoot th').not(":eq(2)").each( function () {
    // $('#y tfoot th').not(":eq(0),:eq(-1)").each( function () {
        // var title = $.trim($(this).text());
        // console.log($(this));
        // $(this).html( '<input type="text" class="form-control input-sm" placeholder="Search '+$.trim(title)+'" />' );
        $(this).html( '<input type="text" class="form-control input-sm" placeholder="Type Here" />' );
        // var disable='';
        // var placeholder = 'placeholder="Search here"';
        // if (title=='tools' || title=='#') { disable="disabled=disabled readonly "; placeholder='style="background-color:transparent; border-color:transparent; display:none"'; }
          // $(this).html( '<input type="text" '+ disable + ' class="form-control input-sm"'+placeholder+' />' );
    } );
 
    // Apply the search
    var timeout = null;

    patient.columns().every( function () {
        var that = this;
        var ind = this.index();
        //if (this.index()==0 || this.index()==5 ) {return;}//to disable search in first at last col
        $( 'input, select', this.footer() ).on( 'keyup change', function () {
            search_val = this.value;
            // var that = this; 
            // console.log(search_val);
            if (timeout !== null) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function () {
              that
                  .search( search_val )
                  .draw();
            }, 700);
        } );
    } );


    $(document).on('click', '#cleaner', function(event) {
      $('#cleaner > :first-child').addClass('spin-backw');
        patient.ajax.reload(null, false);
    });

    $('#datetimepicker1').datepicker({
            format: " yyyy",
            autoclose: true,
            todayHighlight: true,
            viewMode: "years", 
            minViewMode: "years"
        });

    $("#editnewpatientform").submit(function(event){
        event.preventDefault();
        // here we add or edit the patient, if pid exisits then its edit otherwise its add
        var $form = $(event.target);
        // console.log($.trim($("#p_id").val()));
        if ($.trim($("#p_id").val())=="") {
            var op_type = "&addnewP=true";
            var new_edt = 1;
            $("#newP_btn_icon").removeClass("fa-plus-square");
            $("#newP_btn_icon").addClass("fa-cog fa-spin");
        }else {
            var op_type = "&editP=true";
            var new_edt = 2;
            $("#editP_btn_icon").removeClass("fa-pencil-square-o");
            $("#editP_btn_icon").addClass("fa-cog fa-spin");
        }
        $.ajax({
            // url: $form.attr('action'),
            url: "../includes/ajax/patient_operation.php",
            type: 'POST',
            data: $form.serialize()+op_type,
            success: function(result) {
                if (result == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                }else{
                    patient.ajax.reload(null, false);
                    if (new_edt==1) { //new button
                        $("#newP_btn_icon").removeClass("fa-cog fa-spin");
                        $("#newP_btn_icon").addClass("fa-plus-square");
                    }else{ //edit button
                        $("#editP_btn_icon").removeClass("fa-cog fa-spin");
                        $("#editP_btn_icon").addClass("fa-pencil-square-o");
                    }
                }
            }
        });
        $('#editnewpatient').modal('hide');
    });
    $('#editnewpatient').on('hide.bs.modal', function() {
        $("#p_id").val("");
        $('#editnewpatient').find('.modal-title').html('Add New Patient');
        $(".tobehidden").show();
        $("#editnewpatientform")[0].reset();
    });
//////

    $(document).on("change","#maritalst", function (e) {
        // console.log(e);
        if (this.value>1) {
            $("#female_n_childnum_div").slideDown('slow');
            $("#childnum").val(0);
            $('#childnum').attr('required', 'required');
        }else{
            $("#female_n_childnum_div").slideUp('slow');
            $("#childnum").val('');
            $('#childnum').removeAttr('required');
        }
    });

      $('.btn-group i[data-toggle="tooltip"]').tooltip();



///
$("#datetimepicker5").timepicker({
	'minTime' : '7:00am',
    'maxTime' : '12:00am',
    'step' : 5,
    'timeFormat' : 'g:i a'
});

} );
/*
to add object dynamically
$(document).on('focus',".datepicker_recurring_start", function(){
    $(this).datepicker();
});
*/
</script>
