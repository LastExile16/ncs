<link rel="stylesheet" type="text/css" href="../css/jquery.timepicker.min.css">
<script type="text/javascript" src="../js/jquery.timepicker.min.js"></script>

<style type="text/css">
    .printPdata {
        display: none !important;
    }
    #cancel_patient,
    #edit_fee{
        display: inline-block;
        vertical-align: top;
    }
    #p_fee_editor {
        padding: 3px;
        border: 1px solid #ccc;
        border-radius: 4px;
    }
    .glowing {
        background-color: #004A7F;
        -webkit-border-radius: 10px;
        border-radius: 10px;
        border: none;
        color: #FFFFFF !important;
        cursor: pointer;
        display: inline-block;
        /*font-family: Arial;*/
        /*font-size: 20px;*/
        /*padding: 5px 10px;*/
        text-align: center;
        text-decoration: none;
    }
    .glowing *{
        color: #FFFFFF !important;
    }
    @-webkit-keyframes glowing {
      0% { background-color: #8e44ad; -webkit-box-shadow: 0 0 3px #8e44ad; }
      50% { background-color: #e400ff; -webkit-box-shadow: 0 0 30px #e400ff; }
      100% { background-color: #8e44ad; -webkit-box-shadow: 0 0 3px #8e44ad; }
    }

    @-moz-keyframes glowing {
      0% { background-color: #8e44ad; -moz-box-shadow: 0 0 3px #8e44ad; }
      50% { background-color: #e400ff; -moz-box-shadow: 0 0 30px #e400ff; }
      100% { background-color: #8e44ad; -moz-box-shadow: 0 0 3px #8e44ad; }
    }

    @-o-keyframes glowing {
      0% { background-color: #8e44ad; box-shadow: 0 0 3px #8e44ad; }
      50% { background-color: #e400ff; box-shadow: 0 0 30px #e400ff; }
      100% { background-color: #8e44ad; box-shadow: 0 0 3px #8e44ad; }
    }

    @keyframes glowing {
      0% { background-color: #8e44ad; box-shadow: 0 0 3px #8e44ad; }
      50% { background-color: #e400ff; box-shadow: 0 0 30px #e400ff; }
      100% { background-color: #8e44ad; box-shadow: 0 0 3px #8e44ad; }
    }

    .glowing {
      -webkit-animation: glowing 1500ms infinite;
      -moz-animation: glowing 1500ms infinite;
      -o-animation: glowing 1500ms infinite;
      animation: glowing 1500ms infinite;
    }

    /*GALLERY*/
    .gallery{
        padding: 0px;
        margin-top: -10px;
        margin-bottom: 15px;
    }
    .gallery-1{
        margin-top: 5px;
    }
    .gallery-grid{
        padding-left: 5px;
        padding-right: 5px;
    }
    .gallery-border{
        border: 1px dashed #337AB7;
        padding-bottom: 5px;
    }
    .input-group {
        margin-bottom: 0px;
    }
    .img-wrap {
        position: relative;
        border: 1px solid #d1cfcf;
        border-radius: 4px;
    }
    .img-wrap .close {
        position: absolute;
        top: -3px;
        right: 2px;
        color: red;
        opacity: 0.4;
        z-index: 100;
    }
    .example-image{
        border-radius: 4px;
    }

    
    .img-wrap .close:focus, .img-wrap .close:hover {
        color: #F30909;
        background-color: white;
        border-style: solid;
        border-width: 1px;
        border-color: gray;
        border-top-width: 0px;
        box-shadow: 1px 1px 2px -1px red, -1px -1px 2px -1px red; 
        opacity: 0.8;
    }

    #other_info {
        white-space: pre-line;
        font-weight: bolder;
    }

</style>
<script type="text/javascript" src="./js/modernizr.custom.79639.js"></script>

<div class="contact row">

    <!-- BEGIN SEARCH TABLE PORTLET-->
    
<!-- END SEARCH TABLE PORTLET-->
    <div class="col-md-2">
    </div>
</div>

<?php 
    echo isset($_GET['err_msj']) && !empty($_GET['err_msj'])? "
    <div class='row'>
        <div class='form-group errmsj'>
            <div class='col-md-12'>
                <div class='alert alert-danger alert-dismissable'>
                <button type='button' class='close' aria-hidden='true'>&times; </button>
                    {$_GET['err_msj']}
                </div>
            </div>
        </div>
    </div>":""; 
?>
<div class="row">
     
     <!-- BEGIN SAMPLE TABLE PORTLET-->
     <div class="col-md-12">
        
    <?php  
        $today = date('Y-m-d');
    ?>
    <?php
        if (isset($_POST["err_msj"]) && !empty($_POST["err_msj"])) {
            $error_alert = "<div class='alert alert-danger'>
              <strong>Error!</strong> {$_POST['err_msj']}
            </div>";        
        }
    ?>
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="caption panel-title">
            <div class="row">
                <div class="col-md-3">
                <span class='fa fa-stethoscope table_head_icon'></span><span> Current Patient</span>
                </div>
                <div class="col-md-9" style="direction:rtl">
                    <a class="btn btn-info" data-toggle="modal" href="#currentQ" id="curr_q"><i></i> Current Queue</a>
                </div>
            </div>
            </div>
        </div>
        <div class="panel-body resposive">
        <div class="row">
        <?php
            /*
            p.p_id, p.fullname, p.sex, p.address, p.dob, p.occupation, p.past_hx, p.family_hx, p.drug_hx, p.other_hx,
            v.v_id, v.visit_date, v.followup_date, v.marital_status, v.pregnant, v.breast_feed, v.no_of_child, v.other_info, v.fee
        */
            $patient_set = get_this_p($today);
            if (mysql_num_rows($patient_set)>0) {

                $patient = mysql_fetch_assoc($patient_set);
                $procedure_counter = count(array_filter(explode(', ', $patient['procedures_list'])));
                $age = date('Y')-$patient['dob'];
                $sex ="";
                if ($patient['sex']=='1') {
                    $sex = "<div style='text-align:center'><i class='fa fa-mars'><span style='display:none'>1</span></i></div>"; 
                }else if ($patient['sex']=='2') {
                    $sex = "<div style='text-align:center'><i class='fa fa-venus'><span style='display:none'>2</span></i></div>";    
                }else if ($patient['sex']=='3') {
                    $sex = "<div style='text-align:center'><i class='fa fa-venus-mars'><span style='display:none'>3</span></i></div>";   
                }
                $marital_st = "";
                switch ($patient['marital_status']) {
                    case '1':
                        $marital_st = "Single";
                        break;
                    case '2':
                        $marital_st = "Married";
                        break;
                    case '3':
                        $marital_st = "Divorced";
                        break;
                    case '4':
                        $marital_st = "Widowed";
                        break;
                }
                $pregnant = $patient['pregnant']==1?"Pregnant":"Not Pregnant";
                $breast_feed = $patient['breast_feed']==1?"Yes":"No";
                $p_basic_info = "
                <div class='row'> 
                <div class='col-md-12'>
                    <a class='btn btn-danger' href='javascript:;' id='cancel_patient' data-id='{$patient['v_id']}' data-pid='{$patient['p_id']}'> Cancel Patient <i class='fa fa-minus-circle'></i></a>
                
                    <a class='btn btn-success' href='javascript:;' id='edit_fee' data-id='{$patient['v_id']}' data-pid='{$patient['p_id']}' data-state='1'> <span>Edit Fee</span> <i class='glyphicon glyphicon-erase'></i></a>
                </div>
                </div>
                    <div class='row'>
                        <div class='col-md-3'>
                            <label>Code and Name: </label> <label id='pcode_n_name'>{$patient['p_id']}, {$patient['fullname']}</label>   
                        </div>
                        <div class='col-md-3'>
                            <label>sex: </label> <label id='sex' data-sex={$patient['sex']}>{$sex}</label>
                            <label>; Procedures: </label> <label id='procedure_counter'>{$procedure_counter}</label>
                        </div>
                        <div class='col-md-3'>
                            <label>age: </label> <label id='p_age' data-dob={$patient['dob']}>{$age}</label>
                        </div>
                        <div class='col-md-3'>
                            <label>Marital Status: </label> <label id='marital_st'>{$marital_st}</label>
                        </div>
                    </div>";
                $p_basic_info .= "
                    <div class='row'>
                        <div class='col-md-3'>
                            <label>Occupation: </label> <label id='occupation'>{$patient['occupation']}</label>   
                        </div>
                        <div class='col-md-3'>
                            <label>Fee: </label> 
                            <input type='number' value='{$patient['fee']}' min='0' disabled='disabled' id='p_fee_editor' />
                        </div>
                        <div class='col-md-3'>
                            <label>Address: </label> <label id='address'>{$patient['address']}</label>
                        </div>
                        <div class='col-md-3'>
                            <label>Expected visit was on: </label> <label>{$patient['latest_followup_visit']}</label>
                        </div>
                    </div>";
                $p_basic_info .= "
                    <div class='row'>
                        <div class='col-md-3'>
                            <label>no. of children: </label> <label>{$patient['no_of_child']}</label>
                        </div>
                        <div class='col-md-3'>
                            <label>other info: </label> <p id='other_info'>{$patient['other_info']}</p>  
                        </div>
                    ";
                    //female related info
                $p_basic_info .= $patient['sex']!=1?"
                        <div class='col-md-3'>
                            <label>Breastfeeding: </label> <label>{$breast_feed}</label>
                        </div>
                        <div class='col-md-3'>
                            <label>Pregnancy: </label> <label>{$pregnant}</label>
                        </div>
                    </div>":"</div>";
                $p_basic_info .= "<hr>";
                echo $p_basic_info;

                $hx_button = "<input type='checkbox' id='show_hx' class='toggledbox' >";
                $p_history = $hx_button . "<hr><div id='p_history_info' class='container' style='display:none'>
                <form class='form-horizontal' name='p_hx' id='p_hx' action=''>";

                $p_history .= "
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='past_hx'>Past History</label>  
                    <div class='col-md-7'>
                        <textarea id='past_hx' name='past_hx' disabled='disabled' placeholder='Past History' class='form-control'>{$patient['past_hx']}</textarea>
                    </div>
                </div>

                <div class='form-group'>
                    <label class='col-md-3 control-label' for='family_hx'>Family History</label>  
                    <div class='col-md-7'>
                        <textarea id='family_hx' name='family_hx' disabled='disabled' placeholder='Family History' class='form-control'>{$patient['family_hx']}</textarea>
                    </div>
                </div>
                
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='drug_hx'>Drug History</label>  
                    <div class='col-md-7'>
                        <textarea id='drug_hx' name='drug_hx' disabled='disabled' placeholder='Drug History' class='form-control'>{$patient['drug_hx']}</textarea>
                    </div>
                </div>
                
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='other_hx'>Other History</label>  
                    <div class='col-md-7'>
                        <textarea id='other_hx' name='other_hx' disabled='disabled' placeholder='Other History' class='form-control'>{$patient['other_hx']}</textarea>
                    </div>
                </div>

                <div class='form-group row'>  
                    <div class='col-md-2 col-md-offset-8 ffooter'>
                        <a id='edit_hx' name='edit_hx' class='btn btn-primary col-md-12'>Edit History</a>
                    </div>
                </div>
                <input type='hidden' name='p_id' value='{$patient['p_id']}'>
                ";
                $p_history .= "</form> </div>";

                echo $p_history;
            
        ?>
        </div>
        <div class="clearfix"></div>
        <?php  
            /*
            I need $prev_visit_id_list, to update the surgerylist and procedure list in patient table to be the newest list.
            I send $prev_visit_id_list as hidden input to form_submission to query for the data easily.
            */
            $prev_visit_id_list = 0;
            $tabs_list = "No prev visits";
            $tab_content = "";
            $prev_visits_list_set = get_prev_visits_list($patient['p_id']);
            if (mysql_num_rows($prev_visits_list_set)>0) {
                $tabs_list = "";
                $prev_visit_id_array=array();
                while ($prev_visit = mysql_fetch_assoc($prev_visits_list_set)) {
                    $prev_visit_id_array[] = $prev_visit['v_id'];
                    //return all related attachments
                    $attachments_output = "no attachments";
                    $attachment_set = get_attachment($prev_visit['v_id']);
                    if (mysql_num_rows($attachment_set)>0) {
                    $attachments_output = "
                    <div class='gallery gallery-border row'>
                         <div class='container col-md-12'>
                             <div class='gallery-bottom'>";
                             $i = 0;
                             $flag=true;
                        while ( $attachment = mysql_fetch_assoc($attachment_set)) {
                           $datavideo='';
                            $alt='alt="broken image"';
                            if(!preg_match('/^.*\.(jpg|jpeg|png|gif)$/i', $attachment['filename'])) {
                               $datavideo='data-video=true';
                               $alt='alt="Video file"';
                            }
                            //4 pics in a row (by creating a new gallery-1)
                            if ($i % 4 == 0) {
                                $attachments_output .=  "<div class='gallery-1'>";
                                $flag = true;
                            }
                            $attachments_output .=  "<div class='col-md-3 gallery-grid'>
                                            <div class='img-wrap'><span data-id='{$attachment['a_id']}' file-name='{$attachment['filename']}' id='close_{$attachment['a_id']}' class='close delete-image'>&times;</span>
                                                <a class='example-image-link' href='uploads/{$attachment['filename']}' data-lightbox='album_{$prev_visit['v_id']}' {$datavideo} >
                                                    <img class='example-image' src='uploads/{$attachment['filename']}' {$alt} />
                                                </a>
                                                </div>
                                            </div>";
                                // $attachments_output .= "<span>i: {$i}, flag: $flag </span>";
                            if (++$i % 4 == 0) {
                                $attachments_output .= "<div class='clearfix'></div></div>";
                                $flag = false;
                            }
                            // $i++;
                        }

                        //if <div gallery-1> was opened but not closed then close it. 
                        if ($flag) {
                            $attachments_output .= "<div class='clearfix'></div></div>";
                        }
                    $attachments_output .= "</div>
                                 </div>
                            </div>";

                    }

                    //now create the previous visits form with there values
                    /*<li>
                            <a href="#messages" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1" aria-expanded="false">Messages</a>
                        </li>

                        <li>
                            <a href="#settings" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2" aria-expanded="false">Settings</a>
                        </li>
                            
                        SELECT v.v_id, v.visit_date, v.followup_date, v.marital_status, v.pregnant, v.breast_feed, v.no_of_child, v.other_info, v.fee, v.chief_comp_n_dur, v.examination, v.investigation, v.note, v.treatment_note
                        */

                    $tabs_list .= "
                        <li>
                            <a href='#tabpane{$prev_visit['v_id']}' role='tab' class='hxtab_btn' data-id='{$prev_visit['v_id']}' id='dropdown{$prev_visit['v_id']}-tab' data-toggle='tab' aria-controls='dropdown1' aria-expanded='false'>{$prev_visit['visit_date']}</a>
                        </li>
                        ";
                    $tab_content .= "<div role='tabpanel' class='tab-pane fade in' id='tabpane{$prev_visit['v_id']}'>";

                        include './includes/prev_visit_form.php';
                        $tab_content .= $prev_p_basic_info;
                        $tab_content .= $prev_data;
                    $tab_content .= '</div>';
                }
                $prev_visit_id_list = implode(', ', array_filter($prev_visit_id_array));
            }

            $tabs_bar = '
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" id="myTabDrop1" data-toggle="dropdown" > 
                        Previous Visits <span class="caret"> </span>
                    </a>

                    <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents"> ';
            $tabs_bar .= $tabs_list;
                        
            $tabs_bar .= '</ul>
                    </li>';
        ?>
        <div class="row">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#new_data" aria-controls="new_data" role="tab" data-toggle="tab"><?php echo $today; ?></a></li>

                <?php echo $tabs_bar; ?>
            </ul>

    <?php
        $options = array("","","");
        $option_attr1_0="selected='selected'";
        $option_attr1_1="";
        $option_attr1_2="";
        $option_attr1_3="";
        $option_tags_attr2 = "";
        $option_tags_attr3 = "";
        $style_attr2="style='display:none'";
        $style_attr3="style='display:none'";
        $attr_list = get_attr_list($patient['v_id']);
        if (mysql_num_rows($attr_list)>0) {
                
            $i=0;
            while($attributes =mysql_fetch_assoc( $attr_list))
            {
                $options[$i] = $attributes['attr'];
                $i++;
            }
            if ($options[0]=='Cardiac') {
                $option_attr1_1="selected='selected'";
                $option_attr1_0 = '';
                $style_attr2 = '';
                $style_attr3 = '';

                // $data2 = array( array("name" => "Adult", "value" => "Adult") , array("name" => "Pediatric", "value" => "Pediatric"));
                if($options[1]=='Adult') {
                    $selected2_0 = "selected='selected'";
                    $data3 = array("CABG","Valve","Aorta");
                    
                    $selected3 = '';
                    $j=0;
                    while ( $j<3) {
                        $selected3 = $options[2] == $data3[$j]?"selected='selected'":"";
                        $option_tags_attr3 .= "<option {$selected3} value='{$data3[$j]}'>{$data3[$j]}</option>";
                        $j++;
                    }
                }
                elseif($options[1]=='Pediatric') {
                    $selected2_1 = "selected='selected'";
                    $data3 = array( "ASD", "VSD", "TOF", "Sub Aortic ridge", "PA-Banding", "Glenn", "CoAractation", "PDA");
                    $selected3 = '';
                    $j=0;
                    while ( $j<8) {
                        $selected3 = $options[2] == $data3[$j]?"selected='selected'":"";
                        $option_tags_attr3 .= "<option {$selected3} value='{$data3[$j]}'>{$data3[$j]}</option>";
                        $j++;
                    }
                }
                $option_tags_attr2 = "<option {$selected2_0} value='Adult'>Adult</option>";
                $option_tags_attr2 .= "<option {$selected2_1} value='Pediatric'>Pediatric</option>";

            }elseif ($options[0]=='Vascular') {

                $option_attr1_2="selected='selected'";
                $option_attr1_0 = '';
                $style_attr2 = '';
                $data2 = array('Arterial Diseases', 'Peripheral Vascular', 'Aneurysm', 'AV-Fistula Creation', 'Venous Diseases');
                $selected2 = '';
                $h=0;
                while ( $h<5) {
                    $selected2 = $options[1] == $data2[$h]?"selected='selected'":"";
                    $option_tags_attr2 .= "<option {$selected2} value='{$data2[$h]}'>{$data2[$h]}</option>";
                    $h++;
                }

                if($options[1]=="Venous Diseases")
                {
                    $style_attr3 = '';
                    $selected3_1 = '';
                    $selected3_0 = '';
                    if($options[2]="DVT") {
                        $selected3_1 = "selected='selected'";
                    }else {
                        $selected3_0 = "selected='selected'";
                    }
                    $option_tags_attr3 = "<option {$selected3_0} value='Varicose Veins'>Varicose Veins</option>";
                    $option_tags_attr3 .= "<option {$selected3_1} value='DVT'>DVT</option>";
                }
            }elseif ($options[0]=='Thoracic') {
                $option_attr1_3="selected='selected'";
                $option_attr1_0 = '';
                $style_attr2 = '';
                $data3 = array("Lung Rejection Surgery","Decortication","Hydatid. Lung");
                $selected3 = '';
                $j=0;
                while ( $j<3) {
                    $selected3 = $options[1] == $data3[$j]?"selected='selected'":"";
                    $option_tags_attr2 .= "<option {$selected3} value='{$data3[$j]}'>{$data3[$j]}</option>";
                    $j++;
                }
            }else{
                $option_attr1_0="selected='selected'";
                $style_attr2 = '';
                $style_attr3 = '';
            }
        }
    ?>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="new_data">
                    <div class='row'>
                        <!-- <form method="POST" action="./includes/print_patient_info.php"> -->
                            <!-- <button id="printP" name="printP" data-vid="" data-pid="" class='btn btn-default glowing printPdata'>Print </button> -->
                        <!-- </form> -->
                        <!-- if doctor asked to remove the 'required' attribute then you can just add initial values to inputs like 'no duration or no examination' ...  -->
                        <form class='form-horizontal' enctype='multipart/form-data' action='./includes/form_submitions.php?to=all_p' id='editnewpatientform' method="POST" onsubmit="return checkfiles('attachfiles')"  data-ajax="false">
                             <input id='p_id' name='p_id' class='form-control' type='hidden' value="<?php echo $patient['p_id']; ?>" >
                             <input id='v_id' name='v_id' class='form-control' type='hidden' value="<?php echo $patient['v_id']; ?>" >
                             <input id='sex' name='sex' class='form-control' type='hidden' value="<?php echo $patient['sex']; ?>" >
                             <input id='marital_status' name='marital_status' class='form-control' type='hidden' value="<?php echo $patient['marital_status']; ?>" >
                             <input id='pregnant' name='pregnant' class='form-control' type='hidden' value="<?php echo $patient['pregnant']; ?>" >
                             <input id='breast_feed' name='breast_feed' class='form-control' type='hidden' value="<?php echo $patient['breast_feed']; ?>" >
                             <input id='no_of_child' name='no_of_child' class='form-control' type='hidden' value="<?php echo $patient['no_of_child']; ?>" >
                             
                             <input id='visit_date' name='visit_date' class='form-control' type='hidden' value="<?php echo $patient['visit_date']; ?>" >
                             <input id='prev_visit_id_list' name='prev_visit_id_list' class='form-control' type='hidden' value="<?php echo $prev_visit_id_list; ?>" >
                            
                            <!-- visit attributes -->
                            <div class="form-group">
                              <label class="col-md-3 control-label" for="attr1">Attribute1</label>
                              <div class="col-md-7">
                                <!-- data-visits=1: its current form, 2: its prev form -->
                                <select data-visits='1' data-vid="<?php echo $patient['v_id']; ?>" id="attr1" name="attr1" class="form-control attr1Class">
                                   <option id='0' <?php echo $option_attr1_0; ?> value='0'>None</option>
                                   <option id='1' <?php echo $option_attr1_1; ?> value='Cardiac'>Cardiac</option>
                                   <option id='2' <?php echo $option_attr1_2; ?> value='Vascular'>Vascular</option>
                                   <option id='3' <?php echo $option_attr1_3; ?> value='Thoracic'>Thoracic</option>
                             </select>
                              </div>
                            </div>

                            <div class="form-group" id="attr2list" <?php echo $style_attr2; ?> >
                              <label class="col-md-3 control-label" for="attr2">Attribute2</label>
                              <div class="col-md-7">
                                <!-- data-id=1: its current form, 2: its prev form -->
                                <select data-visits='1' id="attr2" name="attr2" class="form-control attr2Class">
                                <?php echo $option_tags_attr2; ?>
                             </select>
                              </div>
                            </div>


                            <div class="form-group" id="attr3list" <?php echo $style_attr3; ?> >
                              <label class="col-md-3 control-label" for="attr3">Attribute3</label>
                              <div class="col-md-7">
                                <!-- data-id=1: its current form, 2: its prev form -->
                                <select data-visits='1' id="attr3" name="attr3" class="form-control attr3Class">
                                <?php echo $option_tags_attr3; ?>
                             </select>
                              </div>
                            </div>
                            <!-- end of visit attributes -->

                            <!-- Text input-->
                            <div class='form-group'>
                              <label class='col-md-3 control-label' for='chief_com_n_duration'>chief complain and present history</label>  
                              <div class='col-md-7'>
                              <input id='chief_com_n_duration' name='chief_com_n_duration' autofocus='autofocus' placeholder='chief complain & present history' class='form-control' type='text'>
                                
                              </div>
                            </div>
                            <!-- Text input-->
                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='examination'>Examination</label>  
                                <div class='col-md-1'>
                                    <small id="bp_label" class="form-text text-muted">BP</small>
                                    <input id='bp' name='examination[]' placeholder='BP' class='form-control' type='text'>
                                </div>
                                <div class='col-md-1'>
                                    <small id="pr_label" class="form-text text-muted">PR</small>
                                    <input id='pr' name='examination[]' placeholder='PR' class='form-control' type='text'>
                                </div>
                                <div class='col-md-1'>
                                    <small id="chest_label" class="form-text text-muted">Chest</small>
                                    <input id='chest' name='examination[]' placeholder='Chest' class='form-control' type='text'>
                                </div>
                                <div class='col-md-1'>
                                    <small id="heart_label" class="form-text text-muted">Heart</small>
                                    <input id='heart' name='examination[]' placeholder='Heart' class='form-control' type='text'>
                                </div>
                                <div class='col-md-1'>
                                    <small id="abdomin_label" class="form-text text-muted">Abdomin</small>
                                    <input id='abdomin' name='examination[]' placeholder='Abdomin' class='form-control' type='text'>
                                </div>
                                <div class='col-md-2'>
                                    <small id="other_label" class="form-text text-muted">other</small>
                                    <input id='other' name='examination[]' placeholder='other' class='form-control' type='text'>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='investigation'>Investigation</label>  
                                <div class='col-md-7'>
                                    <div class="input-group">
                                        <input id='investigation' name='investigation' autocomplete='false' placeholder='use comma to add multiple investigations' class='form-control' type='text'>
                                        <a class="input-group-addon" id="inv_print" href="javascript:;">
                                            <span class="glyphicon glyphicon-print"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='investigation_note'>Investigation Notes</label>  
                                <div class='col-md-7'>
                                    <textarea id='investigation_note' name='investigation_note' placeholder='investigation note' class='form-control'></textarea>
                                </div>
                            </div>

                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='diagnosis'>Diagnosis</label>  
                                <div class='col-md-7'>
                                    <input id='diagnosis' name='diagnosis' autocomplete="off" placeholder='diagnosis not accepted here CANNOT be stored' class='form-control' type='text'>
                                </div>
                            </div>
                            
                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='treatment'>Treatments</label>  
                                <div class='col-md-7'>
                                    <input id='treatment' name='treatment' autocomplete="off" placeholder='treatments not accepted here CANNOT be stored' class='form-control' type='text'>
                                </div>
                            </div>

                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='treatment_note'>Treatments Notes</label>  
                                <div class='col-md-7'>
                                    <textarea id='treatment_note' name='treatment_note' placeholder='Notes will be printed along with the slip' class='form-control'></textarea>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label" for="end_visit"></label>
                                <div class="col-md-7">
                                    <input type="button" id="prinslip2" name="prinslip" class="btn btn-primary prinslip" value="test">
                                </div>
                            </div> -->
                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='surgery'>Surgeries</label>  
                                <div class='col-md-7'>
                                    <input id='surgery' name='surgery' autocomplete="off" placeholder='Surgeries not accepted here CANNOT be stored' class='form-control' type='text'>
                                </div>
                            </div>

                            <div class="form-group">
                               <label for="datetimepicker2" class="col-md-3 control-label">Surgery Date</label>
                               <div class="col-md-7">
                                    <div class='input-group date rangepick' id='datetimepicker2'>
                                        <input type='text' autocomplete="off" id="surgery_date" class="form-control" name="surgery_date" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                               </div>
                            </div>
                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='surgery_note'>surgery Notes</label>  
                                <div class='col-md-7'>
                                    <textarea id='surgery_note' name='surgery_note' placeholder='Surgery Notes' class='form-control'></textarea>
                                </div>
                            </div>
                            <!-- procedures -->
                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='procedure'>Other Procedure</label>  
                                <div class='col-md-7'>
                                    <input id='procedure' name='procedure' autocomplete="off" placeholder='Procedures not accepted here CANNOT be stored' class='form-control' type='text'>
                                </div>
                            </div>

                            <div class="form-group">
                               <label for="datetimepicker3" class="col-md-3 control-label">Procedure Date</label>
                               <div class="col-md-7">
                                    <div class='input-group date rangepick' id='datetimepicker3'>
                                        <input type='text' autocomplete="off" id="procedure_date" class="form-control" name="procedure_date" value="<?php echo date('Y-m-d'); ?>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                               </div>
                            </div>
                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='procedure_note'>Procedure Notes</label>  
                                <div class='col-md-7'>
                                    <textarea id='procedure_note' name='procedure_note' placeholder='procedure note' class='form-control'></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                               <label for="attachment" class="col-md-3 control-label">Attachments</label>
                               <div class="col-md-7">
                                  <div class="input-group">
                                      <label class="input-group-btn">
                                            <span class="btn btn-primary">
                                              Browse&hellip; <input type="file" id="attachfiles" name="files[]" accept="image/gif,image/jpeg,image/jpg,image/pjpeg,image/png,image/x-png,.jpeg,video/*" style="display: none;" multiple="multiple">
                                              
                                            </span>
                                      </label>
                                      <input type="text" class="form-control" readonly="readonly" disabled="disabled">
                                  </div>
                               </div>
                            </div>
                            
                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='note'>Notes</label>  
                                <div class='col-md-7'>
                                    <textarea id='note' name='note' placeholder='note' class='form-control'></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                               <label for="datetimepicker1" class="col-md-3 control-label">Follow-up Visit</label>
                               <div class="col-md-7">
                                    <div class='input-group date datepick' id='datetimepicker1'>
                                        <input type='text' autocomplete="off" id="followup_date" class="form-control" name="followup_date" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                               </div>
                            </div>
                            <div class="form-group">
                               <label for="datetimepicker4" class="col-md-3 control-label">Visit Time</label>
                               <div class="col-md-7">
                                    <div class='input-group' id='thetime'>
                                        <input type='text' autocomplete="off" class="form-control" name="followup_time" id="datetimepicker4" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                        </span>
                                    </div>
                               </div>
                            </div>
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="end_visit"></label>
                                <div class="col-md-7">
                                    <button id="end_visit" name="end_visit" class="btn btn-primary">Save Changes</button>
                                </div>
                            </div>
                            
                        </form>
                        </div>
                </div>
                <?php echo $tab_content; ?>
            </div>
        </div>

          <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save changes</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
<?php 
    }//mysql_num_rows IF 
    else{
        echo "<div class='row'>
                <div class='form-group errmsj row'>
                    <div class='col-md-12'>
                        <div class='alert alert-warning'>
                            Sorry! No patient available right now.
                        </div>
                    </div>
                </div>
            </div>";
    }
?>
          <div class="modal fade" id="currentQ" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Current Queue</h4>
                </div>
                <div class="modal-body">
                  <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
</div>

        </div>
    </div>
    </div>
    

<script>
    lightbox.option({
      'positionFromTop': 30,
      'hasVideo': true
    })
</script>
<script type="text/javascript">

$(document).ready(function() {

    $('.toggledbox').bootstrapToggle({
        on: "<i class='fa fa-chevron-circle-up'></i> Hide Hx",
        off: "<i class='fa fa-chevron-circle-down'></i> Show Hx",
        onstyle: 'success'
    });

    $("#show_hx").on('change', function(event) {
        $("#p_history_info").slideToggle('slow');
    });

    $(document).on('click','#edit_hx', function(event) {
        event.preventDefault();
        $("#p_hx :input").not('a').prop("disabled", false);
        $("#p_hx :input").not('a').addClass('hxform_en');
        // $("#edit_hx").removeClass('btn-primary');
        // $("#edit_hx").addClass('btn-danger');
        // $("#edit_hx").text("Upadte");
        // $("#edit_hx").attr('id', 'update_hx');
        // $("#edit_hx").attr('name', 'update_hx');
        $("#edit_hx").remove();

        $(".ffooter").html("<button id='update_hx' name='update_hx' class='btn btn-danger col-md-12'><i class='fa fa-pencil'> </i> Update</button>");
    });

    $('#edit_fee').on('click', function(event) {
        /*
            1: edit
            2: save
        */
        var state = $(this);
        if( state.data('state') == '1') {
            state.data('state', '2');
            $("#p_fee_editor").prop("disabled", false);
            $("#p_fee_editor").addClass('hxform_en');
            // $("#edit_fee").removeClass('glowing');
            $("#edit_fee").removeClass('btn-success');
            $("#edit_fee").addClass('btn-warning');
            $("#edit_fee > span").text("Save");

        }else {
            vid = state.data('id');
            pid = state.data('pid');
            $("#edit_fee i").removeClass("glyphicon glyphicon-erase");
            $("#edit_fee i").addClass("fa fa-cog fa-spin");
            $("#edit_fee").prop("disabled", true);
             $.ajax({
            url: "./includes/ajax/dr_p_operation.php",
            type: 'POST',
            data: "pid="+pid+"&vid="+vid+"&new_fee="+$("#p_fee_editor").val()+"&update_fee_data=true",
            success: function(result) {
                if (result == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                }else {
                    $("#edit_fee i").removeClass("fa fa-cog fa-spin");
                    $("#edit_fee i").addClass("glyphicon glyphicon-erase");
                    
                    state.data('state', '1');
                    $("#p_fee_editor").prop("disabled", true);
                    $("#p_fee_editor").removeClass('hxform_en');
                    $("#edit_fee").removeClass('btn-warning');
                    $("#edit_fee").addClass('btn-success');
                    $("#edit_fee > span").text("Edit Fee");
                }

            }
        });     
            
        }
        // $("#edit_fee").attr('id', 'update_hx');
        // $("#edit_fee").attr('name', 'update_hx');
        
    });

    $(document).on('submit', '#p_hx', function(e) {
        e.preventDefault();
        $("#update_hx i").removeClass("fa-pencil");
        $("#update_hx i").addClass("fa-cog fa-spin");
        $("#update_hx").prop("disabled", true);
        var $form = $(e.target);
        $.ajax({
            // url: $form.attr('action'),
            url: "./includes/ajax/dr_p_operation.php",
            type: 'POST',
            data: $form.serialize()+"&update_hx=true",
            success: function(result) {
                if (result == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                }else {
                    $("#update_hx i").removeClass("fa-cog fa-spin");
                    $("#update_hx i").addClass("fa-pencil");
                    $("#p_hx :input").not('button').prop("disabled", true);
                    $("#p_hx :input").not('button').removeClass('hxform_en');

                    $("#update_hx").remove();
                    $(".ffooter").html("<a id='edit_hx' name='edit_hx' class='btn btn-primary col-md-12'>Edit History</a>");
                }

            }
        });
        
    });

    $('#currentQ').on('show.bs.modal', function() {
        $('#currentQ').find('.modal-body').html( "<div class='row' style='text-align: center;'><div class='fa fa-spinner fa-spin fa-4x'></div></div>");

        $.ajax({
            url: "./includes/ajax/dr_queue_list.php",
            type: 'POST',
            data: "&curr_q=true",
            success: function(result) {
                if (result == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                    $("#currentQ").hide();
                }else {
                    $('#currentQ').find('.modal-body').html(result);
                    $('#queue').DataTable();
                }
            }
        });
    });

    var diagnosis_list=[{value:'red'}, {value:'red'}, {value:'red'}, {value:'red'}];
    var treatment_list=[{value:'red'}, {value:'red'}, {value:'red'}, {value:'red'}];
    var surgery_list=[{value:'red'}, {value:'red'}, {value:'red'}, {value:'red'}];
    var procedure_list=[{value:'red'}, {value:'red'}, {value:'red'}, {value:'red'}];
    var diagnosis_engine = "";
    var treatment_engine = "";
    var surgery_engine = "";
    var procedure_engine = "";

    $.ajax({
        url: "./includes/ajax/treats_diagns_list.php",
        type: 'POST',
        data: "&treat_n_diagn=true",
        success: function(result) {
            // console.log(result);
            if (result == -1) {
                swal({
                    title: "Loading Data Failed!",
                    text: "Loading necessary data has failed, please contact website administrator!",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
            }else {
                list = JSON.parse(result);
                // console.log(list[0]);
                // console.log(list[1]);
                diagnosis_list = list[0];
                treatment_list = list[1];
                surgery_list = list[2];
                procedure_list = list[3];

                diagnosis_engine = new Bloodhound({
                    local: list[0],
                    datumTokenizer: function(d) {
                        return Bloodhound.tokenizers.whitespace(d.value);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                
                var diagn_init = diagnosis_engine.initialize();
                diagn_init
                .done(function() { console.log('ready to go!'); })
                .fail(function() { 
                                    swal({
                                        title: "Initializing Data Failed!",
                                        text: "Loading necessary data has failed, please contact website administrator!",
                                        type: "error",
                                        confirmButtonColor: "#C9302C"
                                    }); 
                                });

                $('#diagnosis').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: diagnosis_engine.ttAdapter() }
                    ]
                } );
                // used in history of patient
                $('.prev_lists_dig').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: diagnosis_engine.ttAdapter() }
                    ]
                });

                treatment_engine = new Bloodhound({
                    local: list[1],
                    datumTokenizer: function(d) {
                        return Bloodhound.tokenizers.whitespace(d.value);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });

                var treat_init = treatment_engine.initialize();
                treat_init
                .done(function() { console.log('ready to go!'); })
                .fail(function() { 
                                    swal({
                                        title: "Initializing Data Failed!",
                                        text: "Loading necessary data has failed, please contact website administrator!",
                                        type: "error",
                                        confirmButtonColor: "#C9302C"
                                    }); 
                                });
                $('#treatment').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: treatment_engine.ttAdapter() }
                    ]
                });
                // used in history of patient
                $('.prev_lists_treat').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: treatment_engine.ttAdapter() }
                    ]
                });

                surgery_engine = new Bloodhound({
                    local: list[2],
                    datumTokenizer: function(d) {
                        return Bloodhound.tokenizers.whitespace(d.value);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
												                
                var surg_init = surgery_engine.initialize();
                surg_init
                .done(function() { console.log('ready to go!'); })
                .fail(function() { 
                                    swal({
                                        title: "Initializing Data Failed!",
                                        text: "Loading necessary data has failed, please contact website administrator!",
                                        type: "error",
                                        confirmButtonColor: "#C9302C"
                                    }); 
                                });

                $('#surgery').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: surgery_engine.ttAdapter() }
                    ]
                });

                // used in history of patient
                $('.prev_lists_surg').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: surgery_engine.ttAdapter() }
                    ]
                });

                procedure_engine = new Bloodhound({
	                local: list[3],
	                datumTokenizer: function(d) {
	                    return Bloodhound.tokenizers.whitespace(d.value);
	                },
	                queryTokenizer: Bloodhound.tokenizers.whitespace
	            });

	            var prd_init = procedure_engine.initialize();
	            prd_init
	            .done(function() { console.log('ready to go!'); })
	            .fail(function() { 
	                                swal({
	                                    title: "Initializing Data Failed!",
	                                    text: "Loading necessary data has failed, please contact website administrator!",
	                                    type: "error",
	                                    confirmButtonColor: "#C9302C"
	                                }); 
	                            });
	            $('#procedure').tokenfield({
	                typeahead: [
	                    {hint: true,highlight: true}, 
	                    { source: procedure_engine.ttAdapter() }
	                ]
	            });
	            // used in history of patient
	            $('.prev_lists_prd').tokenfield({
	                typeahead: [
	                    {hint: true,highlight: true}, 
	                    { source: procedure_engine.ttAdapter() }
	                ]
	            });
            }
        }
    });

    //prevent duplicate tokens
    $('#diagnosis, #treatment, #surgery, #procedure').on('tokenfield:createtoken', function (event) {
        var existingTokens = $(this).tokenfield('getTokens');
        $.each(existingTokens, function(index, token) {
            if (token.value === event.attrs.value)
                event.preventDefault();
            // $('#diagnosis').parent().parent().addClass('has-error');

            // i should delete duplicate if get method returns it
        });
    });

    //prevent duplicate tokens, used in history of patient
    $('.prev_lists_dig, .prev_lists_treat, .prev_lists_surg, .prev_lists_prd').on('tokenfield:createtoken', function (event) {
        var existingTokens = $(this).tokenfield('getTokens');
        $.each(existingTokens, function(index, token) {
            if (token.value === event.attrs.value)
                event.preventDefault();
            // $('#diagnosis').parent().parent().addClass('has-error');

            // i should delete duplicate if get method returns it
        });
    });

    // file selection
    $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
    });
    $(':file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
          log = numFiles > 1 ? numFiles + ' files selected' : label;
        if( input.length ) {
          input.val(log);
          // console.log(log);
        } 
        else {
          if( log ) alert(log);
        }
    });//end of file selection

    // image delete
    $('.delete-image').click(function()
    {

        var id = $(this).data('id');
        var filename = $(this).attr('file-name');
        var parent = $(this).parent().parent();
        swal({
            title: "DELETE!?",
            text: "You will not be able to recover the attachment!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "DELETE!"
        },
        function(){
            // console.log(id);
            var data = 'image_delete_id=' + id +'&filename='+filename;

            $.ajax(
            {
               type: "POST",
               url: "./includes/delete_attachment.php",
               data: data,
               cache: false,
            
               success: function(data1)
               {
                    if (data1=="") {
                        parent.fadeOut('slow', function() {$(this).remove();});
                    }else{
                    alert(data1);
                    }
               },
               
               statusCode: 
               {
                    404: function() {
                    alert('ERROR 404 \npage not found');
                    }
               }

            });
        
        });             
        
    }); //end of image delete

clicked = false;
    $(".enabler").on('click', function(event) {
        // event.preventDefault();
        // $(this).off('click');
if (clicked) { return false; }
else{
        var evt_btn = $(this);
        var prev_v_id = evt_btn.data("id");
        var prev_name = evt_btn.data("name");
        var new_data = "";
        //special needed vars
        var surg_date = "";
        var surg_note = "";
        var prd_date = "";
        var prev_p_id = "";

        var attr1 = "";
        var attr2 = "";
        var attr3 = "";

        if(prev_name<10){
            new_data = evt_btn.parent().prevAll("input,textarea").val();
            if (prev_name == 7) {
            	if ($("#procedure_date-"+prev_v_id).val()==null || $("#procedure_date-"+prev_v_id).val()=="") {
	                swal({
	                    title: "Oops!",
	                    text: "please fill DATE correctly.",
	                    type: "error",
	                    confirmButtonColor: "#C9302C"
	                });
	                return false;
	            }
	            else{
	            	new_data_mayhavecomma = $("#procedure-"+prev_v_id).val();
                    new_data = new_data_mayhavecomma.replace(/(^,)|(,$)/g, "");
	                prd_date = $("#procedure_date-"+prev_v_id).val();
                    prev_p_id = evt_btn.data("pid");
	            }
            }
        }else if (prev_name==16) {
            new_data_mayhavecomma = $("#diagnosis-"+prev_v_id).val();
            new_data = new_data_mayhavecomma.replace(/(^,)|(,$)/g, "");
        }else if(prev_name==17) {
            new_data_mayhavecomma = $("#treatment-"+prev_v_id).val();
            new_data = new_data_mayhavecomma.replace(/(^,)|(,$)/g, "");
        }else if(prev_name==18) {
            // sometimes when update button pressed without editing the surgery data, it gets a trailing comma
            new_data_mayhavecomma = $("#surgery-"+prev_v_id).val();
            new_data = new_data_mayhavecomma.replace(/(^,)|(,$)/g, "");
            if ($("#surgery_date-"+prev_v_id).val()==null || $("#surgery_date-"+prev_v_id).val()=="") {
                swal({
                    title: "Oops!",
                    text: "please fill DATE correctly.",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                return false;
            }
            else{
                surg_date = $("#surgery_date-"+prev_v_id).val();
                surg_note = $("#surgery_note-"+prev_v_id).val();
                prev_p_id = evt_btn.data("pid");
            }
        }else if(prev_name==19) {
            attr1 = $("#attr1-"+prev_v_id).val();
            if (attr1==='Cardiac') {
                attr2 = $("#attr2-"+prev_v_id).val();
                attr3 = $("#attr3-"+prev_v_id).val();

            }else if(attr1==='Vascular') {
                attr2 = $("#attr2-"+prev_v_id).val();
                if (attr2==='Venous Diseases') {
                    attr3 = $("#attr3-"+prev_v_id).val();
                }

            }else if(attr1==='Thoracic') {
                attr2 = $("#attr2-"+prev_v_id).val();
            }
        }else {
            //there is something wrong with the internal code
            swal({
                title: "Oops!",
                text: "Something is wrong with the provided data, please contact system administrator.",
                type: "error",
                confirmButtonColor: "#C9302C"
            });
            return false;
        }
        clicked = true;
        // evt_btn.parent().prevAll("input,textarea").prop('disabled', false);
        evt_btn.removeClass("btn-default");
        evt_btn.addClass("btn-danger");
        evt_btn.children().first().removeClass("fa-cloud-upload");
        evt_btn.children().first().addClass("fa-cog fa-spin");
        
        // console.log(evt_btn.parent().prevAll("input,textarea").val());
        var data = "new_data="+new_data+"&prev_v_id="+prev_v_id+"&prev_name="+prev_name+"&surg_date="+surg_date+"&surg_note="+surg_note+"&prd_date="+prd_date+"&pid="+prev_p_id+"&attr1="+attr1+"&attr2="+attr2+"&attr3="+attr3+"&edit_prev_hx=true";
        $.ajax(
        {
           type: "POST",
           url: "./includes/ajax/dr_p_operation.php",
           data: data,
           cache: false,
        
           success: function(result)
           {
                if (result == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                }
                // evt_btn.parent().prevAll("input,textarea").prop('disabled', true);
                evt_btn.removeClass("btn-danger");
                evt_btn.addClass("btn-default");
                evt_btn.children().first().removeClass("fa-cog fa-spin");
                evt_btn.children().first().addClass("fa-cloud-upload");

                clicked = false;
           },
           
           statusCode: 
           {
                404: function() {
                alert('ERROR 404 \npage not found');
                }
           }

        });
}
        
    });

    $(".re_uploader").on('click', function(event) {
        // event.preventDefault();
        console.log($(this));
        evt_btn = $(this);
        if(checkfiles("attachfiles-"+evt_btn.data("id"))){
            evt_btn.removeClass("btn-warning");
            evt_btn.addClass("btn-danger");
            evt_btn.children(0).addClass("fa fa-cog fa-spin");
            
            data = $('#editprevpatientform-'+evt_btn.data("id"))[0];
            formdata = new FormData(data);
            formdata.append("prev_edit_att", "true");
            $.ajax({
                type: "POST",             // Type of request to be send, called as method
                url: "./includes/ajax/dr_p_operation.php", // Url to which the request is send
                data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(result)   // A function to be called if request succeeds
                {
                    /*if (result != "") {
                        swal({
                            title: "Operation Failed!",
                            text: result,
                            type: "error",
                            confirmButtonColor: "#C9302C"
                        });
                    }*/
                    evt_btn.removeClass("btn-danger");
                    evt_btn.addClass("btn-warning");
                    evt_btn.children(0).removeClass("fa fa-cog fa-spin");
                }
            });
        }
    });


    $(".prev_followup_date").on('change', function(event) {
        var evt_btn = $(this);
        evt_btn.nextAll("span").addClass('danger_');
        evt_btn.nextAll("span").children(0).removeClass("glyphicon glyphicon-calendar");
        evt_btn.nextAll("span").children(0).addClass("fa fa-cog fa-spin");


        prev_v_id = evt_btn.data("id");
        new_data = evt_btn.val();
        prev_p_id = evt_btn.data("pid");
        data = "new_data="+new_data+"&prev_v_id="+prev_v_id+"&prev_p_id="+prev_p_id+"&edit_prev_fll_date=true";
        $.ajax(
        {
           type: "POST",
           url: "./includes/ajax/dr_p_operation.php",
           data: data,
           cache: false,
        
           success: function(result)
           {
                if (result == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                }
                evt_btn.nextAll("span").removeClass('danger_')
                evt_btn.nextAll("span").children(0).removeClass("fa-cog fa-spin");
                evt_btn.nextAll("span").children(0).addClass("glyphicon glyphicon-calendar");

                clicked = false;
           },
           
           statusCode: 
           {
                404: function() {
                alert('ERROR 404 \npage not found');
                }
           }

        });
    });

    $(".hxtab_btn").on('click', function(event) {
        /*console.log($(this).data('id'));
        console.log(diagnosis_list);
        console.log(treatment_list);

        vid = $(this).data('id');*/

        /*nazanm brem chand s3atm lawai kushtia hata awha 3ilajm krd
            tokenfield menu dakawta zher inputakani dikai form la prev form
            aw codai xwarawa wak awa waya resetian bkatawa
        */
        // $(".form-group input, .form-group .btn, .form-group textarea").css("z-index","0");
        $(".goBehind").css("z-index","0");
        $(".goBehind1").css("z-index","1");
        $(".goBehind2").css("z-index","2");
        $(".goBehind3").css("z-index","3");
        $(".goBehind4").css("z-index","4");
    });

    $("#cancel_patient").on('click', function() {
        the_btn = $(this);
        swal({
            title: "Cancel This Patient",
            text: "Are you sure you want to cancel the visit?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#C9302C"
        },
        function(){
        var v_id = the_btn.data("id");
        var p_id = the_btn.data("pid");
            $.ajax({
                url: "./includes/ajax/dr_p_operation.php",
                type: 'POST',
                data: "v_id="+v_id+"&p_id="+p_id+"&cancelv=true",
                success: function(result) {
                    if (result == -1) {
                        swal({
                            title: "Operation Failed!",
                            text: "Somthing Wrong Happened, if problem persist please contact system administrator.",
                            type: "error",
                            confirmButtonColor: "#C9302C"
                        });
                    }else {
                        swal("Done!","Operation completed successfully.","success");
                        location.reload();
                    }
                }
            });
        });
    });

    $("#end_visit, .prinslip").on('click', function(event) {
        if(!checkfiles("attachfiles")) { console.log(checkfiles("attachfiles")); return false;}
        // #treatment
        // #treatment_note
        var pcode_n_name = $('#pcode_n_name').text();
        var p_age = $('#p_age').text();
        // var p_date = $('#p_date').text();
        var treatment = $('#treatment').val();
        var treatment_note = $('#treatment_note').val();
        if (!empty(treatment)) {
            treatment_arr = treatment.split(', ');
        }else{
            treatment_arr = new Array();
        }
        if (!empty(treatment_note)) {
            treatment_note_arr = treatment_note.split('\n');
        }else{
            treatment_note_arr = new Array();
        }

        treatment_arr_count = treatment_arr.length;
        treatment_note_arr_count = treatment_note_arr.length;

        treatment_slip_note = "";
        for(i=0; i<treatment_arr_count; i++)
        {

            treatment_slip_note += treatment_arr[i]+": "+treatment_note_arr[i]+"@";
        }
         treatment_slip_note = treatment_slip_note.replace(/undefined/g, "no data");
        // console.log(treatment_slip_note);
          
          
            
            todayis = new Date();
            today_year = todayis.getFullYear();
            today_month = todayis.getMonth()+1;
            today_day = todayis.getDate();
            
            date = today_year+'-'+today_month+'-'+today_day;
            
            // console.log(page);
          print_slip(date, pcode_n_name, treatment_slip_note, p_age);

    });

    //current visit printing
    $("#inv_print").on('click', function(event) {    
        // #treatment
        // #treatment_note
        var pcode_n_name = $('#pcode_n_name').text();
        var p_age = $('#p_age').text();
        // var p_date = $('#p_date').text();
        var investigation = $('#investigation').val();
        var investigation_note = $('#investigation_note').val();
        if (!empty(investigation)) {
            // investigation_arr = investigation.split(',');
            investigation_arr = investigation.trim().split(/\s*,\s*/);
        }else{
            investigation_arr = new Array();
        }
        if (!empty(investigation_note)) {
            investigation_note_arr = investigation_note.split('\n');
        }else{
            investigation_note_arr = new Array();
        }

        investigation_arr_count = investigation_arr.length;
        investigation_note_arr_count = investigation_note_arr.length;

        // investigation_slip_note = "<p>";
        investigation_slip_note = "";
        for(i=0; i<investigation_arr_count; i++)
        {

            investigation_slip_note += investigation_arr[i] +": "+ investigation_note_arr[i]+"@";
        }
        // investigation_slip_note += " </p>";
        // replace with "" bcz some invs may not need any notes
         investigation_slip_note = investigation_slip_note.replace(/undefined/g, "");
        // console.log(investigation);
            todayis = new Date();
            today_year = todayis.getFullYear();
            today_month = todayis.getMonth()+1;
            today_day = todayis.getDate();
            
            date = today_year+'-'+today_month+'-'+today_day;
            
          print_slip(date, pcode_n_name, investigation_slip_note, p_age);
    });

    //prev visits printing
    // print investigations
    $(".print_inv").on('click', function(event) {
       
       var id = $(this).data('id');
       // for pcode_n_name I don't need to concate it with prev_id, because I have only one modal at a time which has one basic information and no more.
       
       var pcode_n_name = $('#pcode_n_name').text();
       var p_age = $('#p_age').text();
        // var p_date = $('#p_date').text();
        var investigation = $('#investigation-'+id).val();
        var investigation_note = $('#investigation_note-'+id).val();
        if (!empty(investigation)) {
            investigation_arr = investigation.trim().split(/\s*,\s*/);
        }else{
            investigation_arr = new Array();
        }
        if (!empty(investigation_note)) {
            investigation_note_arr = investigation_note.split('\n');
        }else{
            investigation_note_arr = new Array();
        }

        investigation_arr_count = investigation_arr.length;
        investigation_note_arr_count = investigation_note_arr.length;
        if (investigation_arr_count<1) {
            return;
        }
        investigation_slip_note = "";
        for(i=0; i<investigation_arr_count; i++)
        {

            investigation_slip_note += investigation_arr[i] +": "+ investigation_note_arr[i] +"@";
        }
         investigation_slip_note = investigation_slip_note.replace(/undefined/g, "");
        // console.log(investigation_slip_note);
            
            date = $('#dropdown'+id+'-tab').text();
            
            // console.log(page);
          print_slip(date, pcode_n_name, investigation_slip_note, p_age);
    });
    $(".printPdata").click(function () {
        /*$.post("../includes/ajax/print_patient_info.php?","printp=1&v_id="+$(this).data("vid")+"&p_id="+$(this).data("pid"),
            function(data) {
                console.log(data);
                newWin= window.open("");
                newWin.document.write(data);
                newWin.print();
                newWin.close();
            });*/
        var $form = $('#editnewpatientform');
        $.ajax({
            // url: $form.attr('action'),
            url: "./includes/ajax/print_patient_info.php",
            type: 'POST',
            data: $form.serialize()+"&printp=1",
            success: function(data) {
                if (data == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                }else {

                }
            }});

        // $('#editnewpatientform').printThis({
        //     debug: false,
        //     importCSS: "<link rel='stylesheet' type='text/css' href='../css/print-patient/print_patient.css'>",
        //     // importStyle: false,
        //     // printDelay: 5000,
        //     // removeScripts: true
        // });
    });
    $(document).on("change", ".attr1Class", function (e) {
        attr1_option = $(this).find('option:selected');
        option_id = parseInt(attr1_option.attr('id'));
        // prev_v = $(this).data('visits')>1?true:false;
        // console.log(prev_v);
        prev_v_id = $(this).data('visits')>1?"-"+$(this).data("vid"):"";

        if (option_id>1) {
            /*$.getJSON or $.post is a shorthand Ajax function but speciallized for JSON parsed data */
            $.post("../includes/ajax/attributes_list.php?","attr1=1&id="+option_id, function(data) {
            $("#attr2"+prev_v_id+" option").remove();
            $("#attr3"+prev_v_id+" option").remove();
            /*
              $.each(data, function(index, value){
              console.log(index);
              console.log(value);
              this wata value awja la value ka arrayaki dikaya ama dalein .value w .name
            */
            $.each(data, function(){
              
              $("#attr2"+prev_v_id).append('<option value="'+ this.value +'">'+ this.name +'</option>')
            });
            // agar data-visits=1 bu awja slide down w up bka agar na dabi blei #attr2list+vid . slidedown...
            $("#attr2list"+prev_v_id).slideDown('slow');
            $("#attr3list"+prev_v_id).slideUp('slow');
          }, "json");
            
        }else if(option_id==1){
          $.post("../includes/ajax/attributes_list.php?","attr1=1&id="+option_id, function(data) {
            $("#attr2"+prev_v_id+" option").remove();
            $("#attr3"+prev_v_id+" option").remove();
            data2 = data['data2'];
            data3 = data['data3'];
            $.each(data2, function(){
              
              $("#attr2"+prev_v_id).append('<option value="'+ this.value +'">'+ this.name +'</option>')
            });

            $.each(data3, function(){
              
              $("#attr3"+prev_v_id).append('<option value="'+ this.value +'">'+ this.name +'</option>')
            });
            $("#attr2list"+prev_v_id).slideDown('slow');
            $("#attr3list"+prev_v_id).slideDown('slow');
          }, "json");
            
        } else{
            $("#attr2list"+prev_v_id).slideUp('slow');
            $("#attr3list"+prev_v_id).slideUp('slow');
            $("#attr2"+prev_v_id+" option").remove();
            $("#attr3"+prev_v_id+" option").remove();
        }
    });
    
    $(document).on("change", ".attr2Class", function (e) {
        /*
          three cases:
          Pediatric
          Venous Diseases
          Adult
        */
        attr2_option = $(this).find('option:selected');
        // prev_v = $(this).data('visits')>1?true:false;
        // console.log(prev_v);
        prev_v_id = $(this).data('visits')>1?"-"+$(this).data("vid"):"";

        if (attr2_option.attr('value')==='Pediatric' || attr2_option.attr('value')==='Venous Diseases' || attr2_option.attr('value')==='Adult') {
          /*$.getJSON or $.post is a shorthand Ajax function but speciallized for JSON parsed data */
          $.post("../includes/ajax/attributes_list.php?","attr2=1&id="+attr2_option.attr('value'), function(data) {
          $("#attr3"+prev_v_id+" option").remove();
          $.each(data, function(){
            
            $("#attr3"+prev_v_id).append('<option value="'+ this.value +'">'+ this.name +'</option>')
          });
          
          $("#attr3list"+prev_v_id).slideDown('slow');
        }, "json");
      }else{
        $("#attr3list"+prev_v_id).slideUp('slow');
        $("#attr3"+prev_v_id+" option").remove();
      }
            
    });
    $("#datetimepicker4").timepicker({
        'minTime' : '7:00am',
        'maxTime' : '12:00am',
        'step' : 5,
        'timeFormat' : 'g:i a'
    });
});// end of document.ready

function print_slip(thedate, code_name, treats, p_age) {

    window.open('racheta2.php?today='+thedate+'&code_name='+code_name+'&treats='+treats+'&age='+p_age);
    // newWin.print();
    // setTimeout(function () { newWin.print(); }, 500);
    // newWin.onfocus = function () { setTimeout(function () { newWin.close(); }, 500); }
          // newWin.print();
          // newWin.close();
          
          // OpenWindow();
}
function checkfiles(input_id) { //same in patient_more_info

    console.log(input_id);
    //check whether browser fully supports all File API
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
        var flag_type = true;
        var flag_size = true;

        //get the file size and file type from file input field
        for (var i = $('#'+input_id)[0].files.length - 1; i >= 0; i--) {
            // $('#'+input_id)[0].files[i];
            var fsize = $('#'+input_id)[0].files[i].size;
            var ftype = $('#'+input_id)[0].files[i].type;
            var fname = $('#'+input_id)[0].files[i].name;
           console.log(ftype);
           switch(ftype)
            {
                case 'image/png':
                case 'image/gif':
                case 'image/jpeg':
                case 'image/pjpeg':
                case 'video/m4v':
                case 'video/avi':
                case 'video/mp4':
                case 'video/mov':
                case 'video/mpg':
                case 'video/mpeg':
                case 'video/quicktime':
                case 'video/x-ms-wmv':
                    // alert("Acceptable image file!");
                    break;
                default:
                    // alert('Unsupported File!');
                    flag_type = false;
                    break;
            }
            //check file size
            // if(fsize>5242880) //do something if file size more than 5 mb (5242880)
            if(fsize>10485760) // 10mb
            {
                flag_size = false;
            }

            /*if (flag_type && flag_size) {
                return true;
            }else*/ if(!flag_type){
                swal({
                    title: "One or Multiple Unsupported File Type! " + ftype,
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                return false;
            }else if(!flag_size){
                swal({
                    title: fsize +" \nOne or Multiple Files Exceeding Size Limit of 10 MB!",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                return false;
            }
            // if function is not returned until now, then variables should still be TRUE;
            // var flag_type = true;
            // var flag_size = true;
        } //end for loop
        return flag_type && flag_size;
    }else{
        swal({
                title: "Can't Use Upload",
                text: "Please upgrade your browser, because your current browser lacks file upload and some other new features we need!",
                type: "error",
                confirmButtonColor: "#C9302C"
            });
        return false;
    }
}
</script>