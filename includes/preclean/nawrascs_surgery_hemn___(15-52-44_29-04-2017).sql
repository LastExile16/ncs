SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
--
-- Database: `nawrascs_surgery_hemn`
--




CREATE TABLE `patient` (
  `p_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'key: yymmdd[session(autonumber)], autonum: count today visits then start increment, plan failed',
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'patient name',
  `sex` tinyint(1) NOT NULL COMMENT '1:male, 2:female, 3:transgender',
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'patient address',
  `dob` int(4) DEFAULT NULL COMMENT 'year of birth',
  `phone` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'phone number, max is 2 numbers',
  `occupation` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'patient occupation',
  `past_hx` text COLLATE utf8_unicode_ci COMMENT 'past history (should be merged in each visit not replaced and use nl2b function)',
  `family_hx` text COLLATE utf8_unicode_ci COMMENT 'family history (should be merged in each visit)',
  `drug_hx` text COLLATE utf8_unicode_ci COMMENT 'drug history (should be merged ...)',
  `other_hx` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other history to mention (should be merged ...)',
  `latest_visit` date DEFAULT NULL COMMENT 'store latest visit if available (i added this to increase query performance for "patients" page) )',
  `latest_followup_visit` date DEFAULT NULL COMMENT 'latest_followup_visit to show expected visit in current_p',
  `surgeries` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'here we combine all surgeries from visit_surgery table here to show it easily in all_p',
  `queue_status` tinyint(1) NOT NULL DEFAULT '-1' COMMENT '1:in queue, -1:not in queue; current queue status',
  `u_id_f` int(11) NOT NULL COMMENT 'user foreign key',
  `view` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted',
  PRIMARY KEY (`p_id`),
  KEY `name` (`fullname`),
  KEY `u_id_f` (`u_id_f`),
  KEY `view` (`view`),
  CONSTRAINT `patient_ibfk_1` FOREIGN KEY (`u_id_f`) REFERENCES `user` (`u_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='later make view index';


INSERT INTO patient VALUES
("18","shukria anwar","2","hawar","1993","45678","student","some hx","family was ...","a lot of drugs","and something else and more","2017-03-05","2017-05-15","surg1, surg2","-1","2","1"),
("19","bazhdar haji","1","Tokyo","1980","123","any","past data is important","NEW","add new drug hx","","2017-03-11","0000-00-00","surg2, surg4","-1","1","1"),
("20","sami xayat","1","erbil","1985","07701234567","worker","","","","","2017-03-11","2017-04-10","surg2, surg4, surg1, new surg","-1","2","1"),
("21","kamaran aziz","1","waziran","1978","07731234567","worker","","","","","2017-03-08","2017-04-17","surg2, surg1, surg3","-1","2","1"),
("31","Mohammed hamed","1","Bnaslawa","1951","07507828268","kasb","","","","","2017-03-25","2017-05-27","","-1","2","1"),
("32","Raana wady","2","Chamchamal","1931","07501012127","Zhni mall","","","","","2017-03-25","0000-00-00","","-1","2","1"),
("33","Khatwn ahmed ibrahim","2","Erbil","1953","07504701012","Zhni mall","","","","","2017-03-25","2017-06-15","","-1","2","1"),
("34","Hussen ali wssw","1","Rania","1938","07501143059","kasb","","","","","2017-03-25","0000-00-00","","-1","2","1"),
("37","Abdulrahman omer khadhr","1","Taq taq","1971","07503472954","Polise","","","","","2017-03-27","0000-00-00","","-1","2","1"),
("40","Abbas ibrahim ahmed","1","karkuk","1963","07701254394","Bekar","CABG","negative","negative","nill","2017-03-27","0000-00-00","","-1","1","1"),
("42","Neaamat khdr mjrw","1","Sedakan","1940","07504600820","not available","","","","","2017-03-26","0000-00-00","","-1","2","1"),
("43","Kalsum fkre","2","Barzan","1998","07504560849","Qutabi","","","","","2017-03-26","0000-00-00","","-1","2","1"),
("44","Dlzar khaleel faqee hussen","1","Shaqlawa","1972","07504746743","kasb","","","","","2017-03-26","0000-00-00","","-1","2","1"),
("45","Amjad bahjat","1","not available","1990","07507445252","kasb","","","","","2017-03-26","2017-06-26","","-1","2","1"),
("46","Esmat osman abdulrahman","1","Bshrean","1965","07504778714","Bekar","","","","","2017-03-26","0000-00-00","","-1","2","1"),
("47","Salah abubakr","1","Gweer","1962","07504796421","kasb","","","","","2017-03-26","2017-06-17","","-1","2","1"),
("48","Ronak maarwf yusf","2","Erbil mnaray choli","1966","07504220167","Zhni mall","","","","","2017-03-29","0000-00-00","","-1","2","1"),
("50","Mohammed ahmed bakr","1","Khabat","2017","07504143015","not available","","","","","2017-03-29","0000-00-00","","-1","2","1"),
("51","Kawa rushdi rasho","1","Barzan","1988","07507422261","Peshmarga","","","","","2017-03-29","0000-00-00","","-1","2","1"),
("52","Hussen jasm mohammed","1","Erbil mamzawa","1956","07822426052","Bekar","","","","","2017-03-29","0000-00-00","","-1","2","1"),
("54","Esmael qasem mohammed","1","Erbil","1983","07504901566","Karmandi la matar","","","","","2017-03-29","0000-00-00","","-1","2","1"),
("55","Zara kak shar sleman","2","Mergasor","1959","07504444172","Zhni mall","","","","","2017-03-29","0000-00-00","","-1","2","1"),
("57","Khaleel salih","1","Erbil","1957","07504521769","Farmanbar","","","","","2017-04-01","0000-00-00","","-1","2","1"),
("58","Fars majed hama","1","Taq taq","1963","07504952569","Polise","","","","","2017-04-05","0000-00-00","","-1","2","1"),
("59","Haji talb wali ahmed","1","karkuk","1948","07702344446","Bekar","","","","","2017-04-01","0000-00-00","","-1","2","1"),
("60","Hamen bahjat","2","Grdmamk","1947","07504843168","Zhni mall","","","EXFORGE TAB 10/160/12.5\nALDACTON TAB 100 MG","","2017-04-02","0000-00-00","","-1","1","1"),
("61","Zahabia Saed jamal","2","Qaran choghan","1979","07504686955","Zhni mall","","","","","2017-04-01","0000-00-00","","-1","2","1"),
("62","Namiq haeni rasul","1","Erbil","1942","07504712881","Bekar","","","","chronic smocker","2017-04-01","0000-00-00","","-1","1","1"),
("63","Nawzad ismael ali","1","Erbil","1996","07504999587","kasb","","","","","2017-04-02","0000-00-00","","-1","2","1"),
("64","Hawar hassan ahmed","1","Erbil","1966","07504451433","kasb","","","","","2017-04-03","0000-00-00","","-1","2","1"),
("65","Yasr ghambar","1","Erbil","2016","07504998811","Mndal","","","","","2017-04-03","0000-00-00","","-1","2","1"),
("66","Salahaddin najmaden mohammed","1","karkuk","1962","07703711906","Karmand la tandrusti","","","","","2017-04-04","0000-00-00","","-1","2","1"),
("67","Zhean mohammed wali","2","karkuk","1973","07701301724","Zhni mall","","","","","2017-04-04","0000-00-00","","-1","2","1"),
("68","Sahera hadi hamad","2","Erbil","1969","07507408055","Zhni mall","","","","","2017-04-04","0000-00-00","","-1","2","1"),
("69","Nazer saaed","1","Harer","1974","07503357549","Karmand","","","","","2017-04-05","0000-00-00","","-1","2","1"),
("71","Ismael mohammed","1","Erbil","1965","07504653161","Bekar","","","","","2017-04-05","0000-00-00","","-1","2","1");




CREATE TABLE `visit` (
  `v_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'visits id of patient',
  `p_id_f` int(11) unsigned NOT NULL COMMENT 'patient foreign key',
  `visit_date` date NOT NULL COMMENT 'patient date of the visit',
  `followup_date` date NOT NULL COMMENT 'followup date: next visit',
  `marital_status` tinyint(1) unsigned NOT NULL COMMENT '1:single, 2:married, 3:divorced, 4:widowed.',
  `pregnant` tinyint(1) unsigned NOT NULL COMMENT '1:pregnant, 2:not pregnant',
  `breast_feed` tinyint(1) unsigned NOT NULL COMMENT '1:breasfeeding, 2:not breast feeding',
  `no_of_child` int(2) unsigned NOT NULL COMMENT 'number of children',
  `other_info` varchar(60) COLLATE utf8_unicode_ci NOT NULL COMMENT 'other patient-specific info ',
  `chief_comp_n_dur` text COLLATE utf8_unicode_ci COMMENT 'chief_complaint and duration',
  `examination` text COLLATE utf8_unicode_ci,
  `investigation` text COLLATE utf8_unicode_ci,
  `note` text COLLATE utf8_unicode_ci COMMENT 'notes',
  `treatment_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'a note to describe the treatments given, this note will be printed along with the treatment_description data',
  `fee` decimal(13,3) NOT NULL COMMENT 'per visit fee',
  `in_queue` tinyint(1) NOT NULL DEFAULT '1' COMMENT '-1: visit completed so not in queue, 1:in queue, 2:in doctors room',
  `u_id_f` int(11) NOT NULL,
  `view` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'active:1, delete:-1',
  PRIMARY KEY (`v_id`),
  KEY `p_id_f` (`p_id_f`),
  KEY `u_id_f` (`u_id_f`),
  KEY `in_queue` (`in_queue`),
  KEY `visit_date` (`visit_date`),
  KEY `view` (`view`),
  CONSTRAINT `visit_ibfk_2` FOREIGN KEY (`u_id_f`) REFERENCES `user` (`u_id`) ON UPDATE CASCADE,
  CONSTRAINT `visit_ibfk_3` FOREIGN KEY (`p_id_f`) REFERENCES `patient` (`p_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO visit VALUES
("44","20","2017-03-05","2017-04-09","2","0","0","3","","some test data","some test data examination","some test data investigation","more notes on patient","some notes for treatments","25000.000","-1","2","1"),
("45","19","2017-03-05","2017-04-17","1","0","0","0","","some test data complain","some test data examination 2","some test data investigation 2","here is some test notes","writing notes is important","25000.000","-1","2","1"),
("46","18","2017-03-05","2017-05-15","1","2","2","0","","some test data complain 3","some test data examination 3","some test data investigation 3","","notes to be written","25000.000","-1","2","1"),
("47","21","2017-03-07","2017-04-11","1","0","0","0","","a complain","test Examination","test investigation","","some notes","20000.000","-1","2","1"),
("48","21","2017-03-08","2017-04-17","1","0","0","0","","online test complain","on test examination","on test investigation","","some notes","25000.000","-1","2","1"),
("49","19","2017-03-11","0000-00-00","1","0","0","0","","","","","","","0.000","-1","2","1"),
("51","20","2017-03-11","2017-04-10","2","0","0","2","","","","","","use it somehow\nanother note","0.000","-1","2","1"),
("58","31","2017-03-25","2017-05-27","2","0","0","9","","post pulmonary end arterctomy","","","asprin tab 100 mg x 1","","25000.000","-1","2","1"),
("59","32","2017-03-25","0000-00-00","2","2","2","2","","SOB and dry cough for 2 months durarion","","CT : right pulmonary artery chronic thromboembolism , Echo : sever pulmonary hypertension and thrombus like mass in pulmonary artery","","","25000.000","-1","2","1"),
("60","33","2017-03-25","2017-06-15","1","2","2","0","","multiple pulmonary nodules in lung","dysnia","CT guided study showed inflamatary pathway","","","25000.000","-1","2","1"),
("61","34","2017-03-25","0000-00-00","2","0","0","5","","dyspnea  and chest pain of one week duration","bp 105/55 , pr 50 , chest exam : bilateral chest crepitation","echo : sever systolic LV dysfunction.","","","25000.000","-1","2","1"),
("64","37","2017-03-27","0000-00-00","2","0","0","6","","varicose veins of both legs","visible varicose more onleft side","","","","25000.000","-1","2","1"),
("66","40","2017-03-27","0000-00-00","2","0","0","4","","blakish discoloration of left big toe","doppler detected pw od","","","","25000.000","-1","2","1"),
("67","42","2017-03-26","0000-00-00","2","0","0","6","","chest pain , loss of apetide , subcarinal LAP","","CT: large subcarinal LN","","","25000.000","-1","2","1"),
("68","43","2017-03-26","0000-00-00","1","0","0","0","","post phontan","","pt : 1.7","","","25000.000","-1","2","1"),
("69","44","2017-03-26","0000-00-00","2","0","0","4","","post chemical pneumonitis , SOB on exertion","Bp : 150 / 60 mg","CT : non specific grawnd glass masess in lawer lob","","","25000.000","-1","2","1"),
("70","45","2017-03-26","2017-06-26","2","0","0","2","","right sided hemothorax after stab wound to chest","","CXR : expanded lung","","","25000.000","-1","2","1"),
("71","46","2017-03-26","0000-00-00","2","0","0","6","","atypical chest and abdominal pain of one month duration","bp: 150 / 90","","","","25000.000","-1","2","1"),
("72","47","2017-03-26","2017-06-17","2","0","0","6","","post AVR","PR : 55","INR : 2.40","","","25000.000","-1","2","1"),
("74","48","2017-03-29","0000-00-00","2","2","2","5","","SVT","PR : 86","ECG : SVT","","","0.000","-1","2","1"),
("76","50","2017-03-29","0000-00-00","1","0","0","0","","pesistant fetal circulation wuth pulmonary hupertention","","","","","25000.000","-1","2","1"),
("77","51","2017-03-29","0000-00-00","2","0","0","5","","post operative  chronic DVT","","doppler : old DVT    ,","","","25000.000","-1","2","1"),
("78","52","2017-03-29","0000-00-00","2","0","0","8","","chest pain  of 10 days duration","bp : 180 / 110","","","","25000.000","-1","2","1"),
("80","54","2017-03-29","0000-00-00","2","0","0","2","","pointed chest pain of 2 years duration","normal","chest xr :N , ECG : N , blood : N","","","0.000","-1","2","1"),
("81","55","2017-03-29","0000-00-00","2","2","2","1","","left sided leg swelling with visible varicose","telengetasia","","","","25000.000","-1","2","1"),
("82","57","2017-04-01","0000-00-00","1","0","0","0","","dyspnea and cough","","Brochoscopy : ulsarating mass in left main bronchus , CT : left hilar mass and moderate pleural effusion","","225 mg 1 x 2","25000.000","-1","2","1"),
("83","58","2017-04-02","0000-00-00","2","0","0","4","","AV fistula creation under local anesthesia","","","","","25000.000","-1","2","1"),
("84","59","2017-04-01","0000-00-00","2","0","0","11","","left sided leg swelling and pain  with visible varicose","visible telengectasia","doppler","","","25000.000","-1","2","1"),
("85","60","2017-04-02","0000-00-00","2","2","2","7","","Aortic stenosis and hyperension","bp : 230 / 110 mg","ECHO :","","","25000.000","-1","2","1"),
("86","61","2017-04-01","0000-00-00","1","2","2","0","","myesthenia and post resection of a malignat thymoma through median sternitomy","ptosis","CT :","","","25000.000","-1","2","1"),
("87","62","2017-04-01","0000-00-00","2","0","0","5","","sever dyspnia","strider","CT : very large  right center mass , bronchoscopy : right main bronchus big mass","","","25000.000","-1","2","1"),
("88","63","2017-04-02","0000-00-00","1","0","0","0","","stab wound of the left side of the chest with hemo pneumothorax","","CXR : CLEAR","","","25000.000","-1","2","1"),
("89","64","2017-04-03","0000-00-00","2","0","0","4","","pericardial cyst","","CT : well defined pericardial cyst","","","25000.000","-1","2","1"),
("90","65","2017-04-03","0000-00-00","1","0","0","0","","post vsd closure","","","","1x2","0.000","-1","2","1"),
("91","66","2017-04-04","0000-00-00","2","0","0","3","","right intermittant  claudication of one year duration","negative DPA and PTA","CT peripheral angiogram : critical stenosis at right EIA and right SFA","x smoker and drinker\ndiabetic","100 mg x 2","25000.000","-1","2","1"),
("92","67","2017-04-04","0000-00-00","2","2","2","6","","leg swelling of 11 years duration","left sided leg swelling with bilateral varicose veins","doppler ultrasound","","","25000.000","-1","2","1"),
("93","68","2017-04-04","0000-00-00","2","2","2","5","","post porta cath insertion foliow up","","cath twisted  to the pulmonary artery","","","25000.000","-1","2","1"),
("96","71","2017-04-05","0000-00-00","2","0","0","11","","post CABG dyspnia","diminished air entry to left side of chest","CXR :","","","0.000","-1","2","1"),
("98","69","2017-04-05","0000-00-00","2","0","0","4","","post DVT","","INR : 2.5","","","0.000","-1","2","1"),
("99","58","2017-04-05","0000-00-00","2","0","0","4","","post AVF creation","","","","","0.000","-1","2","1");




/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;