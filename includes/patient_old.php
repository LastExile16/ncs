<script type="text/javascript" src="../js/modernizr.custom.79639.js"></script>
<div class="contact row">

	<!-- BEGIN SEARCH TABLE PORTLET-->
	
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>

<div class="row">
	 
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="col-md-12">
	 	
	 
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="caption panel-title">
				<span class='fa fa-address-card-o'></span> Patient
			</div>
		</div>
		<div class="panel-body resposive">
	<table class="rtable rtable--flip table table-bordered table-condensed" id="patient" style="width:100%">
	<thead>
		<th>Code</th>
		<th>Full Name</th>
		<th>Sex</th>
		<th>Address</th>
		<th>DOB</th>
		<th>Phone Number</th>
		<th>Occupation</th>
		<th>Latest Visit</th>
	</thead>

	<tfoot>
		<th>Code</th>
		<th>Full Name</th>
		<th>Sex <?php  ?></th>
		<th>Address</th>
		<th>DOB</th>
		<th>Phone Number</th>
		<th>Occupation</th>
		<th>Latest Visit</th>
	</tfoot>
	
  </table>

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <form class="form-horizontal" action="index.php" id="addQu_editP">
        <div class="modal-body">
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div class="modal fade" id="editnewpatient" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
          <form class="form-horizontal" action="" id="editnewpatientform">
        <div class="modal-body">
            <div class="form-group">
              <label class="col-md-3 control-label" for="p_id">p_id</label>  
              <div class="col-md-7">
              <input id="p_id" name="p_id" class="form-control" type="hidden">
                
              </div>
            </div>
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="fullname">Full Name</label>  
			  <div class="col-md-7">
			  <input id="fullname" name="fullname" placeholder="Full Name" class="form-control" required="required" type="text">
			    
			  </div>
			</div>

			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="sex">Sex</label>
			  <div class="col-md-7">
			    <select id="sex" name="sex" class="form-control">
			      <option value="1">male</option>
			      <option value="2">female</option>
			      <option value="3">transgender</option>
			    </select>
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="address">Address</label>  
			  <div class="col-md-7">
			  <input id="address" name="address" placeholder="Address" class="form-control" required="required" type="text">
			    
			  </div>
			</div>

			<div class="form-group">
			   <label for="datetimepicker1" class="col-md-3 control-label">Year of Birth</label>
			   <div class="col-md-7">
				  <div class='input-group date datepick' id='datetimepicker1'>
				  <input type='text' autocomplete="false" id="yearOfB" class="form-control" name="date" required="required" />
				  <span class="input-group-addon">
					 <span class="glyphicon glyphicon-calendar"></span>
				  </span>
					 </div>
			   </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="phone">Phone Number</label>  
			  <div class="col-md-7">
			  <input id="phone" name="phone" autocomplete="false" placeholder="Phone Number" class="form-control" required="required" type="text">
			    
			  </div>
			</div>

			<div class="form-group">
			  <label class="col-md-3 control-label" for="occupation">Occupation</label>  
			  <div class="col-md-7">
			  <input id="occupation" name="occupation" placeholder="Occupation" class="form-control" required="required" type="text">
			    
			  </div>
			</div>



        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" id="addnewP" name="addnewP" class="btn btn-primary" >Save changes</button>
        </div>
			</form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

</div>

		</div>
	</div>
	<div class="row" >
		<div class="col-md-12" >
			<a href="#editnewpatient" class="btn btn-primary" data-toggle='modal'><i class="fa fa-plus-square"></i> add new patient</a>
			<a href="javascript:;" class="btn btn-danger" id="cleaner"><i class="fa fa-circle-o-notch"></i> clean queue</a>
		</div>
	</div>
	</div>
	
</div>

<!-- <script src="../js/lightbox.js"></script>
<script>
    lightbox.option({
      'positionFromTop': 30
    })
</script> -->
<script type="text/javascript">

// $.fn.dataTable.ext.search.push(
$.fn.dataTableExt.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {
        var iMin = $('#min').val();
        var iMax = $('#max').val();
        d1 = new Date(iMin);
        d2 = new Date(iMax);

        iMin = isNaN(d1.getTime())?"":d1.getTime();
        iMax = isNaN(d2.getTime())?"":d2.getTime();

        target_col_data = aData[7];

        iDate = new Date(target_col_data).getTime();

 		// console.log("iMin = "+iMin);
   //      console.log("iMax = "+iMax);
   //      console.log("iDate = "+iDate);
		// console.log(target_col_data);
        if ( iMin == "" && iMax == "" )
		{
			return true;
		}
		else if ( iMin == "" && iDate < iMax )
		{
			return true;
		}
		else if ( iMin <= iDate && "" == iMax )
		{
			return true;
		}
		else if ( iMin <= iDate && iDate <= iMax )
		{
			return true;
		}
        return false;

    }
);
	$(document).ready(function() {

  var columnDefs = [{
    name: "p_id", title:"Code"
  }, {
    name: "fullname", title:"Full Name"
  }, {
    name: "sex", title:"Sex"
  }, {
    name: "address", title:"Address"
  }, {
    name: "dob", title:"Year of Birth"
  }, {
    name: "phone", title:"Phone Number"
  }, {
    name: "occupation", title:"Occupation"
  }, {
    name: "latest_visit", title:"Latest Visit"
  }];

  var patient;

  patient = $('#patient').DataTable({
  	processing: true,
    "pagingType": "full_numbers",
    // "autoWidth": false,
    columns: columnDefs,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "order": [[ 0, "desc" ]],

    "deferRender": true,
    dom: '<"top row"<"col-md-2"l><"col-md-2"B><"col-md-5 rangefilter"><"col-md-3"f>><"row"<"col-md-12" <"row"r>t>><"bottom row"<"col-md-4"i><"paginationright col-md-8"p>><"clear">',
    select: 'single',
    "serverSide": true,
    "ajax": {
            "url": "../includes/ajax/get_patient.php",
            "data": function(d){
				d.type = "load";
			},
            "type": "POST"
        },
    /*"drawCallback": function( settings ) {

	    var api = this.api();

	    // Output the data for the visible rows to the browser's console
	    console.log( api.rows( {page:'current'} ).data() );
	},*/
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-BUTTONS
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-BUTTONS
buttons: [/*{
			text: 'pdf',
            extend: 'pdfHtml5',
            download: 'open',
            message: 'pdf created by Nawras Clinical Service'
        },*/{
	text: '<i title="add to queue" id="add_to_queue_btn" class="fa fa-arrow-circle-o-up fa-1-5x"></i>',
	extend: 'selected',
  action: function(e, dt, node, config) {
    addToQueuClick(this, dt, node, config)
  }
}, {
  extend: 'selected',
  text: '<i title="Edit" class="fa fa-pencil-square-o fa-1-5x"></i>',
  action: function(e, dt, node, config) {
    var rows = dt.rows({
      selected: true
    }).count();
    //above code just to indicate how many rows are selected
// console.log(rows);
// console.log(dt.rows({selected: true}));
    editClick(this, dt, node, config)
  }
}, {
	extend: 'print',
	text: '<i title="Print" class="fa fa-print fa-1-5x"></i>',
    message: 'This print was produced by Nawras Clinical System',
    exportOptions: {
        columns: ':visible'
    }
}, {
	text: '<i title="hide columns" class="fa fa-eye-slash fa-1-5x"></i>',
	extend:'colvis'
}]/*,
"createdRow": function ( row, data, index ) {
	console.table(data);
	// console.log(data);
	    if (data[2]=='1') {
	    	 // $('td', row).eq(5).addClass('highlight');
			$('td:eq(2)', row).html("<div style='text-align:center'><i class='fa fa-mars'></i></div>");	
		}else if (data[2]=='2') {
			$('td:eq(2)', row).html("<div style='text-align:center'><i class='fa fa-venus'></i></div>");	
		}else if (data[2]=='3') {
			$('td:eq(2)', row).html("<div style='text-align:center'><i class='fa fa-venus-mars'></i></div>");	
		}
	}*/
  });

  /*rows returns an array of that row, but rows() returns objects that consist of 
  many data including the array of row*/
/*$('#patient tbody').on( 'click', 'tr', function () {
    console.log( patient.row( this ).data() );
    var d = patient.row( this ).data();
     d[1]="Yusuf";
    patient
        .row( this )
        .data( d )
        .draw();
} );

//custom search
$('#myInput').on( 'keyup', function () {
    patient.search( this.value ).draw();
} );
*/
  //---------Function to Display modal editButton---------

  function editClick(pointer, oTT, button, conf) {
    var adata = oTT.rows({
      selected: true
    });

    // console.log('adata', adata)
    // console.log(adata.data()[0]);
    // console.log(adata.data()[0][0]);
    // console.log(adata.ids()[0]);
    var id = adata.data()[0][0];

    // console.log(columnDefs);
    var data = "";
    var patient = pointer;
    data += "<div class='form-group' style='display:none'><label for='" + columnDefs[0].title + "'>" + columnDefs[0].title + ":</label><input type='hidden'  id='" + columnDefs[0].title + "' name='" + columnDefs[0].title + "' placeholder='" + columnDefs[0].title + "' style='overflow:hidden'  class='form-control' value='" + adata.data()[0][0] + "'></div>";
    // for (i in columnDefs) {
    for (i=1; i<columnDefs.length; i++) {
    	if (i==2) {continue;}
      data += "<div class='form-group'><label for='" + columnDefs[i].title + "'>" + columnDefs[i].title + ":</label><input type='text'  id='" + columnDefs[i].title + "' name='" + columnDefs[i].title + "' placeholder='" + columnDefs[i].title + "' style='overflow:hidden'  class='form-control' value='" + adata.data()[0][i] + "'></div>";

    }
// console.log(data);

    $('#myModal').on('show.bs.modal', function() {
      $('#myModal').find('.modal-title').html('Edit Record');
      $('#myModal').find('.modal-body').html('<pre>' + data + '</pre>');
      $('#myModal').find('.modal-footer').html("<button type='submit' data-content='remove' class='btn btn-primary' id='editRowBtn'>Update</button>");
    });

    $('#myModal').modal('show');
    $('#myModal input[1]').focus();

  };

  // edit row functionality
  $(document).on('click', '#editRowBtn', function() {

    // edit row function needs to go here, here use your ajax then hide the modal
// console.log('Update Row');
    $('#myModal').modal('hide');

    /*I use row() not rows() because I want to edit only one row and only one row is selectable
    otherwise I should use rows and loop through each selected row*/
    /*var adata = patient.rows({
      selected: true
    }).data()[0];*/

    // below copies the row data into adata variable, so after updating it you should give the data back to
    // patient object to update its' cache
    var adata = patient.row({
      selected: true
    }).data();
    // console.log(editForm[1].value);
    // console.table(adata);
    for (i in adata) {
        // adata[i] = editForm[i].value;
    }
    // console.table(adata);
   patient.row( {selected: true} ).data( adata );
       // patient.draw();
       patient.draw();

       /*
        when using ajax it's better to invalidate data to force datatable do a re-read from the sourse
        then draw() the table again.
        that way you don't need to loop through new data and update the variable <i.e. adata> then give it back to datatable.
        above function is useful only when you don't want to make the change immediately in the database
        instead you update the shown table then create a button like "save" when it's clicked then you make changes permenantly.
        same applies to add and delete
       */
  });

  //---------Function to Display modal add to queue---------

  function addToQueuClick(pointer, oTT, button, conf) {

    var adata = oTT.rows({
      selected: true
    });
    // console.log('adata', adata)
    // console.log(adata.data()[0]);
    // console.log(adata.data()[0][0]);
    // console.log(adata.ids()[0]);
    var id = adata.data()[0][0];
    var data = "";
    var patient = pointer;
    var isfemale = adata.data()[0][2].indexOf("venus")>0; // >0 "true" "female", <0 "false" "male or trans"
    data += "<div class='row top_label_modal'>";
    data += "<div class='col-md-3'>" + columnDefs[0].title + " : <span class='label_data'>"+adata.data()[0][0]+"</span></div>";
    data += "<div class='col-md-5'>" + columnDefs[1].title + " : <span class='label_data'>"+adata.data()[0][1]+"</span></div>";
    data += "<div class='col-md-4'>" + columnDefs[7].title + " : <span class='label_data'>"+adata.data()[0][7]+"</span></div>";
    data += "</div>";

    data += "<input type='hidden' value='" + adata.data()[0][0] + "' name='p_id' >";
    data += "<input type='hidden' value='" + isfemale + "' name='sex' >";
    data +=`<!-- Text input-->
<div class="form-group">
   <label for="datetimepicker2" class="col-md-4 control-label">Visit Date</label>
   <div class="col-md-7">
      <div class='input-group date datepick' id='datetimepicker2'>
      <input type='text' autocomplete="false" class="form-control" name="visit_date" id="visit_date" required="required" />
      <span class="input-group-addon">
         <span class="glyphicon glyphicon-calendar"></span>
      </span>
         </div>
   </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="maritalst">Marital Status</label>
  <div class="col-md-7">
    <select id="maritalst" name="maritalst" class="form-control">
      <option value="1">Single</option>
      <option value="2">Married</option>
      <option value="3">Divorced</option>
      <option value="4">Widowed</option>
    </select>
  </div>
</div>`;

if (isfemale) {
data +=`<div class="form-group">
  <label class="col-md-4 control-label" for="preg">Pregnant? </label>
  <div class="col-md-7">
  <div class="checkbox toggledboxdiv">
      <input type="checkbox" class="toggledbox" name="preg" id="preg" value="1">
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="breastfeed">Breast Feeding </label>
  <div class="col-md-7">
  <div class="checkbox toggledboxdiv">
      <input type="checkbox" class="toggledbox" name="breastfeed" id="breastfeed" value="1">
    </div>
  </div>
</div>`
}

data +=`<div class='form-group' id='childnumdiv' style='display:none'>
  <label class='col-md-4 control-label' for='childnum'>Number of Children</label>  
  <div class='col-md-7'>
  <span class="input-number-decrement">–</span><input class="input-number" name='childnum' id='childnum' type="number" min="0" max="20"><span class="input-number-increment">+</span>
  </div>
</div>

<div class='form-group'>
  <label class='col-md-4 control-label' for='other_inf'>Other Information</label>  
  <div class='col-md-7'>
        <input id='other_inf' name='other_inf' placeholder='Other Information' class='form-control' type='text'>
  </div>
</div>

<div class='form-group'>
  <label class='col-md-4 control-label' for='visit_fee'>Visit Fee</label>  
  <div class='col-md-7'>
  <div class='input-group'>
  <input id='visit_fee' name='visit_fee' placeholder='fee' class='form-control' min='0' type='text'>
    <span class="input-group-addon">IQD</span>
  </div>
  </div>
</div>`;
    $('#myModal').on('show.bs.modal', function() {
      $('#myModal').find('.modal-title').html('Add to Queue');
      $('#myModal').find('.modal-body').html( data);
      $('#myModal').find('.modal-footer').html("<button type='submit' class='btn btn-primary' id='addToQueuebtn'>Confirm</button>");
    });

    $('#myModal').modal('show');
        $('#datetimepicker2').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });

        $('.toggledbox').bootstrapToggle({
            on: 'Yes',
            off: 'No'
        });
        inputNumber($('.input-number'));
  };

  // add to queue row functionality
  // $(document).on('click', '#addToQueuebtn', function(e) {
  $(document).on('submit', '#addQu_editP', function(e) {
    // add to queue row function needs to go here
    /*if ($('#visit_date').val()=="" || $('#visit_date').val()==null) {
        return;
    }*/
    
    $("#add_to_queue_btn").removeClass("fa-arrow-circle-o-up");
    $("#add_to_queue_btn").addClass("fa-cog fa-spin");
    e.preventDefault();
    var $form = $(e.target);
    $.ajax({
        // url: $form.attr('action'),
        url: "../includes/ajax/patient_operation.php",
        type: 'POST',
        data: $form.serialize()+"&addPtoQ=true",
        success: function(result) {
            if (result == -1) {
                swal({
                    title: "Operation Failed!",
                    text: "The operation failed, please write information correctly when filling the form.",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                $("#add_to_queue_btn").removeClass("fa-cog fa-spin");
                $("#add_to_queue_btn").addClass("fa-arrow-circle-o-up");
            }else if(result == -2) {
                swal({
                    title: "Operation Failed!",
                    text: "This patient already added to queue.",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                $("#add_to_queue_btn").removeClass("fa-cog fa-spin");
                $("#add_to_queue_btn").addClass("fa-arrow-circle-o-up");
            }else{
                $("#add_to_queue_btn").removeClass("fa-cog fa-spin");
                $("#add_to_queue_btn").addClass("fa-arrow-circle-o-up");
            }
        }
    });
    $('#myModal').modal('hide');

    var adata = patient.rows({
      selected: true
    }).data();
// console.log(patient.rows({selected: true}).data());
    // adata.row().remove();
    // patient.row( {selected: true} ).data( adata );
    // patient.draw();
// patient.row({selected: true}).remove().draw(false);
    // patient.context[0].jqXHR.abort(); abort ajax request
    // console.log('data to add to queue : ', adata[0]);
  });

		//number of all columns
    // var numCols = $('#patient').DataTable().columns().nodes().length;
	//number of visible columns
    // var numCols = $('#patient thead th').length;

			// $(".panel-info").slideDown('slow');
		
		/*$(".errmsj").slideDown(1000);
		$(".alert button.close").click(function (e) {
		    // $(this).parent().fadeOut('slow');
		    $(this).parent().slideUp('slow');
		});
		*/
		
    $("div.rangefilter").html("<div id='baseDateControl'><div class='dateControlBlock'>Between <input type='text' name='min' id='min' class='date datepick form-control input-sm' value='' /> and <input type='text' name='max' id='max' class='date datepick form-control input-sm' value=''/> <a href='javascript:;' class='btn default btn-sm blue' id='clear_search'>clear</a></div></div>");
	
// l - Length changing
// f - Filtering input
// t - The Table!
// i - Information
// p - Pagination
// r - pRocessing

    /*$('#min, #max').keyup( function() {
        clinics.draw();});*/

    $("#min").keyup ( function() { patient.draw(); } );
	$("#min").change( function() { patient.draw(); } );
	$("#max").keyup ( function() { patient.draw(); } );
	$("#max").change( function() { patient.draw(); } );

	$("#clear_search").on("click", function() {
		$('#min').val("");
		$('#max').val("");
		patient.draw();
	});

// Setup - add a text input to each footer cell
    $('#patient tfoot th').each( function () {
    // $('#y tfoot th').not(":eq(0),:eq(-1)").each( function () {
        // var title = $.trim($(this).text());
        // console.log($(this));
        // $(this).html( '<input type="text" class="form-control input-sm" placeholder="Search '+$.trim(title)+'" />' );
        $(this).html( '<input type="text" class="form-control input-sm" placeholder="Press \'Enter\'" />' );
        // var disable='';
        // var placeholder = 'placeholder="Search here"';
        // if (title=='tools' || title=='#') { disable="disabled=disabled readonly "; placeholder='style="background-color:transparent; border-color:transparent; display:none"'; }
        	// $(this).html( '<input type="text" '+ disable + ' class="form-control input-sm"'+placeholder+' />' );
    } );
 
    // Apply the search
    patient.columns().every( function () {
        var that = this;
        // console.log(this.index());
        //if (this.index()==0 || this.index()==5 ) {return;}//to disable search in first at last col
        $( 'input', this.footer() ).on( 'change', function () {
            /*if (this.index() == 2) {

            }*/
            // if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            // }
        } );
    } );


    $(document).on('click', '#cleaner', function(event) {
    	$('#cleaner > :first-child').addClass('spin-backw');
    });

    $('#datetimepicker1').datepicker({
            format: " yyyy",
            autoclose: true,
            todayHighlight: true,
            viewMode: "years", 
            minViewMode: "years"
        });

    $("#editnewpatientform").submit(function(event){
        event.preventDefault();
        var $form = $(event.target);
        $.ajax({
            // url: $form.attr('action'),
            url: "../includes/ajax/patient_operation.php",
            type: 'POST',
            data: $form.serialize()+"&addnewP=true",
            success: function(result) {
                patient.ajax.reload(null, false);
            }
        });
        $('#editnewpatient').modal('hide');
    });
//////

    $(document).on("change","#maritalst", function (e) {
        // console.log(e);
        if (this.value>1) {
            $("#childnumdiv").slideDown('slow');
            $("#childnum").val(0);
            $('#childnum').attr('required', 'required');
        }else{
            $("#childnumdiv").slideUp('slow');
            $("#childnum").val('');
            $('#childnum').removeAttr('required');
        }
    });

} );

</script>