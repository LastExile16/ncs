<?php
date_default_timezone_set("Asia/Baghdad"); 
//check if user logged in
function logged_in(){
	//return isset($_SESSION['user_id']);

   if(isset($_SESSION['user_id'], $_SESSION['name'], $_SESSION['login_string'])) 
   {	
		$user_id = $_SESSION['user_id'];
		$user_name = $_SESSION['name'];
		$login_string = $_SESSION['login_string'];
		$user_browser = $_SERVER['HTTP_USER_AGENT'];
		$result = mysql_query("SELECT password FROM user WHERE u_id='$user_id' LIMIT 1"); 
		$result = mysql_fetch_array($result); 
		$login_check = hash('sha512', $result['password'].$user_browser);
		if($login_check == $login_string)
		{
			return true;
		}
		else
		{
			/*
				$user_id
				$user_name
				$login_string
				$user_browser
				$login_check
			*/			
			// $time = date("m/d/Y h:i:s a", time());
			// $comparison = var_dump($login_check == $login_string);
			// $txt = "time:{$time}\nuser_id:{$user_id}, user_name:{$user_name}, login_string:{$login_string}, user_browser:{$user_browser}, login_check:{$login_check}, comparison:{$comparison}\n";
			// $logfile = file_put_contents('logs.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
			/*echo "<pre>";
			var_dump($logfile);
			echo "</pre>";*/
			return false;
		}
   }
   else
	{
		/*
		var_dump _SESSION['user_id']
				_SESSION['name']
				_SESSION['login_string']
		*/
		/*$session_user_id = var_dump($_SESSION['user_id']);
		$session_name = var_dump($_SESSION['name']);
		$session_login_string = var_dump($_SESSION['login_string']);
		$time = date("m/d/Y h:i:s a", time());
		$txt = "time:{$time}\nuser_id:{$session_user_id}, user_name:{$session_name}, login_string:{$session_login_string}\n";
		$logfile = file_put_contents('logs.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);*/
		/*echo "<pre>";
		var_dump($logfile);
		echo "</pre>";*/

		return false;
	}
}

//generate random salt
function generateSalt($max = 4 , $temp = 0) 
{
	if($temp == 0)
	{
		$characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@$%&";
	}
	else
	{
		$characterList = "0123456789";
	}
	$i = 0;
	$salt = "";
	do {
		$salt .= $characterList{mt_rand(0,strlen($characterList)-1)};
		$i++;
	} while ($i <= $max);
	return $salt;
}
//verify the input before inserting to a database
function safe($value)
{
	$magic_quotes_active = get_magic_quotes_gpc();
	$new_enough_php = function_exists("mysql_real_escape_string"); //i.e. PHP >=v4.3.0
	if($new_enough_php){//PHP v4.3.0 or higher
	//undo any magic quote effects so mysql_real_escape_string can do the work 
	if($magic_quotes_active){ $value = stripcslashes($value); }
	$value = mysql_real_escape_string($value);
	}else{//before PHP v4.3.0
	//if magic quotes aren't already on then add slashes manually
	if(!$magic_quotes_active){ $value = addslashes($value); }
	//if magic quotes are active, then the slashes already exist
	}
	
	$value=htmlentities($value, ENT_QUOTES, "UTF-8");
	//$value=strip_tags($value, '<a><b>');
	return $value;
}

function is_doctor()
{
	return $_SESSION['role']==9?true:false;

}

// upload files to the server
// $folder_back: la drp operation bakardet taku btwanm folderk bgaremawa awja add bkam
function upload_file($errmsj, $uploading_files, $folder='', $query_part_1, $query_part_2, $folder_back="./")
{
	$err_msj = $errmsj;
	/*echo "<pre>";
	echo print_r($uploading_files);*/
	// echo count($uploading_files['files']['name'])."<br />";
	// echo var_dump(count($uploading_files['files']['name']));
	$num_of_files = count($uploading_files['files']['name']);
	if($num_of_files>0 && !empty($uploading_files['files']['name'][0])) {
		// foreach ($uploading_files['files']['name'] as $file) {
		for ($i=0; $i <$num_of_files; $i++) { 

			// echo $file;
			if (($uploading_files["files"]["type"][$i] == "image/gif")
		      || ($uploading_files["files"]["type"][$i] == "image/jpeg")
		      || ($uploading_files["files"]["type"][$i] == "image/jpg")
		      || ($uploading_files["files"]["type"][$i] == "image/pjpeg")
		      || ($uploading_files["files"]["type"][$i] == "image/x-png")
		      || ($uploading_files["files"]["type"][$i] == "image/png")
		      || ($uploading_files["files"]["type"][$i] == "video/m4v")
		      || ($uploading_files["files"]["type"][$i] == "video/avi")
		      || ($uploading_files["files"]["type"][$i] == "video/mp4")
		      || ($uploading_files["files"]["type"][$i] == "video/mov")
		      || ($uploading_files["files"]["type"][$i] == "video/mpg")
		      || ($uploading_files["files"]["type"][$i] == "video/mpeg")
		      || ($uploading_files["files"]["type"][$i] == "video/quicktime")
		      || ($uploading_files["files"]["type"][$i] == "video/x-ms-wmv")) 
			{
				if( $uploading_files['files']['error'][$i]>0)
				{
					switch($uploading_files['files']['error'][$i])
					{
						case 1:
						$err_msj .= "Error: " . $uploading_files["files"]["error"][$i] . "<br /> maximum size exceeded.<br />";
						break;
						case 3:
						$err_msj .= "Error: " . $uploading_files["files"]["error"][$i] . "<br /> The uploaded file was only partially uploaded.<br />";
						break;
						case 4:
						$err_msj .= "Error: " . $uploading_files["files"]["error"][$i] . "<br /> No file was uploaded.<br />";
						break;
						case 6:
						$err_msj .= "Error: " . $uploading_files["files"]["error"][$i] . "<br /> Missing a temporary folder. <br />";
						break;
						case 7:
						$err_msj .= "Error: " . $uploading_files["files"]["error"][$i] . "<br /> Failed to write file to disk. <br />";
						break;
						default:
						$err_msj .= "Error: unable to upload, please contact site administrator.<br />";
						break;
					}
				}
				else
				{
					//process
					if (!file_exists($folder_back . "../uploads/".$folder)) {
					    mkdir($folder_back . "../uploads/".$folder, 0777, true);
					    mkdir($folder_back . "../uplbcp/".$folder, 0777, true);
					}

					date_default_timezone_set("Asia/Baghdad");
					$todaydate = date('Ymd');
					//we add trailer to Gis because we don't have microsecond so to avoid wrong overwrite
					// we will add file number and user session.
					$todayhours = date('Gis') . $i . $_SESSION['user_id'];
					$temp = explode(".",$uploading_files["files"]["name"][$i]);
					$newfilename_and_path = "{$folder}/" . $todaydate.$todayhours . '.' .end($temp);
					// echo "<pre>". getcwd().DIRECTORY_SEPARATOR ."</pre>";

					//write all data to the database
					// $err_msj .= getcwd().DIRECTORY_SEPARATOR;
					// $err_msj .= "../uploads/" . $newfilename_and_path;
					if(move_uploaded_file($uploading_files["files"]["tmp_name"][$i], $folder_back . "../uploads/" . $newfilename_and_path))
					{
						$copydone = copy($folder_back . "../uploads/" . $newfilename_and_path, $folder_back . "../uplbcp/" . $newfilename_and_path);
						// var_dump($copydone);
						/*$query = "INSERT INTO attachment(filename, file_desc, patient_activity_f) VALUES
									('{$newfilename_and_path}', 'surgery', {$last_inserted_or_updated_ID})";*/
							$query = $query_part_1 . "'{$newfilename_and_path}'" . $query_part_2;	
						// echo $query;
							// $err_msj = $query;

						// mysql_query($query) or die(mysql_error());
						if(!mysql_query($query)){
							$err_msj .= "activity has been added BUT attachments cannot be stored.";
						}
					}
					else
					{
						$err_msj .= "activity has been added BUT Error in moving attachments to correct directory.";
					}
					
				}
				// echo $err_msj;
			}
			else
			{
				$err_msj .= " wrong file type.";
			}
			/*
			if you don't want to give extra information just uncomment this:
			if (!empty($err_msj)) 
			{
				header('HTTP/1.1 500 Internal Server Error');
			}*/
		
			
		}
	} // end of FILE UPLOAD

	// echo $err_msj;
	return $err_msj;
}

//get Patient according to the parameter
function get_patient($p_id=0, $from=-1, $count=-1, $deleted=0, $fromDate=NULL, $toDate=NULL )
{
	
	//FULL $query = "SELECT patient.fullname, patient.sex, patient.address, patient.dob, patient.phone, patient.occupation, patient.past_hx, patient.family_hx, patient.drug_hx, patient.other_hx, patient.latest_visit, queue_status, patient.u_id_f, patient.`view`, patient.p_id FROM `patient`";
 

	$query = "SELECT p.p_id, p.fullname, p.sex, p.address, p.dob, p.phone, p.occupation, p.past_hx, p.family_hx, p.drug_hx, p.other_hx, p.latest_visit, p.procedures_list, p.queue_status, p.u_id_f, p.latest_followup_visit"; 
	
	$query .= "  FROM patient p";

	$query .= $deleted==0?" WHERE p.view = 1 ":" WHERE p.view = -1 ";
	$query .= $p_id!=0?" AND p.p_id = {$p_id} ":" ";

	if (!is_null($fromDate) && !is_null($toDate)) {
		$query .= " AND (p.latest_visit between '$fromDate' AND '$toDate')";
	}

	$query .= " ORDER BY p.latest_visit";
	if ($from >-1) {
		$query .= " LIMIT $from, $count";
	}
	elseif ($p_id!=0) {
		$query .= " LIMIT 1";
	}
	// echo $query;
	
	$patient_set = mysql_query($query) or die("Q1");
	return $patient_set;
}

// current list (or $date list) of queue
function get_queue_list($date)
{
	/*SELECT v_id, p_id_f, visit_date, followup_date, marital_status, pregnant, breast_feed, no_of_child, other_info, chief_comp_n_dur, examination, investigation, note, fee, in_queue, u_id_f, view FROM visit WHERE 1*/	
	$query = "SELECT v.v_id, v.p_id_f, v.visit_time, v.fee, v.marital_status, v.pregnant, v.breast_feed, v.no_of_child, v.other_info, v.in_queue,
				p.fullname, p.sex , p.phone";
	$query .= " FROM visit v, patient p";
	$query .= " WHERE v.view=1 AND p.view=1 AND v.p_id_f=p.p_id";
	$query .= " AND v.visit_date = '{$date}'";
	$query .= " ORDER BY v.visit_time ASC";
	// echo $query;
	
	$queue_list_set = mysql_query($query) or die("Q2");
	return $queue_list_set;
}

// count visits depending on date
function get_total_visits($date=NULL)
{
	
	$query = "SELECT count(v_id) total_visit ";
	$query .= " FROM visit v";
	$query .= " WHERE v.view=1 ";
	if (!is_null($date) ) {
		$query .= " AND v.visit_date = '{$date}'";
	}
	else{
		$today = date('Y-m-d');
		$query .= " AND v.visit_date = '{$today}'";
	}
	$query;
	
	$total_visits_set = mysql_query($query) or die("Q3");
	return $total_visits_set;
}

function already_in_queue($p_id)
{
	$query = "SELECT p_id FROM patient WHERE p_id={$p_id} AND queue_status=1 AND view=1 LIMIT 1";
	return mysql_num_rows(mysql_query($query))>0?true:false;
}

function already_in_Dr_room($v_id)
{
	$query = "SELECT v_id FROM visit WHERE v_id={$v_id} AND in_queue!=1 AND view=1 LIMIT 1";
	return mysql_num_rows(mysql_query($query))>0?true:false;
}

function get_this_p($v_date, $p_id=0)
{
	//FULL $query = "SELECT patient.fullname, patient.sex, patient.address, patient.dob, patient.phone, patient.occupation, patient.past_hx, patient.family_hx, patient.drug_hx, patient.other_hx, patient.latest_visit, queue_status, patient.u_id_f, patient.`view`, patient.p_id FROM `patient`";
 

	$query = "SELECT p.p_id, p.fullname, p.sex, p.address, p.dob, p.occupation, p.past_hx, p.family_hx, p.drug_hx, p.other_hx, p.latest_followup_visit, p.procedures_list, 
		v.v_id, v.visit_date, v.followup_date, v.marital_status, v.pregnant, v.breast_feed, v.no_of_child, v.other_info, v.fee
	"; 
	
	$query .= "  FROM patient p, visit v";

	$query .= " WHERE p.view = 1 AND v.view = 1 AND p.p_id = v.p_id_f";
	$query .= " AND v.visit_date = '{$v_date}'";
	if ($p_id != 0) {
		$query .= " AND p.p_id = {$p_id}";
	}
	$query .= " AND in_queue = 2";
	$query .= " LIMIT 1";
	
	// echo $query;
	
	$patient_set = mysql_query($query) or die("Q6");
	return $patient_set;
}

//prev visits of patient
function get_prev_visits_list($p_id)
{
	$query = "SELECT v.v_id, v.visit_date, v.followup_date, v.marital_status, v.pregnant, v.breast_feed, v.no_of_child, v.other_info, v.fee, v.chief_comp_n_dur, v.examination, v.investigation, v.investigation_note, v.note, v.treatment_note, v.procedure, v.procedure_date, v.procedure_note ";
	$query .= "FROM visit v ";
	$query .= "WHERE v.p_id_f = {$p_id} AND v.in_queue = -1 AND v.view = 1 ";
	$query .= "ORDER BY visit_date DESC ";
	
	// echo $query;
	$prev_visits_list_set = mysql_query($query) or die("Q7");
	return $prev_visits_list_set;
}

//prev visits selected diagnosis
function get_prev_visits_diagnose($v_id)
{
	$query = "SELECT distinct(diagnose) diagnose_list
			FROM visit_diagnose
			WHERE v_id_f = {$v_id} AND view=1";

	// echo $query;
	$prev_visits_diagnose_set = mysql_query($query) or die("Q8");
	return $prev_visits_diagnose_set;
}

//prev visits selected treatments
function get_prev_visits_treatment($v_id)
{
	$query = "SELECT distinct(treatment) treatment_list
			FROM visit_treatment
			WHERE v_id_f = {$v_id} AND view=1";
	
	// echo $query;
	$prev_visits_treatment_set = mysql_query($query) or die("Q9");
	return $prev_visits_treatment_set;
}

//prev visits selected surgery
function get_prev_visits_surgery($v_id)
{
	$query = "SELECT distinct(surgery) surgery_list, surgery_date, surgery_note
			FROM visit_surgery
			WHERE v_id_f = {$v_id} AND view=1";
	
	// echo $query;
	$prev_visits_surgery_set = mysql_query($query) or die("Q9+");
	return $prev_visits_surgery_set;
}

//get attached pictures to specified visit
function get_attachment($v_id)
{
	$query = "SELECT a_id, filename, file_desc, uploaded_date";
	$query .= " FROM attachment ";
	$query .= " WHERE view=1";
	$query .= " AND v_id_f={$v_id}";
	$query .= " ORDER BY uploaded_date";
	// echo $query;
	$attachment_set = mysql_query($query) or die("Q10");
	return $attachment_set;
}

//is user edit has been compromised to edit admin user
function is_compromised($uid=1)
{
	$query = "SELECT u_id";
	$query .= " FROM user ";
	$query .= " WHERE role=9 AND u_id = {$uid}";
	// echo $query;
	$user_set = mysql_query($query) or die("Q11");
	if (mysql_num_rows($user_set)>0) {
		return true;
	}else{
		return false;
	}
}

// ***

// EXAMPLE:   EXPORT_TABLES("localhost","user","pass","db_name" ); 
		//optional: 5th parameter - to backup specific tables only: array("mytable1","mytable2",...)   
		//optional: 6th parameter - backup filename
		// IMPORTANT NOTE for people who try to change strings in SQL FILE before importing, MUST READ:  goo.gl/2fZDQL
					
// https://github.com/tazotodua/useful-php-scripts  
function EXPORT_TABLES($host,$user,$pass,$name,       $tables=false, $backup_name=false){ 
	set_time_limit(3000); $mysqli = new mysqli($host,$user,$pass,$name); $mysqli->select_db($name); $mysqli->query("SET NAMES 'utf8'");
	$queryTables = $mysqli->query('SHOW TABLES'); while($row = $queryTables->fetch_row()) { $target_tables[] = $row[0]; }	if($tables !== false) { $target_tables = array_intersect( $target_tables, $tables); } 
	$content = "SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";\r\nSET time_zone = \"+00:00\";\r\n\r\n\r\n/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;\r\n/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;\r\n/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;\r\n/*!40101 SET NAMES utf8 */;\r\n--\r\n-- Database: `".$name."`\r\n--\r\n\r\n\r\n";
	foreach($target_tables as $table){
		if (empty($table)){ continue; } 
		$result	= $mysqli->query('SELECT * FROM `'.$table.'`');  	$fields_amount=$result->field_count;  $rows_num=$mysqli->affected_rows; 	$res = $mysqli->query('SHOW CREATE TABLE '.$table);	$TableMLine=$res->fetch_row(); 
		$content .= "\n\n".$TableMLine[1].";\n\n";   $TableMLine[1]=str_ireplace('CREATE TABLE `','CREATE TABLE IF NOT EXISTS `',$TableMLine[1]);
		for ($i = 0, $st_counter = 0; $i < $fields_amount;   $i++, $st_counter=0) {
			while($row = $result->fetch_row())	{ //when started (and every after 100 command cycle):
				if ($st_counter%100 == 0 || $st_counter == 0 )	{$content .= "\nINSERT INTO ".$table." VALUES";}
					$content .= "\n(";    for($j=0; $j<$fields_amount; $j++){ $row[$j] = str_replace("\n","\\n", addslashes($row[$j]) ); if (isset($row[$j])){$content .= '"'.$row[$j].'"' ;}  else{$content .= '""';}	   if ($j<($fields_amount-1)){$content.= ',';}   }        $content .=")";
				//every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
				if ( (($st_counter+1)%100==0 && $st_counter!=0) || $st_counter+1==$rows_num) {$content .= ";";} else {$content .= ",";}	$st_counter=$st_counter+1;
			}
		} $content .="\n\n\n";
	}
	$content .= "\r\n\r\n/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;\r\n/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;\r\n/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;";
	$backup_name = $backup_name ? $backup_name : $name.'___('.date('H-i-s').'_'.date('d-m-Y').').sql';
	$myfile = fopen("./includes/preclean/".$backup_name, "w"); // or die("Unable to open file!");
    fwrite($myfile, $content);
    fclose($myfile);
	/* this code downloads file to user PC
	ob_get_clean();
	header('Content-Type: application/octet-stream');
	header("Content-Transfer-Encoding: Binary");
	header('Content-Length: '. (function_exists('mb_strlen') ? mb_strlen($content, '8bit'): strlen($content)) );
	header("Content-disposition: attachment;filename=\"".$backup_name."\"");

	echo $content;
	exit;*/
}      //see import.php too

// ***
//clean queue by deleting previous inserted visits that didn't come to clinics (patients who write their name but don't come)
function clean_queue()
{
	// $today = date('Y-m-d');
	$today_obj = new DateTime();
	$today_obj->modify('-1 day');
	$yesterday = $today_obj->format('Y-m-d');
	$lastmonth_obj = new DateTime();
	$lastmonth_obj->modify('-1 month');
	$lastmonth = $lastmonth_obj->format('Y-m-d');
	// var_dump($lastmonth);
	// EXPORT_TABLES("localhost", "root", ", "nawrascs_surg");
	// EXPORT_TABLES("localhost", "nawrascs_nunu", "7^^y?271m^c[", "nawrascs_surgery_muhammad");
	// EXPORT_TABLES("localhost", "nawrascs_nunu", "7^^y?271m^c[", "nawrascs_surgery_hemn", array('visit','patient'));
	$query_find_sole_records = "SELECT v_id, p_id_f 
								FROM visit 
								WHERE (visit_date BETWEEN '{$lastmonth}' AND '{$yesterday}') AND (in_queue = 1) AND view = 1";
			// $query_find_sole_records = "SELECT v_id, p_id_f FROM visit WHERE (visit_date BETWEEN '2020-02-20' AND '2020-02-24') AND (in_queue = 1) AND view = 1";
	$sole_records_set = mysql_query($query_find_sole_records) or die("Q1213");
	// echo $query_find_sole_records;
	while ($sole_records = mysql_fetch_assoc($sole_records_set)) {
		// $query = "DELETE FROM visit WHERE v_id = {$sole_records['v_id']} LIMIT 1";
		$query = "UPDATE visit SET view=-1 WHERE v_id = {$sole_records['v_id']}";
		mysql_query($query) or die("Q12" . mysql_error());
		$query = "UPDATE patient SET queue_status=-1 WHERE p_id = {$sole_records['p_id_f']} LIMIT 1";
		mysql_query($query) or die("Q13");
	}
	//now let's dlete patients whov'e be created but not added to queue
	/*$sole_patients = "DELETE patient
					FROM patient
					LEFT JOIN visit ON (patient.p_id = visit.p_id_f)
					WHERE visit.p_id_f IS NULL";
	mysql_query($sole_patients) or die("Q1213_sub" . mysql_error());*/

	// in room patients
	$query_find_inroom_patients = "SELECT v_id, p_id_f 
								FROM visit 
								WHERE (visit_date BETWEEN '{$lastmonth}' AND '{$yesterday}') AND (in_queue = 2)";
	$inroom_patients_set = mysql_query($query_find_inroom_patients) or die("Q1415");
	while ($inroom_patients = mysql_fetch_assoc($inroom_patients_set)) {
		$query = "UPDATE visit SET in_queue=-1 WHERE v_id = {$inroom_patients['v_id']} LIMIT 1";
		mysql_query($query) or die("Q14" . mysql_error());
		$query = "UPDATE patient SET queue_status=-1 WHERE p_id = {$inroom_patients['p_id_f']} LIMIT 1";
		mysql_query($query) or die("Q15");
	}
}

function available_visit_dates()
{
	$query = "SELECT visit_date, count(visit_date) listcount
			FROM visit
			WHERE in_queue = 1 AND view =1 
			GROUP BY visit_date";
	$available_visit_dates_set = mysql_query($query) or die("Q14");
	/*echo "<pre>";
	print_r($available_visit_dates_set);
	echo "</pre>";*/
	return $available_visit_dates_set;
}


//get payment reports
//get today or 'date' total payment
function get_today_total_payment($date)
{
	$query = "SELECT SUM(fee) total_payment FROM visit WHERE visit_date = '{$date}' AND in_queue=-1 AND view=1 LIMIT 1";
	$today_total_payment_set = mysql_query($query) or die("Q15");
	$today_total_payment = 0;
	if (mysql_num_rows($today_total_payment_set)>0) {
		$today_total_payment = mysql_fetch_assoc($today_total_payment_set);
		$today_total_payment = $today_total_payment['total_payment'];
		// $today_total_payment = !empty(today_total_payment['total_payment'])?today_total_payment['total_payment']:0;
	}
	return $today_total_payment;
}

function get_total_free_p($date)
{
	$query = "SELECT count(v_id) free_visit FROM visit WHERE visit_date = '{$date}' AND fee=0 AND in_queue=-1 AND view=1 LIMIT 1";
	$total_free_p_set = mysql_query($query) or die("Q16");
	$total_free_p = 0;
	if (mysql_num_rows($total_free_p_set)>0) {
		$total_free_p = mysql_fetch_assoc($total_free_p_set);
		$total_free_p = $total_free_p['free_visit'];
		// $total_free_p = !empty(total_free_p['total_payment'])?total_free_p['total_payment']:0;
	}
	return $total_free_p;
}

function get_monthly_total_income($month="")
{
	// SELECT Month(visit_date),Year(visit_date), SUM(fee) income FROM visit WHERE view = 1 GROUP by Year(visit_date), Month(visit_date)
	$query = "SELECT Month(visit_date),Year(visit_date), SUM(fee) income  FROM visit WHERE view = 1 ";
	if (!empty($month)) {
		// $query .= "MONTH(visit_date) = '{$month}' ";
		$query .= "AND MONTHNAME(visit_date) = '{$month}' GROUP by Year(visit_date) DESC ";
	}else {
		$query .= "GROUP BY visit_date ";
	}
	$query .= "LIMIT 1";
	
	$monthly_total_income_set = mysql_query($query) or die("Q16");
	
	return $monthly_total_income_set;
}

function get_yearly_total_income($year="")
{
	$query = "SELECT SUM(fee) income FROM visit WHERE view = 1 ";
	if (!empty($year)) {
		$query .= "AND YEAR(visit_date) = '{$year}' ";
	}else {
		$query .= "GROUP BY visit_date";
	}
	$query .= "LIMIT 1";
	$yearly_total_income_set = mysql_query($query) or die("Q16");
	
	return $yearly_total_income_set;
}

function get_yearly_total_expense($year="")
{
	$query = "SELECT SUM(amount) expense_amount FROM expense WHERE view = 1 ";
	if (!empty($year)) {
		$query .= "AND YEAR(exp_date) = '{$year}' ";
	}else {
		$query .= "GROUP BY exp_date";
	}
	$query .= "LIMIT 1";
	// echo $query;
	$yearly_total_expense_set = mysql_query($query) or die("Q16_2");
	
	return $yearly_total_expense_set;
}

//list of selected attributes
function get_attr_list($v_id)
{
	$query = "SELECT attr, attr_order ";
	$query .= "FROM visit_attr ";
	$query .= "WHERE v_id_f = {$v_id} AND view = 1 ";
	$query .= "ORDER BY attr_order ASC ";
	
	// echo $query;
	$attr_list = mysql_query($query) or die("Q17");
	return $attr_list;
}

// check view permisssion for current user
function has_view_permission($page)
{
	//not completed
	if ($_SESSION['role']==9) {
		switch ($page) {
			case 'value':
				# code...
				break;
			
			default:
				return false;
				break;
		}
	}
}
// change date format to date format in output
function FormcorrectDate($ddate)
{
	$newdate="";
	//ddate = 24-02-2016 to 2016-02-24
	if (!empty($ddate)) {
		$ddate=explode("-",$ddate);
		$d=$ddate[0];
		$m=$ddate[1];
		$y=$ddate[2];
		$newdate="$y-$m-$d";
	}
	
	
	return $newdate;
}

// USER CONTROL
function get_users()
{
	$query = "SELECT u_id, full_name, username, role, view
				FROM `user`";
	$user_set = mysql_query($query) or die("Q1_usrcntrl ". mysql_error());
	return $user_set;
}

?>