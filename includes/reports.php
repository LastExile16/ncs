<?php
require_once 'connection.php';
require_once 'functions.php';
if (isset($_POST['create_chart'])) {
	/*
		agar la zhmarai patient har halayak habet awa lawanaya
		patientek zyad krabet bas hich visiti lo danandrabi
	*/

/*		$arrDatasets1 = array(array(
            "data"=> array(),
             "backgroundColor"=> [
                "#36A2EB",
                "#FF6384",
                "#FFCE56"
            ],
            "hoverBackgroundColor"=> [
                "#36A2EB",
                "#FF6384",
                "#FFCE56"
            ])
        );
*/

// print_r($arrDatasets1);
		//query to get data
		/*$data = array('1'=>0,'2'=>0,'3'=>0);
		
		$query = "SELECT p.sex, count(p_id) counter
				FROM patient p 
				WHERE p.view=1
				GROUP BY p.sex ORDER BY p.sex";
		$result = mysql_query($query) or die(mysql_error() . "101");

		//loop through the returned data
		while($row = mysql_fetch_assoc($result)) {
			foreach($data as $k=>$v){
				if($k==$row['sex']){
					$data[$k] = $row["counter"];
					break;
				}
			}
		}



		$arrDatasets1[0]["data"]= array($data["1"], $data["2"], $data["3"]);
		
// print_r($arrDatasets1);



		$arrLabels1 = array("Male", "Female", "Transgender");
		$arrReturn[0] = array('labels' => $arrLabels1, 'datasets' => $arrDatasets1);*/

		if (isset($_POST['rdate'])) {
			$date = DateTime::createFromFormat('Y-m', $_POST['rdate']);
			$last6month_obj = clone $date;
			$this_month = $date->format('m');
			$this_year = $date->format('Y');

		}else {
			$this_month = date('m');
			$this_year = date('Y');
			$last6month_obj = new DateTime();
		}

//line chart
		// $month_index = intval(date('m'))-1;
		// $last6month_obj = new DateTime();
		$last6month_obj->modify('-6 month');
		$last_year = $last6month_obj->format('Y');
		$last6month_index = intval($last6month_obj->format('m'));

		$months_label = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		$arrLabels1 = array();
		$dataline = array();
		for ($i=0; $i < 6; $i++) { 
			$arrLabels1[] = $months_label[($last6month_index+$i)%12];
			$key = (($last6month_index+$i)%12)+1;
			$dataline[$key] =  "0";
		}
		// print_r($arrLabels1);
		// print_r($dataline);
		/*
( labels
    [0] => August
    [1] => September
    [2] => October
    [3] => November
    [4] => December
    [5] => January
)

Array
( data
    [8] => 0
    [9] => 0
    [10] => 0
    [11] => 0
    [12] => 0
    [1] => 0
)
		*/
		$arrDatasets1 = array(array(
            "data"=> array(),
            "label" => "Visits",
            "fill" => false,
            "lineTension" => 0,
            "backgroundColor" => "rgba(75,192,192,0.4)",
            "borderColor" => "rgba(75,192,192,1)",
            "pointBorderColor" => "rgba(75,192,192,1)",
            "pointBorderWidth" => 5,
            "pointHoverRadius" => 7,
            "pointHoverBackgroundColor" => "rgba(75,192,192,1)",
            "pointHoverBorderColor" => "rgba(220,220,220,1)",
            "pointHoverBorderWidth" => 2,
            "pointRadius" => 1,
            "pointHitRadius" => 10
            )
        );

		// $this_year = date('Y');
        //query
        $query = "SELECT COUNT(visit.v_id) total_visits, MONTH(visit_date) `month`, YEAR(visit_date) `year`
				FROM `visit` ";
		$query .= " WHERE visit.`view` = 1 ";
		$query .= isset($_POST['rdate'])?" AND (YEAR(visit_date) <= '{$this_year}' AND YEAR(visit_date) >= '{$last_year}')":"";
		$query .= " GROUP BY YEAR(visit_date), MONTH(visit_date)
				ORDER BY `year` ASC,`month`";
		// echo $query;
		// $arrReturn[4] = $query;
		$month_counter_set = mysql_query($query) or die("102");
		//loop through the returned data
		while($row = mysql_fetch_assoc($month_counter_set)) {
			foreach($dataline as $k=>$v) {
				if($k==$row['month']) {
					$dataline[$k] = $row["total_visits"];
					break;
				}
			}
		}

        foreach ($dataline as $value) {
			$arrDatasets1[0]["data"][]= $value;
		}
		$arrReturn[0] = array('labels' => $arrLabels1, 'datasets' => $arrDatasets1);


//surgery

$colors = ["#36A2EB",
        "#FF6384",
        "#FFCE56",
        "#4CFF00",
        "#7FFFFF",
        "#FF6A00",
        "#CEED34",
        "#E78AF2",
        "#604EBA",
        "#FFF6A5",
        "#94CE0C",
        "#EF6926",
        "#BF0DA4",
        "#EF9709",
        "#EF9EE2",
        "#B9D863",
        "#FC3A98",
        "#B2FFF4",
        "#53B3BA",
        "#C0C0C0"];
$hover_colors = ["#2691DA",
                "#EE5273",
                "#EEBC34",
                "#2ADD00",
                "#5DDDDD",
                "#FE5900",
                "#9BB227",
                "#AF6AB7",
                "#463A87",
                "#CCC482",
                "#79A80B",
                "#AF4D1C",
                "#990A83",
                "#D66D1D",
                "#E57EBF",
                "#8BA34B",
                "#CE2F7C",
                "#8ECCC2",
                "#41898E9",
                "#B0B0B0"];
		/*$arrDatasets2 = array(array(
            "data"=> array(),
            "backgroundColor"=> array(),
            "hoverBackgroundColor"=> array()
            )
        );*/

	$query = "SELECT count(vs_id) surg_counter, surgery
			FROM visit_surgery vs
			WHERE vs.view=1 AND MONTH(vs.surgery_date) = '{$this_month}' AND YEAR(vs.surgery_date) = '{$this_year}'
			GROUP BY surgery ORDER BY vs_id";
	$result = mysql_query($query) or die(mysql_error() . "104");
	$row_counter = mysql_num_rows($result);
	$arrLabels2 = array();
	$data_pie_treat = array();
	//loop through the returned data
	$i = 0;
	while($row = mysql_fetch_assoc($result)) {
		$arrLabels2[] = $row['surgery'];
		$arrDatasets2[0]["data"][] = $row['surg_counter'];
		$arrDatasets2[0]["backgroundColor"][] = $colors[$i%$row_counter];
		$arrDatasets2[0]["hoverBackgroundColor"][] = $hover_colors[$i%$row_counter];
		$i++;
	}


	/*foreach ($data_pie_treat as $value) {
		$arrDatasets2[0]["data"][]= $value;
	}*/
	
// print_r($arrDatasets2);
// set these vars if query above returned empty.
	// this is useful to avoid NOTICE report from PHP
if (!isset($arrDatasets2)) {
	$arrDatasets2 = array();
	$arrDatasets2[0]["data"][] = 1;
	$arrDatasets2[0]["backgroundColor"][] = "#B5B1B1";
	$arrDatasets2[0]["hoverBackgroundColor"][] = "#B5B1B1";
}

	$arrReturn[1] = array('labels' => $arrLabels2, 'datasets' => $arrDatasets2);

//procedure
	$query = "SELECT count(v_id) proc_counter, `procedure`
			FROM visit v
			WHERE v.view=1 AND MONTH(v.procedure_date) = '{$this_month}' AND YEAR(v.procedure_date) = '{$this_year}' AND `procedure` != ''
			GROUP BY `procedure` ORDER BY proc_counter";
	$result = mysql_query($query) or die(mysql_error() . "104");
	$row_counter = mysql_num_rows($result);
	$arrLabels3 = array();
	$data_pie_treat = array();
	//loop through the returned data
	$i = 0;
	while($row = mysql_fetch_assoc($result)) {
		$arrLabels3[] = $row['procedure'];
		$arrDatasets3[0]["data"][] = $row['proc_counter'];
		$arrDatasets3[0]["backgroundColor"][] = $colors[$i%$row_counter];
		$arrDatasets3[0]["hoverBackgroundColor"][] = $hover_colors[$i%$row_counter];
		$i++;
	}
	if (!isset($arrDatasets3)) {
		$arrDatasets3 = array();
		$arrDatasets3[0]["data"][] = 1;
		$arrDatasets3[0]["backgroundColor"][] = "#B5B1B1";
		$arrDatasets3[0]["hoverBackgroundColor"][] = "#B5B1B1";
	}
	$arrReturn[2] = array('labels' => $arrLabels3, 'datasets' => $arrDatasets3);
/*
	//clinics
	$query = "SELECT b.name branch, count(id) counter, 'clinics' label
				FROM patient_activity p, branch b 
				WHERE p.view=1 AND b.view=1 AND type=1 AND branch_id_f = b.b_id
				GROUP BY b.name ORDER BY b.b_id";
	$clinics_result = mysql_query($query) or die(mysql_error() . "100");



$arrDatasets3=array();
$arrDatasets3[0] = array(
		"label"=>"clinics",
		"data" => array(),
		"backgroundColor" => array('rgba(255, 99, 132, 0.7)', 'rgba(255, 99, 132, 0.7)', 'rgba(255, 99, 132, 0.7)'),
		"borderColor" => array('rgba(255,99,132,1)','rgba(255,99,132,1)', 'rgba(255,99,132,1)'),
		"borderWidth" => 1
			);
$arrDatasets3[1] = array(
		"label"=>"surgery",
		"data" => array(),
		"backgroundColor" => array('rgba(75, 192, 192, 0.7)', 'rgba(75, 192, 192, 0.7)', 'rgba(75, 192, 192, 0.7)'),
		"borderColor" => array('rgba(75, 192, 192, 1)','rgba(75, 192, 192, 1)', 'rgba(75, 192, 192, 1)'),
		"borderWidth" => 1
			);
$arrDatasets3[2] = array(
		"label"=>"other projects",
		"data" => array(),
		"backgroundColor" => array('rgba(5, 12, 192, 0.7)', 'rgba(5, 12, 192, 0.7)', 'rgba(5, 12, 192, 0.7)'),
		"borderColor" => array('rgba(175, 192, 129, 1)','rgba(175, 192, 192, 1)', 'rgba(175, 192, 129, 1)'),
		"borderWidth" => 1
			);

	// $data_clinics = array();
	$data_clinics = array('Erbil'=>0,'Duhok'=>0,'Sulaimania'=>0);
	//loop through the returned data
	while($row = mysql_fetch_assoc($clinics_result)) {
		// $data_clinics[] = $row['counter'];
		foreach($data_clinics as $k=>$v){
		  if($k==$row['branch']){
		      $data_clinics[$k] = $row["counter"];
		      break;
		    }
		}
	}
	foreach ($data_clinics as $value) {
		$arrDatasets3[0]["data"][]= $value;
	}



	//surgery
	$query = "SELECT b.name branch, count(id) counter, 'surgery' label
					FROM patient_activity p, branch b 
					WHERE p.view=1 AND b.view=1 AND type=2 AND branch_id_f = b.b_id
					GROUP BY b.name ORDER BY b.b_id";
		$result = mysql_query($query) or die(mysql_error() . "101");

		// $data_surgery = array();
		//loop through the returned data
		$data_surgery = array('Erbil'=>0,'Duhok'=>0,'Sulaimania'=>0);
		while($row = mysql_fetch_assoc($result)) {
			// $data_surgery[] = $row['counter'];
			foreach($data_surgery as $k=>$v){
				if($k==$row['branch']){
					$data_surgery[$k] = $row["counter"];
					break;
				}
			}

		}

		foreach ($data_surgery as $value) {
			$arrDatasets3[1]["data"][]= $value;
		}

	//other projects
	$query = "SELECT b.name branch, count(p_id) counter, 'other projects' label
					FROM project p, branch b 
					WHERE p.view=1 AND b.view=1 AND branch_id_f = b.b_id
					GROUP BY b.name ORDER BY b.b_id";
		$result = mysql_query($query) or die(mysql_error() . "102");

		// $data_other = array();

		$data_other = array('Erbil'=>0,'Duhok'=>0,'Sulaimania'=>0);

		//loop through the returned data
		while($row = mysql_fetch_assoc($result)) {
			// $data_other[] = $row['counter'];
			foreach($data_other as $k=>$v){
				if($k==$row['branch']){
					$data_other[$k] = $row["counter"];
					break;
				}
			}
		}

		foreach ($data_other as $value) {
			$arrDatasets3[2]["data"][]= $value;
		}


		$arrLabels = array("Erbil", "Duhok", "Sulaimani");

		$arrReturn[1] = array('labels' => $arrLabels, 'datasets' => $arrDatasets3);

*/
	//now print the data
	print json_encode($arrReturn);
}

?>