<?php  
$prev_vid = $prev_visit['v_id'];
$prev_data =

"<div class='panel panel-danger'>
	<div class='panel-heading clearfix'>
        <span class='print_span btn-group pull-right'>
        	<a href='#' id='printP-{$prev_visit['v_id']}' name='printP' data-vid='{$prev_visit['v_id']}' data-pid='{$patient['p_id']}' class='btn btn-default printPdata red glowing' title='Print Patient Information'>Print Info. </a>
        </span>
		<div class='caption panel-title pull-left'>
	        <span class='fa fa-stethoscope table_head_icon'></span> Previous Visits <span id='prev_visit_date-{$prev_visit['v_id']}'>{$prev_visit['visit_date']}</span>
		</div>
	</div>
	<div class='panel-body resposive'>
		<div class='row'>
			";
?>
		<?php  
			$prev_marital_st = "";
	        switch ($prev_visit['marital_status']) {
	            case '1':
	                $prev_marital_st = "Single";
	                break;
	            case '2':
	                $prev_marital_st = "Married";
	                break;
	            case '3':
	                $prev_marital_st = "Divorced";
	                break;
	            case '4':
	                $prev_marital_st = "Widowed";
	                break;
	        }
	        $prev_pregnant = $prev_visit['pregnant']==1?"Pregnant":"Not Pregnant";
	        $prev_breast_feed = $prev_visit['breast_feed']==1?"Yes":"No";

	        $prev_p_basic_info = "
	        			<div class='row'>
		        			<div class='col-md-3'>
							    <label>Marital Status: </label> <label id='prev_marital_st-{$prev_vid}'>{$prev_marital_st}</label>
							</div>
							<div class='col-md-3'>
							    <label>Fee: </label> <label id='fee-{$prev_vid}'>{$prev_visit['fee']}</label>
							</div>
							<div class='col-md-3'>
							    <label>Expected visit was on: </label> <label id='followup_date-{$prev_vid}'>{$prev_visit['followup_date']}</label>
							</div>
							<div class='col-md-3'>
							    <label>no. of children: </label> <label id='no_of_child-{$prev_vid}'>{$prev_visit['no_of_child']}</label>
							</div>
						</div>
						<div class='row'>
							<div class='col-md-3'>
							    <label>other info: </label> <label id='prev_vid_{$prev_vid}'>{$prev_visit['other_info']}</label>  
							</div>
	        				";
	        //female related info; if preg 0 it means than nothing chosen so the patient should be male
			$prev_p_basic_info .= $prev_visit['pregnant']!=0?"
			        <div class='col-md-3'>
			            <label>Breastfeeding: </label> <label id='prev_breast_feed_{$prev_vid}'>{$prev_breast_feed}</label>
			        </div>
			        <div class='col-md-3'>
			            <label>Pregnancy: </label> <label id='prev_pregnant_{$prev_vid}'>{$prev_pregnant}</label>
			        </div>
			    </div>":"</div>";

			$prev_p_basic_info .= "<hr><div class='clearfix'></div>";

		?>

<?php 
$visits_diagnose_arr = array();
$visits_treatment_arr = array();
$visits_surgery_arr = array();

$prev_visits_diagnose_set = get_prev_visits_diagnose($prev_visit['v_id']);
while($visits_diagnose = mysql_fetch_assoc($prev_visits_diagnose_set))
{
	$visits_diagnose_arr[] = $visits_diagnose['diagnose_list'];
}
$prev_visits_treatment_set = get_prev_visits_treatment($prev_visit['v_id']);
while($visits_treatment =mysql_fetch_assoc( $prev_visits_treatment_set))
{
	$visits_treatment_arr[] = $visits_treatment['treatment_list'];
}
$prev_visits_surgery_set = get_prev_visits_surgery($prev_visit['v_id']);
$prev_visits_surgery_date="";
$prev_visits_surgery_note="";
// echo $prev_visit['v_id'] . "<br>";
while($visits_surgery =mysql_fetch_assoc( $prev_visits_surgery_set))
{
	$visits_surgery_arr[] = $visits_surgery['surgery_list'];
	$prev_visits_surgery_date = $visits_surgery['surgery_date'];
	$prev_visits_surgery_note = $visits_surgery['surgery_note'];
}
// print_r($visits_surgery_arr);
// echo $prev_visits_surgery_date . ", ";
// echo $prev_visits_surgery_note . ", ";
// echo "<br>";
// $visits_diagnose_value = count($visits_diagnose_arr)>1?implode(', ', array_filter($visits_diagnose_arr)):$visits_diagnose_arr[0] . ',';
$diagnose_count = count($visits_diagnose_arr);
if ($diagnose_count>1) {
	$visits_diagnose_value = implode(', ', array_filter($visits_diagnose_arr));
}elseif ($diagnose_count==1) {
	$visits_diagnose_value = $visits_diagnose_arr[0] . ',';
}else{
	$visits_diagnose_value ='';
}

// $visits_treatment_value = count($visits_treatment_arr)>1?implode(', ', array_filter($visits_treatment_arr)):$visits_treatment_arr[0] . ',';
$treatment_count = count($visits_treatment_arr);
if ($treatment_count>1) {
	$visits_treatment_value = implode(', ', array_filter($visits_treatment_arr));
}elseif ($treatment_count==1) {
	$visits_treatment_value = $visits_treatment_arr[0] . ',';
}else{
	$visits_treatment_value ='';
}
// echo $prev_visit['procedure'];

// $visits_surgery_value = count($visits_surgery_arr)>1?implode(', ', array_filter($visits_surgery_arr)):$visits_surgery_arr[0] . ',';
$surgery_count = count($visits_surgery_arr);
if ($surgery_count>1) {
	$visits_surgery_value = implode(', ', array_filter($visits_surgery_arr));
}elseif ($surgery_count==1) {
	//add comma because without comma the tokenfield won't showup
	$visits_surgery_value = $visits_surgery_arr[0] . ',';
}else{
	$visits_surgery_value ='';
}

$prd_date = $prev_visit['procedure_date']!="0000-00-00"?$prev_visit['procedure_date']:"";
$prd_value = preg_match("/,/", $prev_visit['procedure'])==1?$prev_visit['procedure']:$prev_visit['procedure'].',';

/*echo "<pre>";
echo $prev_visit['v_id'];
print_r($visits_treatment_value);
echo "</pre>";*/
$options = array("","","");
$option_attr1_0="selected='selected'";
$option_attr1_1="";
$option_attr1_2="";
$option_attr1_3="";
$option_tags_attr2 = "";
$option_tags_attr3 = "";
$style_attr2="style='display:none'";
$style_attr3="style='display:none'";

$attr_list = get_attr_list($prev_visit['v_id']);
if (mysql_num_rows($attr_list)>0) {
		
	$i=0;
	while($attributes =mysql_fetch_assoc( $attr_list))
	{
		$options[$i] = $attributes['attr'];
		$i++;
	}
	if ($options[0]=='Cardiac') {
		$option_attr1_1="selected='selected'";
		$option_attr1_0 = '';
		$style_attr2 = '';
		$style_attr3 = '';

		// $data2 = array( array("name" => "Adult", "value" => "Adult") , array("name" => "Pediatric", "value" => "Pediatric"));
		if($options[1]=='Adult') {
			$selected2_0 = "selected='selected'";
			$data3 = array("CABG","Valve","Aorta");
			
			$selected3 = '';
			$j=0;
			while ( $j<3) {
				$selected3 = $options[2] == $data3[$j]?"selected='selected'":"";
				$option_tags_attr3 .= "<option {$selected3} value='{$data3[$j]}'>{$data3[$j]}</option>";
				$j++;
			}
		}
		elseif($options[1]=='Pediatric') {
			$selected2_1 = "selected='selected'";
			$data3 = array( "ASD", "VSD", "TOF", "Sub Aortic ridge", "PA-Banding", "Glenn", "CoAractation", "PDA");
			$selected3 = '';
			$j=0;
			while ( $j<8) {
				$selected3 = $options[2] == $data3[$j]?"selected='selected'":"";
				$option_tags_attr3 .= "<option {$selected3} value='{$data3[$j]}'>{$data3[$j]}</option>";
				$j++;
			}
		}
		$option_tags_attr2 = "<option {$selected2_0} value='Adult'>Adult</option>";
		$option_tags_attr2 .= "<option {$selected2_1} value='Pediatric'>Pediatric</option>";

	}elseif ($options[0]=='Vascular') {

		$option_attr1_2="selected='selected'";
		$option_attr1_0 = '';
		$style_attr2 = '';
		$data2 = array('Arterial Diseases', 'Peripheral Vajcular', 'Aneurysm', 'AV-Fistula Creation', 'Venous Diseases');
		$selected2 = '';
		$h=0;
		while ( $h<5) {
			$selected2 = $options[1] == $data2[$h]?"selected='selected'":"";
			$option_tags_attr2 .= "<option {$selected2} value='{$data2[$h]}'>{$data2[$h]}</option>";
			$h++;
		}

		if($options[1]=="Venous Diseases")
		{
			$style_attr3 = '';
			$selected3_1 = '';
			$selected3_0 = '';
			if($options[2]=="DVT") {
				$selected3_1 = "selected='selected'";
			}else {
				$selected3_0 = "selected='selected'";
			}
			$option_tags_attr3 = "<option {$selected3_0} value='Varicose Veins'>Varicose Veins</option>";
			$option_tags_attr3 .= "<option {$selected3_1} value='DVT'>DVT</option>";
		}
	}elseif ($options[0]=='Thoracic') {
		$option_attr1_3="selected='selected'";
		$option_attr1_0 = '';
		$style_attr2 = '';
		$data3 = array("Lung Rejection Surgery","Decortication","Hydatid. Lung");
		$selected3 = '';
		$j=0;
		while ( $j<3) {
			$selected3 = $options[1] == $data3[$j]?"selected='selected'":"";
			$option_tags_attr2 .= "<option {$selected3} value='{$data3[$j]}'>{$data3[$j]}</option>";
			$j++;
		}
	}else{
		$option_attr1_0="selected='selected'";
		$style_attr2 = '';
		$style_attr3 = '';
	}
}

$prev_data .="
			<div class='row'>
            <form class='form-horizontal' enctype='multipart/form-data' action='./includes/form_submitions.php?to=current_p' id='editprevpatientform-{$prev_visit['v_id']}' method='POST' onsubmit='return checkfiles(\"attachfiles-{$prev_visit['v_id']}\")'>
                 
                <input id='v_id-{$prev_visit['v_id']}' name='prev_v_id' class='form-control' type='hidden' value='{$prev_visit['v_id']}' >
                <input id='p_id-{$patient['p_id']}-{$prev_visit['v_id']}' name='prev_p_id_f' class='form-control' type='hidden' value='{$patient['p_id']}' >
                <!-- visit attributes -->
                <div class='form-group'>
                  <label class='col-md-3 control-label' for='attr1'>Attribute1</label>
                  <div class='col-md-7'>
                    <!-- data-visits=1: its current form, 2: its prev form -->
                    <select data-visits='2' data-vid='{$prev_visit['v_id']}' id='attr1-{$prev_visit['v_id']}' name='attr1-{$prev_visit['v_id']}' class='form-control attr1Class'>
                       <option id='0p{$prev_visit['v_id']}' {$option_attr1_0} value='0'>None</option>
                       <option id='1p{$prev_visit['v_id']}' {$option_attr1_1} value='Cardiac'>Cardiac</option>
                       <option id='2p{$prev_visit['v_id']}' {$option_attr1_2} value='Vascular'>Vascular</option>
                       <option id='3p{$prev_visit['v_id']}' {$option_attr1_3} value='Thoracic'>Thoracic</option>
                 </select>
                  </div>
                </div>

                <div class='form-group' id='attr2list-{$prev_visit['v_id']}' {$style_attr2}>
                  <label class='col-md-3 control-label' for='attr2'>Attribute2</label>
                  <div class='col-md-7'>
                    <!-- data-id=1: its current form, 2: its prev form -->
                    <select data-visits='2' data-vid='{$prev_visit['v_id']}' id='attr2-{$prev_visit['v_id']}' name='attr2-{$prev_visit['v_id']}' class='form-control attr2Class'>
	                    {$option_tags_attr2}
                      
                 </select>
                  </div>
                </div>

                <div class='form-group' id='attr3list-{$prev_visit['v_id']}' {$style_attr3}>
                  <label class='col-md-3 control-label' for='attr3'>Attribute3</label>
                  <div class='col-md-7'>
                    <!-- data-id=1: its current form, 2: its prev form -->
                    <select data-visits='2' data-vid='{$prev_visit['v_id']}' id='attr3-{$prev_visit['v_id']}' name='attr3-{$prev_visit['v_id']}' class='form-control attr3Class'>
	                    {$option_tags_attr3}
                      
                 </select>
                  </div>
                </div>
                <div class='form-group'>
                	<label class='col-md-3 control-label' for='prev_attr_save'></label>
                    <a id='prevattributs{$prev_visit['v_id']}' href='javascript:;' class='btn btn-default enabler' data-name='19' data-id='{$prev_visit['v_id']}' title='update data'>
				        	<span class='fa fa-cloud-upload'> </span><span> Update attributes data</span>
			        </a>
                </div>
    <!-- end of visit attributes -->
                <!-- Text input-->
                <div class='form-group'>
                  <label class='col-md-3 control-label' for='chief_com_n_duration'>chief complain and present history</label>  
                  <div class='col-md-7'>
                  <div class='input-group'>
                  <input id='chief_com_n_duration-{$prev_visit['v_id']}' name='chief_com_n_duration_{$prev_visit['v_id']}' placeholder='chief complain & present history' class='form-control' type='text' value='{$prev_visit['chief_comp_n_dur']}' >
                    <span class='input-group-btn'>
				        <a id='chief_com_n_duration{$prev_visit['v_id']}' href='javascript:;' class='btn btn-default enabler' data-name='1' data-id='{$prev_visit['v_id']}' title='update data'>
				        	<span class='fa fa-cloud-upload'> </span>
				        </a>
				    </span>
				    </div>
                  </div>
                </div>

                <!-- Text input-->
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='examination'>Examination</label>  
                    <div class='col-md-7'>
                    <div class='input-group'>
                    
                        <input id='examination-{$prev_visit['v_id']}' name='examination_{$prev_visit['v_id']}' placeholder='Examination' class='form-control' type='text' value='{$prev_visit['examination']}' >
						<span class='input-group-btn'>
					     <a id='examination{$prev_visit['v_id']}' href='javascript:;' class='btn btn-default enabler' data-name='2' data-id='{$prev_visit['v_id']}' title='update data'>
					        	<span class='fa fa-cloud-upload'> </span>
					        </a>
					    </span>
				    </div>
                    </div>
                </div>

                <!-- Text input-->
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='investigation'>Investigation</label>  
                    <div class='col-md-7'>
	                    <div class='input-group'>
							<span class='input-group-btn'>
						        <a class='print_inv btn btn-default' data-id='{$prev_visit['v_id']}' id='inv_print-{$prev_visit['v_id']}' href='javascript:;' title='print'>
								    <span class='glyphicon glyphicon-print'></span>
								</a>
						    </span>
	                        <input id='investigation-{$prev_visit['v_id']}' name='investigation_{$prev_visit['v_id']}' autocomplete='false' placeholder='Investigation' class='form-control' type='text' value='{$prev_visit['investigation']}'>
							<span class='input-group-btn'>
						        <a id='investigation{$prev_visit['v_id']}' href='javascript:;' class='btn btn-default enabler' data-name='3' data-id='{$prev_visit['v_id']}' title='update data'>
						        	<span class='fa fa-cloud-upload'> </span>
						        </a>
						    </span>
					    </div>

                    </div>
                </div>
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='investigation_note'>Investigation Notes</label>  
                    <div class='col-md-7'>
	                    <div class='input-group'>
                        	<textarea id='investigation_note-{$prev_visit['v_id']}' name='investigation_note' placeholder='investigation note' class='form-control'>{$prev_visit['investigation_note']}</textarea>
                    	<span class='input-group-btn'>
					        <a id='investigation_note{$prev_visit['v_id']}' href='javascript:;' class='btn btn-default enabler' data-name='6' data-id='{$prev_visit['v_id']}' title='update data'>
					        	<span class='fa fa-cloud-upload'> </span>
					        </a>
					    </span>
                    	</div>
                    </div>
                </div>

                <div class='form-group'>
                    <label class='col-md-3 control-label' for='diagnosis'>Diagnosis</label>  
                    <div class='col-md-7 goBehind4'>
                    <div class='input-group'>
                        <input id='diagnosis-{$prev_visit['v_id']}' name='diagnosis_{$prev_visit['v_id']}' autocomplete='off' placeholder='diagnosis not accepted here CANNOT be stored' class='form-control prev_lists_dig' required='required' type='text' value='{$visits_diagnose_value}'>
					<span class='input-group-btn'>
				        <a id='diagnosis{$prev_visit['v_id']}' href='javascript:;' class='btn btn-default enabler' data-name='16' data-id='{$prev_visit['v_id']}' title='update data'>
				        	<span class='fa fa-cloud-upload'> </span>
				        </a>
				    </span>
				    </div>
                    </div>
                </div>
                
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='treatment'>Treatments</label>  
                    <div class='col-md-7 goBehind3'>
                    <div class='input-group'>
                        <input id='treatment-{$prev_visit['v_id']}' name='treatment_{$prev_visit['v_id']}' autocomplete='off' placeholder='treatments not accepted here CANNOT be stored' class='form-control prev_lists_treat' required='required' type='text' value='{$visits_treatment_value}'>
					<span class='input-group-btn'>
				        <a id='treatment{$prev_visit['v_id']}' href='javascript:;' class='btn btn-default enabler' data-name='17' data-id='{$prev_visit['v_id']}' title='update data'>
				        	<span class='fa fa-cloud-upload'> </span>
				        </a>
				    </span>
				    </div>
                    </div>
                </div>

                <div class='form-group'>
                    <label class='col-md-3 control-label' for='treatment_note'>Treatments Notes</label>  
                    <div class='col-md-7 goBehind'>
                    <div class='input-group'>
                        <textarea id='treatment_note-{$prev_visit['v_id']}' name='treatment_note_{$prev_visit['v_id']}' placeholder='Notes will be printed along with the slip' class='form-control'>{$prev_visit['treatment_note']} </textarea>
					<span class='input-group-btn'>
				        <a id='treatment_note{$prev_visit['v_id']}' href='javascript:;' class='btn btn-default enabler' data-name='4' data-id='{$prev_visit['v_id']}' title='update data'>
				        	<span class='fa fa-cloud-upload'> </span>
				        </a>
				    </span>
				    </div>
                    </div>
                </div>

                <div class='form-group'>
                    <label class='col-md-3 control-label' for='surgery'>Surgeries</label>  
                    <div class='col-md-7 goBehind2'>
                    <div class='input-group'>
                        <input id='surgery-{$prev_visit['v_id']}' name='surgery_{$prev_visit['v_id']}' autocomplete='off' placeholder='Surgeries not accepted here CANNOT be stored' class='form-control prev_lists_surg' type='text' value='{$visits_surgery_value}'>
					<span class='input-group-btn'>
				        <a id='surgery{$prev_visit['v_id']}' href='javascript:;' class='btn btn-default enabler' data-pid='{$patient['p_id']}' data-name='18' data-id='{$prev_visit['v_id']}' title='update data'>
				        	<span class='fa fa-cloud-upload'> </span>
				        </a>
				    </span>
				    </div>
                    </div>
                </div>

                <div class='form-group'>
				    <label for='datetimepicker2-{$prev_visit['v_id']}' class='col-md-3 control-label'>Surgery date</label>
				    <div class='col-md-7 goBehind'>
				        <div class='input-group date rangepick' id='datetimepicker2-{$prev_visit['v_id']}'>
				            <input type='text' autocomplete='false' id='surgery_date-{$prev_visit['v_id']}' class='form-control prev_surgery_date' name='surgery_date_{$prev_visit['v_id']}' value='{$prev_visits_surgery_date}' data-pid='{$patient['p_id']}' data-id='{$prev_visit['v_id']}' data-name=18 title='update data' />
				            <span class='input-group-addon'>
				                <span class='glyphicon glyphicon-calendar'></span>
				            </span>
				        </div>
				    </div>
				</div>
				<div class='form-group'>
                    <label class='col-md-3 control-label' for='surgery_note'>surgery Notes</label>  
                    <div class='col-md-7'>
                        <textarea id='surgery_note-{$prev_visit['v_id']}' data-pid='{$patient['p_id']}' data-id='{$prev_visit['v_id']}' data-name='18' name='surgery_note' placeholder='Surgery Notes' class='form-control'>{$prev_visits_surgery_note}</textarea>
                    </div>
                </div>

				<!-- procedures -->
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='procedure'>Other Procedure</label>  
                    <div class='col-md-7 goBehind1'>
                    	<div class='input-group'>
                        	<input id='procedure-{$prev_visit['v_id']}' name='procedure' autocomplete='off' placeholder='Procedures not accepted here CANNOT be stored' class='form-control prev_lists_prd' type='text' value='{$prd_value}'>
                    		<span class='input-group-btn'>
						        <a id='procedure{$prev_visit['v_id']}' href='javascript:;' class='btn btn-default enabler' data-name='7'  data-pid='{$patient['p_id']}' data-id='{$prev_visit['v_id']}' title='update data'>
						        	<span class='fa fa-cloud-upload'> </span>
						        </a>
						    </span>
                    	</div>
                    </div>
                </div>

                <div class='form-group'>
				   <label for='datetimepicker3-{$prev_visit['v_id']}' class='col-md-3 control-label'>Procedure Date</label>
				   <div class='col-md-7 goBehind'>
				        <div class='input-group date rangepick' id='datetimepicker3-{$prev_visit['v_id']}'>
				            <input type='text' autocomplete='false' id='procedure_date-{$prev_visit['v_id']}' class='form-control prev_procedure_date' name='procedure_date_{$prev_visit['v_id']}' value='{$prd_date}' data-pid='{$patient['p_id']}' data-id='{$prev_visit['v_id']}' data-name='10' title='update data' />
				            <span class='input-group-addon'>
				                <span class='glyphicon glyphicon-calendar'></span>
				            </span>
				        </div>
				   </div>
				</div>
                <div class='form-group'>
				    <label class='col-md-3 control-label' for='procedure_note'>Procedure Notes</label>  
				    <div class='col-md-7 goBehind'>
				        <div class='input-group'>
				        	<textarea id='procedure_note-{$prev_visit['v_id']}' name='procedure_note_{$prev_visit['v_id']}' placeholder='procedure note' class='form-control'>{$prev_visit['procedure_note']}</textarea>
				        	<span class='input-group-btn'>
						        <a id='procedure_note{$prev_visit['v_id']}' href='javascript:;' class='btn btn-default enabler' data-name='8' data-id='{$prev_visit['v_id']}' title='update data'>
						        	<span class='fa fa-cloud-upload'> </span>
						        </a>
						    </span>
				        </div>
				    </div>
				</div>

                <div class='form-group'>
			   <label for='attachment' class='col-md-3 control-label'>Attachments</label>
			   <div class='col-md-7 goBehind'>
				  <div class='input-group'>
					  <label class='input-group-btn'>
						  	<span class='btn btn-primary'>
							  Browse&hellip; <input type='file' id='attachfiles-{$prev_visit['v_id']}' name='files[]' accept='image/gif,image/jpeg,image/jpg,image/pjpeg,image/png,image/x-png,.jpeg,video/*' style='display: none;' multiple='multiple'>
							  
							</span>
					  </label>
					  <input type='text' class='form-control' readonly='readonly'>

					  <span class='input-group-btn'>
				        <span data-id='{$prev_visit['v_id']}' title='update data' class='btn btn-warning re_uploader'><i> </i> Start Upload</span>
				      </span>
				  </div>
					   
					  	
						{$attachments_output}
					  
			   </div>
			</div>

                
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='note'>Notes</label>  
                    <div class='col-md-7'>
                    <div class='input-group'>
                        <textarea id='note-{$prev_visit['v_id']}' name='note_{$prev_visit['v_id']}' placeholder='note' class='form-control'>{$prev_visit['note']}</textarea>
					<span class='input-group-btn'>
				        <a id='note{$prev_visit['v_id']}' href='javascript:;' class='btn btn-default enabler' data-name='5' data-id='{$prev_visit['v_id']}' title='update data'>
				        	<span class='fa fa-cloud-upload'> </span>
				        </a>
				    </span>
				    </div>
                    </div>
                </div>
                <!-- 
                <div class='form-group'>
                   <label for='datetimepicker1-{$prev_visit['v_id']}' class='col-md-3 control-label'>Followup Visit</label>
                   <div class='col-md-7'>
                        <div class='input-group date datepick' id='datetimepicker1-{$prev_visit['v_id']}'>
                            <input type='text' autocomplete='false' id='followup_date-{$prev_visit['v_id']}' class='form-control prev_followup_date' name='followup_date_{$prev_visit['v_id']}' value='{$prev_visit['followup_date']}' data-pid='{$patient['p_id']}' data-id='{$prev_visit['v_id']}' data-name=2 title='update data' />
                            <span class='input-group-addon'>
                                <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                            
                        </div>
                   </div>
                </div>
                -->

                <!-- Button -->
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='prinslip-{$prev_visit['v_id']}'></label>  
                    <div class='col-md-7'>
                    <div class='input-group'>
                    <span class='input-group-btn'>
                        <a id='prinslip-{$prev_visit['v_id']}' data-id='{$prev_visit['v_id']}' class='prinslip btn btn-warning' href='javascript:;'><i class='fa fa-print'></i></a>
				    </span>
				    </div>
                    </div>
                </div>
                
            </form>
            </div> 

		</div>
	</div>
</div>"
// the datepick or rangepick doesn't matter here bcz the elements are newly added---5 april 18
?>
