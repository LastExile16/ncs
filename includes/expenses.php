
<div class="contact row">

	<!-- BEGIN SEARCH TABLE PORTLET-->
	
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>
<?php 
	echo isset($_GET['err_msj']) && !empty($_GET['err_msj'])? "
	<div class='form-group errmsj row' style='display:none'>
		<div class='col-md-12'>
			<div class='alert alert-danger alert-dismissable'>
			<button type='button' class='close' aria-hidden='true'>&times; </button>
				{$_GET['err_msj']}
			</div>
		</div>
	</div>":""; 
?>
<div class="contact row">
	<div class="add-panel col-md-12">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-money"></i> Expenses</h3>
				<?php 
				 // $r = mysql_query("SELECT DATABASE()") or die(mysql_error());
				 //   echo mysql_result($r,0);
				 ?>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" role="form" action="includes/form_submitions.php?to=expenses" method="POST">
			<div class="form-group">
			   <label for="expense" class="col-md-3 control-label">Expense</label>
			   <div class="col-md-5">
				  <input type="text" required="required" class="form-control" id="expense" name="expense">
			   </div>
			</div>

			<div class="form-group">
			   <label for="amount" class="col-md-3 control-label">Amount</label>
			   <div class="col-md-5">
				  <input type="text" required="required" class="form-control" id="amount" name="amount">
			   </div>
			</div>
			<div class="form-group">
               <label for="datetimepicker" class="col-md-3 control-label">Date</label>
               <div class="col-md-5">
                    <div class='input-group date rangepick' id='datetimepicker'>
                        <input type='text' autocomplete="false" id="exp_date" class="form-control" name="exp_date" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
               </div>
            </div>
			<div class="form-group">
			   <div class=" col-md-7">
				  <button type="submit" name="new_expense" class="btn btn-info actionbutton">Add</button>
			   </div>
			</div>
			
			</form>
			</div>
		</div>
	</div>
</div>

