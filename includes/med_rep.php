<script type="text/javascript" src="./js/ckeditor4.7/ckeditor.js"></script>
<!-- <script type="text/javascript" src="./js/ckeditor4.7/sample.js"></script> -->

<div class="contact row">

	<!-- BEGIN SEARCH TABLE PORTLET-->
	
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>

<div class="row nonprintable">
	 
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="col-md-12">
	 	
	 
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="caption panel-title">
				<span class='fa fa-address-card-o'></span> Medical Report
				<span>
					<input id="lang_change" data-lang="1" type="checkbox" data-on="To Kurdish Report" data-off="To English Report" data-toggle="toggle" data-onstyle="warning" data-offstyle="info" data-width="150px" data-style="ios">
				</span>
					<span><button class="btn btn-success printbutton" onclick="printThis()"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print</button></span>
			</div>
		</div>
		<div class="panel-body resposive">
			
			<div class="printable">
	<div class="page-header row">
				<!-- <button type="button" onClick="printThis()" style="background: pink">
			PRINT ME!
		</button> -->
		<div class="col-md-4 col-sm-4 col-xs-4" id="speciality-wrapper">
			<div id="speciality">
				<p><strong>إختصاص جراحة القلب و الأوعیة الدمویة</strong></p>
				<p>پسپۆری نەشتەرگەری دڵ و بۆڕی خوێن </p>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4">
			<div id="dr_title">
				<span>الــدکتــــور</span><br /><span id="docname">هێمن عبدالرحمن عبداللە</span>
				<p><strong id="certificate" style="background-color: blue !important;color: white !important;font-weight: bold;">M.B.CH.B - F.I.B.M.S</strong></p>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4" id="date-wrapper">
			<div id="report_date">بەروار: <?php echo date("d-m-Y"); ?></div>
		</div>
	</div>

	<div class="page-footer">
		<div>
			<div class="col-md-6 col-sm-6 col-xs-6" id="address-wrapper">
				<div id="address">
					<p>هەولێر/ شەقامی (٤٠) مەتری, بەرامبەر نەخۆشخانەی ڕزگاری - کۆمەلگای تۆپ مێد</p>
					<p>اربیل/ شارع الاربعین, مقابل مستشفی رزگاري - مجمع توپ مید</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4" id="email-wrapper">
				<div id="email">
					<p style="text-align: right">:ایمیل</p>
					<p>hemnabdulrahman@yahoo.com</p>
					<p style="text-align: right">:رقم سکرتیر</p>
					<p><span>0751-424-2655 / 0751-424-2665</span></p>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<div id="logoncs"><img src="images/nawrascs_logo_2_red.png" alt="" width="41" height="42" /> </div>
			</div>
		</div>
	</div>

	<table>
		<thead>
			<tr>
				<td>
					<!--place holder for the fixed-position header-->
					<div class="page-header-space"></div>
				</td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td>
					<!--*** CONTENT GOES HERE ***-->
					
					<div contenteditable="true" id="editDiv" class="editable page" data-placeholder='Start Report Writing...'>
						
					</div>
				</td>
			</tr>
		</tbody>

		<tfoot>
			<tr>
				<td>
					<!--place holder for the fixed-position footer-->
					<div class="page-footer-space"></div>
				</td>
			</tr>
		</tfoot>

	</table>

</div>

    	</div>

	</div>
	</div>
	
</div>


<script type="text/javascript">

var today = new Date().toLocaleString('en-GB').slice(0,10).replace(/\//g,'-');

  $(document).ready(function() {

  	$("#lang_change").on('change', function() {
  		var lang = $("#lang_change").attr("data-lang");
  		if (lang=='1') {
console.log("english"+lang);
  			$('#report_date').html('Date: '+today);
  			var speciality = 
  			`<p><strong>Specialist Surgeon Board<strong>(Doctor).</strong></p>
  			<p>Cardiovascular and Thoracic Surgeon</p>`;
  			$('#speciality').html(speciality);

  			var dr_title = `
			<span>Doctor</span><br><span id="docname">Hemn Abdulrahman Abdulla</span>
			<p><strong id="certificate" style="background-color: blue !important;color: white !important;font-weight: bold;">M.B.CH.B - F.I.B.M.S</strong></p>`;
  			$('#dr_title').html(dr_title);

  			var email = `
	  			<p style="text-align: left">Email: </p>
				<p>hemnabdulrahman@yahoo.com</p>
				<p style="text-align: left">Secretary: </p>
				<p><span>0751-424-2655 / 0751-424-2665</span></p>`;
  			$('#email').html(email);

  			var address = `
	  			<p>TopMed polyclinic, Gulan 40 meters street, beside Rizgari Hospital, 44001 Erbil-Iraq</p>`;
  			$('#address').html(address);
			
			$("#lang_change").data("2");
			$("#lang_change").attr("data-lang", "2");

  		}else {
  			
  			console.log("kurdish"+lang);
  			$('#report_date').html('بەروار: '+today);
  			var speciality = 
	  			`<p><strong>إختصاص جراحة القلب و الأوعیة الدمویة</strong></p>
				<p>پسپۆری نەشتەرگەری دڵ و بۆڕی خوێن </p>`;
  			$('#speciality').html(speciality);

  			var dr_title = `
				<span>الــدکتــــور</span><br /><span id="docname">هێمن عبدالرحمن عبداللە</span>
				<p><strong id="certificate" style="background-color: blue !important;color: white !important;font-weight: bold;">M.B.CH.B - F.I.B.M.S</strong></p>`;
  			$('#dr_title').html(dr_title);

  			var email = `
	  			<p style="text-align: right">:ایمیل</p>
				<p>hemnabdulrahman@yahoo.com</p>
				<p style="text-align: right">:رقم سکرتیر</p>
				<p><span>0751-424-2655 / 0751-424-2665</span></p>`;
  			$('#email').html(email);

  			var address = `
	  			<p>هەولێر/ شەقامی (٤٠) مەتری, بەرامبەر نەخۆشخانەی ڕزگاری - کۆمەلگای تۆپ مێد</p>
				<p>اربیل/ شارع الاربعین, مقابل مستشفی رزگاري - مجمع توپ مید</p>`;
  			$('#address').html(address);

			$("#lang_change").data("1")
			$("#lang_change").attr("data-lang", "1")
  		}
  	});

  	//faking DIV placeholder 
  	// FROM https://community.idera.com/developer-tools/b/blog/posts/faking-a-placeholder-attribute-for-an-editable-div-and-some-css-tricks
	$(document).on('change keydown keypress input', 'div[data-placeholder]', function() {
		if (this.textContent) {
			// console.log(this.textContent);
			this.dataset.divPlaceholderContent = 'true';
		}
		else {
			delete(this.dataset.divPlaceholderContent);
		}
	});
	/*document.querySelector('#editDiv').addEventListener('paste', (e) => {
    console.log("paste");
		if (this.textContent) {
    console.log("paste2");
			this.dataset.divPlaceholderContent = 'true';
		}
		else {
	    // console.log(this.textContent);
			delete(this.dataset.divPlaceholderContent);
		}
	});
*/
$("#editDiv").bind('paste cut', function() {
		if (this.textContent) {
			this.dataset.divPlaceholderContent = 'true';
		}
		else {
	    // console.log(this.textContent);
			delete(this.dataset.divPlaceholderContent);
		}
	});	

  });

function printThis() {
	var printContents = document.getElementsByClassName("printable")[0].innerHTML;
	printContents = printContents.replace(/contenteditable/g,'dum-attr');
	var originalContents = document.body.innerHTML;
var h = screen.height;
var w = screen.width;
	var win = window.open("", "Medical Report", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width="+w+",height="+h+",top="+(screen.height-400)+",left="+(screen.width-840));

    win.document.write("<!DOCTYPE html><html>" + document.head.outerHTML + "<body onload=window.print()>" + printContents + "</body></html>");
    // win.document.getElementById("editDiv").removeAttribute("contenteditable"); 
	win.document.close();

	win.focus();

	/*
	setTimeout( () => {
	     win.print();
	     win.close();
		}, 200);
	*/
/*var h = screen.height;
var w = screen.width;

$('#window-opener').live('click',function (e) {
    window.open(this.href, 'Resource', 'toolbar=no ,location=0, status=no,titlebar=no,menubar=no,width='+w +',height=' +h);
    e.preventDefault();
});​*/
}
</script>