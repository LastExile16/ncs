<script type="text/javascript" src="../js/modernizr.custom.79639.js"></script>
<div class="contact row">

	<!-- BEGIN SEARCH TABLE PORTLET-->
	
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>

<div class="row">
	 
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="col-md-12">
	 	
	 
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="caption panel-title">
				<span class='fa fa-stethoscope table_head_icon'></span> Clinic Queue - <?php echo date('Y-m-d'); ?>
			</div>
		</div>
		<div class="panel-body resposive">
	<table class="rtable rtable--flip table table-bordered table-condensed" id="queue">
	<thead>
		<th>id</th>
		<th>Name</th>
		<th>Position</th>
		<th>Office</th>
		<th>Extn</th>
		<th>Start date</th>
		<th>Salary</th>
	</thead>

	<tfoot>
		<th>id</th>
		<th>Name</th>
		<th>Position</th>
		<th>Office</th>
		<th>Extn</th>
		<th>Start date</th>
		<th>Salary</th>
	</tfoot>
	
  </table>

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <div class="modal fade" id="newpatient" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

</div>

		</div>
	</div>
	<div class="row" >
		<div class="col-md-12" >
			<a href="#newpatient" class="btn btn-primary" data-toggle='modal'><i class="fa fa-plus-square"></i> add new patient</a>
			<a href="javascript:;" class="btn btn-danger" id="cleaner"><i class="fa fa-circle-o-notch"></i> clean queue</a>
		</div>
	</div>
	</div>
	
</div>

<!-- <script src="../js/lightbox.js"></script>
<script>
    lightbox.option({
      'positionFromTop': 30
    })
</script> -->
<script type="text/javascript">

// $.fn.dataTable.ext.search.push(
$.fn.dataTableExt.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {
        var iMin = $('#min').val();
        var iMax = $('#max').val();
        d1 = new Date(iMin);
        d2 = new Date(iMax);

        iMin = isNaN(d1.getTime())?"":d1.getTime();
        iMax = isNaN(d2.getTime())?"":d2.getTime();

        target_col_data = aData[5];

        iDate = new Date(target_col_data).getTime();

 		// console.log("iMin = "+iMin);
   //      console.log("iMax = "+iMax);
   //      console.log("iDate = "+iDate);
		// console.log(target_col_data);
        if ( iMin == "" && iMax == "" )
		{
			return true;
		}
		else if ( iMin == "" && iDate < iMax )
		{
			return true;
		}
		else if ( iMin <= iDate && "" == iMax )
		{
			return true;
		}
		else if ( iMin <= iDate && iDate <= iMax )
		{
			return true;
		}
        return false;

    }
);
	$(document).ready(function() {
		


  var dataSet = [
    ["1","Tiger Nixon", "System Architect", "Edinburgh", "85851258514", "2011-04-25", "$320,800"],
    ["2","Garrett Winters", "Accountant", "Tokyo", "8422", "2011-07-25", "$170,750"],
    ["3","Ashton Cox", "Junior Technical Author", "San Francisco", "1562", "2009-01-12", "$86,000"],
    ["4","Cedric Kelly", "Senior Javascript Developer", "Edinburgh", "85851258514.8578", "2012-03-29", "$433,060"],
    ["5","Airi Satou", "Accountant", "Tokyo", "5407", "2008-11-28", "$162,700"],
    ["6","Brielle Williamson", "Integration Specialist", "New York", "4804", "2012-12-02", "$372,000"],
    ["7","Herrod Chandler", "Sales Assistant", "San Francisco", "9608", "2012-08-06", "$137,500"],
    ["8","Rhona Davidson", "Integration Specialist", "Tokyo", "6200", "2010-10-14", "$327,900"],
    ["9","Colleen Hurst", "Javascript Developer", "San Francisco", "2360", "2009-09-15", "$205,500"],
    ["10","Sonya Frost", "Software Engineer", "Edinburgh", "85851258514.8578", "2008-12-13", "$103,600"],
    ["11","Jena Gaines", "Office Manager", "London", "3814", "2008-12-19", "$90,560"],
    ["12","Quinn Flynn", "Support Lead", "Edinburgh", "85851258514.8578", "2013-03-03", "$342,000"],
    ["13","Charde Marshall", "Regional Director", "San Francisco", "6741", "2008-10-16", "$470,600"],
    ["14","Haley Kennedy", "Senior Marketing Designer", "London", "3597", "2012-12-18", "$313,500"],
    ["15","Tatyana Fitzpatrick", "Regional Director", "London", "1965", "2010-03-17", "$385,750"],
    ["16","Michael Silva", "Marketing Designer", "London", "1581", "2012-11-27", "$198,500"],
    ["17","Paul Byrd", "Chief Financial Officer (CFO)", "New York", "3059", "2010-06-09", "$725,000"],
    ["18","Gloria Little", "Systems Administrator", "New York", "1721", "2009-04-10", "$237,500"],
    ["19","Bradley Greer", "Software Engineer", "London", "2558", "2012-10-13", "$132,000"],
    ["20","Dai Rios", "Personnel Lead", "Edinburgh", "2290.8578", "2012-09-26", "$217,500"],
    ["21","Jenette Caldwell", "Development Lead", "New York", "1937", "2011-09-03", "$345,000"],
    ["22","Yuri Berry", "Chief Marketing Officer (CMO)", "New York", "6154", "2009-06-25", "$675,000"],
    ["23","Caesar Vance", "Pre-Sales Support", "New York", "8330", "2011-12-12", "$106,450"],
    ["24","Doris Wilder", "Sales Assistant", "Sidney", "3023", "2010-09-20", "$85,600"],
    ["25","Angelica Ramos", "Chief Executive Officer (CEO)", "London", "5797", "2009-10-09", "$1,200,000"],
    ["26","Gavin Joyce", "Developer", "Edinburgh", "8822", "2010-12-22", "$92,575"],
    ["27","Jennifer Chang", "Regional Director", "Singapore", "9239", "2010-11-14", "$357,650"],
    ["28","Brenden Wagner", "Software Engineer", "San Francisco", "1314", "2011-06-07", "$206,850"],
    ["29","Fiona Green", "Chief Operating Officer (COO)", "San Francisco", "2947", "2010-03-11", "$850,000"],
    ["30","Shou Itou", "Regional Marketing", "Tokyo", "8899", "2011-08-14", "$163,000"],
    ["31","Michelle House", "Integration Specialist", "Sidney", "2769", "2011-06-02", "$95,400"],
    ["32","Suki Burks", "Developer", "London", "6832", "2009-10-22", "$114,500"],
    ["33","Prescott Bartlett", "Technical Author", "London", "3606", "2011-05-07", "$145,000"],
    ["34","Gavin Cortez", "Team Leader", "San Francisco", "2860", "2008-10-26", "$235,500"],
    ["35","Martena Mccray", "Post-Sales support", "Edinburgh", "8240", "2011-03-09", "$324,050"],
    ["36","Unity Butler", "Marketing Designer", "San Francisco", "5384", "2009-12-09", "$85,675"]
  ];

  var columnDefs = [{
    title: "id", visible: false
  }, {
    title: "Name"
  }, {
    title: "Position"
  }, {
    title: "Office"
  }, {
    title: "Extn.", render: $.fn.dataTable.render.number( ',', '.', 2, '$' ) 
  }, {
    title: "Start date"
  }, {
    title: "Salary"
  }];

  var queue;

  queue = $('#queue').DataTable({
  	processing: true,
    "pagingType": "full_numbers",
    data: dataSet,
    columns: columnDefs,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    
    "deferRender": true,
    dom: '<"top row"<"col-md-4"l><"col-md-5"B><"col-md-3"f>><"row"<"col-md-12"rt>><"bottom row"<"col-md-4"i><"paginationright col-md-8"p>><"clear">',
    select: 'single',
    buttons: [/*{
    			text: 'pdf',
                extend: 'pdfHtml5',
                download: 'open',
                message: 'pdf created by Nawras Clinical Service'
            },*/{
    	text: '<i title="to Doctor" class="fa fa-sign-in fa-1-5x"></i>',
      extend: 'selected',
      action: function(e, dt, node, config) {
        addClick(this, dt, node, config)
      }
    }, {
      extend: 'selected',
      text: '<i title="Cancel" class="fa fa-ban fa-1-5x"></i>',
      action: function(e, dt, node, config) {
        var rows = dt.rows({
          selected: true
        }).count();

        deleteClick(this, dt, node, config)
      }
    }, {
      extend: 'selected',
      text: '<i title="Edit" class="fa fa-pencil-square-o fa-1-5x"></i>',
      action: function(e, dt, node, config) {
        var rows = dt.rows({
          selected: true
        }).count();
        //above code just to indicate how many rows are selected
// console.log(rows);
// console.log(dt.rows({selected: true}));
        editClick(this, dt, node, config)
      }
    }, {
      extend: 'selected',
      text: '<i title="View" class="fa fa-eye fa-1-5x"></i>',
      action: function(e, dt, node, config) {
        var rows = dt.rows({
          selected: true
        }).count();
        //above code just to indicate how many rows are selected
// console.log(rows);
// console.log(dt.rows({selected: true}));
        viewClick(this, dt, node, config)
      }
    }, {
		extend: 'print',
		text: '<i title="Print" class="fa fa-print fa-1-5x"></i>',
        message: 'This print was produced by Nawras Clinical System',
        exportOptions: {
            columns: ':visible'
        }
    }, {
    	text: '<i title="hide columns" class="fa fa-eye-slash fa-1-5x"></i>',
    	extend:'colvis'
    }]
  });

  /*rows returns an array of that row, but rows() returns objects that consist of 
  many data including the array of row*/
/*$('#queue tbody').on( 'click', 'tr', function () {
    console.log( queue.row( this ).data() );
    var d = queue.row( this ).data();
     d[1]="Yusuf";
    queue
        .row( this )
        .data( d )
        .draw();
} );*/
  //---------Function to Display modal editButton---------

  function editClick(pointer, oTT, button, conf) {
    var adata = oTT.rows({
      selected: true
    });

    // console.log('adata', adata)
    // console.log(adata.data()[0]);
    // console.log(adata.data()[0][0]);
    console.log(adata.ids()[0]);
    var id = adata.data()[0][0];

    var data = "";
    var queue = pointer;
    data += "<form name='editForm' role='form'>";
    data += "<div class='form-group' style='display:none'><label for='" + columnDefs[0].title + "'>" + columnDefs[0].title + ":</label><input type='hidden'  id='" + columnDefs[0].title + "' name='" + columnDefs[0].title + "' placeholder='" + columnDefs[0].title + "' style='overflow:hidden'  class='form-control' value='" + adata.data()[0][0] + "'></div>";
    // for (i in columnDefs) {
    for (i=1; i<columnDefs.length; i++) {
      data += "<div class='form-group'><label for='" + columnDefs[i].title + "'>" + columnDefs[i].title + ":</label><input type='text'  id='" + columnDefs[i].title + "' name='" + columnDefs[i].title + "' placeholder='" + columnDefs[i].title + "' style='overflow:hidden'  class='form-control' value='" + adata.data()[0][i] + "'></div>";

    }
    data += "</form>";


    $('#myModal').on('show.bs.modal', function() {
      $('#myModal').find('.modal-title').html('Edit Record');
      $('#myModal').find('.modal-body').html('<pre>' + data + '</pre>');
      $('#myModal').find('.modal-footer').html("<button type='button' data-content='remove' class='btn btn-primary' id='editRowBtn'>Update</button>");
    });

    $('#myModal').modal('show');
    $('#myModal input[1]').focus();

  };

  // edit row functionality
  $(document).on('click', '#editRowBtn', function() {

    // edit row function needs to go here, here use your ajax then hide the modal
    console.log('Update Row');
    $('#myModal').modal('hide');

    /*I use row() not rows() because I want to edit only one row and only one row is selectable
    otherwise I should use rows and loop through each selected row*/
    /*var adata = queue.rows({
      selected: true
    }).data()[0];*/

    // below copies the row data into adata variable, so after updating it you should give the data back to
    // queue object to update its' cache
    var adata = queue.row({
      selected: true
    }).data();
    // console.log(editForm);
    // console.log(editForm[1].value);
    // console.log(adata);
    for (i in adata) {
        adata[i] = editForm[i].value;
    }
    console.table(adata);
   queue.row( {selected: true} ).data( adata );
       queue.draw();

       /*
        when using ajax it's better to invalidate data to force datatable do a re-read from the sourse
        then draw() the table again.
        that way you don't need to loop through new data and update the variable <i.e. adata> then give it back to datatable.
        above function is useful only when you don't want to make the change immediately in the database
        instead you update the shown table then create a button like "save" when it's clicked then you make changes permenantly.
        same applies to add and delete
       */
  });

  //---------Function to Display modal deleteButton---------

  function deleteClick(pointer, oTT, button, conf) {

    var adata = oTT.rows({
      selected: true
    });

    // console.log('adata', adata)
    // console.log(adata.data()[0]);
    // console.log(adata.data()[0][0]);
    console.log(adata.ids()[0]);
    var id = adata.data()[0][0];
    var data = "";
    var queue = pointer;
    data += "<form name='editForm' role='form'>";
    for (i in columnDefs) {

      data += "<div class='form-group'><label for='" + columnDefs[i].title + "'>" + columnDefs[i].title + " : </label><input  type='hidden'  id='" + columnDefs[i].title + "' name='" + columnDefs[i].title + "' placeholder='" + columnDefs[i].title + "' style='overflow:hidden'  class='form-control' value='" + adata.data()[0][i] + "' >" + adata.data()[0][i] + "</input></div>";

    }
    data += "</form>";

    $('#myModal').on('show.bs.modal', function() {
      $('#myModal').find('.modal-title').html('Delete Record');
      $('#myModal').find('.modal-body').html('<pre>' + data + '</pre>');
      $('#myModal').find('.modal-footer').html("<button type='button' data-content='remove' class='btn btn-primary' id='deleteRowBtn'>Delete</button>");
    });

    $('#myModal').modal('show');
    $('#myModal input[0]').focus();
  };

  // delete row functionality
  $(document).on('click', '#deleteRowBtn', function() {
    // delete row function needs to go here
    console.log('Delete Row');
    $('#myModal').modal('hide');

    var adata = queue.rows({
      selected: true
    }).data()[0];
    console.log(queue.rows({selected: true}).data());
    // $('#queue').DataTable().row('.selected').remove().draw(false);
    queue.row({selected: true}).remove().draw(false);
    // queue.row( {selected: true} ).data( adata );
    // queue.draw();
    console.log('data to delete : ', adata[0])
     queue.row('.selected').remove().draw( false );
  });

		//number of all columns
    // var numCols = $('#queue').DataTable().columns().nodes().length;
	//number of visible columns
    // var numCols = $('#queue thead th').length;

			// $(".panel-info").slideDown('slow');
		
		/*$(".errmsj").slideDown(1000);
		$(".alert button.close").click(function (e) {
		    // $(this).parent().fadeOut('slow');
		    $(this).parent().slideUp('slow');
		});
		*/
		
// l - Length changing
// f - Filtering input
// t - The Table!
// i - Information
// p - Pagination
// r - pRocessing

    /*$('#min, #max').keyup( function() {
        clinics.draw();});*/

    $("#min").keyup ( function() { queue.draw(); } );
	$("#min").change( function() { queue.draw(); } );
	$("#max").keyup ( function() { queue.draw(); } );
	$("#max").change( function() { queue.draw(); } );

	$("#clear_search").on("click", function() {
		$('#min').val("");
		$('#max').val("");
		queue.draw();
	});

// Setup - add a text input to each footer cell
    $('#queue tfoot th').each( function () {
    // $('#y tfoot th').not(":eq(0),:eq(-1)").each( function () {
        // var title = $.trim($(this).text());
        // console.log($(this));
        // $(this).html( '<input type="text" class="form-control input-sm" placeholder="Search '+$.trim(title)+'" />' );
        $(this).html( '<input type="text" class="form-control input-sm" placeholder="Search here" />' );
        // var disable='';
        // var placeholder = 'placeholder="Search here"';
        // if (title=='tools' || title=='#') { disable="disabled=disabled readonly "; placeholder='style="background-color:transparent; border-color:transparent; display:none"'; }
        	// $(this).html( '<input type="text" '+ disable + ' class="form-control input-sm"'+placeholder+' />' );
    } );
 
    // Apply the search
    queue.columns().every( function () {
        var that = this;
        // console.log(this.index());
        //if (this.index()==0 || this.index()==5 ) {return;}//to disable search in first at last col
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


    $(document).on('click', '#cleaner', function(event) {
    	$('#cleaner > :first-child').addClass('spin-backw');
    });

} );
</script>