<?php  
$today = date('Y-m-d');
$today_month = date('F');
$today_year = date('Y');

$todays_payment = get_today_total_payment($today);
$todays_free_p = get_total_free_p($today);
$monthly_total_income_set = get_monthly_total_income($today_month);
$yearly_total_income_set = get_yearly_total_income($today_year);
$yearly_total_expense_set = get_yearly_total_expense($today_year);

$monthly_total_income = mysql_fetch_assoc($monthly_total_income_set);
$yearly_total_income = mysql_fetch_assoc($yearly_total_income_set);
$yearly_total_expense = mysql_fetch_assoc($yearly_total_expense_set);
$total_monthly_payment = !empty($monthly_total_income['income'])?$monthly_total_income['income']:0;
$total_yearly_payment = !empty($yearly_total_income['income'])?$yearly_total_income['income']:0;

$total_yearly_exp = !empty($yearly_total_expense['expense_amount'])?$yearly_total_expense['expense_amount']:0;
// clean_queue();
?>

<script type="text/javascript" src="js/Chart.bundle.min.js"></script>
<script type="text/javascript">
	function numberWithCommas(number) {
		if (number == undefined) {
			number = 0;
		}
		//or just use below code if supported by safari
		// return number.toLocaleString("en");
	    var parts = number.toString().split(".");
	    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    return parts.join(".");
	}
</script>
<style type="text/css">
	.input {
  position: relative;
  background: linear-gradient(21deg, #10abff, #1beabd);
  padding: 3px;
  display: inline-block;
  border-radius: 999em;
}
.input *:not(span) {
	width: 5em;
  position: relative;
  display: inherit;
  border-radius: inherit;
  margin: 0;
  border: none;
  outline: none;
  padding: 0 0.325em;
  z-index: 1;
}
.input *:not(span):focus + span {
  opacity: 1;
  transform: scale(0.97, 0.88);
}
.input span {
  transform: scale(0.993);
  transition: transform 0.5s, opacity 0.25s;
  opacity: 0;
  position: absolute;
  z-index: 0;
  margin: 4px;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  border-radius: inherit;
  pointer-events: none;
  box-shadow: inset 0 0 0 3px #fff, 0 0 0 4px #fff, 3px -3px 30px #1beabd, -3px 3px 30px #10abff;
}
input {
  font-family: inherit;
  line-height: inherit;
  color: #2e3750;
}
::placeholder {
  color: #cbd0d5;
}


</style>
<div class="contact row">

	<!-- BEGIN SEARCH TABLE PORTLET-->
	
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>
<div class="row">
	 
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="student-table col-md-12">
	 	
	 

	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="caption panel-title">
				<span class='fa fa-line-chart'></span> Reports 
				<span class="input">
					<input type="text" placeholder="Date"  name='report_date' id='report_date'>
					<span></span>
				</span>
			</div>
		</div>
		<div class="panel-body">
			<div class="row payments">
				<div class="col-md-2">
					<label>Todays' Payment: </label>
					<label><script type="text/javascript">document.write(numberWithCommas(<?php echo $todays_payment; ?>));</script> IQD</label>
				</div>
				<div class="col-md-3">
					<label>Todays' Free Patients: </label>
					<label><script type="text/javascript">document.write(numberWithCommas(<?php echo $todays_free_p; ?>));</script> Patients</label>
				</div>
				<div class="col-md-2">
					<label>Income of <?php echo $today_month; ?>: </label>
					<label><script type="text/javascript">document.write(numberWithCommas(<?php echo $total_monthly_payment; ?>));</script> IQD</label>
				</div>
				<div class="col-md-2">
					<label>Expense of <?php echo $today_year; ?>: </label>
					<label><script type="text/javascript">document.write(numberWithCommas(<?php echo $total_yearly_exp; ?>));</script> IQD</label>
				</div>
				<div class="col-md-3">
					<label>Net Income of <?php echo $today_year; ?>: </label>
					<label><script type="text/javascript">document.write(numberWithCommas(<?php echo $total_yearly_payment - $total_yearly_exp; ?>));</script> IQD</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<canvas id="procedure_piechart" height="400" width="400"></canvas>
				</div>
				<div class="col-md-2"></div>
				<div class="col-md-6">
					<canvas id="surgery_doghnut" height="300" width="400"></canvas>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<canvas id="last6month_line" height="250" width="400"></canvas>
				</div>
			</div>
		</div>
	</div>
	</div>
	
</div>

<script>
var randomScalingFactor = function(){ return Math.round(Math.random()*20)};
var PieChart = 
			{

				labels: ["Erbil", "Duhok", "Sulaimani"],
				datasets:[
			        {
			            data: ["20", randomScalingFactor(), randomScalingFactor()],
			            backgroundColor: [
			                "#FF6384",
			                "#36A2EB",
			                "#FFCE56"
			            ],
			            hoverBackgroundColor: [
			                "#FF6384",
			                "#36A2EB",
			                "#FFCE56"
			            ]
			        }
			    ]
			};
// console.table(PieChart);
		
		

	var modern = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "Erbil",
            fill: false,
            lineTension: 0,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBorderWidth: 5,
            pointHoverRadius: 7,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
        },
        {
            label: "Duhok",
            fill: false,
            lineTension: 0,
            backgroundColor: "rgba(243, 30, 38, 0.4)",
            borderColor: "rgba(243, 30, 38, 1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(243, 30, 38, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 5,
            pointHoverRadius: 7,
            pointHoverBackgroundColor: "rgba(243, 30, 38, 1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()],
            spanGaps: false,
        },
        {
            label: "Sulaimania",
            fill: false,
            lineTension: 0,
            backgroundColor: "rgba(197, 127, 255, 0.4)",
            borderColor: "rgba(197, 127, 255, 1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(197, 127, 255, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 5,
            pointHoverRadius: 7,
            pointHoverBackgroundColor: "rgba(197, 127, 255, 1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()],
            spanGaps: false,
        }
    ]
};


/*
var ctx = document.getElementById("last6month_line");
var myChart = new Chart(ctx, {
    type: 'line',
    data: modern,
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});*/
</script>


<script type="text/javascript">
	$(document).ready(function() {
		var ctx1 = document.getElementById("procedure_piechart");
		var procedure_piechart = new Chart(ctx1, {
											    type: 'pie',
											    date: {labels: [], datasets: [0]},
											        options: {
												        tooltips: {
												            callbacks: {
												                label: function(tooltipItem, data) {
												                    var allData = data.datasets[tooltipItem.datasetIndex].data;
												                    var tooltipLabel = data.labels[tooltipItem.index];
												                    var tooltipData = allData[tooltipItem.index];
												                    // console.log( tooltipData);
												                    // console.table(allData);
												                    var total = 0;
												                    for (var i in allData) {
												                        total += parseInt(allData[i]);
												                    }
												                    // console.log(total);
												                    var tooltipPercentage = Math.round((tooltipData / total) * 100);
												                    return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
												                }
												            }
												        }
												    }
												});

		var ctx2 = document.getElementById("surgery_doghnut");
		var surgery_doghnut = new Chart(ctx2, {
										    type: 'doughnut',
										    date: {labels: [], datasets: [0]},
										        options: {
											        tooltips: {
											            callbacks: {
											                label: function(tooltipItem, data) {
											                    var allData = data.datasets[tooltipItem.datasetIndex].data;
											                    var tooltipLabel = data.labels[tooltipItem.index];
											                    var tooltipData = allData[tooltipItem.index];
											                    // console.log( tooltipData);
											                    // console.table(allData);
											                    var total = 0;
											                    for (var i in allData) {
											                        total += parseInt(allData[i]);
											                    }
											                    // console.log(total);
											                    var tooltipPercentage = Math.round((tooltipData / total) * 100);
											                    return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
											                }
											            }
											        }
											    }
											});
		// data: r_data[1],
		var ctx3 = document.getElementById("last6month_line");
		var last6month_line = new Chart(ctx3, {
										    type: 'line',
										    date: {labels: [], datasets: [0]},
										    options: {
										        scales: {
										            yAxes: [{
										                ticks: {
										                    beginAtZero:true
										                }
										            }]
										        }
										    }
										});
		// data: r_data[2],
		var data = 'create_chart=1' ;
		$.ajax(
		{
		   type: "POST",
		   url: "./includes/reports.php",
		   data: data,
		   cache: false,
		
		   success: function(data1)
		   {
				// console.log(data1);
				var r_data = JSON.parse(data1);
				// console.table(r_data[0]);
					// console.table(r_data[0].dataset !== null);
					// console.table(r_data[1].dataset !== null);
					// console.table(r_data[2].dataset !== null);
				if (r_data[0].datasets !== null) {
					last6month_line.config.data = r_data[0];
					last6month_line.update();
				} else { // if no data available, give it default parameters with zero data

					last6month_line.config.data = {
										      "labels":[
										         "January",
										         "February",
										         "March",
										         "April",
										         "May",
										         "June"
										      ],
										      "datasets":[
										         {
										            "data":[
										               "0",
										               "0",
										               "0",
										               "0",
										               "0",
										               "0"
										            ],
										            "label":"Visits",
										            "fill":false,
										            "lineTension":0,
										            "backgroundColor":"rgba(175,175,175,0.4)",
										            "borderColor":"rgba(175,175,175,1)",
										            "pointBorderColor":"rgba(175,175,175,1)",
										            "pointBorderWidth":5,
										            "pointHoverRadius":7,
										            "pointHoverBackgroundColor":"rgba(175,175,175,1)",
										            "pointHoverBorderColor":"rgba(175,175,175,1)",
										            "pointHoverBorderWidth":2,
										            "pointRadius":1,
										            "pointHitRadius":10
										         }
										      ]
										   };
					last6month_line.update();
				}
				if (r_data[1].datasets !== null) {
					surgery_doghnut.config.data = r_data[1];
					surgery_doghnut.update();
				} else { // if no data available, give it default parameters with zero data

					surgery_doghnut.config.data = {
												  "labels":[
												     "No Surgery"
												  ],
												  "datasets":[
												     {
												        "data":[
												           "1"
												        ],
												        "backgroundColor":[
												           "#AFAFAF",
												           "#AFAFAF"
												        ],
												        "hoverBackgroundColor":[
												           "#AFAFAF",
												           "#AFAFAF"
												        ]
												     }
												  ]
												};
					surgery_doghnut.update();
				}
				if (r_data[2].datasets !== null) {
				    procedure_piechart.config.data = r_data[2];
					procedure_piechart.update();
				} else { // if no data available, give it default parameters with zero data

					procedure_piechart.config.data = {
											      "labels":[
											         "No Procedure"
											      ],
											      "datasets":[
											         {
											            "data":[
											               "1"
											            ],
											            "backgroundColor":[
											               "#AFAFAF",
											               "#AFAFAF"
											            ],
											            "hoverBackgroundColor":[
											               "#AFAFAF",
											               "#AFAFAF"
											            ]
											         }
											      ]
											   };
					procedure_piechart.update();
				}
		   },
		   
		   statusCode: 
		   {
				404: function() {
				// alert('ERROR 404 \npage not found');
				}
		   }

		});
		//////
		
  $('#report_date').datepicker({
    format: "yyyy-mm",
    autoclose: true,
    todayHighlight: true,
    viewMode: "years", 
    minViewMode: "months"
    // weekStart: 5,
    // endDate: "+0d",
    // daysOfWeekHighlighted:'5'
    //datesDisabled:availableDates,
  });
	$("#report_date").change( function() {
  if ($("#report_date").val() != "" && $("#report_date").val() != null) {
  	// 2018-03
    // console.log($("#report_date").val())
    

		var $rdate = $("#report_date").val();		
		var data = 'create_chart=1&rdate='+$rdate;
		$.ajax(
		{
		   type: "POST",
		   url: "./includes/reports.php",
		   data: data,
		   cache: false,
		
		   success: function(data1)
		   {
				// console.log(data1);
				var r_data = JSON.parse(data1);
				// console.log(r_data[4]);
				// console.table(r_data[0]);
					// console.log(r_data[0].datasets);
					// console.log(r_data[0].datasets !== null);
					// console.log(r_data[1].datasets);
					// console.log(r_data[1].datasets !== null);
					// console.log(r_data[2].datasets);
					// console.log(r_data[2].datasets !== null);
				if (r_data[0].datasets !== null) {
					last6month_line.config.data = r_data[0];
					last6month_line.update();
				} else { // if no data available, give it default parameters with zero data

					last6month_line.config.data = {
										      "labels":[
										         "January",
										         "February",
										         "March",
										         "April",
										         "May",
										         "June"
										      ],
										      "datasets":[
										         {
										            "data":[
										               "0",
										               "0",
										               "0",
										               "0",
										               "0",
										               "0"
										            ],
										            "label":"Visits",
										            "fill":false,
										            "lineTension":0,
										            "backgroundColor":"rgba(175,175,175,0.4)",
										            "borderColor":"rgba(175,175,175,1)",
										            "pointBorderColor":"rgba(175,175,175,1)",
										            "pointBorderWidth":5,
										            "pointHoverRadius":7,
										            "pointHoverBackgroundColor":"rgba(175,175,175,1)",
										            "pointHoverBorderColor":"rgba(175,175,175,1)",
										            "pointHoverBorderWidth":2,
										            "pointRadius":1,
										            "pointHitRadius":10
										         }
										      ]
										   };
					last6month_line.update();
				}
				if (r_data[1].datasets !== null) {
					surgery_doghnut.config.data = r_data[1];
					surgery_doghnut.update();
				} else { // if no data available, give it default parameters with zero data

					surgery_doghnut.config.data = {
												  "labels":[
												     "No Surgery"
												  ],
												  "datasets":[
												     {
												        "data":[
												           "1"
												        ],
												        "backgroundColor":[
												           "#AFAFAF",
												           "#AFAFAF"
												        ],
												        "hoverBackgroundColor":[
												           "#AFAFAF",
												           "#AFAFAF"
												        ]
												     }
												  ]
												};
					surgery_doghnut.update();
				}
				if (r_data[2].datasets !== null) {
				    procedure_piechart.config.data = r_data[2];
					procedure_piechart.update();
				} else { // if no data available, give it default parameters with zero data

					procedure_piechart.config.data = {
											      "labels":[
											         "No Procedure"
											      ],
											      "datasets":[
											         {
											            "data":[
											               "1"
											            ],
											            "backgroundColor":[
											               "#AFAFAF",
											               "#AFAFAF"
											            ],
											            "hoverBackgroundColor":[
											               "#AFAFAF",
											               "#AFAFAF"
											            ]
											         }
											      ]
											   };
					procedure_piechart.update();
				}
		   },
		   
		   statusCode: 
		   {
				404: function() {
				// alert('ERROR 404 \npage not found');
				}
		   }

		});

	
  }
} );
});



</script>