<style type="text/css">
    .gallery{
        padding: 0px;
        margin-top: -10px;
        margin-bottom: 15px;
    }
    .gallery-1{
        margin-top: 5px;
    }
    .gallery-grid{
        padding-left: 5px;
        padding-right: 5px;
    }
    .gallery-border{
        border: 1px dashed #337AB7;
        padding-bottom: 5px;
    }
    .input-group {
        margin-bottom: 0px;
    }
    .img-wrap {
        position: relative;
        border: 1px solid #d1cfcf;
        border-radius: 4px;
    }
    .img-wrap .close {
        position: absolute;
        top: -3px;
        right: 2px;
        color: red;
        opacity: 0.4;
        z-index: 100;
    }
    .example-image{
        border-radius: 4px;
    }

    
    .img-wrap .close:focus, .img-wrap .close:hover {
        color: #F30909;
        background-color: white;
        border-style: solid;
        border-width: 1px;
        border-color: gray;
        border-top-width: 0px;
        box-shadow: 1px 1px 2px -1px red, -1px -1px 2px -1px red; 
        opacity: 0.8;
    }

</style>
<script type="text/javascript" src="./js/modernizr.custom.79639.js"></script>
<div class="contact row">

	<!-- BEGIN SEARCH TABLE PORTLET-->
	
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>
<?php 
    echo isset($_GET['err_msj']) && !empty($_GET['err_msj'])? "
    <div class='row'>
        <div class='form-group errmsj row' style='display:none'>
            <div class='col-md-12'>
                <div class='alert alert-danger alert-dismissable'>
                <button type='button' class='close' aria-hidden='true'>&times; </button>
                    {$_GET['err_msj']}
                </div>
            </div>
        </div>
    </div>":""; 
?>
<div class="row">
	 
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="col-md-12">
	 	
    <?php  
        $today = date('Y-m-d');
    ?>
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="caption panel-title">
                <span class='fa fa-stethoscope table_head_icon'></span> Current Patient
				<a class="btn btn-info col-md-offset-9" data-toggle="modal" href="#currentQ" id="curr_q"><i></i> Current Queue</a>
			</div>
		</div>
		<div class="panel-body resposive">
        <div class="row">
        <?php
            /*
            p.p_id, p.fullname, p.sex, p.address, p.dob, p.occupation, p.past_hx, p.family_hx, p.drug_hx, p.other_hx,
            v.v_id, v.visit_date, v.followup_date, v.marital_status, v.pregnant, v.breast_feed, v.no_of_child, v.other_info, v.fee
        */
            $patient_set = get_this_p($today);
            if (mysql_num_rows($patient_set)>0) {

                $patient = mysql_fetch_assoc($patient_set);
                $age = date('Y')-$patient['dob'];
                $sex ="";
                if ($patient['sex']=='1') {
                    $sex = "<div style='text-align:center'><i class='fa fa-mars'><span style='display:none'>1</span></i></div>"; 
                }else if ($patient['sex']=='2') {
                    $sex = "<div style='text-align:center'><i class='fa fa-venus'><span style='display:none'>2</span></i></div>";    
                }else if ($patient['sex']=='3') {
                    $sex = "<div style='text-align:center'><i class='fa fa-venus-mars'><span style='display:none'>3</span></i></div>";   
                }
                $marital_st = "";
                switch ($patient['marital_status']) {
                    case '1':
                        $marital_st = "Single";
                        break;
                    case '2':
                        $marital_st = "Married";
                        break;
                    case '3':
                        $marital_st = "Divorced";
                        break;
                    case '4':
                        $marital_st = "Widowed";
                        break;
                }
                $pregnant = $patient['pregnant']==1?"Pregnant":"Not Pregnant";
                $breast_feed = $patient['breast_feed']==1?"Yes":"No";
                $p_basic_info = "
                    <div class='row'>
                        <div class='col-md-3'>
                            <label>Code and Name: </label> <label>{$patient['p_id']}, {$patient['fullname']}</label>   
                        </div>
                        <div class='col-md-3'>
                            <label>sex: </label> <label>{$sex}</label>
                        </div>
                        <div class='col-md-3'>
                            <label>age: </label> <label>{$age}</label>
                        </div>
                        <div class='col-md-3'>
                            <label>Marital Status: </label> <label>{$marital_st}</label>
                        </div>
                    </div>";
                $p_basic_info .= "
                    <div class='row'>
                        <div class='col-md-3'>
                            <label>Occupation: </label> <label>{$patient['occupation']}</label>   
                        </div>
                        <div class='col-md-3'>
                            <label>Fee: </label> <label>{$patient['fee']}</label>
                        </div>
                        <div class='col-md-3'>
                            <label>Address: </label> <label>{$patient['address']}</label>
                        </div>
                        <div class='col-md-3'>
                            <label>Expected visit was on: </label> <label>{$patient['latest_followup_visit']}</label>
                        </div>
                    </div>";
                $p_basic_info .= "
                    <div class='row'>
                        <div class='col-md-3'>
                            <label>no. of children: </label> <label>{$patient['no_of_child']}</label>
                        </div>
                        <div class='col-md-3'>
                            <label>other info: </label> <label>{$patient['other_info']}</label>  
                        </div>
                    ";
                    //female related info
                $p_basic_info .= $patient['sex']!=1?"
                        <div class='col-md-3'>
                            <label>Breastfeeding: </label> <label>{$breast_feed}</label>
                        </div>
                        <div class='col-md-3'>
                            <label>Pregnancy: </label> <label>{$pregnant}</label>
                        </div>
                    </div>":"</div>";
                $p_basic_info .= "<hr>";
                echo $p_basic_info;

                $hx_button = "<input type='checkbox' id='show_hx' class='toggledbox' >";
                $p_history = $hx_button . "<hr><div id='p_history_info' class='container' style='display:none'>
                <form class='form-horizontal' name='p_hx' id='p_hx' action=''>";

                $p_history .= "
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='past_hx'>Past History</label>  
                    <div class='col-md-7'>
                        <textarea id='past_hx' name='past_hx' disabled='disabled' placeholder='Past History' class='form-control'>{$patient_set['past_hx']}</textarea>
                    </div>
                </div>

                <div class='form-group'>
                    <label class='col-md-3 control-label' for='family_hx'>Family History</label>  
                    <div class='col-md-7'>
                        <textarea id='family_hx' name='family_hx' disabled='disabled' placeholder='Family History' class='form-control'>{$patient_set['family_hx']}</textarea>
                    </div>
                </div>
                
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='drug_hx'>Drug History</label>  
                    <div class='col-md-7'>
                        <textarea id='drug_hx' name='drug_hx' disabled='disabled' placeholder='Drug History' class='form-control'>{$patient_set['drug_hx']}</textarea>
                    </div>
                </div>
                
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='other_hx'>Other History</label>  
                    <div class='col-md-7'>
                        <textarea id='other_hx' name='other_hx' disabled='disabled' placeholder='Other History' class='form-control'>{$patient_set['other_hx']}</textarea>
                    </div>
                </div>

                <div class='form-group row'>  
                    <div class='col-md-2 col-md-offset-8 ffooter'>
                        <a id='edit_hx' name='edit_hx' class='btn btn-primary col-md-12'>Edit History</a>
                    </div>
                </div>
                <input type='hidden' name='p_id' value='{$patient['p_id']}'>
                ";
                $p_history .= "</form> </div>";

                echo $p_history;
            
        ?>
        </div>
        <div class="clearfix"></div>
        <?php  
            $tabs_list = "No prev visits";
            $tab_content = "";
            $prev_visits_list_set = get_prev_visits_list($patient['p_id']);
            if (mysql_num_rows($prev_visits_list_set)>0) {
                $tabs_list = "";
                while ($prev_visit = mysql_fetch_assoc($prev_visits_list_set)) {
                    
                    //return all related attachments
                    $attachments_output = "no attachments";
                    $attachment_set = get_attachment($prev_visit['v_id']);
                    if (mysql_num_rows($attachment_set)>0) {
                    $attachments_output = "
                    <div class='gallery gallery-border row'>
                         <div class='container col-md-12'>
                             <div class='gallery-bottom'>";
                             $i = 0;
                             $flag=true;
                        while ( $attachment = mysql_fetch_assoc($attachment_set)) {
                            //4 pics in a row (by creating a new gallery-1)
                            if ($i % 4 == 0) {
                                $attachments_output .=  "<div class='gallery-1'>";
                                $flag = true;
                            }
                            $attachments_output .=  "<div class='col-md-3 gallery-grid'>
                                            <div class='img-wrap'><span data-id='{$attachment['a_id']}' file-name='{$attachment['filename']}' id='close_{$attachment['a_id']}' class='close delete-image'>&times;</span>
                                                <a class='example-image-link' href='uploads/{$attachment['filename']}' data-lightbox='album_{$prev_visit['v_id']}'>
                                                    <img class='example-image' src='uploads/{$attachment['filename']}' alt='image'/>
                                                </a>
                                                </div>
                                            </div>";
                                // $attachments_output .= "<span>i: {$i}, flag: $flag </span>";
                            if (++$i % 4 == 0) {
                                $attachments_output .= "<div class='clearfix'></div></div>";
                                $flag = false;
                            }
                            // $i++;
                        }

                        //if <div gallery-1> was opened but not closed then close it. 
                        if ($flag) {
                            $attachments_output .= "<div class='clearfix'></div></div>";
                        }
                    $attachments_output .= "</div>
                                 </div>
                            </div>";

                    }

                    //now create the previous visits form with there values
                    /*<li>
                            <a href="#messages" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1" aria-expanded="false">Messages</a>
                        </li>

                        <li>
                            <a href="#settings" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2" aria-expanded="false">Settings</a>
                        </li>
                            
                        SELECT v.v_id, v.visit_date, v.followup_date, v.marital_status, v.pregnant, v.breast_feed, v.no_of_child, v.other_info, v.fee, v.chief_comp_n_dur, v.examination, v.investigation, v.note, v.treatment_note
                        */

                    $tabs_list .= "
                        <li>
                            <a href='#tabpane{$prev_visit['v_id']}' role='tab' class='hxtab_btn' data-id='{$prev_visit['v_id']}' id='dropdown{$prev_visit['v_id']}-tab' data-toggle='tab' aria-controls='dropdown1' aria-expanded='false'>{$prev_visit['visit_date']}</a>
                        </li>
                        ";
                    $tab_content .= "<div role='tabpanel' class='tab-pane fade in' id='tabpane{$prev_visit['v_id']}'>";

                        include './includes/prev_visit_form.php';
                        $tab_content .= $prev_p_basic_info;
                        $tab_content .= $prev_data;
                    $tab_content .= '</div>';
                }
                
            }

            $tabs_bar = '
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" id="myTabDrop1" data-toggle="dropdown" > 
                        Previous Visits <span class="caret"> </span>
                    </a>

                    <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents"> ';
            $tabs_bar .= $tabs_list;
                        
            $tabs_bar .= '</ul>
                    </li>';
        ?>
        <div class="row">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#new_data" aria-controls="new_data" role="tab" data-toggle="tab"><?php echo $today; ?></a></li>

                <?php echo $tabs_bar; ?>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="new_data">
                    <div class='row'>
                        <!-- if doctor asked to remove the 'required' attribute then you can just add initial values to inputs like 'no duration or no examination' ...  -->
                        <form class='form-horizontal' enctype='multipart/form-data' action='./includes/form_submitions.php?to=current_p' id='editnewpatientform' method="POST" onsubmit="return checkfiles('attachfiles')">
                             <input id='p_id' name='p_id' class='form-control' type='hidden' value="<?php echo $patient['p_id']; ?>" >
                             <input id='v_id' name='v_id' class='form-control' type='hidden' value="<?php echo $patient['v_id']; ?>" >
                             <input id='visit_date' name='visit_date' class='form-control' type='hidden' value="<?php echo $patient['visit_date']; ?>" >
                            <!-- Text input-->
                            <div class='form-group'>
                              <label class='col-md-3 control-label' for='chief_com_n_duration'>chief_com_n_duration</label>  
                              <div class='col-md-7'>
                              <input id='chief_com_n_duration' name='chief_com_n_duration' autofocus='autofocus' placeholder='chief_com_n_duration' class='form-control' type='text'>
                                
                              </div>
                            </div>

                            <!-- Text input-->
                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='examination'>Examination</label>  
                                <div class='col-md-7'>
                                    <input id='examination' name='examination' placeholder='Examination' class='form-control' type='text'>
                                
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='investigation'>Investigation</label>  
                                <div class='col-md-7'>
                                    <input id='investigation' name='investigation' autocomplete='false' placeholder='Investigation' class='form-control' type='text'>

                                </div>
                            </div>

                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='diagnosis'>Diagnosis</label>  
                                <div class='col-md-7'>
                                    <input id='diagnosis' name='diagnosis' autocomplete="off" placeholder='diagnosis not accepted here CANNOT be stored' class='form-control' required='required' type='text'>
                                </div>
                            </div>
                            
                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='treatment'>Treatments</label>  
                                <div class='col-md-7'>
                                    <input id='treatment' name='treatment' autocomplete="off" placeholder='treatments not accepted here CANNOT be stored' class='form-control' required='required' type='text'>
                                </div>
                            </div>

                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='treatment_note'>Treatments Notes</label>  
                                <div class='col-md-7'>
                                    <textarea id='treatment_note' name='treatment_note' placeholder='Notes will be printed along with the slip' class='form-control'></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group">
                               <label for="attachment" class="col-md-3 control-label">Attachments</label>
                               <div class="col-md-7">
                                  <div class="input-group">
                                      <label class="input-group-btn">
                                            <span class="btn btn-primary">
                                              Browse&hellip; <input type="file" id="attachfiles" name="files[]" accept="image/gif,image/jpeg,image/jpg,image/pjpeg,image/png,image/x-png, .jpeg" style="display: none;" multiple="multiple">
                                              
                                            </span>
                                      </label>
                                      <input type="text" class="form-control" readonly="readonly" disabled="disabled">
                                  </div>
                               </div>
                            </div>
                            
                            <div class='form-group'>
                                <label class='col-md-3 control-label' for='note'>Notes</label>  
                                <div class='col-md-7'>
                                    <textarea id='note' name='note' placeholder='note' class='form-control'></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                               <label for="datetimepicker1" class="col-md-3 control-label">Followup Visit</label>
                               <div class="col-md-7">
                                    <div class='input-group date datepick' id='datetimepicker1'>
                                        <input type='text' autocomplete="false" id="followup_date" class="form-control" name="followup_date" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                               </div>
                            </div>
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="end_visit"></label>
                                <div class="col-md-7">
                                    <button id="end_visit" name="end_visit" class="btn btn-primary">Save Changes</button>
                                </div>
                            </div>
                        </form>
                        </div>
                </div>
                <?php echo $tab_content; ?>
            </div>
        </div>

          <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Modal title</h4>
                </div>
                <form class="form-horizontal" name="sendToDr_form" action="index.php" id="sendToDr_form">
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save changes</button>
                </div>
                </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <div class="modal fade" id="currentQ" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Current Queue</h4>
                </div>
                <div class="modal-body">
                  <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
<?php 
    }//mysql_num_rows IF 
    else{
        echo "<div class='row'>
                <div class='form-group errmsj row'>
                    <div class='col-md-12'>
                        <div class='alert alert-danger'>
                            Sorry! No patient available right now.
                        </div>
                    </div>
                </div>
            </div>";
    }
?>
</div>

		</div>
	</div>
	</div>
	
</div>

<script src="./js/lightbox.js"></script>
<script>
    lightbox.option({
      'positionFromTop': 30
    })
</script>
<script type="text/javascript">

$(document).ready(function() {

    $('.toggledbox').bootstrapToggle({
        on: "<i class='fa fa-chevron-circle-up'></i> Hide Hx",
        off: "<i class='fa fa-chevron-circle-down'></i> Show Hx",
        onstyle: 'success'
    });

    $("#show_hx").on('change', function(event) {
        $("#p_history_info").slideToggle('slow');
    });

    $(document).on('click','#edit_hx', function(event) {
        event.preventDefault();
        $("#p_hx :input").not('a').prop("disabled", false);
        $("#p_hx :input").not('a').addClass('hxform_en');
        // $("#edit_hx").removeClass('btn-primary');
        // $("#edit_hx").addClass('btn-danger');
        // $("#edit_hx").text("Upadte");
        // $("#edit_hx").attr('id', 'update_hx');
        // $("#edit_hx").attr('name', 'update_hx');
        $("#edit_hx").remove();

        $(".ffooter").append("<button id='update_hx' name='update_hx' class='btn btn-danger col-md-12'><i class='fa fa-pencil'> </i> Update</button>");

    });

    $(document).on('submit', '#p_hx', function(e) {
        e.preventDefault();
        $("#update_hx i").removeClass("fa-pencil");
        $("#update_hx i").addClass("fa-cog fa-spin");
        $("#update_hx").prop("disabled", true);
        var $form = $(e.target);
        $.ajax({
            // url: $form.attr('action'),
            url: "./includes/ajax/dr_p_operation.php",
            type: 'POST',
            data: $form.serialize()+"&update_hx=true",
            success: function(result) {
                if (result == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                }else {
                    $("#update_hx i").removeClass("fa-cog fa-spin");
                    $("#update_hx i").addClass("fa-pencil");
                    $("#p_hx :input").not('button').prop("disabled", true);
                    $("#p_hx :input").not('button').removeClass('hxform_en');

                    $("#update_hx").remove();
                    $(".ffooter").append("<a id='edit_hx' name='edit_hx' class='btn btn-primary col-md-12'>Edit History</a>");
                }

            }
        });
        
    });

    $('#currentQ').on('show.bs.modal', function() {
        $('#currentQ').find('.modal-body').html( "<div class='row' style='text-align: center;'><div class='fa fa-spinner fa-spin fa-4x'></div></div>");

        $.ajax({
            url: "./includes/ajax/dr_queue_list.php",
            type: 'POST',
            data: "&curr_q=true",
            success: function(result) {
                if (result == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                    $("#currentQ").hide();
                }else {
                    $('#currentQ').find('.modal-body').html(result);
                    $('#queue').DataTable();
                }
            }
        });
    });

    var diagnosis_list=[{value:'red'}, {value:'red'}, {value:'red'}, {value:'red'}];
    var treatment_list=[{value:'red'}, {value:'red'}, {value:'red'}, {value:'red'}];
    var diagnosis_engine = "";
    var treatment_engine = "";

    $.ajax({
        url: "./includes/ajax/treats_diagns_list.php",
        type: 'POST',
        data: "&treat_n_diagn=true",
        success: function(result) {
            if (result == -1) {
                swal({
                    title: "Loading Data Failed!",
                    text: "Loading necessary data has failed, please contact website administrator!",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
            }else {
                list = JSON.parse(result);
                // console.log(list[0]);
                // console.log(list[1]);
                diagnosis_list = list[0];
                treatment_list = list[1];
                diagnosis_engine = new Bloodhound({
                    local: list[0],
                    datumTokenizer: function(d) {
                        return Bloodhound.tokenizers.whitespace(d.value);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                
                var diagn_init = diagnosis_engine.initialize();
                diagn_init
                .done(function() { console.log('ready to go!'); })
                .fail(function() { 
                                    swal({
                                        title: "Initializing Data Failed!",
                                        text: "Loading necessary data has failed, please contact website administrator!",
                                        type: "error",
                                        confirmButtonColor: "#C9302C"
                                    }); 
                                });

                $('#diagnosis').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: diagnosis_engine.ttAdapter() }
                    ]
                } );

                $('.prev_lists_dig').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: diagnosis_engine.ttAdapter() }
                    ]
                });

                treatment_engine = new Bloodhound({
                    local: list[1],
                    datumTokenizer: function(d) {
                        return Bloodhound.tokenizers.whitespace(d.value);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });

                var treat_init = treatment_engine.initialize();
                treat_init
                .done(function() { console.log('ready to go!'); })
                .fail(function() { 
                                    swal({
                                        title: "Initializing Data Failed!",
                                        text: "Loading necessary data has failed, please contact website administrator!",
                                        type: "error",
                                        confirmButtonColor: "#C9302C"
                                    }); 
                                });
                
        
                $('#treatment').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: treatment_engine.ttAdapter() }
                    ]
                });

                $('.prev_lists_treat').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: treatment_engine.ttAdapter() }
                    ]
                });
            }
        }
    });


    //prevent duplicate tokens
    $('#diagnosis, #treatment').on('tokenfield:createtoken', function (event) {
        var existingTokens = $(this).tokenfield('getTokens');
        $.each(existingTokens, function(index, token) {
            if (token.value === event.attrs.value)
                event.preventDefault();
            // $('#diagnosis').parent().parent().addClass('has-error');

            // i should delete duplicate if get method returns it
        });
    });

    // file selection
    $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
    });
    $(':file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
          log = numFiles > 1 ? numFiles + ' files selected' : label;
        if( input.length ) {
          input.val(log);
          // console.log(log);
        } 
        else {
          if( log ) alert(log);
        }
    });//end of file selection

    // image delete
    $('.delete-image').click(function()
    {

        var id = $(this).data('id');
        var filename = $(this).attr('file-name');
        var parent = $(this).parent().parent();
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the attachment!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!"
        },
        function($the_btn){
            // swal("Deleted!", "Your imaginary file has been deleted.", "success");

            // console.log(id);
            var data = 'image_delete_id=' + id +'&filename='+filename;

            $.ajax(
            {
               type: "POST",
               url: "./includes/delete_attachment.php",
               data: data,
               cache: false,
            
               success: function(data1)
               {
                    if (data1=="") {
                        parent.fadeOut('slow', function() {$(this).remove();});
                    }else{
                    alert(data1);
                    }
               },
               
               statusCode: 
               {
                    404: function() {
                    alert('ERROR 404 \npage not found');
                    }
               }

            });
        
        });             
        
    }); //end of image delete

clicked = false;
    $(".enabler").on('click', function(event) {
        // event.preventDefault();
        // $(this).off('click');
if (clicked) { return false; }
else{
        var evt_btn = $(this);
        new_data = evt_btn.parent().prevAll("input,textarea").val();
        clicked = true;
        //editprevpatientform-prev_v_id : form id
        // $("#p_hx :input")
        var prev_v_id = evt_btn.data("id");
        var prev_name = evt_btn.data("name");
        // evt_btn.parent().prevAll("input,textarea").prop('disabled', false);
        evt_btn.removeClass("btn-default");
        evt_btn.addClass("btn-danger");
        evt_btn.children(0).removeClass("fa-cloud-upload");
        evt_btn.children(0).addClass("fa-cog fa-spin");
        
        // console.log(evt_btn.parent().prevAll("input,textarea").val());
        var data = "new_data="+new_data+"&prev_v_id="+prev_v_id+"&prev_name="+prev_name+"&edit_prev_hx=true";
        $.ajax(
        {
           type: "POST",
           url: "./includes/ajax/dr_p_operation.php",
           data: data,
           cache: false,
        
           success: function(result)
           {
                if (result == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                }
                // evt_btn.parent().prevAll("input,textarea").prop('disabled', true);
                evt_btn.removeClass("btn-danger");
                evt_btn.addClass("btn-default");
                evt_btn.children(0).removeClass("fa-cog fa-spin");
                evt_btn.children(0).addClass("fa-cloud-upload");

                clicked = false;
           },
           
           statusCode: 
           {
                404: function() {
                alert('ERROR 404 \npage not found');
                }
           }

        });
}
        
    });

    $(".re_uploader").on('click', function(event) {
        // event.preventDefault();
        // console.log($(this));
        evt_btn = $(this);
        if(checkfiles("attachfiles-"+evt_btn.data("id"))){
            evt_btn.removeClass("btn-warning");
            evt_btn.addClass("btn-danger");
            evt_btn.children(0).addClass("fa fa-cog fa-spin");
            
            data = $('#editprevpatientform-'+evt_btn.data("id"))[0];
            formdata = new FormData(data);
            formdata.append("prev_edit_att", "true");
            $.ajax({
                type: "POST",             // Type of request to be send, called as method
                url: "./includes/ajax/dr_p_operation.php", // Url to which the request is send
                data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(result)   // A function to be called if request succeeds
                {
                    /*if (result != "") {
                        swal({
                            title: "Operation Failed!",
                            text: result,
                            type: "error",
                            confirmButtonColor: "#C9302C"
                        });
                    }*/
                    evt_btn.removeClass("btn-danger");
                    evt_btn.addClass("btn-warning");
                    evt_btn.children(0).removeClass("fa fa-cog fa-spin");
                }
            });
        }
    });


    $(".prev_followup_date").on('change', function(event) {
        var evt_btn = $(this);
        evt_btn.nextAll("span").addClass('danger_');
        evt_btn.nextAll("span").children(0).removeClass("glyphicon glyphicon-calendar");
        evt_btn.nextAll("span").children(0).addClass("fa fa-cog fa-spin");


        prev_v_id = evt_btn.data("id");
        new_data = evt_btn.val();
        data = "new_data="+new_data+"&prev_v_id="+prev_v_id+"&edit_prev_fll_date=true";
        $.ajax(
        {
           type: "POST",
           url: "./includes/ajax/dr_p_operation.php",
           data: data,
           cache: false,
        
           success: function(result)
           {
                if (result == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                }
                evt_btn.nextAll("span").removeClass('danger_')
                evt_btn.nextAll("span").children(0).removeClass("fa-cog fa-spin");
                evt_btn.nextAll("span").children(0).addClass("glyphicon glyphicon-calendar");

                clicked = false;
           },
           
           statusCode: 
           {
                404: function() {
                alert('ERROR 404 \npage not found');
                }
           }

        });
    });

        
    $(".hxtab_btn").on('click', function(event) {
        console.log($(this).data('id'));
        console.log(diagnosis_list);
        console.log(treatment_list);

        vid = $(this).data('id');
        /*
            nazanm brem chand s3atm lawai kushtia hata awha 3ilajm krd
            tokenfield menu dakawta zher inputakani dikai form la prev form
            aw codai xwarawa wak awa waya resetian bkatawa
        */
// $(".form-group input, .form-group .btn, .form-group textarea").css("z-index","0");
$(".goBehind").css("z-index","0");
$(".goBehind1").css("z-index","1");
        //prevent duplicate tokens
        $('#diagnosis-'+vid+', #treatment-'+vid).on('tokenfield:createtoken', function (event) {
            var existingTokens = $(this).tokenfield('getTokens');
            $.each(existingTokens, function(index, token) {
                if (token.value === event.attrs.value)
                    event.preventDefault();
                // $('#diagnosis').parent().parent().addClass('has-error');

                // i should delete duplicate if get method returns it
            });
        });
    });

});// end of document.ready


function checkfiles(input_id) {
    // console.log(input_id);
    //check whether browser fully supports all File API
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
        var flag_type = true;
        var flag_size = true;

        //get the file size and file type from file input field
        for (var i = $('#'+input_id)[0].files.length - 1; i >= 0; i--) {
            // $('#'+input_id)[0].files[i];
            var fsize = $('#'+input_id)[0].files[i].size;
            var ftype = $('#'+input_id)[0].files[i].type;
            var fname = $('#'+input_id)[0].files[i].name;
           
           switch(ftype)
            {
                case 'image/png':
                case 'image/gif':
                case 'image/jpeg':
                case 'image/pjpeg':
                    // alert("Acceptable image file!");
                    break;
                default:
                    // alert('Unsupported File!');
                    flag_type = false;
                    break;
            }
            //check file size
            // if(fsize>5242880) //do something if file size more than 5 mb (5242880)
            if(fsize>15242880)
            {
                flag_size = false;
            }

            if (flag_type && flag_size) {
                return true;
            }else if(!flag_type){
                // alert('Unsupported File!');
                swal({
                    title: "Unsupported File!",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                return false;
            }else if(!flag_size){
                swal({
                    title: fsize +" \nfile size Too big!",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                return false;
            }

        }
    }else{
        alert("Please upgrade your browser, because your current browser lacks some new features we need!");
        swal({
                title: "Can't Use Upload",
                text: "Please upgrade your browser, because your current browser lacks some new features we need!",
                type: "error",
                confirmButtonColor: "#C9302C"
            });
    }
}
</script>