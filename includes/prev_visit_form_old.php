<?php  
$prev_data =

"<div class='panel panel-danger'>
	<div class='panel-heading'>
		<div class='caption panel-title'>
	        <span class='fa fa-stethoscope table_head_icon'></span> Previous Visits {$prev_visit['visit_date']};
		</div>
	</div>
	<div class='panel-body resposive'>
		<div class='row'>";
?>
		<?php  
			$prev_marital_st = "";
	        switch ($prev_visit['marital_status']) {
	            case '1':
	                $prev_marital_st = "Single";
	                break;
	            case '2':
	                $prev_marital_st = "Married";
	                break;
	            case '3':
	                $prev_marital_st = "Divorced";
	                break;
	            case '4':
	                $prev_marital_st = "Widowed";
	                break;
	        }
	        $prev_pregnant = $prev_visit['pregnant']==1?"Pregnant":"Not Pregnant";
	        $prev_breast_feed = $prev_visit['breast_feed']==1?"Yes":"No";

	        $prev_p_basic_info = "
	        			<div class='row'>
		        			<div class='col-md-3'>
							    <label>Marital Status: </label> <label>{$prev_marital_st}</label>
							</div>
							<div class='col-md-3'>
							    <label>Fee: </label> <label>{$prev_visit['fee']}</label>
							</div>
							<div class='col-md-3'>
							    <label>Expected visit was on: </label> <label>{$prev_visit['followup_date']}</label>
							</div>
							<div class='col-md-3'>
							    <label>no. of children: </label> <label>{$prev_visit['no_of_child']}</label>
							</div>
						</div>
						<div class='row'>
							<div class='col-md-3'>
							    <label>other info: </label> <label>{$prev_visit['other_info']}</label>  
							</div>
	        				";
	        //female related info; if preg 0 it means than nothing chosen so the patient should be male
			$prev_p_basic_info .= $prev_visit['pregnant']!=0?"
			        <div class='col-md-3'>
			            <label>Breastfeeding: </label> <label>{$prev_breast_feed}</label>
			        </div>
			        <div class='col-md-3'>
			            <label>Pregnancy: </label> <label>{$prev_pregnant}</label>
			        </div>
			    </div>":"</div>";

			$prev_p_basic_info .= "<hr><div class='clearfix'></div>";

		?>

<?php  ?>
			<div class='row'>
            <form class='form-horizontal' enctype='multipart/form-data' action='./includes/form_submitions.php?to=current_p' id='editnewpatientform-<?php echo $prev_visit['v_id']; ?>' method="POST_<?php echo $prev_visit['v_id']; ?>" onsubmit="return checkfiles()">
                 
                <input disabled="disabled" id='v_id-<?php echo $prev_visit['v_id']; ?>' name='v_id_<?php echo $prev_visit['v_id']; ?>' class='form-control' type='hidden' value="<?php echo $prev_visit['v_id']; ?>" >
                <!-- Text input-->
                <div class='form-group'>
                  <label class='col-md-3 control-label' for='chief_com_n_duration'>chief_com_n_duration</label>  
                  <div class='col-md-7'>
                  <div class="input-group">
                  <input disabled="disabled" id='chief_com_n_duration-<?php echo $prev_visit['v_id']; ?>' name='chief_com_n_duration_<?php echo $prev_visit['v_id']; ?>' placeholder='chief_com_n_duration' class='form-control' type='text' value="<?php echo $prev_visit['chief_comp_n_dur']; ?>" >
                    <span class="input-group-btn">
				        <a href="javascript:;" class="btn btn-default" >
				        	<span class="fa fa-pencil-square-o"> </span>
				        </a>
				    </span>
				    </div>
                  </div>
                </div>

                <!-- Text input-->
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='examination'>Examination</label>  
                    <div class='col-md-7'>
                    <div class="input-group">
                    
                        <input disabled="disabled" id='examination-<?php echo $prev_visit['v_id']; ?>' name='examination_<?php echo $prev_visit['v_id']; ?>' placeholder='Examination' class='form-control' type='text' value="<?php echo $prev_visit['examination']; ?>" >
						<span class="input-group-btn">
					        <a href="javascript:;" class="btn btn-default" >
					        	<span class="fa fa-pencil-square-o"> </span>
					        </a>
					    </span>
				    </div>
                    </div>
                </div>

                <!-- Text input-->
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='investigation'>Investigation</label>  
                    <div class='col-md-7'>
                    <div class="input-group">
                        <input disabled="disabled" id='investigation-<?php echo $prev_visit['v_id']; ?>' name='investigation_<?php echo $prev_visit['v_id']; ?>' autocomplete='false' placeholder='Investigation' class='form-control' type='text' value="<?php echo $prev_visit['investigation']; ?>">
					<span class="input-group-btn">
				        <a href="javascript:;" class="btn btn-default" >
				        	<span class="fa fa-pencil-square-o"> </span>
				        </a>
				    </span>
				    </div>

                    </div>
                </div>

                <div class='form-group'>
                    <label class='col-md-3 control-label' for='diagnosis'>Diagnosis</label>  
                    <div class='col-md-7'>
                    <div class="input-group">
                        <input disabled="disabled" id='diagnosis-<?php echo $prev_visit['v_id']; ?>' name='diagnosis_<?php echo $prev_visit['v_id']; ?>' autocomplete="off" placeholder='diagnosis not accepted here CANNOT be stored' class='form-control' required='required' type='text' >
					<span class="input-group-btn">
				        <a href="javascript:;" class="btn btn-default" >
				        	<span class="fa fa-pencil-square-o"> </span>
				        </a>
				    </span>
				    </div>
                    </div>
                </div>
                
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='treatment'>Treatments</label>  
                    <div class='col-md-7'>
                    <div class="input-group">
                        <input disabled="disabled" id='treatment-<?php echo $prev_visit['v_id']; ?>' name='treatment_<?php echo $prev_visit['v_id']; ?>' autocomplete="off" placeholder='treatments not accepted here CANNOT be stored' class='form-control' required='required' type='text' >
					<span class="input-group-btn">
				        <a href="javascript:;" class="btn btn-default" >
				        	<span class="fa fa-pencil-square-o"> </span>
				        </a>
				    </span>
				    </div>
                    </div>
                </div>

                <div class='form-group'>
                    <label class='col-md-3 control-label' for='treatment_note'>Treatments Notes</label>  
                    <div class='col-md-7'>
                    <div class="input-group">
                        <textarea disabled="disabled" id='treatment_note-<?php echo $prev_visit['v_id']; ?>' name='treatment_note_<?php echo $prev_visit['v_id']; ?>' placeholder='Notes will be printed along with the slip' class='form-control'><?php echo $prev_visit['treatment_note']; ?> </textarea>
					<span class="input-group-btn">
				        <a href="javascript:;" class="btn btn-default" >
				        	<span class="fa fa-pencil-square-o"> </span>
				        </a>
				    </span>
				    </div>
                    </div>
                </div>

                <div class="form-group">
			   <label for="attachment" class="col-md-3 control-label">Attachments</label>
			   <div class="col-md-7">
				  <div class="input-group">
					  <label class="input-group-btn">
						  	<span class="btn btn-primary">
							  Browse&hellip; <input type="file" id="attachfiles-<?php echo $prev_visit['v_id']; ?>" name="files<?php echo $prev_visit['v_id']; ?>[]" accept="image/gif,image/jpeg,image/jpg,image/pjpeg,image/png,image/x-png, .jpeg" style="display: none;" multiple="multiple">
							  
							</span>
					  </label>
					  <input type="text" class="form-control" readonly="readonly" disabled="disabled">

					  <span class="input-group-btn">
				        <a href="javascript:;" class="btn btn-warning" >Start Upload</a>
				      </span>
				  </div>
					  <?php  
					  	
						echo $attachments_output;
					  ?>
			   </div>
			</div>

                
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='note'>Notes</label>  
                    <div class='col-md-7'>
                    <div class="input-group">
                        <textarea id='note-<?php echo $prev_visit['v_id']; ?>' name='note_<?php echo $prev_visit['v_id']; ?>' placeholder='note' class='form-control' disabled="disabled"></textarea>
					<span class="input-group-btn">
				        <a href="javascript:;" class="btn btn-default" >
				        	<span class="fa fa-pencil-square-o"> </span>
				        </a>
				    </span>
				    </div>
                    </div>
                </div>
                <div class="form-group">
                   <label for="datetimepicker1-<?php echo $prev_visit['v_id']; ?>" class="col-md-3 control-label">Followup Visit</label>
                   <div class="col-md-7">

                        <div class="input-group date datepick" id="datetimepicker1-<?php echo $prev_visit['v_id']; ?>">
                            <input type="text" autocomplete="false" id="followup_date-<?php echo $prev_visit['v_id']; ?>" class="form-control" name="followup_date_<?php echo $prev_visit['v_id']; ?>" value="<?php echo $prev_visit['followup_date']; ?>" disabled="disabled" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            
                        </div>
                   </div>
                </div>

                <!-- Button -->
                
            </form>
            </div> 

		</div>
	</div>
</div>
