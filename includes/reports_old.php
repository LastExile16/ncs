<?php
require_once 'connection.php';
require_once 'functions.php';

if (isset($_POST['activity_per_branch'])) {
	//clinics
	$query = "SELECT b.name branch, count(id) counter, 'clinics' label
				FROM patient_activity p, branch b 
				WHERE p.view=1 AND type=1 AND branch_id_f = b.b_id
				GROUP BY b.name";
	$result = mysql_query($query) or die(mysql_error() . "100");

	$data = array();

	//loop through the returned data
	while($row = mysql_fetch_assoc($result)) {
		$data[] = $row;
	}
	
	//surgery
	$query = "SELECT b.name branch, count(id) counter, 'surgery' label
					FROM patient_activity p, branch b 
					WHERE p.view=1 AND type=2 AND branch_id_f = b.b_id
					GROUP BY b.name";
		$result = mysql_query($query) or die(mysql_error() . "101");


		//loop through the returned data
		while($row = mysql_fetch_assoc($result)) {
			$data[] = $row;
		}
	//other projects
	$query = "SELECT b.name branch, count(p_id) counter, 'other' label
					FROM project p, branch b 
					WHERE p.view=1 AND branch_id_f = b.b_id
					GROUP BY b.name";
		$result = mysql_query($query) or die(mysql_error() . "102");


		//loop through the returned data
		while($row = mysql_fetch_assoc($result)) {
			$data[] = $row;
		}

	//now print the data
	print json_encode($data);
}

?>