<?php 
 // this page includes prev_visit_form.php
require_once '../connection.php';
require_once '../functions.php';
if (isset($_POST['p_id']) && is_numeric($_POST['p_id']) && isset($_POST['more_info'])) 
    {
        if(!isset($_SESSION))
        {
            session_start();
        }

        $remote_pid = $_POST['p_id'];
?>
<style type="text/css">
    .gallery{
        padding: 0px;
        margin-top: -10px;
        margin-bottom: 15px;
    }
    .gallery-1{
        margin-top: 5px;
    }
    .gallery-grid{
        padding-left: 5px;
        padding-right: 5px;
    }
    .gallery-border{
        border: 1px dashed #337AB7;
        padding-bottom: 5px;
    }
    .input-group {
        margin-bottom: 0px;
    }
    .img-wrap {
        position: relative;
        border: 1px solid #d1cfcf;
        border-radius: 4px;
    }
    .img-wrap .close {
        position: absolute;
        top: -3px;
        right: 2px;
        color: red;
        opacity: 0.4;
        z-index: 100;
    }
    .example-image{
        border-radius: 4px;
    }

    
    .img-wrap .close:focus, .img-wrap .close:hover {
        color: #F30909;
        background-color: white;
        border-style: solid;
        border-width: 1px;
        border-color: gray;
        border-top-width: 0px;
        box-shadow: 1px 1px 2px -1px red, -1px -1px 2px -1px red; 
        opacity: 0.8;
    }
    .glowing {
        background-color: #004A7F;
        -webkit-border-radius: 10px;
        border-radius: 10px;
        border: none;
        color: #FFFFFF !important;
        cursor: pointer;
        display: inline-block;
        /*font-family: Arial;*/
        /*font-size: 20px;*/
        /*padding: 5px 10px;*/
        text-align: center;
        text-decoration: none;
    }
    .glowing *{
        color: #FFFFFF !important;
    }
    @-webkit-keyframes glowing {
      0% { background-color: #8e44ad; -webkit-box-shadow: 0 0 3px #8e44ad; }
      50% { background-color: #e400ff; -webkit-box-shadow: 0 0 30px #e400ff; }
      100% { background-color: #8e44ad; -webkit-box-shadow: 0 0 3px #8e44ad; }
    }

    @-moz-keyframes glowing {
      0% { background-color: #8e44ad; -moz-box-shadow: 0 0 3px #8e44ad; }
      50% { background-color: #e400ff; -moz-box-shadow: 0 0 30px #e400ff; }
      100% { background-color: #8e44ad; -moz-box-shadow: 0 0 3px #8e44ad; }
    }

    @-o-keyframes glowing {
      0% { background-color: #8e44ad; box-shadow: 0 0 3px #8e44ad; }
      50% { background-color: #e400ff; box-shadow: 0 0 30px #e400ff; }
      100% { background-color: #8e44ad; box-shadow: 0 0 3px #8e44ad; }
    }

    @keyframes glowing {
      0% { background-color: #8e44ad; box-shadow: 0 0 3px #8e44ad; }
      50% { background-color: #e400ff; box-shadow: 0 0 30px #e400ff; }
      100% { background-color: #8e44ad; box-shadow: 0 0 3px #8e44ad; }
    }

    .glowing {
      -webkit-animation: glowing 1500ms infinite;
      -moz-animation: glowing 1500ms infinite;
      -o-animation: glowing 1500ms infinite;
      animation: glowing 1500ms infinite;
    }


</style>
<script type="text/javascript" src="./js/modernizr.custom.79639.js"></script>

<div class="contact row">

    <!-- BEGIN SEARCH TABLE PORTLET-->
    
<!-- END SEARCH TABLE PORTLET-->
    <div class="col-md-2">
    </div>
</div>
<?php 
    echo isset($_GET['err_msj']) && !empty($_GET['err_msj'])? "
    <div class='row'>
        <div class='form-group errmsj row' style='display:none'>
            <div class='col-md-12'>
                <div class='alert alert-danger alert-dismissable'>
                <button type='button' class='close' aria-hidden='true'>&times; </button>
                    {$_GET['err_msj']}
                </div>
            </div>
        </div>
    </div>":""; 
?>
<div class="row">
     
     <!-- BEGIN SAMPLE TABLE PORTLET-->
     <div class="col-md-12">
        
    <?php  
        $today = date('Y-m-d');
    ?>
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="caption panel-title">
            <div class="row">
                <div class="col-md-3">
                <span class='fa fa-stethoscope table_head_icon'></span><span>Patient </span>
                </div>
                
            </div>
            </div>
        </div>
        <div class="panel-body resposive">
        <div class="row">
        <?php
            /*
            p.p_id, p.fullname, p.sex, p.address, p.dob, p.phone, p.occupation, p.past_hx, p.family_hx, p.drug_hx, p.other_hx, p.latest_visit, p.queue_status, p.u_id_f, p.latest_followup_visit
        */
            $patient_set = get_patient($remote_pid);
            //if (mysql_num_rows($patient_set)>0) {

                $patient = mysql_fetch_assoc($patient_set);
                $procedure_counter = count(array_filter(explode(', ', $patient['procedures_list'])));
                // $d1 = new DateTime();//today
                // $d2 = new DateTime($patient['dob']);
                // $diff = $d2->diff($d1);
                //echo $diff->y;
                $age = date('Y')-$patient['dob'];
                $sex ="";
                if ($patient['sex']=='1') {
                    $sex = "<div style='text-align:center'><i class='fa fa-mars'><span style='display:none'>1</span></i></div>"; 
                }else if ($patient['sex']=='2') {
                    $sex = "<div style='text-align:center'><i class='fa fa-venus'><span style='display:none'>2</span></i></div>";    
                }else if ($patient['sex']=='3') {
                    $sex = "<div style='text-align:center'><i class='fa fa-venus-mars'><span style='display:none'>3</span></i></div>";   
                }
                
                $p_basic_info = "
                
                    <div class='row'>
                        <div class='col-md-4'>
                            <label>Code and Name: </label> <label id='pcode_n_name'>{$patient['p_id']}, {$patient['fullname']}</label>   
                        </div>
                        <div class='col-md-4'>
                            <label>sex: </label> <label id='sex' data-sex={$patient['sex']}>{$sex}</label>
                            <label>; Procedures: </label> <label id='procedure_counter'>{$procedure_counter}</label>
                        </div>
                        <div class='col-md-4'>
                            <label>age: </label> <label id='p_age' data-dob='{$patient['dob']}'>{$age}</label>
                        </div>
                    </div>";
                $p_basic_info .= "
                    <div class='row'>
                        <div class='col-md-4'>
                            <label>Occupation: </label> <label id='occupation'>{$patient['occupation']}</label>
                        </div>
                        <div class='col-md-4'>
                            <label>Address: </label> <label id='address'>{$patient['address']}</label>
                        </div>
                        <div class='col-md-4'>
                            <label>Expected visit is on: </label> <label>{$patient['latest_followup_visit']}</label>
                        </div>
                    </div>";
                
                $p_basic_info .= "<hr>";
                echo $p_basic_info;

                $hx_button = "<input type='checkbox' id='show_hx' class='toggledbox' >";
                $p_history = $hx_button . "<hr><div id='p_history_info' class='row' style='display:none'>
                <form class='form-horizontal' name='p_hx' id='p_hx' action=''>";

                $p_history .= "
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='past_hx'>Past History</label>  
                    <div class='col-md-7'>
                        <textarea id='past_hx' name='past_hx' disabled='disabled' placeholder='Past History' class='form-control'>{$patient['past_hx']}</textarea>
                    </div>
                </div>

                <div class='form-group'>
                    <label class='col-md-3 control-label' for='family_hx'>Family History</label>  
                    <div class='col-md-7'>
                        <textarea id='family_hx' name='family_hx' disabled='disabled' placeholder='Family History' class='form-control'>{$patient['family_hx']}</textarea>
                    </div>
                </div>
                
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='drug_hx'>Drug History</label>  
                    <div class='col-md-7'>
                        <textarea id='drug_hx' name='drug_hx' disabled='disabled' placeholder='Drug History' class='form-control'>{$patient['drug_hx']}</textarea>
                    </div>
                </div>
                
                <div class='form-group'>
                    <label class='col-md-3 control-label' for='other_hx'>Other History</label>  
                    <div class='col-md-7'>
                        <textarea id='other_hx' name='other_hx' disabled='disabled' placeholder='Other History' class='form-control'>{$patient['other_hx']}</textarea>
                    </div>
                </div>

                <div class='form-group row'>  
                    <div class='col-md-2 col-md-offset-8 ffooter'>
                        <a id='edit_hx' name='edit_hx' class='btn btn-primary col-md-12'>Edit History</a>
                    </div>
                </div>
                <input type='hidden' name='p_id' value='{$patient['p_id']}'>
                ";
                $p_history .= "</form> </div>";

                echo $p_history;
            
        ?>
        </div>
        <div class="clearfix"></div>
        <?php  
            $tabs_list = "No prev visits";
            $tab_content = "";
            $prev_visits_list_set = get_prev_visits_list($remote_pid);
            if (mysql_num_rows($prev_visits_list_set)>0) {
                $tabs_list = "";
                $j = 1;
                while ($prev_visit = mysql_fetch_assoc($prev_visits_list_set)) {
                    
                    //return all related attachments
                    $attachments_output = "no attachments";
                    $attachment_set = get_attachment($prev_visit['v_id']);
                    if (mysql_num_rows($attachment_set)>0) {
                    $attachments_output = "
                    <div class='gallery gallery-border row'>
                         <div class='container col-md-12'>
                             <div class='gallery-bottom'>";
                             $i = 0;
                             $flag=true;
                        while ( $attachment = mysql_fetch_assoc($attachment_set)) {
                            //4 pics in a row (by creating a new gallery-1)
                            if ($i % 4 == 0) {
                                $attachments_output .=  "<div class='gallery-1'>";
                                $flag = true;
                            }
                            $datavideo='';
                            $alt='alt="broken image"';
                            if(!preg_match('/^.*\.(jpg|jpeg|png|gif)$/i', $attachment['filename'])) {
                               $datavideo='data-video=true';
                               $alt='alt="Video file"';
                            }
                            $attachments_output .=  "<div class='col-md-3 gallery-grid'>
                                            <div class='img-wrap'><span data-id='{$attachment['a_id']}' file-name='{$attachment['filename']}' id='close_{$attachment['a_id']}' class='close delete-image'>&times;</span>
                                                <a class='example-image-link' href='uploads/{$attachment['filename']}' data-lightbox='album_{$prev_visit['v_id']}' {$datavideo} >
                                                    <img class='example-image pimage-{$prev_visit['v_id']}' src='uploads/{$attachment['filename']}' {$alt} />
                                                </a>
                                                </div>
                                            </div>";
                                // $attachments_output .= "<span>i: {$i}, flag: $flag </span>";
                            if (++$i % 4 == 0) {
                                $attachments_output .= "<div class='clearfix'></div></div>";
                                $flag = false;
                            }
                            // $i++;
                        }

                        //if <div gallery-1> was opened but not closed then close it. 
                        if ($flag) {
                            $attachments_output .= "<div class='clearfix'></div></div>";
                        }
                    $attachments_output .= "</div>
                                 </div>
                            </div>";

                    }

                    //now create the previous visits form with there values
                    /*<li>
                            <a href="#messages" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1" aria-expanded="false">Messages</a>
                        </li>

                        <li>
                            <a href="#settings" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2" aria-expanded="false">Settings</a>
                        </li>
                            
                        SELECT v.v_id, v.visit_date, v.followup_date, v.marital_status, v.pregnant, v.breast_feed, v.no_of_child, v.other_info, v.fee, v.chief_comp_n_dur, v.examination, v.investigation, v.note, v.treatment_note
                        */

                    $tabs_list .= "
                        <li>
                            <a href='#tabpane{$prev_visit['v_id']}' role='tab' class='hxtab_btn' data-id='{$prev_visit['v_id']}' id='dropdown{$prev_visit['v_id']}-tab' data-toggle='tab' aria-controls='dropdown1' aria-expanded='false'>{$prev_visit['visit_date']}</a>
                        </li>
                        ";
                    $active = $j==1?"active":"";
                    $tab_content .= "<div role='tabpanel' class='tab-pane fade in {$active}' id='tabpane{$prev_visit['v_id']}'>";

                        include '../prev_visit_form.php';
                        $tab_content .= $prev_p_basic_info;
                        $tab_content .= $prev_data;
                    $tab_content .= '</div>';
                    $j++;
                }
                
            }

            $tabs_bar = '
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" id="myTabDrop1" data-toggle="dropdown" > 
                        Previous Visits <span class="caret"> </span>
                    </a>

                    <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents"> ';
            $tabs_bar .= $tabs_list;
                        
            $tabs_bar .= '</ul>
                    </li>';
        ?>
        <div class="row">
            <ul class="nav nav-tabs" role="tablist">
               

                <?php echo $tabs_bar; ?>
            </ul>
            <div class="tab-content">
                
                <?php echo $tab_content; ?>
            </div>
        </div>

          <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save changes</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
<?php 
   /* }//mysql_num_rows IF 
    else{
        echo "<div class='row'>
                <div class='form-group errmsj row'>
                    <div class='col-md-12'>
                        <div class='alert alert-warning'>
                            Sorry! No patient available right now.
                        </div>
                    </div>
                </div>
            </div>";
    }*/
?>
         
</div>

        </div>
    </div>
    </div>
    
</div>
<!-- http://lokeshdhakar.com/projects/lightbox2 -->
<script src="./js/lightbox.js"></script>
<script>
    lightbox.option({
      'positionFromTop': 30,
      'hasVideo': true
    })
</script>
<script type="text/javascript">

$(document).ready(function() {

    $('.toggledbox').bootstrapToggle({
        on: "<i class='fa fa-chevron-circle-up'></i> Hide Hx",
        off: "<i class='fa fa-chevron-circle-down'></i> Show Hx",
        onstyle: 'success'
    });

    $("#show_hx").on('change', function(event) {
        $("#p_history_info").slideToggle('slow');
    });

    $(document).on('click','#edit_hx', function(event) {
        event.preventDefault();
        $("#p_hx :input").not('a').prop("disabled", false);
        $("#p_hx :input").not('a').addClass('hxform_en');
        // $("#edit_hx").removeClass('btn-primary');
        // $("#edit_hx").addClass('btn-danger');
        // $("#edit_hx").text("Upadte");
        // $("#edit_hx").attr('id', 'update_hx');
        // $("#edit_hx").attr('name', 'update_hx');
        $("#edit_hx").remove();

        $(".ffooter").html("<button id='update_hx' name='update_hx' class='btn btn-danger col-md-12'><i class='fa fa-pencil'> </i> Update</button>");

    });

    $(document).on('submit', '#p_hx', function(e) {
        e.preventDefault();
        $("#update_hx i").removeClass("fa-pencil");
        $("#update_hx i").addClass("fa-cog fa-spin");
        $("#update_hx").prop("disabled", true);
        var $form = $(e.target);
        $.ajax({
            // url: $form.attr('action'),
            url: "./includes/ajax/dr_p_operation.php",
            type: 'POST',
            data: $form.serialize()+"&update_hx=true",
            success: function(result) {
                if (result == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                }else {
                    $("#update_hx i").removeClass("fa-cog fa-spin");
                    $("#update_hx i").addClass("fa-pencil");
                    $("#p_hx :input").not('button').prop("disabled", true);
                    $("#p_hx :input").not('button').removeClass('hxform_en');

                    $("#update_hx").remove();
                    $(".ffooter").html("<a id='edit_hx' name='edit_hx' class='btn btn-primary col-md-12'>Edit History</a>");
                }

            }
        });
        
    });


    var diagnosis_list=[{value:'red'}, {value:'red'}, {value:'red'}, {value:'red'}];
    var treatment_list=[{value:'red'}, {value:'red'}, {value:'red'}, {value:'red'}];
    var surgery_list=[{value:'red'}, {value:'red'}, {value:'red'}, {value:'red'}];
    var procedure_list=[{value:'red'}, {value:'red'}, {value:'red'}, {value:'red'}];

    var procedure_engine = "";
    var diagnosis_engine = "";
    var treatment_engine = "";
    var surgery_engine = "";
    var procedure_engine = "";

    $.ajax({
        url: "./includes/ajax/treats_diagns_list.php",
        type: 'POST',
        asynch: true,
        data: "&treat_n_diagn=true",
        success: function(result) {
            if (result == -1) {
                swal({
                    title: "Loading Data Failed!",
                    text: "Loading necessary data has failed, please contact website administrator!",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
            }else {
                list = JSON.parse(result);
                // console.log(list[0]);
                // console.log(list[1]);
                diagnosis_list = list[0];
                treatment_list = list[1];
                surgery_list = list[2];
                procedure_list = list[3];

                diagnosis_engine = new Bloodhound({
                    local: list[0],
                    datumTokenizer: function(d) {
                        return Bloodhound.tokenizers.whitespace(d.value);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                
                var diagn_init = diagnosis_engine.initialize();
                diagn_init
                .done(function() { console.log('ready to go!'); })
                .fail(function() { 
                                    swal({
                                        title: "Initializing Data Failed!",
                                        text: "Loading necessary data has failed, please contact website administrator!",
                                        type: "error",
                                        confirmButtonColor: "#C9302C"
                                    }); 
                                });

                $('.prev_lists_dig').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: diagnosis_engine.ttAdapter() }
                    ]
                });

                treatment_engine = new Bloodhound({
                    local: list[1],
                    datumTokenizer: function(d) {
                        return Bloodhound.tokenizers.whitespace(d.value);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });

                var treat_init = treatment_engine.initialize();
                treat_init
                .done(function() { console.log('ready to go!'); })
                .fail(function() { 
                                    swal({
                                        title: "Initializing Data Failed!",
                                        text: "Loading necessary data has failed, please contact website administrator!",
                                        type: "error",
                                        confirmButtonColor: "#C9302C"
                                    }); 
                                });

                $('.prev_lists_treat').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: treatment_engine.ttAdapter() }
                    ]
                });

                surgery_engine = new Bloodhound({
                    local: list[2],
                    datumTokenizer: function(d) {
                        return Bloodhound.tokenizers.whitespace(d.value);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });

                var surg_init = surgery_engine.initialize();
                surg_init
                .done(function() { console.log('ready to go!'); })
                .fail(function() { 
                                    swal({
                                        title: "Initializing Data Failed!",
                                        text: "Loading necessary data has failed, please contact website administrator!",
                                        type: "error",
                                        confirmButtonColor: "#C9302C"
                                    }); 
                                });
                $('.prev_lists_surg').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: surgery_engine.ttAdapter() }
                    ]
                });

                procedure_engine = new Bloodhound({
                    local: list[3],
                    datumTokenizer: function(d) {
                        return Bloodhound.tokenizers.whitespace(d.value);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });

                var prd_init = procedure_engine.initialize();
                prd_init
                .done(function() { console.log('ready to go!'); })
                .fail(function() { 
                                    swal({
                                        title: "Initializing Data Failed!",
                                        text: "Loading necessary data has failed, please contact website administrator!",
                                        type: "error",
                                        confirmButtonColor: "#C9302C"
                                    }); 
                                });
                $('#procedure').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: procedure_engine.ttAdapter() }
                    ]
                });
                // used in history of patient
                $('.prev_lists_prd').tokenfield({
                    typeahead: [
                        {hint: true,highlight: true}, 
                        { source: procedure_engine.ttAdapter() }
                    ]
                });
            }
        }
    });

    //prevent duplicate tokens
    $('.prev_lists_dig, .prev_lists_treat, .prev_lists_surg, .prev_lists_prd').on('tokenfield:createtoken', function (event) {
        var existingTokens = $(this).tokenfield('getTokens');
        $.each(existingTokens, function(index, token) {
            if (token.value === event.attrs.value)
                event.preventDefault();
            // $('#diagnosis').parent().parent().addClass('has-error');

            // i should delete duplicate if get method returns it
        });
    });

    // file selection
    $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
    });
    $(':file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
          log = numFiles > 1 ? numFiles + ' files selected' : label;
        if( input.length ) {
          input.val(log);
          // console.log(log);
        } 
        else {
          if( log ) alert(log);
        }
    });//end of file selection

    // image delete
    $('.delete-image').click(function()
    {

        var id = $(this).data('id');
        var filename = $(this).attr('file-name');
        var parent = $(this).parent().parent();
        swal({
            title: "DELETE!?",
            text: "You will not be able to recover the attachment!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "DELETE!"
        },
        function(){
            // console.log(id);
            var data = 'image_delete_id=' + id +'&filename='+filename;

            $.ajax(
            {
               type: "POST",
               url: "./includes/delete_attachment.php",
               data: data,
               cache: false,
            
               success: function(data1)
               {
                    if (data1=="") {
                        parent.fadeOut('slow', function() {$(this).remove();});
                    }else{
                    alert(data1);
                    }
               },
               
               statusCode: 
               {
                    404: function() {
                    alert('ERROR 404 \npage not found');
                    }
               }

            });
        
        });             
        
    }); //end of image delete

clicked = false;
    $(".enabler").on('click', function(event) {
        // event.preventDefault();
        // $(this).off('click');
if (clicked) { return false; }
else{
        var evt_btn = $(this);
        var prev_v_id = evt_btn.data("id");
        var prev_name = evt_btn.data("name");
        var new_data = "";
        // surg_date is only used in prev_name 8 bcs when we insert new data
        // we need to have the new date to be inserted too.
        patient_id = "";
        
        //special needed vars
        var surg_date = "";
        var surg_note = "";
        var prd_date = "";
        var prev_p_id = "";

        var attr1 = "";
        var attr2 = "";
        var attr3 = "";

        if(prev_name<10){
            new_data = evt_btn.parent().prevAll("input,textarea").val();
            if (prev_name == 7) {
                if ($("#procedure_date-"+prev_v_id).val()==null || $("#procedure_date-"+prev_v_id).val()=="") {
                    swal({
                        title: "Oops!",
                        text: "please fill DATE correctly.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                    return false;
                }
                else{
                    new_data_mayhavecomma = $("#procedure-"+prev_v_id).val();
                    new_data = new_data_mayhavecomma.replace(/(^,)|(,$)/g, "");
                    prd_date = $("#procedure_date-"+prev_v_id).val();
                    prev_p_id = evt_btn.data("pid");
                }
            }
        }else if (prev_name==16) {
            new_data_mayhavecomma = $("#diagnosis-"+prev_v_id).val();
            new_data = new_data_mayhavecomma.replace(/(^,)|(,$)/g, "");
        }else if(prev_name==17) {
            new_data_mayhavecomma = $("#treatment-"+prev_v_id).val();
            new_data = new_data_mayhavecomma.replace(/(^,)|(,$)/g, "");
        }else if(prev_name==18) {
            new_data_mayhavecomma = $("#surgery-"+prev_v_id).val();
            new_data = new_data_mayhavecomma.replace(/(^,)|(,$)/g, "");
            if ($("#surgery_date-"+prev_v_id).val()==null || $("#surgery_date-"+prev_v_id).val()=="") {
                swal({
                    title: "Oops!",
                    text: "please fill DATE correctly.",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                return false;
            }
            else{
                surg_date = $("#surgery_date-"+prev_v_id).val();
                surg_note = $("#surgery_note-"+prev_v_id).val();
                prev_p_id = evt_btn.data("pid");
            }
        }else if(prev_name==19) {
            attr1 = $("#attr1-"+prev_v_id).val();
            if (attr1==='Cardiac') {
                attr2 = $("#attr2-"+prev_v_id).val();
                attr3 = $("#attr3-"+prev_v_id).val();

            }else if(attr1==='Vascular') {
                attr2 = $("#attr2-"+prev_v_id).val();
                if (attr2==='Venous Diseases') {
                    attr3 = $("#attr3-"+prev_v_id).val();
                }

            }else if(attr1==='Thoracic') {
                attr2 = $("#attr2-"+prev_v_id).val();
            }
        }else {
            //there is something wrong with the internal code
            swal({
                title: "Oops!",
                text: "Something is wrong with the provided data, please contact system administrator.",
                type: "error",
                confirmButtonColor: "#C9302C"
            });
            return false;
        }
        /*
        else if(prev_name==8) {
            new_data = $("#surgery-"+prev_v_id).val();
            surg_date = $("#surgery_date-"+prev_v_id).val();
            patient_id = evt_btn.data("pid");
            // i need this pid to update surgeries column in patient table
        }
        */
        clicked = true;
        // evt_btn.parent().prevAll("input,textarea").prop('disabled', false);
        evt_btn.removeClass("btn-default");
        evt_btn.addClass("btn-danger");
        evt_btn.children().first().removeClass("fa-cloud-upload");
        evt_btn.children().first().addClass("fa-cog fa-spin");
        
        // console.log(evt_btn.parent().prevAll("input,textarea").val());
        // var data = "new_data="+new_data+"&prev_v_id="+prev_v_id+"&surg_date="+surg_date+"&pid="+patient_id+"&prev_name="+prev_name+"&edit_prev_hx=true";
        var data = "new_data="+new_data+"&prev_v_id="+prev_v_id+"&prev_name="+prev_name+"&surg_date="+surg_date+"&surg_note="+surg_note+"&prd_date="+prd_date+"&pid="+prev_p_id+"&attr1="+attr1+"&attr2="+attr2+"&attr3="+attr3+"&edit_prev_hx=true";
        $.ajax(
        {
           type: "POST",
           url: "./includes/ajax/dr_p_operation.php",
           data: data,
           cache: false,
        
           success: function(result)
           {
                if (result == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                }
                // evt_btn.parent().prevAll("input,textarea").prop('disabled', true);
                evt_btn.removeClass("btn-danger");
                evt_btn.addClass("btn-default");
                evt_btn.children().first().removeClass("fa-cog fa-spin");
                evt_btn.children().first().addClass("fa-cloud-upload");

                clicked = false;
           },
           
           statusCode: 
           {
                404: function() {
                alert('ERROR 404 \npage not found');
                }
           }

        });
}
        
    });

    $(".re_uploader").on('click', function(event) {
        // event.preventDefault();
        console.log($(this));
        evt_btn = $(this);
        if(checkfiles("attachfiles-"+evt_btn.data("id"))){
            evt_btn.removeClass("btn-warning");
            evt_btn.addClass("btn-danger");
            evt_btn.children(0).addClass("fa fa-cog fa-spin");
            
            data = $('#editprevpatientform-'+evt_btn.data("id"))[0];
            formdata = new FormData(data);
            formdata.append("prev_edit_att", "true");
            $.ajax({
                type: "POST",             // Type of request to be send, called as method
                url: "./includes/ajax/dr_p_operation.php", // Url to which the request is send
                data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(result)   // A function to be called if request succeeds
                {
                    /*if (result != "") {
                        swal({
                            title: "Operation Failed!",
                            text: result,
                            type: "error",
                            confirmButtonColor: "#C9302C"
                        });
                    }*/
                    evt_btn.removeClass("btn-danger");
                    evt_btn.addClass("btn-warning");
                    evt_btn.children(0).removeClass("fa fa-cog fa-spin");
                }
            });
        }
    });


    $(".prev_followup_date, .prev_surgery_date, .prev_procedure_date").on('change', function(event) {
        var evt_btn = $(this);
        evt_btn.nextAll("span").addClass('danger_');
        evt_btn.nextAll("span").children(0).removeClass("glyphicon glyphicon-calendar");
        evt_btn.nextAll("span").children(0).addClass("fa fa-cog fa-spin");


        prev_v_id = evt_btn.data("id");
        prev_p_id = evt_btn.data("pid");
        prev_inputtype = evt_btn.data("name");
        new_data = evt_btn.val();
        data = "new_data="+new_data+"&prev_v_id="+prev_v_id+"&inputtype="+prev_inputtype+"&prev_p_id="+prev_p_id+"&edit_prev_date=true";
        $.ajax(
        {
           type: "POST",
           url: "./includes/ajax/dr_p_operation.php",
           data: data,
           cache: false,
        
           success: function(result)
           {
                if (result == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                }
                evt_btn.nextAll("span").removeClass('danger_')
                evt_btn.nextAll("span").children(0).removeClass("fa-cog fa-spin");
                evt_btn.nextAll("span").children(0).addClass("glyphicon glyphicon-calendar");

                clicked = false;
           },
           
           statusCode: 
           {
                404: function() {
                alert('ERROR 404 \npage not found');
                }
           }

        });
    });

    //not inside any event, just do it
    $(".goBehind").css("z-index","0");
    $(".goBehind4").css("z-index","4");
    $(".goBehind3").css("z-index","3");
    $(".goBehind2").css("z-index","2");
    $(".goBehind1").css("z-index","1");
   
    $("#end_visit, .prinslip").on('click', function(event) {
       
       var id = $(this).data('id');
       // for pcode_n_name I don't need to concate it with prev_id, because I have only one modal at a time which has one basic information and no more.
       
       if(!checkfiles("attachfiles-"+id)) { return false;}

       var pcode_n_name = $('#pcode_n_name').text();
       var p_age = $('#p_age').text();
        // var p_date = $('#p_date').text();
        var treatment = $('#treatment-'+id).val();
        var treatment_note = $('#treatment_note-'+id).val();
        if (!empty(treatment)) {
            treatment_arr = treatment.split(', ');
        }else{
            treatment_arr = new Array();
        }
        if (!empty(treatment_note)) {
            treatment_note_arr = treatment_note.split('\n');
        }else{
            treatment_note_arr = new Array();
        }

        treatment_arr_count = treatment_arr.length;
        treatment_note_arr_count = treatment_note_arr.length;

        treatment_slip_note = "";
        for(i=0; i<treatment_arr_count; i++)
        {

            treatment_slip_note += treatment_arr[i]+": "+treatment_note_arr[i]+"@";
        }
         treatment_slip_note = treatment_slip_note.replace(/undefined/g, "no data");
        // console.log(treatment_slip_note);
            
            date = $('#dropdown'+id+'-tab').text();
            
            // console.log(page);
          print_slip(date, pcode_n_name, treatment_slip_note, p_age);
    });

    // print investigations
    $(".print_inv").on('click', function(event) {
       
       var id = $(this).data('id');
       // for pcode_n_name I don't need to concate it with prev_id, because I have only one modal at a time which has one basic information and no more.
       
       var pcode_n_name = $('#pcode_n_name').text();
       var p_age = $('#p_age').text();
        // var p_date = $('#p_date').text();
        var investigation = $('#investigation-'+id).val();
        var investigation_note = $('#investigation_note-'+id).val();
        if (!empty(investigation)) {
            investigation_arr = investigation.trim().split(/\s*,\s*/);
        }else{
            investigation_arr = new Array();
        }
        if (!empty(investigation_note)) {
            investigation_note_arr = investigation_note.split('\n');
        }else{
            investigation_note_arr = new Array();
        }

        investigation_arr_count = investigation_arr.length;
        investigation_note_arr_count = investigation_note_arr.length;

        investigation_slip_note = "";
        for(i=0; i<investigation_arr_count; i++)
        {

            investigation_slip_note += investigation_arr[i] +": "+ investigation_note_arr[i]+"@";
        }
         investigation_slip_note = investigation_slip_note.replace(/undefined/g, "no data");
        // console.log(investigation_slip_note);
            
            date = $('#dropdown'+id+'-tab').text();
            
            // console.log(page);
          print_slip(date, pcode_n_name, investigation_slip_note, p_age);
    });
    $(".printPdata").click(function () {
        /*$.post("../includes/ajax/print_patient_info.php?","printp=1&v_id="+$(this).data("vid")+"&p_id="+$(this).data("pid"),
            function(data) {
                console.log(data);
                newWin= window.open("");
                newWin.document.write(data);
                newWin.print();
                newWin.close();
            });*/
            // console.log("vid: "+$(this).data('vid'))
        var form = $('#editprevpatientform-'+$(this).data('vid'));
        var pcode_n_name = $("#pcode_n_name").text();
        var sex = $("#sex").data('sex');
        var procedure_counter = $("#procedure_counter").text();
        var dob = $("#p_age").data('dob');
        var occupation = $("#occupation").text();
        var address = $("#address").text();
        var past_hx = $("#past_hx").val();
        var family_hx = $("#family_hx").val();
        var drug_hx = $("#drug_hx").val();
        var other_hx = $("#other_hx").val();
        // var other_info = $("#other_info").text();

		// prev_marital_st-"+$(this).data('vid')
		// fee-"+$(this).data('vid')
		// followup_date-"+$(this).data('vid')
		// no_of_child-"+$(this).data('vid')
		// prev_vid_"+$(this).data('vid') //this is other info
		// prev_breast_feed_"+$(this).data('vid')
		// prev_pregnant_"+$(this).data('vid')
		
        var prev_visit_date = $("#prev_visit_date-"+$(this).data('vid')).text();
        var pexpect_visit = $("#followup_date-"+$(this).data('vid')).text();
		var pmarit_status = $("#prev_marital_st-"+$(this).data('vid')).text();
		var pfee = $("#fee-"+$(this).data('vid')).text();
		var pchildren = $("#no_of_child-"+$(this).data('vid')).text();
		var other_info = $("#prev_vid_"+$(this).data('vid')).text();
        // var img_set = $('.pimage-'+$(this).data('vid'));
        // img_set = .eq(0).attr('src');
        
        var img_set = [];
        $('.pimage-'+ $(this).data('vid')).each(function(){
            img_set.push(this.src);
        })
        var images = img_set.join(';')
        if(sex===2){
			var prev_breast_feed = $("#prev_breast_feed_"+$(this).data('vid')).text();
            var prev_pregnant = $("#prev_pregnant_"+$(this).data('vid')).text();

        }else{
            var prev_breast_feed = 0; 
            var prev_pregnant = 0;
        }

        /*$.ajax({
            // url: form.attr('action'),
            url: "./includes/ajax/print_patient_info.php",
            type: 'POST',
            data: form.serialize()+"&printp=1&pcode_n_name="+pcode_n_name+"&sex="+sex+"&procedure_counter="+procedure_counter+"&dob="+dob+"&occupation="+occupation+"&address="+address+"&past_hx="+past_hx+"&family_hx="+family_hx+"&drug_hx="+drug_hx+"&other_hx="+other_hx+"&other_info="+other_info+"&prev_breast_feed="+prev_breast_feed+"&prev_pregnant="+prev_pregnant+"&image="+images,
            success: function(data) {
                if (data == -1) {
                    swal({
                        title: "Operation Failed!",
                        text: "The operation failed, please write information correctly when filling the form.",
                        type: "error",
                        confirmButtonColor: "#C9302C"
                    });
                }else {

                }
            }});*/
        // patient = form.serialize()+"&printp=1&pcode_n_name="+pcode_n_name+"&sex="+sex+"&procedure_counter="+procedure_counter+"&dob="+dob+"&occupation="+occupation+"&address="+address+"&past_hx="+past_hx+"&family_hx="+family_hx+"&drug_hx="+drug_hx+"&other_hx="+other_hx+"&other_info="+other_info+"&prev_breast_feed="+prev_breast_feed+"&prev_pregnant="+prev_pregnant+"&image="+images;
        formdata = form.serializeArray();

        formdata.push({name: 'pcode_n_name', value: pcode_n_name},
                        {name: 'sex', value: sex},
                        {name: 'procedure_counter', value: procedure_counter},
                        {name: 'dob', value: dob},
                        {name: 'occupation', value: occupation},
                        {name: 'address', value: address},
                        {name: 'past_hx', value: past_hx},
                        {name: 'family_hx', value: family_hx},
                        {name: 'drug_hx', value: drug_hx},
                        {name: 'other_hx', value: other_hx},
                        {name: 'other_info', value: other_info},
                        {name: 'prev_visit_date', value: prev_visit_date},
                        {name: 'pexpect_visit', value: pexpect_visit},
                        {name: 'pmarit_status', value: pmarit_status},
                        {name: 'pfee', value: pfee},
                        {name: 'pchildren', value: pchildren},
                        {name: 'other_info', value: other_info},
                        {name: 'prev_breast_feed', value: prev_breast_feed},
                        {name: 'prev_pregnant', value: prev_pregnant});
                        // {name: 'image', value: images});
        // var data = jQuery.parseJSON(JSON.stringify( formdata ));
        var data = { };
		$.each(formdata, function() {
		    data[this.name] = this.value;
		});
        // console.table(data);
        // console.table(data[0]['name']);
        // console.log("iMAGES:"); 
        var img_path = [];
        if (img_set.length !== 0) {
            $.each(img_set, function(index, element) {
                img_path.push(element.substr(element.indexOf(".com")+5))
            });   
        }
        // console.log(img_path); 
        // console.log("p data:");
        // var html_page="";
      //   $.each(data, function(index, element) {
      //       console.log(element.name+": "+element.value); 
      //       html_page +=`<div class="col-md-6 col-sm-6 col-xs-6">
						// </div>`
      //       html_page += `<div class="row">
      //                       <div class="col-md-4">
      //                           <label>`;
      //       html_page += element.name;
      //       html_page +=        `: </label> <label id="pcode_n_name">`
      //       html_page += element.value;

      //       html_page +=        `</label>   
      //                       </div>
      //                       <div class="col-md-4">
      //                           <label>sex: </label> <label id="sex" data-sex="1"><div style="text-align:center"><i class="fa fa-mars"><span style="display:none">1</span></i></div></label>
      //                           <label>; Procedures: </label> <label id="procedure_counter">5</label>
      //                       </div>
      //                       <div class="col-md-4">
      //                           <label>age: </label> <label id="p_age" data-dob="1995">23</label>
      //                       </div>
      //                   </div>`
      //   });
        var p_info_window = window.open('p_info_print.html', '_blank');
        p_info_window.addEventListener('load', function() {

			if (data['sex'] === 1) {
                data['sex'] = "Male";
                var element = p_info_window.document.getElementById('basic3');
                element.parentNode.removeChild(element)
            }else if(data['sex'] === 2){
                data['sex'] = "Female";
            p_info_window.document.getElementById('ppregnant').getElementsByTagName('span')[1].innerHTML = data['prev_pregnant'];
            p_info_window.document.getElementById('pbreast_feed').getElementsByTagName('span')[1].innerHTML = data['prev_breast_feed'];
            }else if(data['sex'] === '3'){
                data['sex'] = "Trans";
            }
            diagnosis = data['diagnosis_'+data['prev_v_id']].replace(/^\,+|\,+$/g, '');
            treatments = data['treatment_'+data['prev_v_id']].replace(/^\,+|\,+$/g, '');
            treatment_notes = data['treatment_note_'+data['prev_v_id']];

            surgeries = data['surgery_'+data['prev_v_id']].replace(/^\,+|\,+$/g, '');
            procedures = data['procedure'].replace(/^\,+|\,+$/g, '');
            
            attributes = "";
            if ( data['attr1-'+data['prev_v_id']] !== '0') {
                if (data['attr1-'+data['prev_v_id']]==='Cardiac') {
                    attributes = `<tr>
                                    <td class="col-md-2">Attribute1</td>
                                    <td class="col-md-10">`+data['attr1-'+data['prev_v_id']]+`</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2">Attribute2</td>
                                    <td class="col-md-10">`+data['attr2-'+data['prev_v_id']]+`</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2">Attribute3</td>
                                    <td class="col-md-10">`+data['attr3-'+data['prev_v_id']]+`</td>
                                </tr>`;

                }else if(data['attr1-'+data['prev_v_id']]==='Vascular') {
                    attributes = `<tr>
                                    <td class="col-md-2">Attribute1</td>
                                    <td class="col-md-10">`+data['attr1-'+data['prev_v_id']]+`</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2">Attribute2</td>
                                    <td class="col-md-10">`+data['attr2-'+data['prev_v_id']]+`</td>
                                </tr>`;
                    if (data['attr2-'+data['prev_v_id']]==='Venous Diseases') {
                        attributes += `<tr>
                                    <td class="col-md-2">Attribute3</td>
                                    <td class="col-md-10">`+data['attr3-'+data['prev_v_id']]+`</td>
                                </tr>`;
                    }

                }else if(data['attr1-'+data['prev_v_id']]==='Thoracic') {
                    attributes = `<tr>
                                    <td class="col-md-2">Attribute1</td>
                                    <td class="col-md-10">`+data['attr1-'+data['prev_v_id']]+`</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2">Attribute2</td>
                                    <td class="col-md-10">`+data['attr2-'+data['prev_v_id']]+`</td>
                                </tr>`;
                }
            }

            if (!empty(treatments)) {
                treatment_arr = treatments.split(', ');
            }else{
                treatment_arr = new Array();
            }
            if (!empty(treatment_notes)) {
                treatment_note_arr = treatment_notes.split('\n');
            }else{
                treatment_note_arr = new Array();
            }

            treatment_arr_count = treatment_arr.length;
            treatment_note_arr_count = treatment_note_arr.length;

            treatment_slip_note = "";
            for(i=0; i<treatment_arr_count; i++)
            {

                treatment_slip_note += "<p>"+treatment_arr[i]+": "+treatment_note_arr[i]+"</p>";
            }
            treatment_slip_note = treatment_slip_note.replace(/undefined/g, "no data");

            p_info_window.document.getElementById('pname').getElementsByTagName('span')[1].innerHTML = data['pcode_n_name']+", vid: "+data['prev_v_id'];
            p_info_window.document.getElementById('pdob_n_sex').getElementsByTagName('span')[1].innerHTML = data['dob']+", "+data['sex'];
            p_info_window.document.getElementById('proc_count').getElementsByTagName('span')[1].innerHTML = data['procedure_counter'];
            p_info_window.document.getElementById('pdate').getElementsByTagName('span')[1].innerHTML = data['prev_visit_date'];
            p_info_window.document.getElementById('poccup').getElementsByTagName('span')[1].innerHTML = data['occupation'];
            p_info_window.document.getElementById('paddr').getElementsByTagName('span')[1].innerHTML = data['address'];
            p_info_window.document.getElementById('pexpect_visit').getElementsByTagName('span')[1].innerHTML = data['pexpect_visit'];
            p_info_window.document.getElementById('pmarit_status').getElementsByTagName('span')[1].innerHTML = data['pmarit_status'];
            p_info_window.document.getElementById('pfee').getElementsByTagName('span')[1].innerHTML = data['pfee'];
            p_info_window.document.getElementById('pchildren').getElementsByTagName('span')[1].innerHTML = data['pchildren'];
            p_info_window.document.getElementById('potherinfo').getElementsByTagName('span')[1].innerHTML = data['other_info']; 
            

            // right_col
            // left_col
            content = `<div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        <table class="table table-bordered table-striped table-sm" >
                            <tbody>
                                <tr>
                                    <td class="col-md-2 col-sm-2 col-xs-2">Past Hx</td>
                                    <td class="col-md-10 col-sm-10 col-xs-10">`+data['past_hx']+`</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2 col-sm-2 col-xs-2">Family Hx</td>
                                    <td class="col-md-10 col-sm-10 col-xs-10">`+data['family_hx']+`</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2 col-sm-2 col-xs-2">Drug Hx</td>
                                    <td class="col-md-10 col-sm-10 col-xs-10">`+data['drug_hx']+`</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2 col-sm-2 col-xs-2">Other Hx</td>
                                    <td class="col-md-10 col-sm-10 col-xs-10">`+data['other_hx']+`</td>
                                </tr>`
            content += attributes;
            content += `
                        <tr>
                            <td class="col-md-2">chief complain and present history</td>
                            <td class="col-md-10">`+data['chief_com_n_duration_'+data['prev_v_id']]+`</td>
                        </tr>
                        <tr>
                            <td class="col-md-2">Examination</td>
                            <td class="col-md-10">`+data['examination_'+data['prev_v_id']]+`</td>
                        </tr>
                        <tr>
                            <td class="col-md-2">Investigation</td>
                            <td class="col-md-10">`+data['investigation_'+data['prev_v_id']]+`</td>
                        </tr>
                        <tr>
                            <td class="col-md-2">Investigation Notes</td>
                            <td class="col-md-10">`+data['investigation_note']+`</td>
                        </tr>
                        <tr>
                            <td class="col-md-2">Diagnosis</td>
                            <td class="col-md-10">`+diagnosis+`</td>
                        </tr>
                        <tr>
                            <td class="col-md-2">treatments</td>
                            <td class="col-md-10">`+treatment_slip_note+`</td>
                        </tr>
                        
                        <tr>
                            <td class="col-md-2">Surgeries</td>
                            <td class="col-md-10">`+surgeries+`</td>
                        </tr>
                        <tr>
                            <td class="col-md-2">Surgery Date</td>
                            <td class="col-md-10">`+data['surgery_date_'+data['prev_v_id']]+`</td>
                        </tr>
                        <tr>
                            <td class="col-md-2">Surgery Notes</td>
                            <td class="col-md-10">`+data['surgery_note']+`</td>
                        </tr>
                        <tr>
                            <td class="col-md-2">Procedures</td>
                            <td class="col-md-10">`+procedures+`</td>
                        </tr>
                        <tr>
                            <td class="col-md-2">Procedure Date</td>
                            <td class="col-md-10">`+data['procedure_date_'+data['prev_v_id']]+`</td>
                        </tr>
                        <tr>
                            <td class="col-md-2">Procedure Note</td>
                            <td class="col-md-10">`+data['procedure_note_'+data['prev_v_id']]+`</td>
                        </tr>
                        <tr>
                            <td class="col-md-2">Note</td>
                            <td class="col-md-10">`+data['note_'+data['prev_v_id']]+`</td>
                        </tr>

                        `
            content +=`</tbody>
                        </table>
                        </div>
                        </div>
                        `;
            content += `<div class="row">`;
            var i; var m="";
            for (i = 0; i < img_path.length; ++i) {
                m += `<div class="col-md-6 col-sm-6 col-xs-6">`;
                m += "<img class='pimage' style='border: 1px black solid; margin-top: 10px; margin-bottom: 10px;' width='500' height='auto' src='"+img_path[i]+"'>";
                m += `</div>`;
            }

            content += m;
            content += `</div>`;
            p_info_window.document.getElementById('treatments').innerHTML = content;
        });
    });
    $(document).on("change", ".attr1Class", function (e) {
        attr1_option = $(this).find('option:selected');
        option_id = parseInt(attr1_option.attr('id'));
        // prev_v = $(this).data('visits')>1?true:false;
        // console.log(prev_v);
        prev_v_id = $(this).data('visits')>1?"-"+$(this).data("vid"):"";

        if (option_id>1) {
            /*$.getJSON or $.post is a shorthand Ajax function but speciallized for JSON parsed data */
            $.post("../includes/ajax/attributes_list.php?","attr1=1&id="+option_id, function(data) {
            $("#attr2"+prev_v_id+" option").remove();
            $("#attr3"+prev_v_id+" option").remove();
            /*
              $.each(data, function(index, value){
              console.log(index);
              console.log(value);
              this wata value awja la value ka arrayaki dikaya ama dalein .value w .name
            */
            $.each(data, function(){
              
              $("#attr2"+prev_v_id).append('<option value="'+ this.value +'">'+ this.name +'</option>')
            });
            // agar data-visits=1 bu awja slide down w up bka agar na dabi blei #attr2list+vid . slidedown...
            $("#attr2list"+prev_v_id).slideDown('slow');
            $("#attr3list"+prev_v_id).slideUp('slow');
          }, "json");
            
        }else if(option_id==1){
          $.post("../includes/ajax/attributes_list.php?","attr1=1&id="+option_id, function(data) {
            $("#attr2"+prev_v_id+" option").remove();
            $("#attr3"+prev_v_id+" option").remove();
            data2 = data['data2'];
            data3 = data['data3'];
            $.each(data2, function(){
              
              $("#attr2"+prev_v_id).append('<option value="'+ this.value +'">'+ this.name +'</option>')
            });

            $.each(data3, function(){
              
              $("#attr3"+prev_v_id).append('<option value="'+ this.value +'">'+ this.name +'</option>')
            });
            $("#attr2list"+prev_v_id).slideDown('slow');
            $("#attr3list"+prev_v_id).slideDown('slow');
          }, "json");
            
        } else{
            $("#attr2list"+prev_v_id).slideUp('slow');
            $("#attr3list"+prev_v_id).slideUp('slow');
            $("#attr2"+prev_v_id+" option").remove();
            $("#attr3"+prev_v_id+" option").remove();
        }
    });
    
    $(document).on("change", ".attr2Class", function (e) {
        /*
          three cases:
          Pediatric
          Venous Diseases
          Adult
        */
        attr2_option = $(this).find('option:selected');
        // prev_v = $(this).data('visits')>1?true:false;
        // console.log(prev_v);
        prev_v_id = $(this).data('visits')>1?"-"+$(this).data("vid"):"";

        if (attr2_option.attr('value')==='Pediatric' || attr2_option.attr('value')==='Venous Diseases' || attr2_option.attr('value')==='Adult') {
          /*$.getJSON or $.post is a shorthand Ajax function but speciallized for JSON parsed data */
          $.post("../includes/ajax/attributes_list.php?","attr2=1&id="+attr2_option.attr('value'), function(data) {
          $("#attr3"+prev_v_id+" option").remove();
          $.each(data, function(){
            
            $("#attr3"+prev_v_id).append('<option value="'+ this.value +'">'+ this.name +'</option>')
          });
          
          $("#attr3list"+prev_v_id).slideDown('slow');
        }, "json");
      }else{
        $("#attr3list"+prev_v_id).slideUp('slow');
        $("#attr3"+prev_v_id+" option").remove();
      }
            
    });
});// end of document.ready

function print_slip(thedate, code_name, treats, p_age) {
    newWin= window.open('racheta2.php?today='+thedate+'&code_name='+code_name+'&treats='+treats+'&age='+p_age);

        setTimeout(function () { newWin.print(); }, 500);
        // newWin.onfocus = function () { setTimeout(function () { newWin.close(); }, 500); }
          // newWin.print();
          // newWin.close();
          
          // OpenWindow();
}
function checkfiles(input_id) { //same in patient_more_info

    console.log(input_id);
    //check whether browser fully supports all File API
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
        var flag_type = true;
        var flag_size = true;

        //get the file size and file type from file input field
        for (var i = $('#'+input_id)[0].files.length - 1; i >= 0; i--) {
            // $('#'+input_id)[0].files[i];
            var fsize = $('#'+input_id)[0].files[i].size;
            var ftype = $('#'+input_id)[0].files[i].type;
            var fname = $('#'+input_id)[0].files[i].name;
           console.log(ftype);
           switch(ftype)
            {
                case 'image/png':
                case 'image/gif':
                case 'image/jpeg':
                case 'image/pjpeg':
                case 'video/m4v':
                case 'video/avi':
                case 'video/mp4':
                case 'video/mov':
                case 'video/mpg':
                case 'video/mpeg':
                case 'video/quicktime':
                case 'video/x-ms-wmv':
                    // alert("Acceptable image file!");
                    break;
                default:
                    // alert('Unsupported File!');
                    flag_type = false;
                    break;
            }
            //check file size
            // if(fsize>5242880) //do something if file size more than 5 mb (5242880)
            if(fsize>10485760) // 10mb
            {
                flag_size = false;
            }

            /*if (flag_type && flag_size) {
                return true;
            }else*/ if(!flag_type){
                swal({
                    title: "One or Multiple Unsupported File Type! " + ftype,
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                return false;
            }else if(!flag_size){
                swal({
                    title: fsize +" \nOne or Multiple Files Exceeding Size Limit of 10 MB!",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                return false;
            }
            // if function is not returned until now, then variables should still be TRUE;
            // var flag_type = true;
            // var flag_size = true;
        } //end for loop
        return flag_type && flag_size;
    }else{
        swal({
                title: "Can't Use Upload",
                text: "Please upgrade your browser, because your current browser lacks file upload and some other new features we need!",
                type: "error",
                confirmButtonColor: "#C9302C"
            });
        return false;
    }
}
</script>
<?php 

} 

?>