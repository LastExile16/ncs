<?php
	require_once '../connection.php';
	require_once '../functions.php';

	if (isset($_POST['treat_n_diagn'])) 
	{
		$d_desc = array();
		$t_desc = array();
		$s_desc = array();
		$p_desc = array();

		$query = "SELECT DISTINCT(diagnose) FROM visit_diagnose WHERE view=1";
		$diagn_set = mysql_query($query) or die(-1);
		while ($diagn = mysql_fetch_assoc($diagn_set)) {
			$d_desc[] = array('value'=>$diagn['diagnose']);
		}
		$list[] = $d_desc;//array( array('value' => 'red'), array('value' => 'red1'), array('value' => 'red2') );

		$query = "SELECT DISTINCT(treatment) FROM visit_treatment WHERE treatment != '' AND view=1";
		$treat_set = mysql_query($query) or die(-1);
		while ($treat = mysql_fetch_assoc($treat_set)) {
			$t_desc[] = array('value'=>$treat['treatment']);
		}
		$list[] = $t_desc;
		
		$query = "SELECT DISTINCT(surgery) FROM visit_surgery WHERE surgery != '' AND view=1";
		$surg_set = mysql_query($query) or die(-1);
		while ($surg = mysql_fetch_assoc($surg_set)) {
			$s_desc[] = array('value'=>$surg['surgery']);
		}
		$list[] = $s_desc;

		$query = "SELECT DISTINCT(`procedure`) FROM visit WHERE `procedure` != '' AND view=1";
		$prd_set = mysql_query($query) or die(-1);
		while ($prd = mysql_fetch_assoc($prd_set)) {
			$p_desc[] = array('value'=>$prd['procedure']);
		}
		$list[] = $p_desc;

		echo json_encode($list);
	}
	
?>