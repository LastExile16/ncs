<?php 
require_once '../connection.php';
// require_once '../functions.php';
// why importing function.php for just one function that i need, right?
function safe($value)
{   
    $magic_quotes_active = get_magic_quotes_gpc();
    $new_enough_php = function_exists("mysql_real_escape_string"); //i.e. PHP >=v4.3.0
    if($new_enough_php){//PHP v4.3.0 or higher
        //undo any magic quote effects so mysql_real_escape_string can do the work 
        if($magic_quotes_active){ $value = stripcslashes($value); }
        $value = mysql_real_escape_string($value);
    }else{//before PHP v4.3.0
    //if magic quotes aren't already on then add slashes manually
    if(!$magic_quotes_active){ $value = addslashes($value); }
    //if magic quotes are active, then the slashes already exist
    }
    
    $value=htmlentities($value, ENT_QUOTES, "UTF-8");
    //$value=strip_tags($value, '<a><b>');
    return $value;
}
if (isset($_POST["printp"]) && is_numeric($_POST["printp"])) {
    if ($_POST["printp"]==1) {
                // echo "<h1>Hello there</h1>";
                // echo "<pre>";
        		// print_r($_POST);
        		// echo "</pre>";
                /*
        hawre jawad info
        Array
        (
            [prev_v_id] => 393
            [prev_p_id_f] => 98
            [attr1-393] => 0
            [chief_com_n_duration_393] => chief complain
            [examination_393] => ex1
            [investigation_393] => inv
            [investigation_note] => some notes inv
            [diagnosis_393] => dig1,
            [treatment_393] => treat2, new treatment
            [treatment_note_393] => new treats 
            [surgery_393] => surg2, surg3
            [surgery_date_393] => 2018-11-12
            [surgery_note] => surg notes
            [procedure] => procedure test, special procedure 1
            [procedure_date_393] => 2018-10-15
            [procedure_note_393] => some proc notes
            [note_393] => 
            [printp] => 1

            [pcode_n_name] => 98, Hawre Jawad
            [sex] => 1
            [procedure_counter] => 5
            [dob] => 1995
            [occupation] => Student
            [address] => Choman
            [past_hx] => past hx
            [family_hx] => family hx
            [drug_hx] => drug hx
            [other_hx] => other hx
            [other_info] => 
            [prev_breast_feed] => 0
            [prev_pregnant] => 0
            [image] => http://hemnv2018.nawrascs.com/uploads/98/393/201807056250501.png;http://hemnv2018.nawrascs.com/uploads/98/393/2018102916541801.jpg
        )		*/

        //patient info
        $pcode_n_name = 0;
        if(!empty($_POST['pcode_n_name'])) {
            $pcode_n_name = safe(trim($_POST['pcode_n_name']));
        }
        $procedure_counter = 0;
        if(!empty($_POST['procedure_counter'])) {
            $procedure_counter = safe(trim($_POST['procedure_counter']));
        }
        $dob = 0;
        if(!empty($_POST['dob'])) {
            $dob = safe(trim($_POST['dob']));
        }
        $occupation = 0;
        if(!empty($_POST['occupation'])) {
            $occupation = safe(trim($_POST['occupation']));
        }
        $address = 0;
        if(!empty($_POST['address'])) {
            $address = safe(trim($_POST['address']));
        }
        $past_hx = 0;
        if(!empty($_POST['past_hx'])) {
            $past_hx = safe(trim($_POST['past_hx']));
        }
        $family_hx = 0;
        if(!empty($_POST['family_hx'])) {
            $family_hx = safe(trim($_POST['family_hx']));
        }
        $drug_hx = 0;
        if(!empty($_POST['drug_hx'])) {
            $drug_hx = safe(trim($_POST['drug_hx']));
        }
        $other_hx = 0;
        if(!empty($_POST['other_hx'])) {
            $other_hx = safe(trim($_POST['other_hx']));
        }
        $other_info = 0;
        if(!empty($_POST['other_info'])) {
            $other_info = safe(trim($_POST['other_info']));
        }

        $sex = 0;
        $prev_breast_feed = 0;
        $prev_pregnant = 0;
        if(!empty($_POST['sex'])) {
            $sex = safe(trim($_POST['sex']));
            if ($sex == 2) {
                if(!empty($_POST['prev_breast_feed'])) {
                    $prev_breast_feed = safe(trim($_POST['prev_breast_feed']));
                }
                if(!empty($_POST['prev_pregnant'])) {
                    $prev_pregnant = safe(trim($_POST['prev_pregnant']));
                }
            }
        }

        // visit info
        $img_path = array();
        if(!empty($_POST['image'])) {
            $image = explode(';', $_POST['image']);
            $pattern = '.com/';
            foreach ($image as $imgurl) {
                $img_path[] = substr($imgurl, strpos($imgurl, $pattern )+5);
            }
            // print_r($img_path);
        }
        $prev_v_id = 0;
        if(!empty($_POST['prev_v_id'])) {
            $prev_v_id = safe(trim($_POST['prev_v_id']));
        }
        $prev_p_id_f = 0;
        if(!empty($_POST['prev_p_id_f'])) {
            $prev_p_id_f = safe(trim($_POST['prev_p_id_f']));
        }
        $attr1 = 0;
        if(!empty($_POST['attr1'])) {
            $attr1 = safe(trim($_POST['attr1']));
        }
        $chief_com_n_duration = 0;
        if(!empty($_POST['chief_com_n_duration_{$prev_v_id}'])) {
            $chief_com_n_duration = safe(trim($_POST['chief_com_n_duration_{$prev_v_id}']));
        }
        $examination = 0;
        if(!empty($_POST['examination_{$prev_v_id}'])) {
            $examination = safe(trim($_POST['examination_{$prev_v_id}']));
        }
        $investigation = 0;
        if(!empty($_POST['investigation_{$prev_v_id}'])) {
            $investigation = safe(trim($_POST['investigation_{$prev_v_id}']));
        }
        $investigation_note = 0;
        if(!empty($_POST['investigation_note'])) {
            $investigation_note = safe(trim($_POST['investigation_note']));
        }
        $diagnosis = 0;
        if(!empty($_POST['diagnosis_{$prev_v_id}'])) {
            $diagnosis = safe(trim($_POST['diagnosis_{$prev_v_id}']));
        }
        $treatment = 0;
        if(!empty($_POST['treatment_{$prev_v_id}'])) {
            $treatment = safe(trim($_POST['treatment_{$prev_v_id}']));
        }
        $treatment_note = 0;
        if(!empty($_POST['treatment_note_{$prev_v_id}'])) {
            $treatment_note = safe(trim($_POST['treatment_note_{$prev_v_id}']));
        }
        $surgery = 0;
        if(!empty($_POST['surgery_{$prev_v_id}'])) {
            $surgery = safe(trim($_POST['surgery_{$prev_v_id}']));
        }
        $surgery_date = 0;
        if(!empty($_POST['surgery_date_{$prev_v_id}'])) {
            $surgery_date = safe(trim($_POST['surgery_date_{$prev_v_id}']));
        }
        $surgery_note = 0;
        if(!empty($_POST['surgery_note'])) {
            $surgery_note = safe(trim($_POST['surgery_note']));
        }
        $procedure = 0;
        if(!empty($_POST['procedure'])) {
            $procedure = safe(trim($_POST['procedure']));
        }
        $procedure_date = 0;
        if(!empty($_POST['procedure_date_{$prev_v_id}'])) {
            $procedure_date = safe(trim($_POST['procedure_date_{$prev_v_id}']));
        }
        $procedure_note = 0;
        if(!empty($_POST['procedure_note_{$prev_v_id}'])) {
            $procedure_note = safe(trim($_POST['procedure_note_{$prev_v_id}']));
        }
        $note = 0;
        if(!empty($_POST['note_{$prev_v_id}'])) {
            $note = safe(trim($_POST['note_{$prev_v_id}']));
        }
   	}
}

?>