<?php
// there is a warning on strtotime function asking to set a default timezone
 date_default_timezone_set("Asia/Baghdad");
 if( isset($_POST['type']) && ($_POST['type'] == "load")) {
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Easy set variables
     */
     
    // DB table to use
    $table = 'patient';
    /*$table = <<<EOT
     (
        SELECT 
          a.id, 
          a.name, 
          a.father_id, 
          b.name AS father_name
        FROM table patient AS P
        LEFT JOIN table visit ON a. = b.id
     ) temp
    EOT;
*/
     
    // Table's primary key
    $primaryKey = 'p_id';
     
    // Array of database columns which should be read and sent back to DataTables.
    // The `db` parameter represents the column name in the database, while the `dt`
    // parameter represents the DataTables column identifier. In this case simple
    // indexes
    $columns = array(
        array( 'db' => 'p_id',        'dt' => 0 ),
        array( 'db' => 'fullname',    'dt' => 1,
                'formatter' => function( $d, $row ) {
                    return $row[10]>=1?"<span class='inqueue' data-queue='{$row[10]}'>{$d}</span>":$d;
                }),
        array( 'db' => 'sex',         'dt' => 2,
                'formatter' => function($d, $row)
                {
                    if ($d=='1') {
                         // $('td', row).eq(5).addClass('highlight');
                        return "<div style='text-align:center'><i class='fa fa-mars'></i></div>"; 
                    }else if ($d=='2') {
                        return "<div style='text-align:center'><i class='fa fa-venus'></i></div>";    
                    }else if ($d=='3') {
                        return "<div style='text-align:center'><i class='fa fa-venus-mars'></i></div>";   
                    }
                } 
            ),
        array( 'db' => 'address',     'dt' => 3 ),
        array( 'db' => 'dob',         'dt' => 4 ),
        array( 'db' => 'phone',       'dt' => 5 ),
        array( 'db' => 'occupation',  'dt' => 6 ),
        array(  'db'=> 'latest_visit', 'dt'=> 7,
                'formatter' => function( $d, $row ) {
                    return $d!="" && $d!="0000-00-00"?date( 'Y-m-d', strtotime($d)):"";
                }
            ),
        array( 'db' => 'surgeries',      'dt' => 8 ),
        array( 'db' => 'procedures_list',      'dt' => 9 ),
        array( 'db' => 'queue_status',      'dt' => 10 ),
        array( 'db' => 'u_id_f',      'dt' => 11 ),
        array( 'db' => 'queue_status',      'dt' => 12 )
    );
     
    // mySQL connection information
    // needs PDO extension extension=php_pdo_mysql.dll
    require_once '../variables_c.php';
     
     
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP
     * server-side, there is no need to edit below this line.
     */
     
    require( '../../datatables/ssp.class.php' );
    
    $condition="view=1";
    if (isset($_POST['minDate']) && !empty($_POST['minDate']) && 
        isset($_POST['maxDate']) && !empty($_POST['maxDate'])) 
    {
        $condition = "(latest_visit between '{$_POST['minDate']}' AND '{$_POST['maxDate']}') AND view=1";
    }
    elseif (isset($_POST['minDate']) && !empty($_POST['minDate'])) {
        $condition = "latest_visit >= '{$_POST['minDate']}' AND view=1";
    }
    elseif (isset($_POST['maxDate']) && !empty($_POST['maxDate'])) {
        $condition = "latest_visit <= '{$_POST['maxDate']}' AND view=1";
    }
    echo json_encode(
        SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, null, $condition )
    );

}
?>