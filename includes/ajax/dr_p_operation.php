<?php
	require_once '../connection.php';
	require_once '../functions.php';

	if (isset($_POST['update_hx'])) 
	{
		if(!isset($_SESSION))
		{
		    session_start();
		}
		// print_r($_POST);
		/*Array
		(
		    [p_id] => 
		    [past_hx] => 
		    [family_hx] => 
		    [drug_hx] => 
		    [other_hx] => 
		)*/
		$form_filled_correctly = true;
		$error_loc = "";
		// p_id
		if (isset($_POST['p_id']) && !empty($_POST['p_id'])) {
			$p_id = safe(trim($_POST['p_id']));
		}else{
			$form_filled_correctly = false;
			$error_loc = "p_id";
		}
		// past_hx
		if (isset($_POST['past_hx']) /*&& !empty($_POST['past_hx'])*/) {
			$past_hx = safe(trim($_POST['past_hx']));
		}else{
			$form_filled_correctly = false;
			$error_loc = "past_hx";
		}
		// family_hx
		if (isset($_POST['family_hx']) /*&& !empty($_POST['family_hx'])*/) {
			$family_hx = safe(trim($_POST['family_hx']));
		}else{
			$form_filled_correctly = false;
			$error_loc = "family_hx";
		}
		// drug_hx
		if (isset($_POST['drug_hx']) /*&& !empty($_POST['drug_hx'])*/) {
			$drug_hx = safe(trim($_POST['drug_hx']));
		}else{
			$form_filled_correctly = false;
			$error_loc = "drug_hx";
		}
		// other_hx
		if (isset($_POST['other_hx']) /*&& !empty($_POST['other_hx'])*/) {
			$other_hx = safe(trim($_POST['other_hx']));
		}else{
			$form_filled_correctly = false;
			$error_loc = "other_hx";
		}

		//check the conclusion of the above validations
		if ($form_filled_correctly) {
			$query = "UPDATE patient SET
						past_hx = '{$past_hx}',
						family_hx = '{$family_hx}',
						drug_hx = '{$drug_hx}',
						other_hx = '{$other_hx}',
						u_id_f = {$_SESSION['user_id']}
					WHERE p_id = {$p_id}  AND view = 1";
			
			// echo $query;
			mysql_query($query) or die("-1");
		}else{
			echo "-1";
			// echo $error_loc;
		}
	}

	elseif (isset($_POST['update_fee_data'])) {

		if(!isset($_SESSION))
		{
		    session_start();
		}


		$form_filled_correctly = true;
		$error_loc = "";
		
		// vid
		if (isset($_POST['vid']) && is_numeric($_POST['vid'])) {
			$vid = safe(trim($_POST['vid']));
		}else{
			$form_filled_correctly = false;
			$error_loc = "vid";
		}
		// pid
		if (isset($_POST['pid']) && is_numeric($_POST['pid'])) {
			$pid = safe(trim($_POST['pid']));
		}else{
			$form_filled_correctly = false;
			$error_loc = "pid";
		}
		// new_fee
		if (isset($_POST['new_fee']) && is_numeric($_POST['new_fee'])) {
			$new_fee = safe(trim($_POST['new_fee']));
		}else{
			$form_filled_correctly = false;
			$error_loc = "new_fee";
		}

		//check the conclusion of the above validations
		if ($form_filled_correctly) {
			$query = "UPDATE visit SET
						fee = {$new_fee},
						u_id_f = {$_SESSION['user_id']}
					WHERE v_id = {$vid} 
						AND p_id_f = {$pid} 
						AND in_queue = 2
						AND view = 1";
			
			// echo $query;
			mysql_query($query) or die("-1");
		}else{
			echo "-1";
			// echo $error_loc;
		}
	}
	
	elseif (isset($_POST['edit_prev_hx'])) 
	{
		//update prev_visit_data
		/*
		new_data
		prev_v_id
		prev_name
		edit_prev_hx
		*/
		if(!isset($_SESSION))
		{
		    session_start();
		}
		$form_filled_correctly = true;
		$error_loc = "";
		// prev_v_id
		if (isset($_POST['prev_v_id']) && is_numeric($_POST['prev_v_id'])) {
			$prev_v_id = safe(trim($_POST['prev_v_id']));
		}else{
			$form_filled_correctly = false;
			$error_loc = "prev_v_id";
		}

		// prev_name
		if (isset($_POST['prev_name']) && is_numeric($_POST['prev_name'])) {
			if ($_POST['prev_name']>0 && $_POST['prev_name']<20) {
				$prev_name = safe(trim($_POST['prev_name']));
			}
		}else{
			$form_filled_correctly = false;
			$error_loc = "prev_name";
		}
		// echo var_dump($prev_name); echo " yes ";
		// var_dump(is_numeric($_POST['prev_name']));
		// new_data
		if (isset($_POST['new_data'])) {
			$new_data = safe(trim($_POST['new_data']));
		}else{
			$new_data = "";
		}

		// surg_date
		if (isset($_POST['surg_date'])) {
			$surg_date = safe(trim($_POST['surg_date']));
		}else{
			$surg_date = "0000-00-00";
		}

		// surg_note
		if (isset($_POST['surg_note'])) {
			$surg_note = safe(trim($_POST['surg_note']));
		}else{
			$surg_note = "";
		}

		// prd_date
		if (isset($_POST['prd_date'])) {
			$prd_date = safe(trim($_POST['prd_date']));
		}else{
			$prd_date = "0000-00-00";
		}
		
		// patient_id
		if (isset($_POST['pid'])) {
			$pid = safe(trim($_POST['pid']));
		}else{
			$pid = "";
		}

		if ($form_filled_correctly) {
			//we need this condition to update surgery or procedures data in patient table according to data in all visits up to now.
			if ($prev_name == 7 || $prev_name == 18) {
				$v_id_array = array();
				$surgeries_array = array();
				$vid_set = get_prev_visits_list($pid);
				while ($visits = mysql_fetch_array($vid_set)) {
					$v_id_array[] = $visits['v_id'];
				}
				$v_ids = implode(', ', array_filter($v_id_array));
			}
			if ($prev_name<10) {

				$update_procedures_list = false;

				$query = "UPDATE visit SET ";
				switch ($prev_name) {
					case '1':
						$query .= " chief_comp_n_dur = '{$new_data}' ";
						break;
					case '2':
						$query .= " examination = '{$new_data}' ";
						break;
					case '3':
						$query .= " investigation = '{$new_data}' ";
						break;
					case '4':
						$query .= " treatment_note = '{$new_data}' ";
						break;
					case '5':
						$query .= " note = '{$new_data}' ";
						break;
					case '6':
						$query .= " investigation_note = '{$new_data}' ";
						break;
					case '7':
						$query .= " `procedure` = '{$new_data}' ";
						$query .= ", `procedure_date` = '{$prd_date}' ";
						$update_procedures_list = true;
						break;
					case '8':
						$query .= " procedure_note = '{$new_data}' ";
						break;
					case '9':
						$query .= " procedure_date = '{$new_data}' ";
						break;
				}

				$query .= " WHERE v_id = {$prev_v_id} AND view = 1 LIMIT 1";

				// echo $query;
			// var_dump($update_procedures_list);
				mysql_query($query) or die("-1");
				if ($update_procedures_list == true) {
					//UPDATE the procedurelist in patient table
					$all_proc_list_set = mysql_query("SELECT `procedure` FROM visit WHERE v_id IN ({$v_ids})") or die(-1);
					while ($proc = mysql_fetch_array($all_proc_list_set)) {
						$proc_array[] = $proc['procedure'];
					}
					// print_r($proc_array);
					$new_proc_list = implode(', ', array_unique(array_filter($proc_array)));

					$pquery = "UPDATE patient set procedures_list = '{$new_proc_list}' WHERE p_id = $pid";
					mysql_query($pquery) or die("-1");
				}
			}elseif($prev_name==16) {
				// $delete_old_data_query = "UPDATE visit_diagnose SET view = -1 WHERE v_id_f = {$prev_v_id}";
				$delete_old_data_query = "DELETE FROM visit_diagnose WHERE v_id_f = {$prev_v_id}";
				mysql_query($delete_old_data_query) or die("-1");
				//array_filter if it was empty then exploded array also will be empty
				if (!empty($new_data)) {
					$diagnosis_arr = explode(', ', $new_data);
					$dquery = "INSERT INTO visit_diagnose(v_id_f, diagnose) VALUES ";
					foreach ($diagnosis_arr as $value) {
						$dig_lists[] = " ({$prev_v_id}, '{$value}')";
					}
					$dquery .= implode(',', $dig_lists);

					// echo $dquery;
					mysql_query($dquery) or die("-1");
				}
			
			}elseif($prev_name==17) {
				// $delete_old_data_query = "UPDATE visit_treatment SET view = -1 WHERE v_id_f = {$prev_v_id}";
				$delete_old_data_query = "DELETE FROM visit_treatment WHERE v_id_f = {$prev_v_id}";
				mysql_query($delete_old_data_query) or die("-1");

				if (!empty($new_data)) {
					$treatment_arr = explode(', ', $new_data);
					$tquery = "INSERT INTO visit_treatment(v_id_f, treatment) VALUES ";
					foreach ($treatment_arr as $value) {
						$treat_lists[] = " ({$prev_v_id}, '{$value}')";
					}
					$tquery .= implode(',', $treat_lists);
					// echo $tquery;
					mysql_query($tquery) or die("-1");
				}
			}elseif($prev_name==18) {
				// $surg_date
				// $surg_note
				// $pid
				// $new_data
				// $delete_old_data_query = "UPDATE visit_surgery SET view = -1 WHERE v_id_f = {$prev_v_id}";
				$delete_old_data_query = "DELETE FROM visit_surgery WHERE v_id_f = {$prev_v_id}";
				mysql_query($delete_old_data_query) or die("-1");

				if (!empty($new_data)) {
					$surgery_arr = explode(', ', $new_data);
					$squery = "INSERT INTO visit_surgery(v_id_f, surgery, surgery_date, surgery_note) VALUES ";
					foreach ($surgery_arr as $value) {
						$surg_lists[] = " ({$prev_v_id}, '{$value}', '{$surg_date}', '{$surg_note}')";
					}
					$squery .= implode(',', $surg_lists);
					mysql_query($squery) or die("-1");
				}
				// echo $squery;

				/*
				//update patient table to newest data
				$old_surg = "";
				$find_old_surg = "SELECT surgeries FROM patient WHERE p_id={$pid} LIMIT 1";
				$old_surg_set = mysql_query($find_old_surg) or die("-1");
				if (mysql_num_rows($old_surg_set)>0) {
					$old_surg_set = mysql_fetch_array($old_surg_set);
					$old_surg = $old_surg_set[0];
				}

				// i should union old with new_data somehow
				// I did the union without duplicate but there is a problem with removing surgery, I can't detect which surgery is to be deleted from the surgeries column!!! so it's better to update all of it from original table
					if (!empty($old_surg)) {
						$old_surg_arr = explode(', ', $old_surg);
						$curr_surg_arr = array_filter(explode(', ', $new_data));
						// $new_surgs = $old_surg_arr + $curr_surg_arr;
						$new_surgs = array_unique(array_merge($old_surg_arr, $curr_surg_arr));
						// print_r($old_surg_arr);
						// print_r($curr_surg_arr);
					}
					else{

						$new_surgs = array_filter(explode(', ', $new_data));
					}
					if (count($new_surgs)>1) {
						$new_surgs_data = implode(', ', array_filter(array_filter)($new_surgs));
					}else{
						$new_surgs_data = $new_surgs[0];
					}
					$patquery = "UPDATE patient SET surgeries = '{$new_surgs_data}' WHERE p_id = {$pid} LIMIT 1";
				
				// $patquery = "UPDATE patient SET surgeries = '{$new_data}' WHERE p_id = {$pid} LIMIT 1";
				mysql_query($patquery) or die("-1");*/

				//update all from original table
				
				$surgery_query = "SELECT surgery FROM visit_surgery WHERE v_id_f IN ({$v_ids}) ";
				$surgeries_set = mysql_query($surgery_query) or die("-1 s");
				while ($surgeries = mysql_fetch_array($surgeries_set)) {
					$surgeries_array[] = $surgeries['surgery'];
				}
				// print_r($surgeries_array);
				//surgery unique nakam, chunka ma3qul nia naxoshek dujar haman surgery lo bkre, agar shti wa habu awa bashtra dyarbi w kashf bi
				$new_surgeries_list = implode(', ', array_filter($surgeries_array));
				$patquery = "UPDATE patient SET surgeries = '{$new_surgeries_list}' WHERE p_id = {$pid} LIMIT 1";
				mysql_query($patquery) or die("-1");

			}elseif($prev_name==19) {

				$attr1 = null;
				$attr2 = null;
				$attr3 = null;
				if (isset($_POST['attr1']) &&  !empty($_POST['attr1'])) {
					$attr1 = safe(trim($_POST['attr1']));
				}
				if (isset($_POST['attr2']) &&  !empty($_POST['attr2'])) {
					$attr2 = safe(trim($_POST['attr2']));
				}
				if (isset($_POST['attr3']) &&  !empty($_POST['attr3'])) {
					$attr3 = safe(trim($_POST['attr3']));
				}
				
				$queryDel = "DELETE FROM visit_attr WHERE v_id_f = {$prev_v_id} LIMIT 3";
				mysql_query($queryDel) or die("-1");
				if (!empty($attr1)) {
					$query2 = "INSERT INTO visit_attr(v_id_f, attr, attr_order) VALUES ({$prev_v_id}, '{$attr1}', 1)";
					$query2 .= !empty($attr2)?", ({$prev_v_id}, '{$attr2}', 2)":"";
					$query2 .= !empty($attr3)?", ({$prev_v_id}, '{$attr3}', 3)":"";
					// echo $query2;
					mysql_query($query2) or die("-1");
				}
			}

			
		}else{
			echo "-1";
			// echo $error_loc;
		}
	}
	//followup date update
	elseif (isset($_POST['edit_prev_date'])) {
		//update prev_visit_data
		/*
		new_data
		prev_v_id
		edit_prev_fll_date
		*/
		if(!isset($_SESSION))
		{
		    session_start();
		}
		$form_filled_correctly = true;
		$error_loc = "";
		// prev_v_id
		if (isset($_POST['prev_v_id']) && is_numeric($_POST['prev_v_id'])) {
			$prev_v_id = safe(trim($_POST['prev_v_id']));
		}else{
			$form_filled_correctly = false;
			$error_loc = "prev_v_id";
		}
		
		// prev_p_id
		if (isset($_POST['prev_p_id']) && is_numeric($_POST['prev_p_id'])) {
			$prev_p_id = safe(trim($_POST['prev_p_id']));
		}else{
			$form_filled_correctly = false;
			$error_loc = "prev_p_id";
		}

		// new_data
		if (isset($_POST['new_data'])) {
			$new_data = safe(trim($_POST['new_data']));
		}else{
			$new_data = "";
		}

		// inputtype
		if (isset($_POST['inputtype']) && is_numeric($_POST['inputtype'])) {
			$inputtype = safe(trim($_POST['inputtype']));
		}else{
			$inputtype = "";
			$form_filled_correctly = false;
		}

		if ($form_filled_correctly) {

			if ($inputtype == 1) {
				$query = "UPDATE visit_surgery SET 
						surgery_date = '{$new_data}'
						WHERE v_id_f = {$prev_v_id} AND view = 1 LIMIT 1";
				// echo $query;
				mysql_query($query) or die("-1");
			}elseif($inputtype == 2){
				$query = "UPDATE visit SET 
						followup_date = '{$new_data}'
						WHERE v_id = {$prev_v_id} AND view = 1 LIMIT 1";
				
				// echo $query;
				mysql_query($query) or die("-1");
				$pquery = "UPDATE patient SET 
					latest_followup_visit = '{$new_data}'
					WHERE p_id = {$prev_p_id} AND view = 1 LIMIT 1";
				mysql_query($pquery) or die("-1");
			}elseif($inputtype == 3){
				$query = "UPDATE visit SET 
						procedure_date = '{$new_data}'
						WHERE v_id = {$prev_v_id} AND view = 1 LIMIT 1";
				
				// echo $query;
				mysql_query($query) or die("-1");
			}
		}else{
			echo "-1";
			// echo $error_loc;
		}
	}
	//prev attachment date
	elseif(isset($_FILES) && isset($_POST['prev_edit_att'])) {

		if(!isset($_SESSION))
		{
		    session_start();
		}
		// print_r($_POST);
		// print_r($_FILES);
		if (isset($_FILES) && count($_FILES)>0) {

			$form_filled_correctly = true;
			$error_loc = "";
			// prev_v_id
			if (isset($_POST['prev_v_id']) && is_numeric($_POST['prev_v_id'])) {
				$prev_v_id = safe(trim($_POST['prev_v_id']));
			}else{
				$form_filled_correctly = false;
				$error_loc = "prev_v_id";
			}

			// prev_p_id_f
			if (isset($_POST['prev_p_id_f']) && is_numeric($_POST['prev_p_id_f'])) {
				$prev_p_id_f = safe(trim($_POST['prev_p_id_f']));
			}else{
				$form_filled_correctly = false;
				$error_loc = "prev_p_id_f";
			}
			if ($form_filled_correctly) {
				
				$err_msj="";
				$query_part1 = "INSERT INTO attachment(filename, file_desc, v_id_f) VALUES
										(";
				$file_desc = $prev_p_id_f . " clinics attachment";
				$query_part2 = ", '{$file_desc}', {$prev_v_id})";

				// upload_file($errmsj, $uploading_files, $folder='', $query_part_1, $query_part_2)
				$folder = $prev_p_id_f . "/" . $prev_v_id;
				$err_msj = upload_file($err_msj, $_FILES, $folder, $query_part1, $query_part2, "../");
				echo $err_msj;
			}
			else{
				echo "-1";
			}
		}
	}

	else if (isset($_POST['cancelv'])) {
		
		$form_filled_correctly = true;
		$error_loc = "";

		// p_id
		if (isset($_POST['p_id']) && !empty($_POST['p_id'])) {
			$p_id = safe(trim($_POST['p_id']));
		}else{
			$form_filled_correctly = false;
			$error_loc = "p_id";
		}

		// v_id
		if (isset($_POST['v_id']) && !empty($_POST['v_id'])) {
			$v_id = safe(trim($_POST['v_id']));
		}else{
			$form_filled_correctly = false;
			$error_loc = "v_id";
		}

		if ($form_filled_correctly) {
			// $query = "DELETE FROM visit WHERE v_id = {$v_id} AND p_id_f = {$p_id} AND in_queue=2";
			$query = "UPDATE visit SET view = -1 WHERE v_id = {$v_id} AND p_id_f = {$p_id} AND in_queue=2";
			mysql_query($query) or die("-1");
			$queryDel = "DELETE FROM visit_attr WHERE v_id_f = {$v_id} LIMIT 3";
			mysql_query($queryDel) or die("-1");
			$query = "UPDATE patient SET queue_status=-1 WHERE p_id = {$p_id} ";
			mysql_query($query) or die("-1");
		}
		else{
			echo "-1";
		}

	}
?>