<?php  
if (isset($_POST['curr_q'])) {
    require_once '../connection.php';
    require_once '../functions.php';
    date_default_timezone_set("Asia/Baghdad"); 
    $today = date('Y-m-d');
    $table_data = '<table class="rtable rtable--flip table table-bordered table-condensed" id="queue">
    <thead>
        <th>#</th>
        <th>Full Name</th>
        <th>Code</th>
        <th>Sex</th>
        <th>Phone Number</th>
        <th>Visit Time</th>
        <th>Fee</th>
        <th>Status</th>
    </thead>';

  // marital_status, pregnant, breast_feed, no_of_child, other_info
    $queue_list_set = get_queue_list($today);
    $i = 1;
    while ($today_queue = mysql_fetch_assoc($queue_list_set)) {
        $sex ="";
        if ($today_queue['sex']=='1') {
            $sex = "<div style='text-align:center'><i class='fa fa-mars'><span style='display:none'>1</span></i></div>"; 
        }else if ($today_queue['sex']=='2') {
            $sex = "<div style='text-align:center'><i class='fa fa-venus'><span style='display:none'>2</span></i></div>";    
        }else if ($today_queue['sex']=='3') {
            $sex = "<div style='text-align:center'><i class='fa fa-venus-mars'><span style='display:none'>3</span></i></div>";   
        }
        $in_queue = "";
        if ($today_queue['in_queue']=='1') {
            $in_queue = "<div style='text-align:center'><i style='font-size:1.3em' class='inqu_icn1 fa fa-user-circle-o'><span style='display:none'>1</span></i></div>"; 
        }else if ($today_queue['in_queue']=='2' || $today_queue['in_queue']=='-1') {
            $in_queue = "<div style='text-align:center'><i style='font-size:1.3em; color:green' class='inqu_icn2 fa fa-check-circle-o'><span style='display:none'>2</span></i></div>";    
        }

        $visit_time = '';
        if (!empty($today_queue['visit_time'])) {
          $date = DateTime::createFromFormat( 'H:i:s', $today_queue['visit_time']);
          $visit_time = $date->format( 'h:i A');
        }

        $danger_class = $today_queue['fee']==0?"class='danger'":"";
        $success_class = $today_queue['in_queue']!=1?"class='success'":"";
        $table_data .= "<tr {$success_class} id={$today_queue['v_id']} data-marital_status='{$today_queue['marital_status']}' data-pregnant='{$today_queue['pregnant']}' data-breast_feed='{$today_queue['breast_feed']}' data-no_of_child='{$today_queue['no_of_child']}' data-other_info='{$today_queue['other_info']}' data-in_queue='{$today_queue['in_queue']}'>
                            <td>
                            {$i}
                            </td>
                            <td>
                            {$today_queue['fullname']}
                            </td>
                            <td>
                            {$today_queue['p_id_f']}
                            </td>
                            <td>
                            {$sex}
                            </td>
                            <td>
                            {$today_queue['phone']}
                            </td>
                            <td>
                            {$visit_time}
                            </td>
                            <td {$danger_class}>
                            {$today_queue['fee']}
                            </td>
                            <td>
                            {$in_queue}
                            </td>

                        </tr>";
                        $i++;
    }
  
    $table_data .= '</table>';
    

    echo $table_data;
}
    ?>