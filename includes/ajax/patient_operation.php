<?php
	require_once '../connection.php';
	require_once '../functions.php';

	if (isset($_POST['addnewP'])) 
	{
		if(!isset($_SESSION))
		{
		    session_start();
		}
		// print_r($_POST);
		/*Array
		(
		    [fullname] => namename
		    [sex] => 1
		    [address] => erbil
		    [date] =>  2017
		    [phone] => 456456
		    [occupation] => engineer
		    [addnewP] => true
		)*/

		/*$_SESSION['visit_counter'] +=1;
		$ymd = date("ymd");
		$pnum = $_SESSION['visit_counter']>9?$_SESSION['visit_counter']: '0'.$_SESSION['visit_counter'];
		$patient_code = $ymd . $pnum;*/

		$form_filled_correctly = true;
		// fullname
		if (isset($_POST['fullname']) && !empty($_POST['fullname'])) {
			$fullname = safe(trim($_POST['fullname']));
		}else{
			$form_filled_correctly = false;
		}
		// sex
		if (isset($_POST['sex']) && !empty($_POST['sex'])) {
			$sex = safe(trim($_POST['sex']));
			$preg_flag = $sex==1?0:2;
		}else{
			$form_filled_correctly = false;
			$preg_flag = 0; //set it to zero if user was male not female
		}
		// address
		if (isset($_POST['address']) && !empty($_POST['address'])) {
			$address = safe(trim($_POST['address']));
		}else{
			$address = "not available";
			// $form_filled_correctly = false;
		}
		// date
		if (isset($_POST['date']) && !empty($_POST['date'])) {
			$yearOfBirth = safe(trim($_POST['date']));
		}else{
			$yearOfBirth = "not available";
			// $form_filled_correctly = false;
		}
		// phone
		if (isset($_POST['phone']) && !empty($_POST['phone'])) {
			$phone = safe(trim($_POST['phone']));
		}else{
			$form_filled_correctly = false;
		}
		// occupation
		if (isset($_POST['occupation']) && !empty($_POST['occupation'])) {
			$occupation = safe(trim($_POST['occupation']));
		}else{
			$occupation = "not available";
			// $form_filled_correctly = false;
		}

		// visit_date
		if (isset($_POST['visit_date']) && !empty($_POST['visit_date'])) {
			$visit_date = safe(trim($_POST['visit_date']));
			$qstatus = 1;
		}else{
			$visit_date = "";
			$qstatus = -1;
			// $form_filled_correctly = false;
		}
		// visit_time
		if (isset($_POST['visit_time']) && !empty($_POST['visit_time'])) {
			$visit_time = safe(trim($_POST['visit_time']));
			// $time_input = '11:00 AM';
			$date = DateTime::createFromFormat( 'H:i A', $visit_time);
			$visit_time = $date->format( 'H:i:s');
		}else{
			$visit_time = null;
			// $form_filled_correctly = false;
			// $error_loc.="visit_time";
		}

		//check the conclusion of the above validations
		if ($form_filled_correctly) {

			$query = "INSERT INTO patient(fullname, sex, address, dob, phone, occupation,u_id_f, queue_status) VALUES 
					('{$fullname}', '{$sex}', '{$address}', '{$yearOfBirth}', '{$phone}', '{$occupation}', {$_SESSION['user_id']}, {$qstatus})";
			// echo $query;
			mysql_query($query) or die("-1");
			if (!empty($visit_date)) {
				$last_pid = mysql_insert_id();
					//insert patient to queue (visit)
				$visit_query = "INSERT INTO visit (p_id_f, visit_date, visit_time, marital_status, pregnant, breast_feed, no_of_child, other_info, fee, in_queue, u_id_f) VALUES(
						{$last_pid}, '{$visit_date}', '{$visit_time}', 1, {$preg_flag}, {$preg_flag}, 0, '', 0, 1, {$_SESSION['user_id']})";
				
				mysql_query($visit_query) or die("-1" . mysql_error());
			}
		}else{
			echo "-1";
		}
	}
	//update patient
	elseif (isset($_POST['editP'])) {
		if(!isset($_SESSION))
		{
		    session_start();
		}

		$form_filled_correctly = true;
		// p_id
		if (isset($_POST['p_id']) && !empty($_POST['p_id'])) {
			$p_id = safe(trim($_POST['p_id']));
		}else{
			$form_filled_correctly = false;
		}
		// fullname
		if (isset($_POST['fullname']) && !empty($_POST['fullname'])) {
			$fullname = safe(trim($_POST['fullname']));
		}else{
			$form_filled_correctly = false;
		}
		// sex
		if (isset($_POST['sex']) && !empty($_POST['sex'])) {
			$sex = safe(trim($_POST['sex']));
		}else{
			$form_filled_correctly = false;
		}
		// address
		if (isset($_POST['address']) && !empty($_POST['address'])) {
			$address = safe(trim($_POST['address']));
		}else{
			$form_filled_correctly = false;
		}
		// date
		if (isset($_POST['date']) && !empty($_POST['date'])) {
			$yearOfBirth = safe(trim($_POST['date']));
		}else{
			$form_filled_correctly = false;
		}
		// phone
		if (isset($_POST['phone']) && !empty($_POST['phone'])) {
			$phone = safe(trim($_POST['phone']));
		}else{
			$form_filled_correctly = false;
		}
		// occupation
		if (isset($_POST['occupation']) && !empty($_POST['occupation'])) {
			$occupation = safe(trim($_POST['occupation']));
		}else{
			$form_filled_correctly = false;
		}

		// queue_status
		if (isset($_POST['queue_status']) && !empty($_POST['queue_status'])) {
			$queue_status = safe(trim($_POST['queue_status']));
			// $qstatus = 1;
		}else{
			// this zero doesn't mean anything
			$queue_status = 0;
			// $qstatus = -1;
			// $form_filled_correctly = false;
		}

		// visit_date
		if (isset($_POST['visit_date']) && !empty($_POST['visit_date'])) {
			$visit_date = safe(trim($_POST['visit_date']));
			// $qstatus = 1;
		}else{
			$visit_date = "";
			// $qstatus = -1;
			// $form_filled_correctly = false;
		}
		// visit_time
		if (isset($_POST['visit_time']) && !empty($_POST['visit_time'])) {
			$visit_time = safe(trim($_POST['visit_time']));
			// $time_input = '11:00 AM';
			$date = DateTime::createFromFormat( 'H:i A', $visit_time);
			$visit_time = $date->format( 'H:i:s');
		}else{
			$visit_time = null;
			// $form_filled_correctly = false;
			// $error_loc.="visit_time";
		}

		//check the conclusion of the above validations
		if ($form_filled_correctly) {
			$query = "UPDATE patient SET
						fullname = '{$fullname}',
						sex = '{$sex}',
						address = '{$address}',
						dob = '{$yearOfBirth}',
						phone = '{$phone}',
						occupation = '{$occupation}',
						u_id_f = {$_SESSION['user_id']}
					WHERE p_id = {$p_id}";
			// echo $query;
			mysql_query($query) or die("-1");
			if (!empty($visit_date)) {
				//if queue_status is 1 then this patient have a registered visit--> so we need to update the date of visit, otherwise we have to insert a new visit for this patient
				if ($queue_status == -1) {
					$nvquery = "INSERT INTO visit (p_id_f, visit_date, visit_time, fee, in_queue, u_id_f) VALUES(
					{$p_id}, '{$visit_date}', ";
					$nvquery .= !empty($visit_time)?"'{$visit_time}', ":"0, ";
					$nvquery .= " 0, 1, {$_SESSION['user_id']})";
					mysql_query($nvquery) or die("ERROR1-SUBQ. Please contact system administrator");
				
					$pquery = "UPDATE patient SET 
								latest_visit = '{$visit_date}', queue_status = 1";
					$pquery .= " WHERE p_id = {$p_id} AND queue_status = -1 AND view = 1";
					// echo $pquery . "<br />";
					mysql_query($pquery) or die("ERROR1-2. Please contact system administrator");
				}
				else {
					echo "-1";
				}/*else { no need for else because we have postpone visit in another tab
					$query_updt_v = "UPDATE visit SET visit_date='{$visit_date}' ";
					$query_updt_v .= !empty($visit_time)?", visit_time='{$visit_time}'":"";
					$query_updt_v .= " WHERE p_id_f={$p_id} AND in_queue=1";
					mysql_query($query_updt_v) or die("-1");
				}*/
			}/*elseif(!empty($visit_time)) {
				$query_updt_v = "UPDATE visit SET  visit_time='{$visit_time}' WHERE p_id_f={$p_id} AND in_queue=1";
				mysql_query($query_updt_v) or die("-1");
			}*/
		}else{
			echo "-1";
		}
	}
	// new queue
	elseif (isset($_POST['addPtoQ'])) 
	{
		// print_r($_POST);
		/*	Array
			(
			    [p_id] => 5
			    [sex] => true
			    [visit_date] => 2017-01-04
			    [maritalst] => 2
			    [preg] => 1
			    [breastfeed] => 1
			    [childnum] => 3
			    [other_inf] => otherinfo
			    [visit_fee] => 25000
			    [addPtoQ] => true
			)


		*/
		if(!isset($_SESSION))
		{
		    session_start();
		}

		$form_filled_correctly = true;
		$error_loc="no location";
		// p_id
		if (isset($_POST['p_id']) && is_numeric($_POST['p_id'])) {
			$p_id = safe(trim($_POST['p_id']));
		}else{
			$form_filled_correctly = false;
			$error_loc.="p_id";
		}
		// visit_date
		if (isset($_POST['visit_date']) && !empty($_POST['visit_date'])) {
			$visit_date = safe(trim($_POST['visit_date']));
		}else{
			$form_filled_correctly = false;
			$error_loc.="visit_date";
		}
		// visit_time
		if (isset($_POST['visit_time']) && !empty($_POST['visit_time'])) {
			$visit_time = safe(trim($_POST['visit_time']));
			// $time_input = '11:00 AM';
			$date = DateTime::createFromFormat( 'H:i A', $visit_time);
			$visit_time = $date->format( 'H:i:s');
		}else{
			$visit_time = null;
			// $form_filled_correctly = false;
			// $error_loc.="visit_time";
		}

		// maritalst
		$childnum = 0;
		if (isset($_POST['maritalst']) &&  is_numeric($_POST['maritalst'])) {
			$maritalst = safe(trim($_POST['maritalst']));
			if ($maritalst>1) {
				// childnum
				if (isset($_POST['childnum']) && is_numeric($_POST['childnum'])) {
					$childnum = safe(trim($_POST['childnum']));
				}else{
					$form_filled_correctly = false;
					$error_loc.="childnum";
				}
			}
		}else{
			$form_filled_correctly = false;
			$error_loc.="maritalst";
		}
		// sex
		$preg = 0;
		$breastfeed = 0;
		if (isset($_POST['sex'])) {
			$sex = safe(trim($_POST['sex']));
			// true: female, false: male

			if ($sex == 'true') {
				// preg
				$preg = 2; //2:not pregnant
				if (isset($_POST['preg']) && is_numeric($_POST['preg'])) {
					$preg = safe(trim($_POST['preg']));
				}

				// breastfeeding
				$breastfeed = 2; //2:not breastfeeding
				if (isset($_POST['breastfeed']) && is_numeric($_POST['breastfeed'])) {
					$breastfeed = safe(trim($_POST['breastfeed']));
				}
			}
		}else{
			$form_filled_correctly = false;
			$error_loc.="sex";
		}

		$other_inf = isset($_POST['other_inf'])?safe(trim($_POST['other_inf'])):"";
		$visit_fee = isset($_POST['visit_fee'])&&is_numeric($_POST['visit_fee'])?safe(trim($_POST['visit_fee'])):0;
		if ($form_filled_correctly) {
			if(!already_in_Dr_room_by_Pid($p_id)) {
				// start transaction
				mysql_query("BEGIN");
				$query = "UPDATE patient SET queue_status=1 WHERE p_id = {$p_id}"; 
				$uptdP = mysql_query($query);
				//insert patient to queue (visit)
				$instVisit;
				if($maritalst==-1){
					$query = "INSERT INTO visit (p_id_f, visit_date, visit_time, marital_status, pregnant, breast_feed, no_of_child, other_info, fee, in_queue, u_id_f)
							SELECT {$p_id}, '{$visit_date}', '{$visit_time}', marital_status, pregnant, breast_feed, no_of_child, '{$other_inf}', {$visit_fee}, 1, {$_SESSION['user_id']} FROM visit WHERE p_id_f={$p_id} ORDER BY visit_date DESC LIMIT 1";
					$instVisit = mysql_query($query);
					// if insert visit query run successfully but no row inserted then repeat it with default values
					if ($instVisit && mysql_insert_id() == 0) {
						$query = "INSERT INTO visit (p_id_f, visit_date, visit_time, marital_status, pregnant, breast_feed, no_of_child, other_info, fee, in_queue, u_id_f) VALUES(
								{$p_id}, '{$visit_date}', '{$visit_time}', 1, {$preg}, {$breastfeed}, {$childnum}, '{$other_inf}', {$visit_fee}, 1, {$_SESSION['user_id']})";
						$instVisit = mysql_query($query);
					}

				}else {
					$query = "INSERT INTO visit (p_id_f, visit_date, visit_time, marital_status, pregnant, breast_feed, no_of_child, other_info, fee, in_queue, u_id_f) VALUES(
								{$p_id}, '{$visit_date}', '{$visit_time}', {$maritalst}, {$preg}, {$breastfeed}, {$childnum}, '{$other_inf}', {$visit_fee}, 1, {$_SESSION['user_id']})";
					$instVisit = mysql_query($query);
				}
				if ($uptdP && $instVisit) {
					mysql_query("COMMIT");
				}else {
					mysql_query("ROLLBACK");
					echo "-1";
				}
			}else{
				echo "-2";
			}
		}else{
			echo "-1 ";
			// echo $error_loc;
		}
	}
	// Doctors room directly from all_p page by the doctor
	elseif (isset($_POST['addPtoDr_directly'])) 
	{
		// print_r($_POST);
		/*	Array
			(
			    [p_id] => 5
			    [sex] => true
			    [visit_date] => 2017-01-04
			    [maritalst] => 2
			    [preg] => 1
			    [breastfeed] => 1
			    [childnum] => 3
			    [other_inf] => otherinfo
			    [visit_fee] => 25000
			    [addPtoQ] => true
			)


		*/
		if(!isset($_SESSION))
		{
		    session_start();
		}

		$form_filled_correctly = true;
		$error_loc="no location";
		// p_id
		if (isset($_POST['p_id']) && is_numeric($_POST['p_id'])) {
			$p_id = safe(trim($_POST['p_id']));
		}else{
			$form_filled_correctly = false;
			$error_loc.="p_id";
		}
		// visit_date
		if (isset($_POST['visit_date']) && !empty($_POST['visit_date'])) {
			$visit_date = safe(trim($_POST['visit_date']));
		}else{
			$form_filled_correctly = false;
			$error_loc.="visit_date";
		}
		// visit_time
		if (isset($_POST['visit_time']) && !empty($_POST['visit_time'])) {
			$visit_time = safe(trim($_POST['visit_time']));
			// $time_input = '11:00 AM';
			$date = DateTime::createFromFormat( 'H:i A', $visit_time);
			$visit_time = $date->format( 'H:i:s');
		}else{
			$visit_time = null;
			// $form_filled_correctly = false;
			// $error_loc.="visit_time";
		}

		// maritalst
		$childnum = 0;
		if (isset($_POST['maritalst']) &&  is_numeric($_POST['maritalst'])) {
			$maritalst = safe(trim($_POST['maritalst']));
			if ($maritalst>1) {
				// childnum
				if (isset($_POST['childnum']) && is_numeric($_POST['childnum'])) {
					$childnum = safe(trim($_POST['childnum']));
				}else{
					$form_filled_correctly = false;
					$error_loc.="childnum";
				}
			}
		}else{
			$form_filled_correctly = false;
			$error_loc.="maritalst";
		}
		// sex
		$preg = 0;
		$breastfeed = 0;
		if (isset($_POST['sex'])) {
			$sex = safe(trim($_POST['sex']));
			// true: female, false: male

			if ($sex == 'true') {
				// preg
				$preg = 2; //2:not pregnant
				if (isset($_POST['preg']) && is_numeric($_POST['preg'])) {
					$preg = safe(trim($_POST['preg']));
				}

				// breastfeeding
				$breastfeed = 2; //2:not breastfeeding
				if (isset($_POST['breastfeed']) && is_numeric($_POST['breastfeed'])) {
					$breastfeed = safe(trim($_POST['breastfeed']));
				}
			}
		}else{
			$form_filled_correctly = false;
			$error_loc.="sex";
		}

		$other_inf = isset($_POST['other_inf'])?safe(trim($_POST['other_inf'])):"";
		$visit_fee = isset($_POST['visit_fee'])&&is_numeric($_POST['visit_fee'])?safe(trim($_POST['visit_fee'])):0;
		if ($form_filled_correctly) {
			if(!already_in_Dr_room_by_Pid($p_id)) {
				// start transaction
				mysql_query("BEGIN");
				$query = "UPDATE patient SET queue_status=2 WHERE p_id = {$p_id}"; 
				$uptdP = mysql_query($query);
				//insert patient to queue (visit)
				$instVisit;
				if($maritalst==-1){
					$query = "INSERT INTO visit (p_id_f, visit_date, visit_time, marital_status, pregnant, breast_feed, no_of_child, other_info, fee, in_queue, u_id_f)
							SELECT {$p_id}, '{$visit_date}', '{$visit_time}', marital_status, pregnant, breast_feed, no_of_child, '{$other_inf}', {$visit_fee}, 2, {$_SESSION['user_id']} FROM visit WHERE p_id_f={$p_id} ORDER BY visit_date DESC LIMIT 1";
					$instVisit = mysql_query($query);
					// if insert visit query run successfully but no row inserted then repeat it with default values
					if ($instVisit && mysql_insert_id() == 0) {
						$query = "INSERT INTO visit (p_id_f, visit_date, visit_time, marital_status, pregnant, breast_feed, no_of_child, other_info, fee, in_queue, u_id_f) VALUES(
								{$p_id}, '{$visit_date}', '{$visit_time}', 1, {$preg}, {$breastfeed}, {$childnum}, '{$other_inf}', {$visit_fee}, 2, {$_SESSION['user_id']})";
						$instVisit = mysql_query($query);
					}

				}else {
					$query = "INSERT INTO visit (p_id_f, visit_date, visit_time, marital_status, pregnant, breast_feed, no_of_child, other_info, fee, in_queue, u_id_f) VALUES(
								{$p_id}, '{$visit_date}', '{$visit_time}', {$maritalst}, {$preg}, {$breastfeed}, {$childnum}, '{$other_inf}', {$visit_fee}, 2, {$_SESSION['user_id']})";
					$instVisit = mysql_query($query);
				}
				if ($uptdP && $instVisit) {
					mysql_query("COMMIT");
				}else {
					mysql_query("ROLLBACK");
					echo "-1";
				}
			}else{
				echo "-2";
			}
		}else{
			echo "-1 ";
			// echo $error_loc;
		}
		
		
	}
	
	// send to Doctors' Room
	elseif (isset($_POST['addPtoDr'])) 
	{
		// print_r($_POST);
		/*	Array
			(
			    [v_id] => 5
			    [p_id_f] => 2
			    [sex] => true
			    [maritalst] => 2
			    [preg] => 1
			    [breastfeed] => 1
			    [childnum] => 3
			    [other_inf] => otherinfo
			    [visit_fee] => 25000
			    [addPtoQ] => true
			)
		*/
		if(!isset($_SESSION))
		{
		    session_start();
		}

		$form_filled_correctly = true;
		$error_loc = "def ";
		// p_id_f
		if (isset($_POST['p_id_f']) && is_numeric($_POST['p_id_f'])) {
			$p_id_f = safe(trim($_POST['p_id_f']));
		}else{
			$form_filled_correctly = false;
			$error_loc .= "<br /> p_id_f";
		}
		// v_id
		if (isset($_POST['v_id']) && is_numeric($_POST['v_id'])) {
			$v_id = safe(trim($_POST['v_id']));
		}else{
			$form_filled_correctly = false;
			$error_loc .= "<br /> v_id";
		}
		
		// maritalst
		$childnum = 0;
		if (isset($_POST['maritalst']) &&  is_numeric($_POST['maritalst'])) {
			$maritalst = safe(trim($_POST['maritalst']));
			if ($maritalst>1) {
				// childnum
				if (isset($_POST['childnum']) && is_numeric($_POST['childnum'])) {
					$childnum = safe(trim($_POST['childnum']));
				}else{
					$form_filled_correctly = false;
					$error_loc .= "<br /> childnum";
				}
			}
		}else{
			$form_filled_correctly = false;
			$error_loc .= "<br /> maritalst";
		}
		// sex
		$preg = 0;
		$breastfeed = 0;
		if (isset($_POST['sex'])) {
			$sex = safe(trim($_POST['sex']));
			// true: female, false: male

			if ($sex == 'true') {
				// preg
				$preg = 2; //2:not pregnant
				if (isset($_POST['preg']) && is_numeric($_POST['preg'])) {
					$preg = safe(trim($_POST['preg']));
				}

				// breastfeeding
				$breastfeed = 2; //2:not breastfeeding
				if (isset($_POST['breastfeed']) && is_numeric($_POST['breastfeed'])) {
					$breastfeed = safe(trim($_POST['breastfeed']));
				}
			}
		}else{
			$form_filled_correctly = false;
			$error_loc .= "<br /> sex";
		}

		$other_inf = isset($_POST['other_inf'])?safe(trim($_POST['other_inf'])):"";
		
		// visit_fee
		if (isset($_POST['visit_fee']) && is_numeric($_POST['visit_fee'])) {
			$visit_fee = safe(trim($_POST['visit_fee']));
		}else{
			$form_filled_correctly = false;
			$error_loc .= "<br /> visit_fee";
		}

		//attruibutes
		$attr1 = null;
		$attr2 = null;
		$attr3 = null;
		if (isset($_POST['attr1']) &&  !empty($_POST['attr1'])) {
			$attr1 = safe(trim($_POST['attr1']));
		}
		if (isset($_POST['attr2']) &&  !empty($_POST['attr2'])) {
			$attr2 = safe(trim($_POST['attr2']));
		}
		if (isset($_POST['attr3']) &&  !empty($_POST['attr3'])) {
			$attr3 = safe(trim($_POST['attr3']));
		}
		
		$inqueue = 1;
		if ($form_filled_correctly) {
			if(!already_in_Dr_room($v_id)) {
				//Update patient in queue (visit)
				$query = "
					UPDATE visit SET
								marital_status = {$maritalst},
								pregnant = {$preg},
								breast_feed = {$breastfeed},
								no_of_child = {$childnum},
								other_info = '{$other_inf}',
								fee = {$visit_fee},
								in_queue = 2,
								u_id_f = {$_SESSION['user_id']}
					WHERE v_id = {$v_id} AND p_id_f = {$p_id_f}";
				mysql_query($query) or die("-1");
				if (!empty($attr1)) {
					$query2 = "INSERT INTO visit_attr(v_id_f, attr, attr_order) VALUES ({$v_id}, '{$attr1}', 1)";
					$query2 .= !empty($attr2)?", ({$v_id}, '{$attr2}', 2)":"";
					$query2 .= !empty($attr3)?", ({$v_id}, '{$attr3}', 3)":"";
					mysql_query($query2) or die("-1". "Error 2");
				}
			}else{
				echo "-2";
				// echo $error_loc;
				// echo var_dump($error_loc);
			}
		}else{
			echo "-1";
			// echo $error_loc;
			// echo var_dump($error_loc);
		}
		
		
	}

	// postpon current visit to another day
	elseif (isset($_POST['postponv'])) {
		
		$form_filled_correctly = true;

		//p_id_f
		if (isset($_POST['p_id_f']) && is_numeric($_POST['p_id_f'])) {
			$p_id_f = safe(trim($_POST['p_id_f']));
		}else{
			$form_filled_correctly = false;
		}
		// v_id
		if (isset($_POST['v_id']) && is_numeric($_POST['v_id'])) {
			$v_id = safe(trim($_POST['v_id']));
		}else{
			$form_filled_correctly = false;
		}
		// postponed date
		if (isset($_POST['postponed_date']) && !empty($_POST['postponed_date'])) {
			$postponed_date = safe(trim($_POST['postponed_date']));
		}else{
			$form_filled_correctly = false;
		}
		// visit_time
		if (isset($_POST['visit_time']) && !empty($_POST['visit_time'])) {
			$visit_time = safe(trim($_POST['visit_time']));
			// $time_input = '11:00 AM';
			$date = DateTime::createFromFormat( 'H:i A', $visit_time);
			$visit_time = $date->format( 'H:i:s');
		}else{
			$visit_time = null;
			// $form_filled_correctly = false;
			// $error_loc.="visit_time";
		}
		
		if ($form_filled_correctly) {
			$query = "
					UPDATE visit SET
								visit_date = '{$postponed_date}' ";
			$query .= !empty($visit_time)?", visit_time='{$visit_time}'":"";
			$query .= " WHERE v_id = {$v_id} AND p_id_f = {$p_id_f}";
				mysql_query($query) or die("-1");
		} else{
			echo "-1";
		}
	}
	elseif (isset($_POST['deletev'])) {
		// print_r($_POST);
		$form_filled_correctly = true;

		//p_id_f
		if (isset($_POST['p_id_f']) && is_numeric($_POST['p_id_f'])) {
			$p_id_f = safe(trim($_POST['p_id_f']));
		}else{
			$form_filled_correctly = false;
		}
		// v_id
		if (isset($_POST['v_id']) && is_numeric($_POST['v_id'])) {
			$v_id = safe(trim($_POST['v_id']));
		}else{
			$form_filled_correctly = false;
		}
		if ($form_filled_correctly) {
			// $query = "DELETE FROM visit WHERE v_id = {$v_id} AND p_id_f = {$p_id_f} ";
			$query = "UPDATE visit SET view = -1 WHERE v_id = {$v_id} AND p_id_f = {$p_id_f} ";
			mysql_query($query) or die("-1");
			$query = "UPDATE patient SET queue_status=-1 WHERE p_id = {$p_id_f} ";
			mysql_query($query) or die("-1");
		}else {
			echo "-1";
		}
	}

	elseif (isset($_POST['new_visit_date'])) {
		if(!isset($_SESSION))
		{
		    session_start();
		}

		$form_filled_correctly = true;
		$error_loc="no location";
		// p_id
		if (isset($_POST['p_id']) && is_numeric($_POST['p_id'])) {
			$p_id = safe(trim($_POST['p_id']));
		}else{
			$form_filled_correctly = false;
			$error_loc.="p_id";
		}
		if ($form_filled_correctly) {
			$query = "SELECT v_id, p_id_f, visit_date, DATE_FORMAT(visit_time, '%H:%i') as `time` ";
			$query .= " FROM visit";
			$query .= " WHERE p_id_f = {$p_id} AND in_queue=1 AND view=1 LIMIT 1";
			// echo $query;
			$result_set = mysql_query($query) or die("-1");
			$new_visit_date = "Not Available";
			if (mysql_num_rows($result_set)>0) {
				$row = mysql_fetch_assoc($result_set);
				$new_visit_date = $row['visit_date'] . " " . $row['time'];

				// if the visit date is passed, cancel the visit
				$today = strtotime(date("Y-m-d"));
				if (strtotime($row['visit_date'])<$today) { 
					$query = "UPDATE visit SET view=-1 WHERE v_id = {$row['v_id']} ";
					mysql_query($query) or die("-1" . mysql_error());
					$query = "UPDATE patient SET queue_status=-1 WHERE p_id = {$p_id} ";
					mysql_query($query) or die("-1" . mysql_error());
					$new_visit_date = "Please Refresh the Website.";
				}
			}else {
				$query = "UPDATE patient SET queue_status=-1 WHERE p_id = {$p_id} ";
				mysql_query($query) or die("-1");
				$new_visit_date = "Please Refresh the Website.";
			}
			// echo strtotime(date("Y-m-d"));
			echo $new_visit_date;
			// echo date("d-m-yy H:i");			
			// echo date("Y-m-d");
		}else {
			// echo "-1";
			echo $error_loc;
		}
	}

?>