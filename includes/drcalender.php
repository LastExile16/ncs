<link rel="stylesheet" type="text/css" href="../css/jquery.timepicker.min.css">
<script type="text/javascript" src="../js/jquery.timepicker.min.js"></script>

<script type="text/javascript">
  
<?php 
// var availableDates = ["2017-01-20","2017-01-18"]; 
$available_v_dates = available_visit_dates();
$available_dates_arr = array();
$listcount_arr = array();
while ($date = mysql_fetch_array($available_v_dates)) {
  $available_dates_arr[] = $date['visit_date'];
  $listcount_arr[] = $date['listcount'];
}

echo 'var availableDates = '.json_encode($available_dates_arr).';';
echo 'var listcount = '.json_encode($listcount_arr).';';
?>
</script>
<script type="text/javascript" src="./js/modernizr.custom.79639.js"></script>
<div class="contact row">

	<!-- BEGIN SEARCH TABLE PORTLET-->
	
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>

<div class="row">
	 
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="col-md-12">
	 	
    <?php
      if (isset($_POST['visit_date']) && !empty($_POST['visit_date']) ) {
        $today = safe(trim($_POST['visit_date']));
      }
      else{
        $today = date('Y-m-d');
      }
    ?>
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="caption panel-title">
				<span class='fa fa-stethoscope table_head_icon'></span> Clinic Queue - <span id="current_date"><?php echo $today; ?></span>
			</div>
		</div>
		<div class="panel-body resposive">
	<table class="rtable rtable--flip table table-bordered table-condensed" id="queue">
	<thead>
    <tr>
      <th>#</th>
      <th>Full Name</th>
      <th>Code</th>
      <th>Sex</th>
      <th>Phone Number</th>
      <th>Visit Time</th>
          <th>Fee</th>
      <th>Status</th>
    </tr>
  </thead>
  <?php  

  // marital_status, pregnant, breast_feed, no_of_child, other_info
    $queue_list_set = get_queue_list($today);
    $table_data = "";
    $i = 1;
    $visit_time = '';
    while ($today_queue = mysql_fetch_assoc($queue_list_set)) {
        $sex ="";
        if ($today_queue['sex']=='1') {
            $sex = "<div style='text-align:center'><i class='fa fa-mars'><span style='display:none'>1</span></i></div>"; 
        }else if ($today_queue['sex']=='2') {
            $sex = "<div style='text-align:center'><i class='fa fa-venus'><span style='display:none'>2</span></i></div>";    
        }else if ($today_queue['sex']=='3') {
            $sex = "<div style='text-align:center'><i class='fa fa-venus-mars'><span style='display:none'>3</span></i></div>";   
        }
        $in_queue = "";
        if ($today_queue['in_queue']=='1') {
            $in_queue = "<div style='text-align:center'><i style='font-size:1.3em' class='inqu_icn1 fa fa-user-circle-o'><span style='display:none'>1</span></i></div>"; 
        }else if ($today_queue['in_queue']=='2' || $today_queue['in_queue']=='-1') {
            $in_queue = "<div style='text-align:center'><i style='font-size:1.3em; color:green' class='inqu_icn2 fa fa-check-circle-o'><span style='display:none'>2</span></i></div>";    
        }
        $visit_time = '';
        if (!empty($today_queue['visit_time'])) {
          $date = DateTime::createFromFormat( 'H:i:s', $today_queue['visit_time']);
          $visit_time = $date->format( 'h:i A');
        }

        $danger_class = $today_queue['fee']==0?"class='danger'":"";
        $success_class = $today_queue['in_queue']!=1?"class='success'":"";
        $table_data .= "<tr {$success_class} id={$today_queue['v_id']} data-marital_status='{$today_queue['marital_status']}' data-pregnant='{$today_queue['pregnant']}' data-breast_feed='{$today_queue['breast_feed']}' data-no_of_child='{$today_queue['no_of_child']}' data-other_info='{$today_queue['other_info']}' data-in_queue='{$today_queue['in_queue']}'>
                            <td>
                            {$i}
                            </td>
                            <td>
                            {$today_queue['fullname']}
                            </td>
                            <td>
                            {$today_queue['p_id_f']}
                            </td>
                            <td>
                            {$sex}
                            </td>
                            <td>
                            {$today_queue['phone']}
                            </td>
                            <td>
                            {$visit_time}
                            </td>
                            <td {$danger_class}>
                            {$today_queue['fee']}
                            </td>
                            <td>
                            {$in_queue}
                            </td>

                        </tr>";
                        $i++;
    }
    echo $table_data;
  ?>
	<tfoot>
    <tr>
      <th>#</th>
      <th>Full Name</th>
      <th>Code</th>
      <th>Sex</th>
      <th>Phone Number</th>
      <th>Visit Time</th>
      <th>Fee</th>
      <th>Status</th>
    </tr>
  </tfoot>
	
  </table>

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <form class="form-horizontal" name="sendToDr_form" action="index.php" id="sendToDr_form">
        <div class="modal-body">
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <div class="modal fade" id="newpatient" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

</div>

		</div>
	</div>
	<!-- <div class="row" >
		<div class="col-md-12" >
			<a href="#newpatient" class="btn btn-primary" data-toggle='modal'><i class="fa fa-plus-square"></i> add new patient</a>
			<a href="javascript:;" class="btn btn-danger" id="cleaner"><i class="fa fa-circle-o-notch"></i> clean queue</a>
		</div>
	</div> -->
	</div>
	

<!-- <script src="./js/lightbox.js"></script>
<script>
    lightbox.option({
      'positionFromTop': 30
    })
</script> -->
<script type="text/javascript">

	$(document).ready(function() {

  var columnDefs = [{
    title: "#"
  }, {
    title: "Full Name"
  }, {
    title: "Code"
  }, {
    title: "Sex"
  }, {
    title: "Phone Number"
  }, {
    title: "Visit Time"
  }, {
    title: "Fee", render: $.fn.dataTable.render.number( ',', '.', 0 ) 
  }, {
    /*title: "Status", render: function (data, type, row) {
                        return row[6] + " " + row[3];
                        // console.log(row);
                        // return data;
                    } */
    title:"Status"
  }];

  var queue;

  queue = $('#queue').DataTable({
  	processing: true,
    "pagingType": "full_numbers",
    columns: columnDefs,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "deferRender": true,
    dom: '<"top row"<"col-md-2"l><"col-md-2"B><"col-md-5 rangefilter"><"col-md-3"f>><"row"<"col-md-12" <"row"r>t>><"bottom row"<"col-md-4"i><"paginationright col-md-8"p>><"clear">',
    select: 'single',
    buttons: [/*{
    			text: 'pdf',
                extend: 'pdfHtml5',
                download: 'open',
                message: 'pdf created by Nawras Clinical Service'
            },{
    	text: '<i data-toggle="tooltip" title="to Doctor" id="add_to_dr_btn_icon" class="fa fa-sign-in fa-1-5x"></i>',
      extend: 'selected',
      action: function(e, dt, node, config) {
        addClick(this, dt, node, config)
      }
    }, */{
      extend: 'selected',
      text: '<i data-toggle="tooltip" title="Cancel" class="fa fa-ban fa-1-5x"></i>',
      action: function(e, dt, node, config) {
        var rows = dt.rows({
          selected: true
        }).count();

        deleteClick(this, dt, node, config)
      }
    }, {
      extend: 'selected',
      text: '<i data-toggle="tooltip" title="postpon visit" id="postpon_v_btn_icon" class="fa fa-pencil-square-o fa-1-5x"></i>',
      action: function(e, dt, node, config) {
        var rows = dt.rows({
          selected: true
        }).count();
        //above code just to indicate how many rows are selected
// console.log(rows);
// console.log(dt.rows({selected: true}));
        postponClick(this, dt, node, config)
      }
    }, {
		extend: 'print',
		text: '<i data-toggle="tooltip" title="Print" class="fa fa-print fa-1-5x"></i>',
        message: 'This print was produced by Nawras Clinical System',
        exportOptions: {
            columns: ':visible'
        }
    }, {
    	text: '<i data-toggle="tooltip" title="hide columns" class="fa fa-eye-slash fa-1-5x"></i>',
    	extend:'colvis'
    }]
  });

 
  //---------Function to Display modal editButton---------

  function postponClick(pointer, oTT, button, conf) {
    if ($("tr.selected").data("in_queue") != '1') {
            swal({
                title: "Operation Failed!",
                text: "This patient is already in Doctors' Room.",
                type: "error",
                confirmButtonColor: "#C9302C"
            });
            return;
        }

    var adata = oTT.rows({
      selected: true
    });

    var id = $("tr.selected").attr("id");

    var data = "";
    var queue = pointer;
    
    data += "<input type='hidden' value='" + id + "' name='v_id' id='v_id' >";
    data += "<input type='hidden' value='" + adata.data()[0][2] + "' name='p_id_f' >";
    
    data += `
    <div class="form-group">
   <label for="datetimepicker2" class="col-md-4 control-label">Postpon the visit To</label>
   <div class="col-md-7">
      <div class='input-group date datepick' id='datetimepicker2'>
      <input type='text' autocomplete="off" class="form-control" name="postponed_date" id="postponed_date" required="required" />
      <span class="input-group-addon">
         <span class="glyphicon glyphicon-calendar"></span>
      </span>
         </div>
   </div>
</div>
<div class="form-group">
   <label for="datetimepicker4" class="col-md-4 control-label">Visit Time</label>
   <div class="col-md-7">
        <div class='input-group' id='thetime'>
            <input type='text' autocomplete="off" class="form-control" name="visit_time" id="datetimepicker4" />
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-time"></span>
            </span>
        </div>
   </div>
</div>
    `

    $('#myModal').on('show.bs.modal', function() {

      $('#myModal').find('form').attr('id','postpon_v_form');
      $('#myModal').find('form').attr('name','postpon_v_form');
      $('#myModal').find('.modal-title').html('Postpon Visit');
      $('#myModal').find('.modal-body').html(data);
      $('#myModal').find('.modal-footer').html("<button type='submit' data-content='remove' class='btn btn-primary' id='editRowBtn'>Update</button>");
    });

    $('#myModal').modal('show');
    $('#myModal input[1]').focus();
    $('#datetimepicker2').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true,
            weekStart: 5,
            startDate: "+1d",
            daysOfWeekHighlighted:'5'
        });
    $("#datetimepicker4").timepicker({
          'minTime' : '7:00am',
        'maxTime' : '12:00am',
        'step' : 5,
        'timeFormat' : 'g:i a'
        });

  };

  // edit row functionality
  $(document).on('submit', '#postpon_v_form', function(e) {
    $("#postpon_v_btn_icon").removeClass("fa-pencil-square-o");
    $("#postpon_v_btn_icon").addClass("fa-cog fa-spin");
    e.preventDefault();
    var $form = $(e.target);
    $.ajax({
        // url: $form.attr('action'),
        url: "./includes/ajax/patient_operation.php",
        type: 'POST',
        data: $form.serialize()+"&postponv=true",
        success: function(result) {
            if (result == -1) {
                swal({
                    title: "Operation Failed!",
                    text: "The operation failed, please write information correctly when filling the form.",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
            }else if(result == -2) {
                swal({
                    title: "Operation Failed!",
                    text: "This patient already added to queue.",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                
            }else {
                // i change in_queue just to make sure nothing wrong will happen if
                // this operation was aborted somehow.
                $("#"+$("#v_id").val()).data("in_queue", "2");

                queue.row('.selected').remove().draw( false );
            }

            $("#postpon_v_btn_icon").removeClass("fa-cog fa-spin");
            $("#postpon_v_btn_icon").addClass("fa-pencil-square-o");
        }
    });
    // edit row function needs to go here, here use your ajax then hide the modal
    $('#myModal').modal('hide');

  });

  //---------Function to Display modal deleteButton---------

  function deleteClick(pointer, oTT, button, conf) {
    if ($("tr.selected").data("in_queue") != '1') {
            swal({
                title: "Operation Failed!",
                text: "This patient is already in Doctors' Room.",
                type: "error",
                confirmButtonColor: "#C9302C"
            });
            return;
        }

    var adata = oTT.rows({
      selected: true
    });

    swal({
            title: "Delete Visit",
            text: "Are you sure you want to delete this visit?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#C9302C"
        },
        function(){
        var v_id = $("tr.selected").attr("id");
        var p_id = adata.data()[0][2];
            $.ajax({
                url: "./includes/ajax/patient_operation.php",
                type: 'POST',
                data: "v_id="+v_id+"&p_id_f="+p_id+"&deletev=true",
                success: function(result) {
                    if (result == -1) {
                        swal({
                            title: "Operation Failed!",
                            text: "Somthing Wrong Happened, if problem persist please contact system administrator.",
                            type: "error",
                            confirmButtonColor: "#C9302C"
                        });
                    }else {
                        queue.row('.selected').remove().draw( false );
                        swal("Done!","Operation completed successfully.","success");
                    }
                }
            });
        });
    
  };

filtered_date = $("#current_date").text();
$("div.rangefilter").html("<form id='visits_list' action='index.php?page=drcalender' method='POST'><div id='baseDateControl'><div class='dateControlBlock'>Filter Date <input type='text' name='visit_date' id='visit_date' class='form-control input-sm' autocomplete='off' /> </div></div></form>");

  // $("#visit_date").keyup ( function() { submit_value(); } );
  $("#visit_date").change( function() { submit_value(); } );

/*  $("#clear_search").on("click", function() {
    $('#visit_date').val("");
    queue.draw();
  });*/

  
  
  $('#visit_date').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayHighlight: true,
    weekStart: 5,
    // startDate: "+0d",
    daysOfWeekHighlighted:'5',
    //datesDisabled:availableDates,
    beforeShowDay: function(d) {
      // console.log(d.getMonth());
      // var dmy = (d.getMonth()+1);
      var dmy = d.getFullYear(); 
      dmy+= "-"; 
      
      if(d.getMonth()<9) dmy+="0"; 

      dmy+=d.getMonth()+1 + "-";
      day = (d.getDate()); 
      if(d.getDate()<10) 
          dmy+="0"+day;
      else
          dmy+=day;

      
      // console.log(dmy+' : '+($.inArray(dmy, availableDates)));
      dateindex = $.inArray(dmy, availableDates);
      if (dateindex != -1) {
          return {enabled:true, classes:"enabled_dates", tooltip:listcount[dateindex]}; 
      } else{
           return {enabled:false, classes:"", tooltip:"unAvailable"}; 
      }
    }



  });

// Setup - add a text input to each footer cell
    $('#queue tfoot th').each( function () {
    // $('#y tfoot th').not(":eq(0),:eq(-1)").each( function () {
        // var title = $.trim($(this).text());
        // console.log($(this));
        // $(this).html( '<input type="text" class="form-control input-sm" placeholder="Search '+$.trim(title)+'" />' );
        if ($(this).html() == 'Sex') {
            list = `<select class="form-control input-sm">
            <option value=""></option>
            <option value="1">Male</option>
            <option value="2">Female</option>
            <option value="3">Transgender</option>
        </select>`;
            $(this).html(list);
        }else{
            $(this).html( '<input type="text" class="form-control input-sm" placeholder="Type Here" />' );
        }
        // var disable='';
        // var placeholder = 'placeholder="Search here"';
        // if (title=='tools' || title=='#') { disable="disabled=disabled readonly "; placeholder='style="background-color:transparent; border-color:transparent; display:none"'; }
        	// $(this).html( '<input type="text" '+ disable + ' class="form-control input-sm"'+placeholder+' />' );
    } );
 
    // Apply the search
    var timeout = null;
    queue.columns().every( function () {
        var that = this;
        var ind = this.index();
        //if (this.index()==0 || this.index()==5 ) {return;}//to disable search in first at last col
        $( 'input, select', this.footer() ).on( 'keyup change', function () {
            search_val = this.value;
            if (timeout !== null) {
              clearTimeout(timeout);
            }
            timeout = setTimeout(function () {
              that
                  .search( search_val )
                  .draw();
            }, 700);
        } );
    } );


    $(document).on('click', '#cleaner', function(event) {
    	$('#cleaner > :first-child').addClass('spin-backw');
      // dabi hamu visitakan clean bkren ka queueyan != -1, wata awai queuei yak yan dwa dabo delete bi
    });


    $("#maritalst").on("change", function (e) {
        // console.log(e);
        if (this.value>1) {
            $("#female_n_childnum_div").slideDown('slow');
            $("#childnum").val(0);
            $('#childnum').attr('required', 'required');
        }else{
            $("#female_n_childnum_div").slideUp('slow');
            $("#childnum").val('');
            $('#childnum').removeAttr('required');
        }
    });

   $('.btn-group i[data-toggle="tooltip"]').tooltip();
} );

function submit_value()
{
  if ($("#visit_date").val() != "" && $("#visit_date").val() != null) {
    $("#visits_list").submit();
  }
}
</script>