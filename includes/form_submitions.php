<?php
require_once './connection.php';
// require_once "./functions.php";
if(!isset($_SESSION))
{
    session_start();
}
require_once './functions.php';
if (!logged_in()) 
{
    header("Location: login.php");
    exit();
}
// add new expense
if (isset($_POST['new_expense'])) {
	$err_msj="";
	if (isset($_POST['expense'])) {
		$expense = safe(trim($_POST['expense']));
	}
	else {
		$err_msj = "please fill the form correctly or contact the administrator";
	}
	if (isset($_POST['amount']) && is_numeric($_POST['amount'])) {
		$amount = safe(trim($_POST['amount']));
	}
	else {
		$err_msj .= "please fill the form correctly or contact the administrator";
	}
	if (isset($_POST['exp_date'])) {
		$exp_date = safe(trim($_POST['exp_date']));
	}
	else {
		$err_msj .= "please fill the form correctly or contact the administrator";
	}
	if (empty($err_msj)) {
		$query = "INSERT INTO expense (expense_name, amount, exp_date) VALUES ('{$expense}', {$amount}, '{$exp_date}')";
		if(!mysql_query($query))
		{
			$err_msj .= " An Error occured. Please contant the Administrator";
		}
	}
	header('Location: ../index.php?page=expenses&err_msj=' . $err_msj);
	exit();
}
//change profile info
if (isset($_POST['change_profile'])) {
	
	$err_msj="";
	$clean_to_change_pass=false;
	$fullname = safe(trim($_POST['fullname']));
	// $username = safe(trim($_POST['username']));
	if (isset($_POST['changepass'])) {
		if (isset($_POST['old_password']) && !empty($_POST['old_password']) &&
			isset($_POST['new_password']) && !empty($_POST['new_password']) &&
			isset($_POST['new_password_2']) && !empty($_POST['new_password_2'])
			) {
		
			$old_pass = safe(trim($_POST['old_password']));
			$old_pass = sha1(md5($old_pass));
			$new_pass = safe(trim($_POST['new_password']));
			$new_pass2 = safe(trim($_POST['new_password_2']));
			if ($new_pass != $new_pass2) {
				header('Location: ../index.php?page=my_profile&err_msj=new password does not match!');
				exit();
			}
			
			$DB_old_pass=mysql_query("SELECT password FROM user WHERE u_id={$_SESSION['user_id']} AND username='{$_SESSION['name']}' AND password='{$old_pass}' LIMIT 1") or die("wrong user credentials");
			
			if (mysql_num_rows($DB_old_pass)==1) {
				$new_pass = sha1(md5($new_pass));
				$clean_to_change_pass = true;
			}
		}
	}
	if (isset($_POST['changepass']) && !$clean_to_change_pass) {
		header('Location: ../index.php?page=my_profile&err_msj=Wrong old password!');
		exit();
	}
	$query = "UPDATE user SET
				fullname = '{$fullname}'";
	$query .= isset($_POST['changepass']) && $clean_to_change_pass?" , password='{$new_pass}'":" ";
	$query .= " WHERE u_id = {$_SESSION['user_id']}";
	if (!mysql_query($query)) {
		$err_msj = "failed to update user data, please choose another username.";
	}
	else{
		$_SESSION['fullname'] = $fullname;
		// $_SESSION['name'] = $username;
	}
	header('Location: ../index.php?page=my_profile&err_msj=' . $err_msj);
	exit();
}
// process the above form as its change profile which used by both doc and sec then
// check if not doctor =, log it out. 
if (!is_doctor()) {
	header("Location: logout.php");
    exit();
}
$err_msj = "";
$direct_to_pid = "";
$to = isset($_GET['to']) && !empty($_GET['to'])?"{$_GET['to']}":"current_p";
//  add patient visit data by dr
if (isset($_POST['end_visit']) ) {
	// prev_visit_id_list%20 other_info%20 diagnosis%20 treatment
	/*echo "<pre>";
	print_r($_POST);
	print_r($_FILES);
	echo "</pre>";
	*/
	/*
	[p_id] => 4
	[v_id] => 6
    [chief_com_n_duration] => 1.5 hours
    [examination] => test Examination
    [investigation] => test investigation
    [diagnosis] => dig1, dig2, dig3
    [treatment] => treat1, treat2
    [treatment_note] => some_notes
    [surgery] => surgery1, surgery2
    [surgery_date] => 2017-03-4
    [note] => some notes
    [followup_date] => 2017-02-10
    [end_visit] =>  
    */
    $form_filled_correctly = true;
    $error_loc = "";
    // p_id
	if (isset($_POST['p_id']) && !empty($_POST['p_id'])) {
		$p_id = safe(trim($_POST['p_id']));
	}else{
		$form_filled_correctly = false;
		$error_loc .= " p_id ";
	}
	// v_id
	if (isset($_POST['v_id']) && !empty($_POST['v_id'])) {
		$v_id = safe(trim($_POST['v_id']));
	}else{
		$form_filled_correctly = false;
		$error_loc .= " v_id ";
	}
	// prev_visit_id_list
	if (isset($_POST['prev_visit_id_list'])) {
		$prev_visit_id_list = safe(trim($_POST['prev_visit_id_list']));
	}else{
		// echo "<br>"; var_dump($_POST['prev_visit_id_list']);
		$form_filled_correctly = false;
		$error_loc .= " prev_visit_id_list ";
	}
	// marital_status	
	if (isset($_POST['marital_status']) && !empty($_POST['marital_status'])) {
		$marital_status = safe(trim($_POST['marital_status']));
	}else{
		$form_filled_correctly = false;
		$error_loc .= " marital_status ";
	}
	// pregnant
	if (isset($_POST['pregnant']) && is_numeric($_POST['pregnant'])) {
		$pregnant = safe(trim($_POST['pregnant']));
	}else{
		$form_filled_correctly = false;
		$error_loc .= " pregnant ";
	}
	// echo "<br>"; var_dump($_POST['breast_feed']);
	// breast_feed
	if (isset($_POST['breast_feed']) && is_numeric($_POST['breast_feed'])) {
		$breast_feed = safe(trim($_POST['breast_feed']));
	}else{
		$form_filled_correctly = false;
		$error_loc .= " breast_feed ";
	}
	// no_of_child
	if (isset($_POST['no_of_child']) && is_numeric($_POST['no_of_child'])) {
		$no_of_child = safe(trim($_POST['no_of_child']));
	}else{
		$form_filled_correctly = false;
		$error_loc .= " no_of_child ";
	}
	// other_info
	/*if (isset($_POST['other_info']) && !empty($_POST['other_info'])) {
		$other_info = safe(trim($_POST['other_info']));
	}else{
		echo "<br>"; var_dump($_POST['other_info']);
		$form_filled_correctly = false;
		$error_loc .= " other_info ";
	}*/
	
	// chief_com_n_duration
	if (isset($_POST['chief_com_n_duration']) && !empty($_POST['chief_com_n_duration'])) {
		$chief_com_n_duration = safe(trim($_POST['chief_com_n_duration']));
	}else{
		$chief_com_n_duration = "";
	}
	// examination
	if (isset($_POST['examination']) && !empty($_POST['examination'])) {
		// $examination = safe(trim($_POST['examination']));
		$examination_arr2 = array();
		$examination_arr = array_map("trim", $_POST['examination']);
		if (!empty($examination_arr[0])) {
			 $examination_arr2[] = "BP: " . $examination_arr[0] . " mmhg";
		}
		if (!empty($examination_arr[1])) {
			 $examination_arr2[] = "PR: " . $examination_arr[1] . " b/min";
		}
		if (!empty($examination_arr[2])) {
			 $examination_arr2[] = "Chest: " . $examination_arr[2];
		}
		if (!empty($examination_arr[3])) {
			 $examination_arr2[] = "Heart: " . $examination_arr[3];
		}
		if (!empty($examination_arr[4])) {
			 $examination_arr2[] = "Abdomin: " . $examination_arr[4];
		}
		if (!empty($examination_arr[5])) {
			 $examination_arr2[] = "Other: " . $examination_arr[5];
		}
		
		// print_r($examination_arr2);
		// echo "<pre>";
		// var_dump($examination_arr2);
		// echo "</pre>";
		/*for ($i=2; $i < 6; $i++) { 
			if (empty($examination_arr[$i])) {
				$examination_arr[$i] = "none";
			}
		}*/
		$examination = safe(implode(', ', array_filter($examination_arr2)));
	}else{
		$examination = "";
	}
	// investigation
	if (isset($_POST['investigation']) && !empty($_POST['investigation'])) {
		$investigation = safe(trim($_POST['investigation']));
	}else{
		$investigation = "";
	}
	// investigation note
	if (isset($_POST['investigation_note']) && !empty($_POST['investigation_note'])) {
		$investigation_note = safe(trim($_POST['investigation_note']));
	}else{
		$investigation_note = "";
	}
	// note
	if (isset($_POST['note']) && !empty($_POST['note'])) {
		$note = safe(trim($_POST['note']));
	}else{
		$note = "";
	}
	
	// followup_date
	if (isset($_POST['followup_date']) && !empty($_POST['followup_date'])) {
		$followup_date = safe(trim($_POST['followup_date']));
	}else{
		$followup_date = "";
	}
	// followup_time
	if (isset($_POST['followup_time']) && !empty($_POST['followup_time'])) {
		$followup_time = safe(trim($_POST['followup_time']));
		// $time_input = '11:00 AM';
		$date = DateTime::createFromFormat( 'H:i A', $followup_time);
		$followup_time = $date->format( 'H:i:s');
	}else{
		$followup_time = null;
	}
	
	// surgery_date
	if (isset($_POST['surgery_date']) && !empty($_POST['surgery_date'])) {
		$surgery_date = safe(trim($_POST['surgery_date']));
	}else{
		$surgery_date = "";
	}
	// surgery_note
	if (isset($_POST['surgery_note']) && !empty($_POST['surgery_note'])) {
		$surgery_note = safe(trim($_POST['surgery_note']));
	}else{
		$surgery_note = "";
	}
	// procedure_date
	if (isset($_POST['procedure_date']) && !empty($_POST['procedure_date'])) {
		$procedure_date = safe(trim($_POST['procedure_date']));
	}else{
		$procedure_date = "";
	}
	
	// visit_date
	if (isset($_POST['visit_date']) && !empty($_POST['visit_date'])) {
		$visit_date = safe(trim($_POST['visit_date']));
	}else{
		$visit_date = "";
	}
	// diagnosis
	$diagnosis_empty = false;
	if (isset($_POST['diagnosis']) && !empty($_POST['diagnosis'])) {
		$diagnosis = safe(trim($_POST['diagnosis']));
		$diagnosis_arr = explode(', ', $diagnosis);
	}else{
		// echo "<br>"; var_dump($_POST['diagnosis']);
		$diagnosis_empty = true;
		// $form_filled_correctly = false;
		// $error_loc .= " diagnosis ";
	}
	// treatment
	$treatment_empty = false;
	if (isset($_POST['treatment']) && !empty($_POST['treatment'])) {
		$treatment = safe(trim($_POST['treatment']));
		$treatment_arr = explode(', ', $treatment);
	}else{
		// echo "<br>"; var_dump($_POST['treatment']);
		$treatment_empty = true;
		// $form_filled_correctly = false;
		// $error_loc .= " treatment ";
	}
	// treatment_note
	if (isset($_POST['treatment_note']) && !empty($_POST['treatment_note'])) {
		$treatment_note = safe(trim($_POST['treatment_note']));
	}else{
		$treatment_note = "";
	}
	// surgery
	$surgery_empty = false;
	if (isset($_POST['surgery']) && !empty($_POST['surgery'])) {
		$surgery = safe(trim($_POST['surgery'], " ,"));//trim the space and comma if exisits
		$surgery_list = $surgery;
		$surgery_arr = explode(', ', $surgery);
	}else{
		$surgery_empty = true;
		$surgery_list = "";
		// $form_filled_correctly = false;
		// $error_loc .= " surgery ";
	}
	if ($form_filled_correctly) {
		// start transaction
		mysql_query("BEGIN");
		// procedure
		// procedure_counter
		// $procedure_counter = false;
		$procedure_query_result = 1;
		$attributes_query_result = 1;
		$patient_query_result = 1;
		$visit_query_result = 1;
		$surgery_query_result = 1;
		$diagnosis_query_result = 1;
		$treatment_query_result = 1;
		$surgeryP_query_result = 1;
		$new_visit_query_result = 1;

		//attruibutes
		$attr1 = null;
		$attr2 = null;
		$attr3 = null;
		if (isset($_POST['attr1']) &&  !empty($_POST['attr1'])) {
			$attr1 = safe(trim($_POST['attr1']));
		}
		if (isset($_POST['attr2']) &&  !empty($_POST['attr2'])) {
			$attr2 = safe(trim($_POST['attr2']));
		}
		if (isset($_POST['attr3']) &&  !empty($_POST['attr3'])) {
			$attr3 = safe(trim($_POST['attr3']));
		}
		
		$queryDel = "DELETE FROM visit_attr WHERE v_id_f = {$v_id} LIMIT 3";
		mysql_query($queryDel) or die("-1 error D" . mysql_error());
		if (!empty($attr1)) {
			$query2 = "INSERT INTO visit_attr(v_id_f, attr, attr_order) VALUES ({$v_id}, '{$attr1}', 1)";
			$query2 .= !empty($attr2)?", ({$v_id}, '{$attr2}', 2)":"";
			$query2 .= !empty($attr3)?", ({$v_id}, '{$attr3}', 3)":"";
			// echo $query2;
			$attributes_query_result = mysql_query($query2);
		}

		$procedure_note = "";
		$procedure = "";
		if (isset($_POST['procedure']) && !empty($_POST['procedure'])) {
			$procedure = safe(trim($_POST['procedure'], " ,"));//trim space and comma if exists
			//$procedure_counter = true;
			// $procedure_arr = explode(', ', $procedure);
			
			//UPDATE the procedurelist in patient table
			$proc_array = array();
			if(!empty($prev_visit_id_list)){
				$all_proc_list_set = mysql_query("SELECT `procedure` FROM visit WHERE v_id IN ({$prev_visit_id_list})") or die(-1);
				while ($proc = mysql_fetch_array($all_proc_list_set)) {
					$proc_array[] = $proc['procedure'];
				}
			}
			// print_r($proc_array);
			$proc_array[] = $procedure; // last inserted value (current visits' value)
			$new_proc_list = implode(', ', array_unique(array_filter($proc_array)));
			$pquery = "UPDATE patient set procedures_list = '{$new_proc_list}' WHERE p_id = $p_id";
			$procedure_query_result = mysql_query($pquery);
			// we want procedure note only if there is a proceudre
			// procedure_note
			if (isset($_POST['procedure_note']) && !empty($_POST['procedure_note'])) {
				$procedure_note = safe(trim($_POST['procedure_note']));
				// $procedure_note_arr = explode(', ', $procedure_note);
			}else{
				$procedure_note = "";
				// $form_filled_correctly = false;
				// $error_loc .= " procedure_note ";
			}
		}else{
			$procedure = "";
			// $form_filled_correctly = false;
			// $error_loc .= " procedure ";
		}
		$vquery = "UPDATE visit SET 
					chief_comp_n_dur = '{$chief_com_n_duration}',
					examination = '{$examination}',
					investigation = '{$investigation}',
					investigation_note = '{$investigation_note}',
					note = '{$note}',
					followup_date = '{$followup_date}',
					treatment_note = '{$treatment_note}',
					procedure_date = '{$procedure_date}',
					`procedure` = '{$procedure}',
					procedure_note = '{$procedure_note}',
					in_queue = -1
				WHERE v_id = {$v_id} AND p_id_f = {$p_id} AND in_queue = 2 AND view = 1 LIMIT 1
				";
				// echo $vquery;
		// echo $vquery . "<br />";
		$visit_query_result = mysql_query($vquery);
		// marital_status
		// pregnant
		// breast_feed
		// no_of_child
		// other_info
		//register the new visit if followup_date has been chosen
		if(!empty($followup_date)) {
			$nvquery = "INSERT INTO visit (p_id_f, visit_date, visit_time, marital_status, pregnant, breast_feed, no_of_child, other_info, fee, in_queue, u_id_f) VALUES(
				{$p_id}, '{$followup_date}', '{$followup_time}', {$marital_status}, {$pregnant}, {$breast_feed}, {$no_of_child}, '', 0, 1, {$_SESSION['user_id']})";
			$new_visit_query_result = mysql_query($nvquery);
			
			// $pquery .= $procedure_counter==true?", procedure_counter = procedure_counter + 1 ": "";	
			// procedures_list should equal to all procedures of that patient in all prev visits not only current visit update!!!		
			$pquery = "UPDATE patient SET 
						latest_followup_visit = '{$followup_date}', 
						latest_visit = '{$visit_date}'";
			$pquery .= ", queue_status = 1";
			$pquery .= " WHERE p_id = {$p_id} AND queue_status = 1 AND view = 1";
			// echo $pquery . "<br />";
			$patient_query_result = mysql_query($pquery);
		}else {
						// latest_followup_visit = '{$followup_date}', 
			// $pquery .= $procedure_counter==true?" procedure_counter = procedure_counter + 1, ": "";		
			// procedures_list should equal to all procedures of that patient in all prev visits not only current visit update!!!	
			$pquery = "UPDATE patient SET 
						latest_visit = '{$visit_date}'";
			$pquery .= ", queue_status = -1
					WHERE p_id = {$p_id} AND queue_status = 1 AND view = 1
					";
			// echo $pquery . "<br />";
			$patient_query_result = mysql_query($pquery);
		}
		if (!$diagnosis_empty){
			$dquery = "INSERT INTO visit_diagnose(v_id_f, diagnose) VALUES ";
			foreach ($diagnosis_arr as $value) {
				$dig_lists[] = " ({$v_id}, '{$value}')";
			}
			$dquery .= implode(',', $dig_lists);
			// echo $dquery . "<br />";
			$diagnosis_query_result = mysql_query($dquery);
		}
		if (!$treatment_empty){
			$tquery = "INSERT INTO visit_treatment(v_id_f, treatment) VALUES ";
			foreach ($treatment_arr as $value) {
				$treat_lists[] = " ({$v_id}, '{$value}')";
			}
			$tquery .= implode(',', $treat_lists);
			// echo $tquery . "<br />";
			$treatment_query_result = mysql_query($tquery);
		}
		if (!$surgery_empty){
			$squery = "INSERT INTO visit_surgery(v_id_f, surgery, surgery_date, surgery_note) VALUES ";
			foreach ($surgery_arr as $value) {
				$surg_lists[] = " ({$v_id}, '{$value}', '{$surgery_date}', '{$surgery_note}')";
			}
			$squery .= implode(',', $surg_lists);
			// echo $squery . "<br />";
			$surgery_query_result = mysql_query($squery);
			//update surgery list in patient table to be the latest list
			// the new surgery list already inserted as for the new visit id
			$surgeries_array = array();
			if (!empty($prev_visit_id_list)) {
				$surgery_query = "SELECT surgery FROM visit_surgery WHERE v_id_f IN ({$prev_visit_id_list}) ";
				$surgeries_set = mysql_query($surgery_query) or die("-1 s");
				while ($surgeries = mysql_fetch_array($surgeries_set)) {
					$surgeries_array[] = $surgeries['surgery'];
				}
			}
			$surgeries_array[] = $surgery_list;
			// print_r($surgeries_array);
			$new_surgeries_list = implode(', ', array_unique(array_filter($surgeries_array)));
			if (!empty($new_surgeries_list)) {
				$patquery = "UPDATE patient SET surgeries = '{$new_surgeries_list}' WHERE p_id = {$p_id}";
				$surgeryP_query_result = mysql_query($patquery);
			}
		}
		if (isset($_FILES) && count($_FILES)>0) {
			$err_msj="";
			$query_part1 = "INSERT INTO attachment(filename, file_desc, v_id_f) VALUES
									(";
			$file_desc = $_POST['p_id'] . " clinics attachment";
			$query_part2 = ", '{$file_desc}', {$v_id})";
			// upload_file($err_msj, $uploading_files, $folder='', $query_part_1, $query_part_2)
			$folder = $p_id . "/" . $v_id;
			$upload_err_msj = upload_file($err_msj, $_FILES, $folder, $query_part1, $query_part2);
			// echo $err_msj;
			if (!empty($upload_err_msj)) {
				$err_msj .= "Attachemt Error: ". $upload_err_msj;
			}
		}
		if($patient_query_result &&
			$attributes_query_result &&
			$procedure_query_result &&
			$visit_query_result &&
			$surgery_query_result &&
			$diagnosis_query_result &&
			$treatment_query_result &&
			$surgeryP_query_result &&
			$new_visit_query_result) {

				mysql_query("COMMIT");
			}else {
				mysql_query("ROLLBACK");
				echo "An error occured, Please contact the system administrator.\nError Code Pform-{$v_id}";
				exit();
			}
		// after endvisit show up the entered information to the doctor
		$direct_to_pid = '#'.$p_id;
	}//end of if form filled correctly
	else{
		$err_msj .= "Form not filled correctly, contact system administrator.";
		echo $err_msj . " at " . $error_loc;
		exit();
	}
}//end visit
	header("Location: ../index.php?page={$to}&error_loc_{$error_loc}=1&err_msj=" . $err_msj. $direct_to_pid, true, 302);
	exit();
	/*// Response codes behaviors when using 
	header('Location: /target.php', true, $code) to forward user to another page:
	$code = 301;
	// Use when the old page has been "permanently moved and any future requests should be sent to the target page instead. PageRank may be transferred."
	$code = 302; (default)
	// "Temporary redirect so page is only cached if indicated by a Cache-Control or Expires header field."
	$code = 303;
	// "This method exists primarily to allow the output of a POST-activated script to redirect the user agent to a selected resource. The new URI is not a substitute reference for the originally requested resource and is not cached."
	$code = 307;
	// Beware that when used after a form is submitted using POST, it would carry over the posted values to the next page, such if target.php contains a form processing script, it will process the submitted info again!
	// In other words, use 301 if permanent, 302 if temporary, and 303 if a results page from a submitted form.
	// Maybe use 307 if a form processing script has moved.*/
?>