<link rel="stylesheet" type="text/css" href="../css/jquery.timepicker.min.css">
<script type="text/javascript" src="../js/modernizr.custom.79639.js"></script>
<script type="text/javascript" src="../js/jquery.timepicker.min.js"></script>
<div class="contact row">

  <!-- BEGIN SEARCH TABLE PORTLET-->
  
<!-- END SEARCH TABLE PORTLET-->
  <div class="col-md-2">
  </div>
</div>

<div class="row">
   
   <!-- BEGIN SAMPLE TABLE PORTLET-->
   <div class="col-md-12">
    
    <?php  
        $today = date('Y-m-d');
    ?>
  <div class="panel panel-info">
    <div class="panel-heading">
      <div class="caption panel-title">
        <span class='fa fa-stethoscope table_head_icon'></span> Clinic Queue - <?php echo $today; ?>
      </div>
    </div>
    <div class="panel-body resposive">
  <table class="rtable rtable--flip table table-bordered table-condensed" id="queue">
  <thead>
        <th>#</th>
    <th>Full Name</th>
    <th>Code</th>
    <th>Sex</th>
    <th>Phone Number</th>
    <th>Visit Time</th>
        <th>Fee</th>
    <th>Status</th>
  </thead>
  <?php  

  // marital_status, pregnant, breast_feed, no_of_child, other_info
    $queue_list_set = get_queue_list($today);
    $table_data = "";
    $i = 1;
    while ($today_queue = mysql_fetch_assoc($queue_list_set)) {
        $sex ="";
        if ($today_queue['sex']=='1') {
            $sex = "<div style='text-align:center'><i class='fa fa-mars'><span style='display:none'>1</span></i></div>"; 
        }else if ($today_queue['sex']=='2') {
            $sex = "<div style='text-align:center'><i class='fa fa-venus'><span style='display:none'>2</span></i></div>";    
        }else if ($today_queue['sex']=='3') {
            $sex = "<div style='text-align:center'><i class='fa fa-venus-mars'><span style='display:none'>3</span></i></div>";   
        }
        $in_queue = "";
        if ($today_queue['in_queue']=='1') {
            $in_queue = "<div style='text-align:center'><i style='font-size:1.3em' class='inqu_icn1 fa fa-user-circle-o'><span style='display:none'>1</span></i></div>"; 
        }else if ($today_queue['in_queue']=='2' || $today_queue['in_queue']=='-1') {
            $in_queue = "<div style='text-align:center'><i style='font-size:1.3em; color:green' class='inqu_icn2 fa fa-check-circle-o'><span style='display:none'>2</span></i></div>";    
        }

        $visit_time = '';
        if (!empty($today_queue['visit_time'])) {
          $date = DateTime::createFromFormat( 'H:i:s', $today_queue['visit_time']);
          $visit_time = $date->format( 'h:i A');
        }
       // var_dump($date);

        $danger_class = $today_queue['fee']==0?"class='danger'":"";
        $success_class = $today_queue['in_queue']!=1?"class='success'":"";
        $table_data .= "<tr {$success_class} id={$today_queue['v_id']} data-marital_status='{$today_queue['marital_status']}' data-pregnant='{$today_queue['pregnant']}' data-breast_feed='{$today_queue['breast_feed']}' data-no_of_child='{$today_queue['no_of_child']}' data-other_info='{$today_queue['other_info']}' data-in_queue='{$today_queue['in_queue']}'>
                            <td>
                            {$i}
                            </td>
                            <td>
                            {$today_queue['fullname']}
                            </td>
                            <td>
                            {$today_queue['p_id_f']}
                            </td>
                            <td>
                            {$sex}
                            </td>
                            <td>
                            {$today_queue['phone']}
                            </td>
                            <td>
                            {$visit_time}
                            </td>
                            <td {$danger_class}>
                            {$today_queue['fee']}
                            </td>
                            <td>
                            {$in_queue}
                            </td>

                        </tr>";
                        $i++;
    }
    echo $table_data;
  ?>
  <tfoot>
        <th>#</th>
    <th>Full Name</th>
        <th>Code</th>
        <th>Sex</th>
        <th>Phone Number</th>
        <th>Visit Time</th>
        <th>Fee</th>
        <th>Status</th>
  </tfoot>
  
  </table>

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <form class="form-horizontal" name="sendToDr_form" action="index.php" id="sendToDr_form">
        <div class="modal-body">
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <div class="modal fade" id="newpatient" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

</div>

    </div>
  </div>
  <!-- <div class="row" >
    <div class="col-md-12" >
      <a href="#newpatient" class="btn btn-primary" data-toggle='modal'><i class="fa fa-plus-square"></i> add new patient</a>
      <a href="javascript:;" class="btn btn-danger" id="cleaner"><i class="fa fa-circle-o-notch"></i> clean queue</a>
    </div>
  </div> -->
  </div>
  
</div>

<!-- <script src="../js/lightbox.js"></script>
<script>
    lightbox.option({
      'positionFromTop': 30
    })
</script> -->
<script type="text/javascript">

  $(document).ready(function() {

  var columnDefs = [{
    title: "#"
  }, {
    title: "Full Name"
  }, {
    title: "Code"
  }, {
    title: "Sex"
  }, {
    title: "Phone Number"
  },{
    title: "Visit Time"
  }, {
    title: "Fee", render: $.fn.dataTable.render.number( ',', '.', 0 ) 
  }, {
    /*title: "Status", render: function (data, type, row) {
                        return row[6] + " " + row[3];
                        // console.log(row);
                        // return data;
                    } */
    title:"Status"
  }];

  var queue;

  queue = $('#queue').DataTable({
    processing: true,
    "pagingType": "full_numbers",
    columns: columnDefs,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "deferRender": true,
    dom: '<"top row"<"col-md-4"l><"col-md-5"B><"col-md-3"f>><"row"<"col-md-12"rt>><"bottom row"<"col-md-4"i><"paginationright col-md-8"p>><"clear">',
    select: 'single',
    buttons: [/*{
          text: 'pdf',
                extend: 'pdfHtml5',
                download: 'open',
                message: 'pdf created by Nawras Clinical Service'
            },*/{
      text: '<i data-toggle="tooltip" title="to Doctor" id="add_to_dr_btn_icon" class="fa fa-sign-in fa-1-5x"></i>',
      extend: 'selected',
      action: function(e, dt, node, config) {
        addClick(this, dt, node, config)
      }
    }, {
      extend: 'selected',
      text: '<i data-toggle="tooltip" title="Cancel" class="fa fa-ban fa-1-5x"></i>',
      action: function(e, dt, node, config) {
        var rows = dt.rows({
          selected: true
        }).count();

        deleteClick(this, dt, node, config)
      }
    }, {
      extend: 'selected',
      text: '<i data-toggle="tooltip" title="postpon visit" id="postpon_v_btn_icon" class="fa fa-pencil-square-o fa-1-5x"></i>',
      action: function(e, dt, node, config) {
        var rows = dt.rows({
          selected: true
        }).count();
        //above code just to indicate how many rows are selected
// console.log(rows);
// console.log(dt.rows({selected: true}));
        postponClick(this, dt, node, config)
      }
    }, {
    extend: 'print',
    text: '<i data-toggle="tooltip" title="Print" class="fa fa-print fa-1-5x"></i>',
        message: 'This print was produced by Nawras Clinical System',
        exportOptions: {
            columns: ':visible'
        }
    }, {
      text: '<i data-toggle="tooltip" title="hide columns" class="fa fa-eye-slash fa-1-5x"></i>',
      extend:'colvis'
    }]
  });

  //---------Function to Display modal addToDrRoomButton---------

  function addClick(pointer, oTT, button, conf) {
    var adata = oTT.rows({
      selected: true
    });
// console.log('adata', adata)
    // console.log(adata.data()[0]);
    // console.log(adata.data()[0][0]);
    // console.log(adata.ids()[0]);

        if ($("tr.selected").data("in_queue") != '1') {
            swal({
                title: "Operation Failed!",
                text: "This patient is already in Doctors' Room.",
                type: "error",
                confirmButtonColor: "#C9302C"
            });
            return;
        }
        /*
$("tr.selected").data("marital_status")
$("tr.selected").data("pregnant")
$("tr.selected").data("breast_feed")
$("tr.selected").data("no_of_child")
$("tr.selected").data("other_info")
$("tr.selected").data("in_queue")
        */
    var id = $("tr.selected").attr("id");
    var data = "";
    var patient = pointer;
    var isfemale = adata.data()[0][3].indexOf("venus")>0; // >0 "true" "female", <0 "false" "male or trans"
    data += "<div class='row top_label_modal col-md-12'>";
    data += "<div class='col-md-6'>" + columnDefs[2].title + " : <span class='label_data'>"+adata.data()[0][2]+"</span></div>";
    data += "<div style='text-align:right' class='col-md-6'>" + columnDefs[1].title + " : <span class='label_data'>"+adata.data()[0][1]+"</span></div>";
    
    data += "</div>";
    female_n_childnum_div_style = "style='display:none'";
    var marital_selected1 = "";
    var marital_selected2 = "";
    var marital_selected3 = "";
    var marital_selected4 = "";
    switch($("tr.selected").data("marital_status"))
    {
        case 1:
            marital_selected1 = "selected";
            break;
        case 2:
            marital_selected2 = "selected";
            female_n_childnum_div_style = "";
            break;
        case 3:
            marital_selected3 = "selected";
            female_n_childnum_div_style = "";
            break;
        case 4:
            marital_selected4 = "selected";
            female_n_childnum_div_style = "";
            break;

    }
    data += "<input type='hidden' value='" + id + "' name='v_id' id='v_id' >";
    data += "<input type='hidden' value='" + adata.data()[0][2] + "' name='p_id_f' >";
    data += "<input type='hidden' value='" + isfemale + "' name='sex' >";
    data +=`
<div class="form-group">
  <label class="col-md-4 control-label" for="maritalst">Marital Status</label>
  <div class="col-md-7">
    <select id="maritalst" required="required" name="maritalst" class="form-control">`;
      data += "<option " + marital_selected1 + " value='1'>Single</option>";
      data += "<option " + marital_selected2 + " value='2'>Married</option>";
      data += "<option " + marital_selected3 + " value='3'>Divorced</option>";
      data += "<option " + marital_selected4 + " value='4'>Widowed</option>";
data += `</select>
  </div>
</div>`;
data += "<div id='female_n_childnum_div' " + female_n_childnum_div_style + " >";
if (isfemale) {
    var check_preg = $("tr.selected").data("pregnant")==1?"checked":" ";
    var check_brfeed = $("tr.selected").data("breast_feed")==1?"checked":" ";

data +=`<div class="form-group">
  <label class="col-md-4 control-label" for="preg">Pregnant? </label>
  <div class="col-md-7">
  <div class="checkbox toggledboxdiv">
      <input type="checkbox" `;
    data += check_preg;
    data += ` class="toggledbox" name="preg" id="preg" value="1">
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="breastfeed">Breast Feeding </label>
  <div class="col-md-7">
  <div class="checkbox toggledboxdiv">
      <input type="checkbox" `;
    data += check_brfeed;
    data += ` class="toggledbox" name="breastfeed" id="breastfeed" value="1">
    </div>
  </div>
</div>`;
}

data +=`<div class='form-group' id='childnumdiv' >
  <label class='col-md-4 control-label' for='childnum'>Number of Children</label>  
  <div class='col-md-7'>
  <span class="input-number-decrement">–</span><input class="input-number" name='childnum' id='childnum' type="number" min="0" max="20" value='`;
  data += $("tr.selected").data("no_of_child");
  data+= `'><span class="input-number-increment">+</span>
  </div>
</div>
</div> <!-- end female_n_childnum_div -->`;

data +=`
<!-- visit attributes -->
<div class="form-group">
  <label class="col-md-4 control-label" for="attr1">Attribute1</label>
  <div class="col-md-7">
    <select id="attr1" name="attr1" class="form-control">`;
      data += "<option id='0' value='0'>None</option>";
      data += "<option id='1' value='Cardiac'>Cardiac</option>";
      data += "<option id='2' value='Vascular'>Vascular</option>";
      data += "<option id='3' value='Thoracic'>Thoracic</option>";
data += `</select>
  </div>
</div>`;
data +=`
<div class="form-group" id="attr2list" style="display:none">
  <label class="col-md-4 control-label" for="attr2">Attribute2</label>
  <div class="col-md-7">
    <select id="attr2" name="attr2" class="form-control">`;
      
data += `</select>
  </div>
</div>`;

data +=`
<div class="form-group" id="attr3list" style="display:none">
  <label class="col-md-4 control-label" for="attr3">Attribute3</label>
  <div class="col-md-7">
    <select id="attr3" name="attr3" class="form-control">`;
      
data += `</select>
  </div>
</div>
<!-- end of visit attributes -->`;

data+=`<div class='form-group'>
  <label class='col-md-4 control-label' for='other_inf'>Other Information</label>  
  <div class='col-md-7'>
        <textarea id='other_inf' name='other_inf'
        placeholder='Other Information' class='form-control'>`;

        
  data += $("tr.selected").data("other_info");
data += `</textarea>
  </div>
</div>

<div class='form-group'>
  <label class='col-md-4 control-label' for='visit_fee'>Visit Fee</label>  
  <div class='col-md-7'>
  <div class='input-group'>
  <input id='visit_fee' required="required" name='visit_fee' placeholder='fee' class='form-control' min='0' type='number' `;
  data += adata.data()[0][6]>0?"value='"+adata.data()[0][6]+"'":" ";
  data+= ` >
    <span class="input-group-addon">IQD</span>
  </div>
  </div>
</div>`;


    $('#myModal').on('show.bs.modal', function() {
        $('#myModal').find('form').attr('id','sendToDr_form');
        $('#myModal').find('form').attr('name','sendToDr_form');
        $('#myModal').find('.modal-title').html('Send to Doctor\'s Room');
        $('#myModal').find('.modal-body').html( data);
        $('#myModal').find('.modal-footer').html("<button type='submit' class='btn btn-primary' id='addToDrRoombtn'>Confirm</button>");
    });

    $('#myModal').modal('show');
        $('.toggledbox').bootstrapToggle({
            on: 'Yes',
            off: 'No',
            onstyle: 'success'
        });
        inputNumber($('.input-number'));
  };

  // add row functionality
  // $(document).on('click', '#addToDrRoombtn', function() {
    $(document).on('submit', '#sendToDr_form', function(e) {
    
    $("#add_to_dr_btn_icon").removeClass("fa-sign-in");
    $("#add_to_dr_btn_icon").addClass("fa-cog fa-spin");
    e.preventDefault();
    var $form = $(e.target);
    $.ajax({
        // url: $form.attr('action'),
        url: "../includes/ajax/patient_operation.php",
        type: 'POST',
        data: $form.serialize()+"&addPtoDr=true",
        success: function(result) {
            if (result == -1) {
                swal({
                    title: "Operation Failed!",
                    text: "The operation failed, please write information correctly when filling the form.",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
            }else if(result == -2) {
                swal({
                    title: "Operation Failed!",
                    text: "This patient already added to queue.",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                
            }else {
              // console.log("#"+$("#v_id").val()+" .inqu_icn1");
                $("#"+$("#v_id").val()+" .inqu_icn1").removeClass("fa-user-circle-o");
                $("#"+$("#v_id").val()+" .inqu_icn1").addClass("fa-check-circle-o");

                $("#"+$("#v_id").val()+" .danger").removeClass("danger");

                $("#"+$("#v_id").val()).addClass("success");
                $("#"+$("#v_id").val()).data("in_queue", "2");

                var newFee = $("#visit_fee").val();
                var checked = "<div style='text-align:center'>"+$("#"+$("#v_id").val()+" .inqu_icn1")[0].outerHTML+"</div>";
                var adata = queue.row({selected: true}).data();
                adata[6] = newFee;
                adata[7] = checked;
                queue.row( {selected: true} ).data( adata );
                queue.draw();
            }

            $("#add_to_dr_btn_icon").removeClass("fa-cog fa-spin");
            $("#add_to_dr_btn_icon").addClass("fa-sign-in");
        }
    });
    $('#myModal').modal('hide');

    /*I use row() not rows() because I want to edit only one row and only one row is selectable
    otherwise I should use rows and loop through each selected row*/
    /*var adata = queue.rows({
      selected: true
    }).data()[0];*/

    // below copies the row data into adata variable, so after updating it you should give the data back to
    // queue object to update its' cache
    /*var adata = queue.row({
      selected: true
    }).data();*/
    // console.log(sendToDr_form);
    // console.log(sendToDr_form[1].value);
    // console.log(adata);
    /*for (i in adata) {
        adata[i] = sendToDr_form[i].value;
    }
    console.table(adata);
   queue.row( {selected: true} ).data( adata );
       queue.draw();*/
  });

  //---------Function to Display modal editButton---------

  function postponClick(pointer, oTT, button, conf) {
    if ($("tr.selected").data("in_queue") != '1') {
            swal({
                title: "Operation Failed!",
                text: "This patient is already in Doctors' Room.",
                type: "error",
                confirmButtonColor: "#C9302C"
            });
            return;
        }

    var adata = oTT.rows({
      selected: true
    });

    var id = $("tr.selected").attr("id");

    var data = "";
    var queue = pointer;
    
    data += "<input type='hidden' value='" + id + "' name='v_id' id='v_id' >";
    data += "<input type='hidden' value='" + adata.data()[0][2] + "' name='p_id_f' >";
    
    data += `
    <div class="form-group">
   <label for="datetimepicker2" class="col-md-4 control-label">Postpon the visit To</label>
   <div class="col-md-7">
      <div class='input-group date datepick' id='datetimepicker2'>
      <input type='text' autocomplete="off" class="form-control" name="postponed_date" id="postponed_date" required="required" />
      <span class="input-group-addon">
         <span class="glyphicon glyphicon-calendar"></span>
      </span>
         </div>
   </div>
</div>
<div class="form-group">
   <label for="datetimepicker4" class="col-md-4 control-label">Visit Time</label>
   <div class="col-md-7">
        <div class='input-group' id='thetime'>
            <input type='text' autocomplete="off" class="form-control" name="visit_time" id="datetimepicker4" />
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-time"></span>
            </span>
        </div>
   </div>
</div>
    `

    $('#myModal').on('show.bs.modal', function() {

      $('#myModal').find('form').attr('id','postpon_v_form');
      $('#myModal').find('form').attr('name','postpon_v_form');
      $('#myModal').find('.modal-title').html('Postpon Visit');
      $('#myModal').find('.modal-body').html(data);
      $('#myModal').find('.modal-footer').html("<button type='submit' data-content='remove' class='btn btn-primary' id='editRowBtn'>Update</button>");
    });

    $('#myModal').modal('show');
    $('#myModal input[1]').focus();
    $('#datetimepicker2').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true,
            weekStart: 5,
            startDate: "+1d",
            daysOfWeekHighlighted:'5'
        });
    $("#datetimepicker4").timepicker({
          'minTime' : '7:00am',
        'maxTime' : '12:00am',
        'step' : 5,
        'timeFormat' : 'g:i a'
        });

  };

  // edit row functionality
  $(document).on('submit', '#postpon_v_form', function(e) {
    $("#postpon_v_btn_icon").removeClass("fa-pencil-square-o");
    $("#postpon_v_btn_icon").addClass("fa-cog fa-spin");
    e.preventDefault();
    var $form = $(e.target);
    $.ajax({
        // url: $form.attr('action'),
        url: "../includes/ajax/patient_operation.php",
        type: 'POST',
        data: $form.serialize()+"&postponv=true",
        success: function(result) {
            if (result == -1) {
                swal({
                    title: "Operation Failed!",
                    text: "The operation failed, please write information correctly when filling the form.",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
            }else if(result == -2) {
                swal({
                    title: "Operation Failed!",
                    text: "This patient already added to queue.",
                    type: "error",
                    confirmButtonColor: "#C9302C"
                });
                
            }else {
                // i change in_queue just to make sure nothing wrong will happen if
                // this operation was aborted somehow.
                $("#"+$("#v_id").val()).data("in_queue", "2");

                queue.row('.selected').remove().draw( false );
            }

            $("#postpon_v_btn_icon").removeClass("fa-cog fa-spin");
            $("#postpon_v_btn_icon").addClass("fa-pencil-square-o");
        }
    });
    // edit row function needs to go here, here use your ajax then hide the modal
    $('#myModal').modal('hide');

  });

  //---------Function to Display modal deleteButton---------

  function deleteClick(pointer, oTT, button, conf) {
    if ($("tr.selected").data("in_queue") != '1') {
            swal({
                title: "Operation Failed!",
                text: "This patient is already in Doctors' Room.",
                type: "error",
                confirmButtonColor: "#C9302C"
            });
            return;
        }

    var adata = oTT.rows({
      selected: true
    });

    swal({
            title: "Delete Visit",
            text: "Are you sure you want to delete this visit?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#C9302C"
        },
        function(){
        var v_id = $("tr.selected").attr("id");
        var p_id = adata.data()[0][2];
            $.ajax({
                url: "../includes/ajax/patient_operation.php",
                type: 'POST',
                data: "v_id="+v_id+"&p_id_f="+p_id+"&deletev=true",
                success: function(result) {
                    if (result == -1) {
                        swal({
                            title: "Operation Failed!",
                            text: "Somthing Wrong Happened, if problem persist please contact system administrator.",
                            type: "error",
                            confirmButtonColor: "#C9302C"
                        });
                    }else {
                        queue.row('.selected').remove().draw( false );
                        swal("Done!","Operation completed successfully.","success");
                    }
                }
            });
        });
    
  };

// Setup - add a text input to each footer cell
    $('#queue tfoot th').each( function () {
    // $('#y tfoot th').not(":eq(0),:eq(-1)").each( function () {
        // var title = $.trim($(this).text());
        // console.log($(this));
        // $(this).html( '<input type="text" class="form-control input-sm" placeholder="Search '+$.trim(title)+'" />' );
        if ($(this).html() == 'Sex') {
            list = `<select class="form-control input-sm">
            <option value=""></option>
            <option value="1">Male</option>
            <option value="2">Female</option>
            <option value="3">Transgender</option>
        </select>`;
            $(this).html(list);
        }else{
            $(this).html( '<input type="text" class="form-control input-sm" placeholder="Type Here" />' );
        }
        // var disable='';
        // var placeholder = 'placeholder="Search here"';
        // if (title=='tools' || title=='#') { disable="disabled=disabled readonly "; placeholder='style="background-color:transparent; border-color:transparent; display:none"'; }
          // $(this).html( '<input type="text" '+ disable + ' class="form-control input-sm"'+placeholder+' />' );
    } );
 
    // Apply the search
    var timeout = null;

    queue.columns().every( function () {
        var that = this;
        var ind = this.index();
        //if (this.index()==0 || this.index()==5 ) {return;}//to disable search in first at last col
        $( 'input, select', this.footer() ).on( 'keyup change', function () {
            search_val = this.value;
            // var that = this; 
            // console.log(search_val);
            if (timeout !== null) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function () {
              that
                  .search( search_val )
                  .draw();
            }, 700);
        } );
    } );


    $(document).on('click', '#cleaner', function(event) {
      $('#cleaner > :first-child').addClass('spin-backw');
      // dabi hamu visitakan clean bkren ka queueyan != -1, wata awai queuei yak yan dwa dabo delete bi
    });


    $(document).on("change", "#maritalst", function (e) {
        // console.log(e);
        if (this.value>1) {
            $("#female_n_childnum_div").slideDown('slow');
            $("#childnum").val(0);
            $('#childnum').attr('required', 'required');
        }else{
            $("#female_n_childnum_div").slideUp('slow');
            $("#childnum").val('');
            $('#childnum').removeAttr('required');
        }
    });

    $(document).on("change", "#attr1", function (e) {
        attr1_option = $(this).find('option:selected');
        if (attr1_option.attr('id')>1) {
            /*$.getJSON or $.post is a shorthand Ajax function but speciallized for JSON parsed data */
            $.post("../includes/ajax/attributes_list.php?","attr1=1&id="+attr1_option.attr('id'), function(data) {
            $("#attr2 option").remove();
            $("#attr3 option").remove();
            /*
              $.each(data, function(index, value){
              console.log(index);
              console.log(value);
              this wata value awja la value ka arrayaki dikaya ama dalein .value w .name
            */
            $.each(data, function(){
              
              $("#attr2").append('<option value="'+ this.value +'">'+ this.name +'</option>')
            });
            
            $("#attr2list").slideDown('slow');
            $("#attr3list").slideUp('slow');
          }, "json");
            
        }else if(attr1_option.attr('id')==='1'){
          $.post("../includes/ajax/attributes_list.php?","attr1=1&id="+attr1_option.attr('id'), function(data) {
            $("#attr2 option").remove();
            $("#attr3 option").remove();
            data2 = data['data2'];
            data3 = data['data3'];
            $.each(data2, function(){
              
              $("#attr2").append('<option value="'+ this.value +'">'+ this.name +'</option>')
            });

            $.each(data3, function(){
              
              $("#attr3").append('<option value="'+ this.value +'">'+ this.name +'</option>')
            });
            $("#attr2list").slideDown('slow');
            $("#attr3list").slideDown('slow');
          }, "json");
            
        } else{
            $("#attr2list").slideUp('slow');
            $("#attr3list").slideUp('slow');
            $("#attr2 option").remove();
            $("#attr3 option").remove();
        }
    });
  
  $(document).on("change", "#attr2", function (e) {
        /*
          three cases:
          Pediatric
          Venous Diseases
          Adult
        */
        attr2_option = $(this).find('option:selected');
        if (attr2_option.attr('value')==='Pediatric' || attr2_option.attr('value')==='Venous Diseases' || attr2_option.attr('value')==='Adult') {
          /*$.getJSON or $.post is a shorthand Ajax function but speciallized for JSON parsed data */
          $.post("../includes/ajax/attributes_list.php?","attr2=1&id="+attr2_option.attr('value'), function(data) {
          $("#attr3 option").remove();
          $.each(data, function(){
            
            $("#attr3").append('<option value="'+ this.value +'">'+ this.name +'</option>')
          });
          
          $("#attr3list").slideDown('slow');
        }, "json");
      }else{
        $("#attr3list").slideUp('slow');
        $("#attr3 option").remove();
      }
            
    });

   $('.btn-group i[data-toggle="tooltip"]').tooltip();
});
</script>