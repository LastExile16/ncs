<?php

require_once './includes/connection.php';
if(!isset($_SESSION))
{
    session_start();
}
require_once './includes/functions.php';
if (!logged_in()) 
{
    // if (isset($_SESSION['admin'])) {
        header("Location: login.php");
        exit;
    // }
}elseif (!is_doctor()) {
	header("Location: ./sec/");
        exit;
}
?>

<?php
require_once 'header.php';
?>

<?php
// I have changed it to manualy include the file not through variable 'page' so that no other pages can be viewed rather than what I want.
if (isset($_GET['page']) AND file_exists("includes/".$_GET['page'] . ".php")) {
	switch ($_GET['page']) {
		case "dashboard":
			include_once "includes/dashboard.php";
			break;
		case "current_p":
			include_once "includes/current_p.php";
			break;
		case "all_p":
			include_once "includes/all_p.php";
			break;
		case "drcalender":
			include_once "includes/drcalender.php";
			break;
		case "my_profile":
			include_once "includes/my_profile.php";
			break;
		case "med_rep":
			include_once "includes/med_rep.php";
			break;
		case "expenses":
			include_once "includes/expenses.php";
			break;
		case "users":
			include_once "includes/users.php";
			break;
		
		default:
			include_once "includes/dashboard.php";
			break;
	}
	
}
else {
	include_once 'includes/dashboard.php';
}
?>


<?php
require_once 'footer.php';
?>

<?php
mysql_close();
?>