<?php
if(!isset($_SESSION))
{
    session_start();
}

require_once "./includes/connection.php";
require_once "./includes/functions.php";
if (logged_in()) 
{
    if ($_SESSION['role'] == 9) {
        header('Location: index.php');
        exit;
    }
    elseif($_SESSION['role']==1){
        header('Location: ./sec/index.php');
        exit;
    }
}
$login_error_message = "";
$forgot_error_message = "";
$successful_reset = "";
    // login
    if (isset($_POST['login']) && 
        isset($_POST['username']) && 
        !empty($_POST['username']) &&
        isset($_POST['password']) &&
        !empty($_POST['password'])
        )
    {
        $username = safe(trim($_POST['username']));
        $password = safe(trim($_POST['password']));

        $query = "SELECT * FROM user WHERE username='$username' AND view=1 LIMIT 1";
        $result = mysql_query($query) or die("login".mysql_error());
        
        if(mysql_num_rows($result) == 1)
        {
            $admin = mysql_fetch_array($result);
            $db_pass = $admin['password'];
            $password = sha1(md5($password));
            // echo $password;
            // echo "<br>".$admin['password'];
            if($password == $admin['password'])
            {
                // if ($admin['role'] == 1) //if user is admin
                // {
                    clean_queue();
                    $admin_browser = $_SERVER['HTTP_USER_AGENT'];
                    $_SESSION['login_string'] = hash('sha512', $password.$admin_browser);
                    $_SESSION['user_id'] = $admin['u_id'];
                    $_SESSION['name'] = $admin['username'];
                    $_SESSION['fullname'] = $admin['fullname'];
                    $_SESSION['role'] = $admin['role'];
                    if ($_SESSION['role'] == 9) {
                        $ip = $_SERVER['REMOTE_ADDR'];
                        if (!filter_var($ip, FILTER_VALIDATE_IP)) exit();
                        $file = './uploads/.htaccess';
                        $current = file_get_contents($file);
                        $current .= "\nallow from ".$_SERVER['REMOTE_ADDR'];
                        // BRWA nakam ba kalk bet chunka hamu userakan ba yak ip public daxl dabn kawata allowing that ip means allowing everyone in that area, secondly I need to remove once they logout.
                        file_put_contents($file, $current);

                        header('Location: index.php');
                        exit;
                    }
                    elseif($_SESSION['role']==1){
                        header('Location: ./sec/index.php');
                        exit;
                    }
                // }
            }
            else
            {
                $login_error_message .= "Wrong user credentials, please check your user name and/or password.";
            }
            
        }
        else
        {
            $login_error_message .= "Wrong user credentials, please check your user name and/or password.";
        }

    }
    elseif (isset($_POST['reset'])) {
        if (isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $newpass = generateSalt(6);
            $hashed_newpass = sha1(md5($newpass));
            $query = "UPDATE user SET password = '{$hashed_newpass}' WHERE email='{$_POST['email']}'";
            $resulted_update = mysql_query($query) or die("couldn't update password data");
            if (mysql_affected_rows()>0) {

                $to = $_POST['email'];
                // Subject
                $subject = 'Password Reset Request';

                // Message
                $message = "
                <html>
                <head>
                  <title>Password Reset Request</title>
                </head>
                <body>
                  <p style='background-color:lightblue'>Your password has been changed, please keep your password safe</p>
                  <table>
                    <tr>
                      <td>new password: </td><td style='background-color:yellow'>{$newpass}</td>
                    </tr>
                  </table>
                  <br />
                  <br />
                  <br />
                  <sub>sent from NCS system password manager</sub>
                </body>
                </html>";
                // To send HTML mail, the Content-type header must be set
                $headers[] = 'MIME-Version: 1.0';
                $headers[] = 'Content-type: text/html; charset=utf-8';

                // Additional headers
                // $headers[] = "To: {$_POST['email']}";
                $headers[] = 'From: noreply@nawrascs.com';
                $headers[] = 'Bcc: nawras.nzar@gmail.com';

                // Mail it
                if(mail($to, $subject, $message, implode("\r\n", $headers)))
                {
                    $successful_reset = "we've sent a new password to your email address<br>please check you email inbox and spam folder.";
                }else{
                    $forgot_error_message .= "processing password failed, please try again!<br>";
                }
            }//end-if update query was successful 
            else{
                $forgot_error_message .= "The email address is not recognized!";
            }
        }else {
            $forgot_error_message .= "please enter a valid email address";
        }
    }
?>


<!DOCTYPE html>
<html lang="en">

<head>
<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
<link rel="icon" type="image/png" href="favicon-32x32.png?v=1" sizes="32x32">
<link rel="icon" type="image/png" href="favicon-16x16.png?v=1" sizes="16x16">
<link rel="manifest" href="manifest.json">
<link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">
<meta name="apple-mobile-web-app-title" content="NCS">
<meta name="application-name" content="NCS">
<meta name="theme-color" content="#ffffff">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Login</title>
    <link href="css/font-awesome/css/font-awesome.min.css" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="css/bootstrap-3.1.1.min.css">
    <style type="text/css">

        body
        {
            background-color: #F8F8F8;
        }

        .login-panel
        {
            margin-top: 7em;
        }

        .errmsj
        {
            margin-top: 2em;
        }
    </style>
    <script src="js/jquery.min.js"></script>
    <script src="js/unused js/bootstrap-3.1.1.min.js"></script>

</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">

                    <?php 
                        if (isset($_GET['fpass']) && is_numeric($_GET['fpass']) && $_GET['fpass']==1) {
                    ?>
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Write Your Email Address</h3>
                        </div>
                        <div class="panel-body">
                                <span class="alert alert-danger small">Only emails registered in the NCS system are valid</span>
                            <form class="form" method="POST" action="">
                            <fieldset>
                                <div class="form-group input-group <?php echo !empty($forgot_error_message)? "has-error":""; ?>">
                                <span class="input-group-addon"><i class="fa fa-arrow-up"></i></span>
                                    <input class="form-control" required="required" autocomplete="false" placeholder="Email Address" name="email" type="email" autofocus>
                                </div>
                                
                                <button class="btn btn-lg btn-success btn-block" name="reset">Reset Password</button>
                            </fieldset>
                            <?php echo !empty($forgot_error_message)? "
                                <div class=\"form-group errmsj\">
                                    <div>
                                        <div class=\"alert alert-danger\">$forgot_error_message</div>
                                    </div>
                                </div>":""; 
                                //if successful
                                echo !empty($successful_reset)? "
                                <div class=\"form-group errmsj\">
                                    <div>
                                        <div class=\"alert alert-success\">$successful_reset</div>
                                    </div>
                                </div>":""; 
                            ?>
                        </form>
                        <a href="login.php">&lt; back</a>
                <?php
                        }else{

                ?>
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sign In!</h3>
                        </div>
                        <div class="panel-body">
                        <form class="form" method="POST" action="">
                            <fieldset>
                                <div class="form-group input-group <?php echo !empty($login_error_message)? "has-error":""; ?>">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input class="form-control" required="required" autocomplete="false" placeholder="User Name" name="username" type="text" autofocus>
                                </div>
                                <div class="form-group input-group <?php echo !empty($login_error_message)? "has-error":""; ?>">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input class="form-control" required="required" autocomplete="false" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <button class="btn btn-lg btn-success btn-block" name="login">Sign In</button>
                                <a href="login.php?fpass=1">Forgot Password?</a>
                            </fieldset>
                            <?php echo !empty($login_error_message)? "
                                <div class=\"form-group errmsj\">
                                    <div>
                                        <div class=\"alert alert-danger\">$login_error_message</div>
                                    </div>
                                </div>":""; 
                            ?>
                        </form>
                        <?php }//end else ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
