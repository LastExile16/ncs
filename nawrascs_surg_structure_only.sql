-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 27, 2021 at 09:59 PM
-- Server version: 5.6.41-84.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nawrascs_hemnv2`
--

-- --------------------------------------------------------

--
-- Table structure for table `attachment`
--

CREATE TABLE `attachment` (
  `a_id` int(11) NOT NULL,
  `filename` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'name of the file',
  `file_desc` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'patient attachment' COMMENT 'patient attachment',
  `uploaded_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date of atachment upload',
  `v_id_f` int(11) UNSIGNED NOT NULL COMMENT 'visit foreign key',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `exp_id` int(11) NOT NULL,
  `expense_name` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'name of the expense',
  `amount` int(10) UNSIGNED NOT NULL COMMENT 'amount of expense',
  `exp_date` date NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `p_id` int(11) UNSIGNED NOT NULL COMMENT 'key: yymmdd[session(autonumber)], autonum: count today visits then start increment, plan failed',
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'patient name',
  `sex` tinyint(1) NOT NULL COMMENT '1:male, 2:female, 3:transgender',
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'patient address',
  `dob` int(4) DEFAULT NULL COMMENT 'year of birth',
  `phone` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'phone number, max is 2 numbers',
  `occupation` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'patient occupation',
  `past_hx` text COLLATE utf8_unicode_ci COMMENT 'past history (should be merged in each visit not replaced and use nl2b function)',
  `family_hx` text COLLATE utf8_unicode_ci COMMENT 'family history (should be merged in each visit)',
  `drug_hx` text COLLATE utf8_unicode_ci COMMENT 'drug history (should be merged ...)',
  `other_hx` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other history to mention (should be merged ...)',
  `latest_visit` date DEFAULT NULL COMMENT 'store latest visit if available (i added this to increase query performance for "patients" page) )',
  `latest_followup_visit` date DEFAULT NULL COMMENT 'latest_followup_visit to show expected visit in current_p',
  `surgeries` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'here we combine all surgeries from visit_surgery table here to show it easily in all_p',
  `procedures_list` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'list of procedures so that I don''t need to query from visits to show in patient table',
  `queue_status` tinyint(1) NOT NULL DEFAULT '-1' COMMENT '1:in queue, -1:not in queue; current queue status',
  `u_id_f` int(11) NOT NULL COMMENT 'user foreign key',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted',
  `update_record` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='later make view index';

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `u_id` int(11) NOT NULL,
  `fullname` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:secretary, 9:admin',
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email address',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visit`
--

CREATE TABLE `visit` (
  `v_id` int(11) UNSIGNED NOT NULL COMMENT 'visits id of patient',
  `p_id_f` int(11) UNSIGNED NOT NULL COMMENT 'patient foreign key',
  `visit_date` date NOT NULL COMMENT 'patient date of the visit',
  `visit_time` time DEFAULT NULL COMMENT 'time for the visit]',
  `followup_date` date DEFAULT NULL COMMENT 'followup date: next visit',
  `marital_status` tinyint(1) UNSIGNED NOT NULL COMMENT '1:single, 2:married, 3:divorced, 4:widowed.',
  `pregnant` tinyint(1) UNSIGNED NOT NULL COMMENT '1:pregnant, 2:not pregnant',
  `breast_feed` tinyint(1) UNSIGNED NOT NULL COMMENT '1:breasfeeding, 2:not breast feeding',
  `no_of_child` int(2) UNSIGNED NOT NULL COMMENT 'number of children',
  `other_info` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other patient-specific info ',
  `chief_comp_n_dur` text COLLATE utf8_unicode_ci COMMENT 'chief_complaint and duration',
  `examination` text COLLATE utf8_unicode_ci,
  `investigation` text COLLATE utf8_unicode_ci,
  `investigation_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'investigation notes',
  `note` text COLLATE utf8_unicode_ci COMMENT 'notes',
  `treatment_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'a note to describe the treatments given, this note will be printed along with the treatment_description data',
  `fee` decimal(13,3) NOT NULL COMMENT 'per visit fee',
  `procedure` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'procedures on the patient',
  `procedure_date` date NOT NULL COMMENT 'date  of procedure',
  `procedure_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'procedure notes',
  `in_queue` tinyint(1) NOT NULL DEFAULT '1' COMMENT '-1: visit completed so not in queue, 1:in queue, 2:in doctors room',
  `u_id_f` int(11) NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active:1, delete:-1',
  `update_record` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'to check date of record insert'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visit_attr`
--

CREATE TABLE `visit_attr` (
  `attr_id` int(11) UNSIGNED NOT NULL,
  `v_id_f` int(11) UNSIGNED NOT NULL,
  `attr` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `attr_order` tinyint(1) NOT NULL COMMENT 'to know which one is attr1 and which one is attr2 ...',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '-1:delete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visit_bcp`
--

CREATE TABLE `visit_bcp` (
  `v_id` int(11) UNSIGNED NOT NULL COMMENT 'visits id of patient',
  `p_id_f` int(11) UNSIGNED NOT NULL COMMENT 'patient foreign key',
  `visit_date` date NOT NULL COMMENT 'patient date of the visit',
  `followup_date` date NOT NULL COMMENT 'followup date: next visit',
  `marital_status` tinyint(1) UNSIGNED NOT NULL COMMENT '1:single, 2:married, 3:divorced, 4:widowed.',
  `pregnant` tinyint(1) UNSIGNED NOT NULL COMMENT '1:pregnant, 2:not pregnant',
  `breast_feed` tinyint(1) UNSIGNED NOT NULL COMMENT '1:breasfeeding, 2:not breast feeding',
  `no_of_child` int(2) UNSIGNED NOT NULL COMMENT 'number of children',
  `other_info` varchar(60) COLLATE utf8_unicode_ci NOT NULL COMMENT 'other patient-specific info ',
  `chief_comp_n_dur` text COLLATE utf8_unicode_ci COMMENT 'chief_complaint and duration',
  `examination` text COLLATE utf8_unicode_ci,
  `investigation` text COLLATE utf8_unicode_ci,
  `investigation_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'investigation notes',
  `note` text COLLATE utf8_unicode_ci COMMENT 'notes',
  `treatment_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'a note to describe the treatments given, this note will be printed along with the treatment_description data',
  `fee` decimal(13,3) NOT NULL COMMENT 'per visit fee',
  `procedure` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'procedures on the patient',
  `procedure_date` date NOT NULL COMMENT 'date  of procedure',
  `procedure_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'procedure notes',
  `in_queue` tinyint(1) NOT NULL DEFAULT '1' COMMENT '-1: visit completed so not in queue, 1:in queue, 2:in doctors room',
  `u_id_f` int(11) NOT NULL,
  `view` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'active:1, delete:-1',
  `update_record` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'to check date of record insert'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visit_diagnose`
--

CREATE TABLE `visit_diagnose` (
  `vd_id` int(11) NOT NULL,
  `v_id_f` int(11) UNSIGNED NOT NULL COMMENT 'visit foreign key',
  `diagnose` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'each visit of patient may have multiple diagnoses, but I preferred to have the description on the same table not different table so that not make queries more complex, and if I want to get list of available diagnoses I should query for distinct values of treatment_desc (for example if I want to use it in autocomplete. )',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='each visit may result in many diagnoses';

-- --------------------------------------------------------

--
-- Table structure for table `visit_surgery`
--

CREATE TABLE `visit_surgery` (
  `vs_id` int(11) UNSIGNED NOT NULL,
  `v_id_f` int(11) UNSIGNED NOT NULL,
  `surgery` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `surgery_date` date NOT NULL COMMENT 'hamu surgery of single visit have the same date, bas har lera duplicate i dakam bashtre nawak la dahatu judai bkamawa',
  `surgery_note` text COLLATE utf8_unicode_ci COMMENT 'note for the surgeries',
  `view` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visit_treatment`
--

CREATE TABLE `visit_treatment` (
  `vt_id` int(11) UNSIGNED NOT NULL,
  `v_id_f` int(11) UNSIGNED NOT NULL COMMENT 'visit foreign key',
  `treatment` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'each visit of patient may need multiple treatments, but I preferred to have the description on the same table not different table so that not make queries more complex, and if I want to get list of available treatments I should query for distinct values of treatment_desc (for example if I want to use it in autocomplete. )',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='to get list of available treatments I should query for distinct values of desc';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attachment`
--
ALTER TABLE `attachment`
  ADD PRIMARY KEY (`a_id`),
  ADD KEY `v_id_f` (`v_id_f`),
  ADD KEY `view` (`view`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`exp_id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`p_id`),
  ADD KEY `name` (`fullname`),
  ADD KEY `u_id_f` (`u_id_f`),
  ADD KEY `view` (`view`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`u_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `visit`
--
ALTER TABLE `visit`
  ADD PRIMARY KEY (`v_id`),
  ADD KEY `p_id_f` (`p_id_f`),
  ADD KEY `u_id_f` (`u_id_f`),
  ADD KEY `in_queue` (`in_queue`),
  ADD KEY `visit_date` (`visit_date`),
  ADD KEY `view` (`view`);

--
-- Indexes for table `visit_attr`
--
ALTER TABLE `visit_attr`
  ADD PRIMARY KEY (`attr_id`),
  ADD KEY `v_id_f` (`v_id_f`),
  ADD KEY `v_id_f_2` (`v_id_f`);

--
-- Indexes for table `visit_bcp`
--
ALTER TABLE `visit_bcp`
  ADD PRIMARY KEY (`v_id`),
  ADD KEY `p_id_f` (`p_id_f`),
  ADD KEY `u_id_f` (`u_id_f`),
  ADD KEY `in_queue` (`in_queue`),
  ADD KEY `visit_date` (`visit_date`),
  ADD KEY `view` (`view`);

--
-- Indexes for table `visit_diagnose`
--
ALTER TABLE `visit_diagnose`
  ADD PRIMARY KEY (`vd_id`),
  ADD KEY `v_id_f` (`v_id_f`),
  ADD KEY `view` (`view`);

--
-- Indexes for table `visit_surgery`
--
ALTER TABLE `visit_surgery`
  ADD PRIMARY KEY (`vs_id`),
  ADD KEY `v_id_f` (`v_id_f`);

--
-- Indexes for table `visit_treatment`
--
ALTER TABLE `visit_treatment`
  ADD PRIMARY KEY (`vt_id`),
  ADD KEY `v_id_f` (`v_id_f`),
  ADD KEY `view` (`view`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attachment`
--
ALTER TABLE `attachment`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;

--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `p_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'key: yymmdd[session(autonumber)], autonum: count today visits then start increment, plan failed', AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `visit`
--
ALTER TABLE `visit`
  MODIFY `v_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'visits id of patient', AUTO_INCREMENT=496;

--
-- AUTO_INCREMENT for table `visit_attr`
--
ALTER TABLE `visit_attr`
  MODIFY `attr_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=226;

--
-- AUTO_INCREMENT for table `visit_bcp`
--
ALTER TABLE `visit_bcp`
  MODIFY `v_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'visits id of patient', AUTO_INCREMENT=310;

--
-- AUTO_INCREMENT for table `visit_diagnose`
--
ALTER TABLE `visit_diagnose`
  MODIFY `vd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `visit_surgery`
--
ALTER TABLE `visit_surgery`
  MODIFY `vs_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;

--
-- AUTO_INCREMENT for table `visit_treatment`
--
ALTER TABLE `visit_treatment`
  MODIFY `vt_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attachment`
--
ALTER TABLE `attachment`
  ADD CONSTRAINT `attachment_ibfk_1` FOREIGN KEY (`v_id_f`) REFERENCES `visit` (`v_id`) ON UPDATE CASCADE;

--
-- Constraints for table `patient`
--
ALTER TABLE `patient`
  ADD CONSTRAINT `patient_ibfk_1` FOREIGN KEY (`u_id_f`) REFERENCES `user` (`u_id`) ON UPDATE CASCADE;

--
-- Constraints for table `visit`
--
ALTER TABLE `visit`
  ADD CONSTRAINT `visit_ibfk_2` FOREIGN KEY (`u_id_f`) REFERENCES `user` (`u_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `visit_ibfk_3` FOREIGN KEY (`p_id_f`) REFERENCES `patient` (`p_id`) ON UPDATE CASCADE;

--
-- Constraints for table `visit_attr`
--
ALTER TABLE `visit_attr`
  ADD CONSTRAINT `visit_attr_ibfk_1` FOREIGN KEY (`v_id_f`) REFERENCES `visit` (`v_id`) ON UPDATE CASCADE;

--
-- Constraints for table `visit_diagnose`
--
ALTER TABLE `visit_diagnose`
  ADD CONSTRAINT `visit_diagnose_ibfk_1` FOREIGN KEY (`v_id_f`) REFERENCES `visit` (`v_id`) ON UPDATE CASCADE;

--
-- Constraints for table `visit_surgery`
--
ALTER TABLE `visit_surgery`
  ADD CONSTRAINT `visit_surgery_ibfk_1` FOREIGN KEY (`v_id_f`) REFERENCES `visit` (`v_id`) ON UPDATE CASCADE;

--
-- Constraints for table `visit_treatment`
--
ALTER TABLE `visit_treatment`
  ADD CONSTRAINT `visit_treatment_ibfk_1` FOREIGN KEY (`v_id_f`) REFERENCES `visit` (`v_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
